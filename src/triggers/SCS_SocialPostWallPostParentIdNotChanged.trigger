/********************************************************************** 
 Name:  SCS_SocialPostWallPostParentIdNotChanged 
====================================================== 
Purpose: ParentId from SocialPost cannot be modified after auto creation of Case for Facebook Wallpost.

======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      10/04/2014      Initial Development
***********************************************************************/
trigger SCS_SocialPostWallPostParentIdNotChanged on SocialPost (before update) {
    List<User> Usr = [SELECT SkipValidation__c FROM User WHERE Id = : UserInfo.getUserId() LIMIT 1];

    if(Usr.size() > 0) {
        if(!Usr[0].SkipValidation__c) {
            for(SocialPost sp: Trigger.new) {
                if ('Facebook'.equals(sp.Provider) && sp.ParentId == null && 'Post'.equals(sp.MessageType)) {
                      sp.ParentId.addError('The Wallpost from Facebook with Case cannot be changed');
                }
            }
        }
    }
}