/**********************************************************************
 Name:  KLMFeedbackBeforeInsert
======================================================
Purpose: 

On a new feedback update the last contact moment of the related account

======================================================
History                                                            
-------                                                            
VERSION		AUTHOR				DATE			DETAIL                                 
	1.0		AJ van Kesteren		22/03/2013		INITIAL DEVELOPMENT
	1.1		Patrick Brinksma	27/02/2013		Added Area and Subarea as criteria
***********************************************************************/	
trigger KLMFeedbackBeforeInsert on KLM_Feedback__c (after insert) {
	System.debug('@@@ Will query feedback with account');
	System.debug('@@@ Trigger.newMap: ' + Trigger.newMap);
	System.debug('@@@ Trigger.newMap.keySet(): ' + Trigger.newMap.keySet());
	
	List<KLM_Feedback__c> feedbacks = [select Id, CreatedDate, Account__r.Id 
										from KLM_Feedback__c 
										where Id in :Trigger.newMap.keySet()];
	System.debug('@@@ feedbacks: ' + feedbacks);
	List<Account> accountsToUpdate = new List<Account>();	
	for (KLM_Feedback__c f : feedbacks) {
		f.Account__r.Last_Contact_Moment__c = f.CreatedDate; 
		accountsToUpdate.add(f.Account__r);	
	}
	update accountsToUpdate;
}