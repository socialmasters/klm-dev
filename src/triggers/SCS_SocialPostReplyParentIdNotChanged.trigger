/********************************************************************** 
 Name:  SCS_SocialPostReplyParentIdNotChanged
====================================================== 
Purpose: ParentId from SocialPost cannot be modified after auto creation of Case for Facebook Reply.

======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      22/04/2014      Initial Development
***********************************************************************/
trigger SCS_SocialPostReplyParentIdNotChanged on SocialPost (before update) {
    List<User> Usr = [SELECT SkipValidation__c FROM User WHERE Id = : UserInfo.getUserId() LIMIT 1];

    if(Usr.size() > 0) {
        if(!Usr[0].SkipValidation__c) {
            for(SocialPost sp: Trigger.new) {
                if ('Facebook'.equals(sp.Provider) && sp.ParentId == null && 'Reply'.equals(sp.MessageType)) {
                      sp.ParentId.addError('Sorry, you are not allowed to create a case on Facebook Reply level.');
                }
            }
        }
    }
}