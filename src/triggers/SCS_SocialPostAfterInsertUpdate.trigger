/********************************************************************** 
 Name:  SCS_SocialPostAfterInsertUpdate
====================================================== 
Purpose: 
    After insertion of a "Social Post" by KLM using Social Publisher where isOutbound = true.
    The "InReplyTo" corresponding "Social Post" should be update "Replied Date" / "Status".
    
    The Social Customer Service (SCS) product doesn't yet provide to update the "Replied Date" and "Status" of the Social Post.
    This trigger will be deprecated when the SCS product provides this feature.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      16/01/2014      Initial Development
    1.1     Stevano Cheung      06/06/2014      Modified SF API to version 30.0 and bulk handling to use isOutbound field
    1.2     Stevano Cheung      10/07/2014      After engage Social Publisher (outbound) set the replied post to actioned for Twitter
    1.3     Robert Stankiewicz  06/07/2015      Trigger bulkyfied because of WeChat implementation
***********************************************************************/
trigger SCS_SocialPostAfterInsertUpdate on SocialPost (after insert) {
//New version of SocialPostAfterInsertUpdate trigger - changes performed because of WeChat integration
//Regression testing needs to be done and logic for TW/FB needs to be veryfied

    List<String> socialPostsExternalIds = new List<String>();
    List<String> socialPostsReplyTo = new List<String>();

    List<SocialPost> socialPosts = new List<SocialPost>();
    List<SocialPost> socialPostsFaceBook = new List<SocialPost>();
	Set<String> extIdSet = New Set<String>();

    Map<Id, DateTime> newPostsMap = new Map<Id, DateTime>();

    List<SocialPost> socialPostsToUpdate = new List<SocialPost>();
    List<Id> socialPostsResponseTime = new List<Id>();
    
    for(SocialPost newspost: Trigger.new) {

        System.debug('--+ newspost.NEW: '+newspost);
        if(newspost.isOutbound){

            newPostsMap.put( newspost.Id, newspost.Posted );
            if('WeChat'.equals( newspost.Provider )) {

                socialPostsExternalIds.add( newspost.ExternalPostId );
            } else {

                socialPostsReplyTo.add( newspost.ReplyToId );
            }
        }
    }

    // WeChat
    if( socialPostsExternalIds.size() > 0 ) {
        //external Id is replaced every day even for the same chat sessions. just to be sure we narrow down to the last 2 weeks 
        for (SocialPost sp : [SELECT ParentId, Replied_date__c, Status, Provider, OwnerId, SCS_Status__c, SCS_MarkAsReviewedBy__c, 
                                SCS_MarkAsReviewed__c, IncludeResponseTime__c, Ignore_for_SLA__c, ExternalPostId 
                        FROM SocialPost WHERE CreatedDate IN (TODAY,LAST_N_DAYS:5) and ExternalPostId IN : socialPostsExternalIds AND isOutbound = false 
                        ORDER BY CreatedDate ASC LIMIT 10000]) {
			if (!extIdSet.contains(sp.ExternalPostId)) {
				extIdSet.add(sp.ExternalPostId);
				socialPosts.add(sp);
			}
    	}
    }

    // Facebook, Twitter, etc. 
    if( socialPostsReplyTo.size() > 0 ) {

        for (SocialPost sp : [SELECT ParentId, Replied_date__c, Status, Provider, OwnerId, SCS_Status__c, SCS_MarkAsReviewedBy__c, 
                                SCS_MarkAsReviewed__c, IncludeResponseTime__c, Ignore_for_SLA__c, ExternalPostId 
                        FROM SocialPost WHERE Id IN : socialPostsReplyTo AND isOutbound = false LIMIT 10000]) {
			socialPosts.add(sp);
        }
    }

    for( SocialPost spost : socialPosts ) {

        // Fix for agents replying always on the first incomming social post
        if( spost.Replied_date__c == null ) {

            spost.Replied_date__c = newPostsMap.get( spost.Id ); // previous sp.Posted 
        }
            
        spost.Status = 'Replied';
        if(!'WeChat'.equals(spost.Provider)){
            spost.OwnerId = userInfo.getUserId();
            if (spost.ParentId != null && spost.IncludeResponseTime__c == false ) {
				spost.IncludeResponseTime__c = true;
            }
        }
            
        //if( 'Twitter'.equals(spost.Provider) || 'WeChat'.equals(spost.Provider) ) {
            
            if(spost.SCS_Status__c != 'Actioned') {
                    
                spost.SCS_Status__c = 'Actioned';
                spost.SCS_MarkAsReviewedBy__c = '';
                spost.SCS_MarkAsReviewed__c = false;
                spost.Ignore_for_SLA__c = false;
            }
        //}

        // Workaround for AFKLM_SCS_ResponseTimeLogic
        //if(spost.ParentId != null && spost.IncludeResponseTime__c == false) {
        //
        //    socialPostsResponseTime.add( spost.ParentId );
        //}

        socialPostsToUpdate.add( spost );       
		if (socialPostsToUpdate.size() == 200) {
	    	update socialPostsToUpdate;
	    	socialPostsToUpdate.clear();
	    }
    }
	
    system.debug('--+ socialPostsToUpdate: '+socialPostsToUpdate);
    if (!socialPostsToUpdate.isEmpty()) {
    	update socialPostsToUpdate;
    }
	

    //system.debug('--+ socialPostsResponseTime: '+socialPostsResponseTime);

    /*
    // !!! THIS CODE STILL NEED TO BE TESTED !!!
    List<SocialPost> socialPostsResponseTimeToUpdate = new List<SocialPost>();
    Map<Id, DateTime> sprtTemp = new Map<Id, DateTime>();

    List<SocialPost> spList = [SELECT id, ParentId, Replied_date__c FROM SocialPost 
                                WHERE parentId IN :socialPostsResponseTime AND Status = 'Replied' 
                                    ORDER BY Replied_date__c ASC];
    
    system.debug('--+ spList: '+spList);
    // Workaround for AFKLM_SCS_ResponseTimeLogic - The first 'SocialPost' which is replied,actioned upon one 'Case' will be used for the FB, TW and KLM.com monitoring logic.
    // - Check all the Social Post which are status as 'Replied' and 'Replied' date is the oldest
    // - Mark checkbox IncludeResponseTime__c true first engaged SP for 'Response time' to be used by the monitoring
    if(spList.size() > 0) {

        Id current;
        Id past;
        for(SocialPost sprt : spList) {

            current = sprt.ParentId;
            if(current != past) {
                
                system.debug('--+ current / past: '+current+' / '+past);
                if(sprtTemp.containsKey(current)) {
                    if(sprtTemp.get(current) > sprt.Replied_date__c) {
                        sprtTemp.remove(current);
                        sprtTemp.put(sprt.Id, sprt.Replied_date__c);
                    }

                } else {

                    sprtTemp.put(sprt.Id, sprt.Replied_date__c);    
                }
            }

            past = current;    
        }

        system.debug('--+ sprtTemp: '+sprtTemp);
        // convert to list
        List<Id> idsToUpdateTmp = new List<Id>();
        if(sprtTemp.size() > 0) {
            
            for (Id Id : sprtTemp.keySet()){

                idsToUpdateTmp.add( Id );
            }
        }

        system.debug('--+ idsToUpdateTmp: '+idsToUpdateTmp);

        if(idsToUpdateTmp.size() > 0) {

            List<SocialPost> spIds = new List<SocialPost>();
            spIds = [SELECT id, IncludeResponseTime__c FROM SocialPost WHERE id IN :idsToUpdateTmp  AND Status = 'Replied'];
            
            system.debug('--+ spIds: '+spIds);

            for(SocialPost sptu : spIds) {
                sptu.IncludeResponseTime__c = true;
                socialPostsResponseTimeToUpdate.add( sptu );
            }

            system.debug('--+ socialPostsResponseTimeToUpdate: '+socialPostsResponseTimeToUpdate);
            if(socialPostsResponseTimeToUpdate.size() > 0) {

                update socialPostsResponseTimeToUpdate;
            }
        }
    }
    */




/*
    SocialPost socialPost;

    for(SocialPost sp: Trigger.new) {

        //system.debug('--+ sp: '+sp);
        if (sp.isOutbound) {

            if('WeChat'.equals(sp.Provider)){

                //system.debug('--+ ExternalPostId: '+sp.ExternalPostId);
                socialPost = [SELECT ParentId, Replied_date__c, 
                                            Status, Provider, 
                                            OwnerId, SCS_Status__c, 
                                            SCS_MarkAsReviewedBy__c, 
                                            SCS_MarkAsReviewed__c,
                                            IncludeResponseTime__c, 
                                            Ignore_for_SLA__c 
                                            FROM SocialPost WHERE ExternalPostId =: sp.ExternalPostId AND isOutbound = false ORDER BY CreatedDate ASC LIMIT 1];
            } else {

                //system.debug('--+ ReplyToId: '+sp.ReplyToId);
                socialPost = [SELECT ParentId, Replied_date__c, 
                                            Status, Provider, 
                                            OwnerId, SCS_Status__c, 
                                            SCS_MarkAsReviewedBy__c, 
                                            SCS_MarkAsReviewed__c,
                                            IncludeResponseTime__c, 
                                            Ignore_for_SLA__c 
                                            FROM SocialPost WHERE Id=:sp.ReplyToId LIMIT 1];
            }
            
            // Fix for agents replying always on the first incomming social post
            if(socialPost.Replied_date__c == null) {
                socialPost.Replied_date__c = sp.Posted;
            }
            
            socialPost.Status = 'Replied';
            socialPost.OwnerId = userInfo.getUserId();
            
            if('Twitter'.equals(socialPost.Provider) || 'WeChat'.equals(socialPost.Provider)) {
                if(socialPost.SCS_Status__c != 'Actioned'){
                    socialPost.SCS_Status__c = 'Actioned';
                    socialPost.SCS_MarkAsReviewedBy__c = '';
                    socialPost.SCS_MarkAsReviewed__c = false;
                    socialPost.Ignore_for_SLA__c = false;
                }
            }
            
            update socialPost;
            
            // This logic is used for the 'Response Time' for KLM.com, FB and TW
            if(socialPost.ParentId != null && socialPost.IncludeResponseTime__c == false) {
                AFKLM_SCS_ResponseTimeLogic.socialPostResponseTime(socialPost.ParentId);
            }
        }
    }
*/
}