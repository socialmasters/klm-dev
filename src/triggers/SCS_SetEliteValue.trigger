trigger SCS_SetEliteValue on SocialPost (before insert) {
    Map<Id, SocialPost> socialPostIds = new Map<Id, SocialPost>();

    for(SocialPost sp : Trigger.new) {
        if (sp.whoId != null) {
        	socialPostIds.put(sp.whoId, sp);
        }
    }
    
    if (socialPostIds.size() > 0) {
        List<Account> parentAccount = [Select a.Id, a.Elite_non_elite__c, a.Influencer__c
                                    From Account a
                                    Where a.Id in :socialPostIds.keySet()];
    	
        for (Account accountToProcess: parentAccount) {
            socialPostIds.get(accountToProcess.Id).Elite__c = accountToProcess.Elite_non_elite__c; 
            socialPostIds.get(accountToProcess.Id).Influencers_SP__c = accountToProcess.Influencer__c;
        }
    }
}