/**********************************************************************
 Name:  MobileFeedbackBeforeInsertUpdate
======================================================
Purpose: If fbNummer contains an @, set email to this value and set fb nummer to null.

======================================================
History                                                            
-------                                                            
VERSION		AUTHOR				DATE			DETAIL                                 
	1.0		AJ van Kesteren		29/03/2013		INITIAL DEVELOPMENT
***********************************************************************/
trigger MobileFeedbackBeforeInsertUpdate on feedback__c (before update, before insert) {

	for (feedback__c feedback : trigger.new) {
		if (feedback.fbNumber__c != null && feedback.fbNumber__c.contains('@')) {
			feedback.Email__c = feedback.fbNumber__c;
			feedback.fbNumber__c = null;
		}
	}

}