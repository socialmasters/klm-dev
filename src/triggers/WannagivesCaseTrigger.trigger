/**********************************************************************
 Name:  WannagivesCaseTrigger
======================================================
Purpose: 

Handle various status changes for a Wannagives Case

======================================================
History                                                            
-------                                                            
VERSION		AUTHOR				DATE			DETAIL                                 
	1.0		AJ van Kesteren		06/09/2013		Initial Development
***********************************************************************/	
trigger WannagivesCaseTrigger on Case (before insert, after update) {

	private Id WANNAGIVES_RECORD_TYPE_ID = [
		select Id 
    	from RecordType 
    	where SObjectType = 'Case'
    	and DeveloperName = 'Wannagives'
    	limit 1].Id;


	// Filter all cases in scope of this trigger on record type id
	List<Case> wannagivesCases = new List<Case>();
	for (Case c : Trigger.new) {
		if (c.RecordTypeId == WANNAGIVES_RECORD_TYPE_ID) {
			wannagivesCases.add(c);		
		}	
	}
	
	// Only do something if there are actually Wannagives cases in scope
	if (wannagivesCases.size() > 0) {
		WannagivesIntegrationManager manager = new WannagivesIntegrationManager();
		if (Trigger.isInsert) {
			
			System.debug('@@@@ New Wannagives Cases Created');
			manager.handleNewCases(wannagivesCases);	
		
		} else if (Trigger.isUpdate) {

			List<Case> casesToUpdate = new List<Case>();			
			for (Case c : wannagivesCases) {
			
				// Do a toUpperCase on the old and new status, just to make the solution resilient to case configuration changes
				String oldStatus = Trigger.oldMap.get(c.Id).Status.toUpperCase();
				String newStatus = c.Status.toUpperCase();
				
				if (oldStatus == 'NEW' && newStatus == 'OK') {
					System.debug('@@@@ Wannagives Case Status change: New -> Ok');	
					Case c2 = [select Wannagives_Integration_Status__c from Case where Id = :c.Id limit 1];
					c2.Wannagives_Integration_Status__c = WannagivesIntegrationManager.INTEGRATION_STATUS_PENDING;
					casesToUpdate.add(c2);
					WannagivesIntegrationManager.handleOkClosedCase(c.Id);
				} else if (oldstatus == 'NEW' && newStatus == 'NOT OK') {
					System.debug('@@@@ Wannagives Case Status change: New -> Not Ok');
					Case c2 = [select Wannagives_Integration_Status__c from Case where Id = :c.Id limit 1];
					c2.Wannagives_Integration_Status__c = WannagivesIntegrationManager.INTEGRATION_STATUS_PENDING;
					casesToUpdate.add(c2);
					WannagivesIntegrationManager.handleNotOkClosedCase(c.Id); 
				}
			}
			update casesToUpdate;
			
		}
	}

}