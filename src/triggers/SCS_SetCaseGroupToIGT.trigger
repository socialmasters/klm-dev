trigger SCS_SetCaseGroupToIGT on SocialPost (after update) {
	List<Case> casesToUpdate = new List<Case>();
	Map<Id,Case> updateCases = new Map<Id,Case>();
	List<String> topicProfileNames = new List<String>{'KLM Canada','KLM Ireland','KLM USA','KLM UK','KLM Malaysia','KLM Indonesia','KLM Singapore','KLM South Africa','KLM Hong Kong','KLM China','KLM Switzerland'};
	
	for(SocialPost sp : Trigger.new){
		//IBO:For the future 'IGT Spanish' needs to be added to IF statement
		if(sp.ParentId != null && 
			('IGT English'.equals(sp.SCS_Post_Tags__c) || 'IGT Spanish'.equals(sp.SCS_Post_Tags__c))
		) {
			List<Case> cs = [SELECT id, Group__c FROM Case WHERE Id =: sp.parentId LIMIT 1];
			
			if(!cs.isEmpty()){
				cs[0].Group__c = sp.Group__c;
				
				if(!updateCases.containsKey(cs[0].id)){
					updateCases.put(cs[0].id,cs[0]); 
				}
			}
		}
	}
	
	if(!updateCases.isEmpty()){
		casesToUpdate.addAll(updateCases.values());
		update casesToUpdate;
	}
}