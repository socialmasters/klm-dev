/**********************************************************************
 Name:  FeedbackToolCaseAfterInsertUpdate
======================================================
Purpose: 

Handle various status changes for a Feedback Tool Case

======================================================
History                                                            
-------                                                            
VERSION		AUTHOR				DATE			DETAIL                                 
	1.0		AJ van Kesteren		22/03/2013		INITIAL DEVELOPMENT
***********************************************************************/	
trigger FeedbackToolCaseAfterInsertUpdate on Case (after insert, after update) {
	System.debug('@@@ FeedbackToolCaseAfterInsertUpdate trigger started');	
	
	RecordType rt = [select Id, Name from RecordType where Name = 'Customer Feedback Tool Case' and SobjectType = 'Case' limit 1];
	System.debug('@@@ Retrieved record type: ' + rt.Name);
	
	List<Case> cases = [select Id, Status, CaseNumber, RecordTypeId, PNR__c, Description, Owner.Email, Owner.Name,
								Account.Id, Account.Flying_Blue_Number__c, Account.Official_First_Name__c, Account.Official_Last_Name__c
						from Case 
						where Id in :Trigger.newMap.keySet()];
	
	for (Case c : cases) {
		System.debug('@@@ Handling case: ' + c.CaseNumber);
		if (c.RecordTypeId == rt.Id) {
			System.debug('@@@ Current case is customer feedback tool case');
			String oldStatus = null;
			if (Trigger.oldMap != null && Trigger.oldMap.get(c.Id) != null) {
				oldStatus = Trigger.oldMap.get(c.Id).Status;
			}
			String newStatus = c.Status;
			System.debug('@@@ State transition: ' + oldstatus + ' -> ' + newStatus);
			
			if (!newStatus.equals(oldStatus)) {
				System.debug('@@@ A state change occurred');
				if (newStatus == 'Sent to Customer Care') {
					System.debug('@@@ State has become Sent to Customer Care. We need to send an e-mail.');
					AFKLM_CFT_CustomerCareEmailSender.sendEmail(c.Id);
				}
				
				if (newStatus == 'Waiting for Answer Social Media Hub') {
					System.debug('@@@ State has become Waiting for Answer Social Media Hub. We need to send an e-mail.');
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					mail.setToAddresses(new List<String>{AFKLM_CFT_Emails__c.getValues('smh').email__c});
					mail.setSubject('Customer Feedback Tool: Please have a look at this case.');
					String s = 'Dear Social Media Hub,<br/><br/>';
					s += 'Please have a look at the case with number: ' + c.CaseNumber + '.<br/><br/>';
					s += 'Thank you very much!<br/><br/>';
					s += 'Kind regards,<br/>';
					s += 'The Customer Feedback Tool Team';
					mail.setHtmlBody(s);
					Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
				}
				if (newStatus == 'Processed by Social Media Hub') {
					System.debug('@@@ State has become Processed by Social Media Hub. We need to send an e-mail.');
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					mail.setToAddresses(new List<String>{c.Owner.Email});
					mail.setSubject('Customer Feedback Tool: Social Media Hub processed your Case.');
					String s = 'Dear Case Owner,<br/><br/>';
					s += 'Social Media Hub just processed case with number: ' + c.CaseNumber + '.<br/><br/>';
					s += 'Kind regards,<br/>';
					s += 'The Customer Feedback Tool Team';
					mail.setHtmlBody(s);
					Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });					
				}
				
				if (newStatus == 'Waiting for Internal answer Flying Blue') {
					System.debug('@@@ State has become Waiting for Answer Flying Blue Team. We need to send an e-mail.');
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					mail.setToAddresses(new List<String>{AFKLM_CFT_Emails__c.getValues('fb').email__c});
					mail.setSubject('Customer Feedback Tool: Please have a look at this case.');
					String s = 'Dear Flying Blue Team,<br/><br/>';
					s += 'Please have a look at the case with number: ' + c.CaseNumber + '.<br/><br/>';
					s += 'Thank you very much!<br/><br/>';
					s += 'Kind regards,<br/>';
					s += 'The Customer Feedback Tool Team';
					mail.setHtmlBody(s);
					Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
				}
				if (newStatus == 'Processed by Flying Blue Team') {
					System.debug('@@@ State has become Processed by Flying Blue Team. We need to send an e-mail.');
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					mail.setToAddresses(new List<String>{c.Owner.Email});
					mail.setSubject('Customer Feedback Tool: The Flying Bleu Team processed your Case.');
					String s = 'Dear Case Owner,<br/><br/>';
					s += 'The Flying Bleu Team just processed case with number: ' + c.CaseNumber + '.<br/><br/>';
					s += 'Kind regards,<br/>';
					s += 'The Customer Feedback Tool Team';
					mail.setHtmlBody(s);
					Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });					
				}	
				
				if (newStatus == 'Sent to SIN Airport Management') {
					System.debug('@@@ State has become Sent to SIN Airport Management. We need to send an e-mail.');
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					mail.setToAddresses(new List<String>{AFKLM_CFT_Emails__c.getValues('airport_sin').email__c});
					mail.setSubject('Customer Feedback Tool: Please have a look at this case.');
					String s = 'Dear SIN Airport Manager,<br/><br/>';
					s += 'Please have a look at the case with number: ' + c.CaseNumber + '.<br/><br/>';
					s += 'Thank you very much!<br/><br/>';
					s += 'Kind regards,<br/>';
					s += c.Owner.Name;
					mail.setHtmlBody(s);
					Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
				}				
							
			}		
		}					
	}
	
	System.debug('@@@ FeedbackToolCaseAfterInsertUpdate trigger completed');	
}