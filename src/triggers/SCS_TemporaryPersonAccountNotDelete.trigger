// TODO: SC - Something wrong with deletion of Twitter 'PersonAccount' test test

trigger SCS_TemporaryPersonAccountNotDelete on Account (before delete, before update) {
    if(Trigger.isUpdate){
        for(Account a: [select id, LastName from Account where id in :Trigger.new /*and LastName='TemporaryPersonAccount'*/]){
            if(a.LastName=='TemporaryPersonAccount'){
                Trigger.newMap.get((Id)a.get('Id')).addError(' TemporaryPersonAccount cannot be deleted or modified!');
            }
        }
    } else if (Trigger.isDelete){
        for(Account a: [select id, LastName from Account where id in :Trigger.oldMap.keySet() /*and LastName='TemporaryPersonAccount'*/]){
            if(a.LastName=='TemporaryPersonAccount'){
                Trigger.oldMap.get((Id)a.get('Id')).addError(' TemporaryPersonAccount cannot be deleted or modified!');
            }
        }
    }
    
}