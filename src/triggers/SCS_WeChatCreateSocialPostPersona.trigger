trigger SCS_WeChatCreateSocialPostPersona on Nexmo_Post__c (after insert) {
	
	private SocialPost newSocialPost;
	private SocialPersona persona;
	
	private List<SocialPersona> personas {
		get {

			if(personas == NULL) {
				personas = new List<SocialPersona>();
			}

			return personas;
		}
		set;}

	private List<SocialPost> inboundPosts {
		get {

			if(inboundPosts == NULL) {
				inboundPosts = new List<SocialPost>();
			}

			return inboundPosts;
		}
		set;}

	private List<SocialPost> outboundPosts {
		get {

			if(outboundPosts == NULL) {
				outboundPosts = new List<SocialPost>();
			}

			return outboundPosts;
		}
		set;}
	
	private String picUrl = '/resource/SocialCustomerServiceResources/SocialCustomerServiceResources/img/noface.gif';

	AFKLM_SCS_WeChatSettings__c custSettings = AFKLM_SCS_WeChatSettings__c.getValues('Settings');

	// to remove duplicates from the list
	Set<SocialPersona> personasTmpSet = new Set<SocialPersona>();
	List<SocialPersona> personasTmpList = new List<SocialPersona>();

	for(Nexmo_Post__c nexmoPost : Trigger.new){

		// Create Social Post - Outbound path
		if('Outbound'.equals(nexmoPost.Source__c)){
			
			try{
				newSocialPost = new SocialPost(
					Name = 'Message from: '+custSettings.SCS_SysAccountName_del__c,
					Content = nexmoPost.Text__c,
					Handle = custSettings.SCS_SysPersonaName__c,
					WhoId = custSettings.SCS_SysAccountId__c,
					SCS_Status__c = 'New',
					Posted = nexmoPost.Send_Time__c,
					ExternalPostId = nexmoPost.Chat_Hash__c,
					IsOutbound = true,
					Provider = 'WeChat', 
					Company__c = 'KLM'
					);

				outboundPosts.add( newSocialPost );
			} catch (Exception e){
				
				System.debug(e.getMessage());
			}

			try{
				persona = new SocialPersona(
					Name = custSettings.SCS_SysPersonaName__c,
					ParentId = custSettings.SCS_SysAccountId__c,
					Provider = 'WeChat',
					ExternalPictureURL = picUrl,
					RealName = custSettings.SCS_SysPersonaName__c,
					ExternalId = custSettings.SCS_SysPersonaExternalId__c			
				);

			} catch(Exception e){

				System.debug('Error message '+e.getMessage());
			}
				
		} else {

			//Part for the Inbound Social Post
			try{
				newSocialPost = new SocialPost(
					Name = 'Message from: '+nexmoPost.User_Real_Name__c,
					Content = nexmoPost.Text__c,
					Handle = nexmoPost.User_Real_Name__c,
					SCS_Status__c = 'New',
					Posted = nexmoPost.Send_Time__c,
					ExternalPostId = nexmoPost.Chat_Hash__c,
					IsOutbound = false,
					Provider = 'WeChat', 
					Company__c = 'KLM'
					//SP_Traffic__c = nexmoPost.Direction__c
				);

				inboundPosts.add( newSocialPost );
			} catch (Exception e){
				
				System.debug('--+ Error message: '+e.getMessage());
			}

			try{
				persona = new SocialPersona(
					Name = nexmoPost.User_Real_Name__c,
					ParentId = custSettings.SCS_TemporaryPersonAccount__c,
					Provider = 'WeChat',
					ExternalPictureURL = picUrl,
					RealName = nexmoPost.User_Real_Name__c,
					ExternalId = nexmoPost.From_User_Id__c
					//WeChat_User_ID__c = nexmoPost.From_User_Id__c			
				);

			} catch(Exception e){

				System.debug('Error message '+e.getMessage());
			}

		}

		//personas.add(persona);
		personasTmpList.add(persona);
	}

	// remove all duplicates
	System.debug('--+ CreatePostPersona - personasTmpList: '+personasTmpList);
	personasTmpSet.addAll( personasTmpList );
	System.debug('--+ CreatePostPersona - personasTmpSet: '+personasTmpSet);
	personas.addAll( personasTmpSet );
	System.debug('--+ CreateTmpSet - personas: '+personas);

	AFKLM_SCS_InboundWeChatHandlerImpl handler = new AFKLM_SCS_InboundWeChatHandlerImpl();
	if(!inboundPosts.isEmpty()){    
		system.debug('^^^^Personas '+personas);
		handler.handleInboundSocialPost(inboundPosts,personas);
	}

	if(!outboundPosts.isEmpty()){
	    
	    handler.handleInboundSocialPost(outboundPosts,personas);
	}

}