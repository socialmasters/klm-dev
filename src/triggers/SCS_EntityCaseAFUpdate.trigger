trigger SCS_EntityCaseAFUpdate on SocialPost (after update) {
    Map<Id, String> parentCaseIds = new Map<Id, String>();

    for(SocialPost sp : Trigger.new) {
        if (sp.ParentId != null) {
            if('AirFrance'.equals(sp.Company__c)) {
                parentCaseIds.put(sp.ParentId, sp.Entity__c);
            }
        }
    }
    
    if (parentCaseIds.size() > 0) {
        List<Case> parentCases = [Select c.Id, c.Entity__c
                                    From Case c
                                    Where c.Id in :parentCaseIds.keySet()];
    
        List<Case> parentCasesToUpdate = new List<Case>();

        for (Case parent: parentCases) {
            parent.Entity__c = parentCaseIds.get(parent.Id);
            parentCasesToUpdate.add(parent);  
        }
        
        //Bulk update
        if (parentCasesToUpdate.size()>0) {
            update parentCasesToUpdate;
        }
    }
}