/********************************************************************** 
 Name:  SCS_CaseBeforeUpdate
====================================================== 
Purpose: Updates the stage datetime when the Status has been changed.

======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      24/01/2014      Initial Development
    1.1     Stevano Cheung      11/02/2014      Added recordtype for SCS servicing
***********************************************************************/
trigger SCS_CaseBeforeUpdate on Case (before update) {
    RecordType rt = [select Id, Name from RecordType where Name = 'Servicing' and SobjectType = 'Case' limit 1];
    RecordType rtAF = [select Id, Name from RecordType where Name = 'AF Servicing' and SobjectType = 'Case' limit 1];
    System.debug('@@@ Retrieved record type: ' + rt.Name + ' AirFrance: '+rtAf.Name);

    for(Case cs: Trigger.new) {
        if (cs.RecordTypeId == rt.Id || cs.RecordTypeId == rtAF.Id) {
            System.debug('@@@ Current case is servicing case');
            cs.Case_stage_datetime__c = Datetime.now();
        }
   }
}