/********************************************************************** 
 Name:  SocialCustomerServiceUtils
 Task:    N/A
 Runs on: AFKLM_SCS_ControllerHelper, Social Post Javascript Custom Button
====================================================== 
Purpose: 
    - Used for Custom Salesforce javascript button execution on Social Post on 'Reviewed'/'No Reply' and 'Reverse'.
    - Also for AFKLM_SCS_ControllerHelper on Social Post for 'Reviewed'/'Spam' 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      11/02/2015      Initial Development  
***********************************************************************/
global class SocialCustomerServiceUtils {
	WebService static void userSetSkipValidation(Boolean skipValidation) {
    	List<User> usr = [SELECT SkipValidation__c FROM User WHERE Id = : UserInfo.getUserId() LIMIT 1];
    	
		if(usr != null && usr.size() > 0) {
			usr[0].SkipValidation__c = skipValidation;
			update usr;
		}
	}
}