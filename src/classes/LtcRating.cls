/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman
 * @description     : Frontend class to hold the business logic before storing it in the Rating object
 * @log:    13MAR2014: version 1.0
 * @log:    27AUG2015: version 1.1 added img url fields
 * 
 */
global class LtcRating { 
    LtcCrypto ltcc;
    
    public LtcRating() {
        ltcc = new LtcCrypto();
    }
    
    /**
     * set rating by Json Rating Post format
     **/
    public String setRating(LtcRatingPostModel.Rating rating, String acceptLanguage) {
        return setRating(rating.rating, rating.positiveComment, rating.negativeComment, rating.isPublished,
            rating.passenger.firstName, rating.passenger.familyName, rating.passenger.email, rating.flight.flightNumber, rating.passenger.seat.seatNumber, 
            rating.flight.travelDate, rating.flight.origin, rating.flight.destination, acceptLanguage);
        
    }
     
    /**
     * Set rating direct by SFDC fields
     * @return the id of the new created Rating__c object
     **/
    public String setRating(String ratingNum, String positiveFeedback, String negativeFeedBack, String isPublished,
            String firstName, String familyName, String email, String flightNumber, String seatNumber, String travelDate,
    		String origin, String destination, String acceptLanguage) {
        Double ratingNumber = Double.valueof(ratingNum);
        Date trDate = Date.valueof(travelDate);
        flightNumber = LtcUtil.cleanFlightNumber(flightNumber);
        Boolean pub = 'TRUE'.equalsIgnoreCase(isPublished);
        Id flightId;
        Id flightDateId;
        Flight__c flight;
        Flight_Date__c flight_Date;
        if (seatNumber != null) {
            seatNumber = seatNumber.toLowerCase();
        }
        
        //Get or create flight information
        List<Flight__c> flights = [select f.Id, f.Flight_Number__c from Flight__c f where f.Flight_Number__c =: flightNumber and f.Scheduled_Departure_Date__c =: trDate];
        if (flights != null && !flights.isEmpty() && flights[0] != null) {
            flightId = flights[0].Id;
        } else {
            flight = new Flight__c(Flight_Number__c = flightNumber, Scheduled_Departure_Date__c = trDate);
            insert flight;
            flightId = flight.Id;
        }
        
        //Get or create flight date
        List<Flight_Date__c> flightDates = [select f.Id, f.Full_Date__c from Flight_Date__c f where f.Full_Date__c =: trDate];
        if (flightDates != null && !flightDates.isEmpty() && flightDates[0] != null) {
            flightDateId = flightDates[0].Id;
        } else {
            flight_Date = new Flight_Date__c(Full_Date__c = trDate);
            insert flight_Date;
            flightDateId = flight_Date.Id;
        }
        
        // Store the passenger
        Passenger__c passenger = new Passenger__c(
            First_Name__c = firstName, 
            Family_Name__c = familyName,
            Email__c = email,
            AllowEmailContact__c = !String.isBlank(email)
        );
        insert passenger;
        
        // Determine language and country
        String country = LtcUtil.getCountry(acceptLanguage);
        String language = LtcUtil.getLanguage(acceptLanguage);
        
        Boolean hasComments = (positiveFeedback != null && !''.equals(positiveFeedback))
        	|| (negativeFeedBack != null && !''.equals(negativeFeedBack));
        
        // Store the Rating with links to the passenger and the flight information
        Rating__c rating = new Rating__c(
            Flight_Info__c = flightId, 
            Flight_Date__c = flightDateId,
        	Origin__c = origin,
       		Destination__c = destination,
            Passenger__c = passenger.Id, 
            Negative_comments__c = negativeFeedBack, 
            Positive_comments__c = positiveFeedback, 
        	Has_Comments__c = hasComments,
            Publish__c = pub, 
            Rating_Number__c = ratingNumber, 
            Seat_Number__c = seatNumber,
            Country__c = country,
            Language__c = language
        );
        insert rating;
        
        return ltcc.encodeText(rating.Id);
    }

    /**
     * Update the isPublished parameter by Rating Id
     **/
    public String updateRating(LtcRatingPutModel.Rating rating) {
        Boolean pub = 'TRUE'.equalsIgnoreCase(rating.isPublished);
        Rating__c rat = new Rating__c(Id = ltcc.decodeText(rating.ratingId), Publish__c = pub);
        update rat;
        return ltcc.encodeText(rat.Id);
    }
    
    /**
     * Get all flight ratings in Flight Get format
     **/
    public LtcRatingGetModel.Flight getRatings(String flightNumber, String startDate, String endDate, List<String> ratingIds, Integer limitTo, Integer offset) {
    
    	// defaults for pagination
        if (limitTo == null) { 
        	limitTo = 15; 
        }
        if (offset == null) { 
        	offset = 0;
        }

        Date start = Date.valueof(startDate);
        Date ending = Date.valueof(endDate);
        List<Rating__c> ratingList = getFlightRatings(flightNumber, start, ending);
        
        LtcRatingGetModel ltcRatingGetModel = new LtcRatingGetModel();
        return ltcRatingGetModel.getRatingResponse(flightNumber, startDate, endDate, ratingIds, ratingList, limitTo, offset); 
    }
    
    /**
     * Get all flight ratings in SFDC format, string dates
     **/
    public List<Rating__c> getFlightRatings(String flightNumber, String startDate, String endDate) {
        Date start = Date.valueof(startDate);
        Date ending = Date.valueof(endDate);
       	return getFlightRatings(flightNumber, start, ending);
    }
    
    /**
     * Get all flight ratings in SFDC format, real dates
     **/
    public List<Rating__c> getFlightRatings(String flightNumber, Date startDate, Date endDate) {
        List<Rating__c> ratingList;
        if ('KL1'.equals(flightNumber)) {
            ratingList = [
                select 
                    r.Rating_Number__c, 
                    r.Id, 
                    r.Positive_comments__c, 
                    r.Seat_Number__c, 
                    r.Flight_Date__r.Full_Date__c, 
                    r.Flight_Date__c,  
                    r.Flight_Info__r.Flight_Number__c, 
                    r.Flight_Info__r.Scheduled_Departure_Date__c, 
                    r.Flight_Info__c, 
                    r.Passenger__r.First_Name__c, 
                    r.Passenger__r.Family_Name__c, 
                    r.Passenger__r.Hidden_Name__c, 
                    r.Passenger__c, 
                    r.KLM_Reply__c, 
                    r.Negative_comments__c, 
                    r.Publish__c, 
                    r.Hidden_Comments__c, 
                    r.Hidden_Seat_Number__c, 
                    r.CreatedDate 
                from Rating__c r 
                where  (r.Flight_Info__r.Flight_Number__c like: 'KL1%'
                     or r.Flight_Info__r.Flight_Number__c like: 'KL09%')
                and r.Flight_Date__r.Full_Date__c >=: startDate
                and r.Flight_Date__r.Full_Date__c <=: endDate
                order by r.CreatedDate desc
            ];
        }  else {
                ratingList = [
                    select 
                        r.Rating_Number__c, 
                        r.Id, 
                        r.Positive_comments__c, 
                        r.Seat_Number__c, 
                        r.Flight_Date__r.Full_Date__c, 
                        r.Flight_Date__c,  
                        r.Flight_Info__r.Flight_Number__c, 
                        r.Flight_Info__r.Scheduled_Departure_Date__c, 
                        r.Flight_Info__c, 
                        r.Passenger__r.First_Name__c, 
                        r.Passenger__r.Family_Name__c, 
                        r.Passenger__r.Hidden_Name__c, 
                        r.Passenger__c, 
                        r.KLM_Reply__c, 
                        r.Negative_comments__c, 
                        r.Publish__c, 
                        r.Hidden_Comments__c, 
                        r.Hidden_Seat_Number__c, 
                        r.CreatedDate 
                    from Rating__c r 
                    where  r.Flight_Info__r.Flight_Number__c like: flightNumber + '%' 
                    and r.Flight_Date__r.Full_Date__c >=: startDate
                    and r.Flight_Date__r.Full_Date__c <=: endDate
                    order by r.CreatedDate desc
                ];
        }
        //Hide if needed and convert Null to ""
        for (Rating__c rat : ratingList) {
            cleanupRating(rat);
        }
        return ratingList;
    }
    
    private void cleanupRating(Rating__c rat) {
            if (rat.hidden_comments__c == true || rat.Positive_comments__c == null) {
                rat.Positive_comments__c = '';
            }
            if (rat.hidden_comments__c == true || rat.Negative_comments__c == null) {
                rat.Negative_comments__c = '';
            }
            if (rat.hidden_comments__c == true || rat.KLM_Reply__c == null) {
                rat.KLM_Reply__c = '';
            }
            if (rat.Passenger__r.First_Name__c == null) {
                rat.Passenger__r.First_Name__c = '';
            }
            if (rat.Passenger__r.Family_Name__c == null) {
                rat.Passenger__r.Family_Name__c = '';
            }
            if (rat.hidden_seat_number__c == true || rat.Seat_Number__c == null) {
                rat.Seat_Number__c = '';
            }           
    }
    
    /**
     * Get all rated flights
     **/
    public Set<String> getRatedFlightNumbers() {
        List<Rating__c> ratingList = [
            select r.Flight_Number__c
            from Rating__c r 
            order by r.Flight_Number__c 
            asc
        ];
        System.debug(ratingList);
        Set<String> uniqueFlightNumbers = new Set<String>();
        for (Rating__c rating : ratingList) {
            uniqueFlightNumbers.add(rating.Flight_Number__c);
            System.debug('added flightnr=' + rating.Flight_Number__c);
        }
        return uniqueFlightNumbers;
    }
    
    /**
     * Get all flight ratings in SFDC format with matching flightnumber for 365 days from today
     **/
    public List<Rating__c> getFlightRatingsForStarInterface(String flightNumber, Date travelDate) {
        Date fromDate = Date.today().addDays(-365);
        
        List<Rating__c> resultList = selectRatingsFromDate(flightNumber, fromDate);
        System.debug(LoggingLevel.INFO, 'resultList.size()=' + resultList.size());
        
        //Hide if needed and convert Null to ""
        for (Rating__c rat : resultList) {
            cleanupRating(rat);         
        }
        return resultList;
    }

    private List<Rating__c> selectRatingsFromDate(String flightNumber, Date fromDate) {
        System.debug(LoggingLevel.DEBUG, 'flNo=' + flightNumber + ' fromDate=' + fromDate);
        return [
            select 
                r.Rating_Number__c, 
                r.Id, 
                r.Positive_comments__c, 
                r.Seat_Number__c, 
                r.Flight_Date__r.Full_Date__c, 
                r.Flight_Date__c,  
                r.Flight_Info__r.Flight_Number__c, 
                r.Flight_Info__r.Scheduled_Departure_Date__c, 
                r.Flight_Info__c, 
                r.Passenger__r.First_Name__c, 
                r.Passenger__r.Family_Name__c, 
                r.Passenger__r.Hidden_Name__c, 
                r.Passenger__c, 
                r.KLM_Reply__c, 
                r.Negative_comments__c, 
                r.Publish__c, 
                r.Hidden_Comments__c, 
                r.Hidden_Seat_Number__c, 
                r.CreatedDate 
            from Rating__c r 
            where  r.Flight_Info__r.Flight_Number__c =: flightNumber
            and r.Flight_Date__r.Full_Date__c >=: fromDate
            order by r.CreatedDate desc
        ];
    }

    public Integer countRatings(String flightNumber, Date travelDate) {
        Integer result = 0;
        List<AggregateResult> aggs = [
            SELECT count(Id) nrOfRatings
            FROM Rating__c
            GROUP BY Flight_Info__r.Flight_Number__c, Flight_Info__r.Scheduled_Departure_Date__c 
            HAVING Flight_Info__r.Flight_Number__c =: flightNumber
            AND Flight_Info__r.Scheduled_Departure_Date__c =: travelDate
        ];
        for (AggregateResult ar : aggs) {
            result = (Integer) ar.get('nrOfRatings');
        }
        return result;
    }    
    
//    private List<Rating__c> selectRatingsOnDayOfWeek(String flightNumber, Date travelDate, Date fromDate) {
//        Flight_Date__c fDate = new Flight_Date__c();
//        fDate.full_date__c = travelDate;
//        Integer dayOfWeek = getDayOfWeek(travelDate);
//        System.debug(LoggingLevel.INFO, 'flNo=' + flightNumber + ' travelDate=' + travelDate + ' dayOfWeek=' + dayOfWeek + ' fromDate=' + fromDate);
//        return [
//            select 
//                r.Rating_Number__c, 
//                r.Id, 
//                r.Positive_comments__c, 
//                r.Seat_Number__c, 
//                r.Flight_Date__r.Full_Date__c, 
//                r.Flight_Date__c,  
//                r.Flight_Info__r.Flight_Number__c, 
//                r.Flight_Info__r.Scheduled_Departure_Date__c, 
//                r.Flight_Info__c, 
//                r.Passenger__r.First_Name__c, 
//                r.Passenger__r.Family_Name__c, 
//                r.Passenger__r.Hidden_Name__c, 
//                r.Passenger__c, 
//                r.KLM_Reply__c, 
//                r.Negative_comments__c, 
//                r.Publish__c, 
//                r.Hidden_Comments__c, 
//                r.Hidden_Seat_Number__c, 
//                r.CreatedDate 
//            from Rating__c r 
//            where  r.Flight_Info__r.Flight_Number__c =: flightNumber
//            and r.Flight_Date__r.Full_Date__c >=: fromDate
//            and r.Flight_Date__r.Day_Of_Week_Formula__c =: dayOfWeek
//            order by r.CreatedDate desc
//        ];
//    }
//
//    private Integer getDayOfWeek(Date dateToCalculate ) {
//        // Cast the Date variable into a DateTime
//        DateTime myDateTime = (DateTime) dateToCalculate;
//        String dow = myDateTime.format('E').toLowerCase();
//        Integer result = 7;
//        // dayOfWeek is Sun, Mon, Tue, etc.
//        if ('sun'.equals(dow)) {
//            result = 1;
//        } else if ('mon'.equals(dow)) {
//            result = 2;
//        } else if ('tue'.equals(dow)) {
//            result = 3;
//        } else if ('wed'.equals(dow)) {
//            result = 4;
//        } else if ('thu'.equals(dow)) {
//            result = 5;
//        } else if ('fri'.equals(dow)) {
//            result = 6;
//        } else if ('sat'.equals(dow)) {
//            result = 0;
//        }
//        System.debug('dayOfWeek of ' + myDateTime + ' weekday=' + dow + ' result=' + result);
//        return result;
//    }
//  
}