/**
 * Mocks calling the locations API
 **/
@isTest
public with sharing class LtcLocationsCalloutMock implements HttpCalloutMock {
	
	public HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        if (req.getEndPoint().contains('airports')) {
            res.setBody('{"code":"AMS","name":"Schiphol","description":"Amsterdam - Schiphol (AMS), Nederland","coordinates":{"latitude":52.30833,"longitude":4.76806},"parent":{"code":"AMS","name":"Amsterdam","description":"Amsterdam (AMS)","coordinates":{"latitude":52.31667,"longitude":4.78417},"parent":{"code":"NL","name":"Nederland","description":"Nederland (NL)","coordinates":{"latitude":52.3,"longitude":5.45},"parent":{"code":"EUR","name":"Europa","description":"Europa (EUR)","coordinates":{"latitude":51.179,"longitude":11.25}}}}}');
        } else {
            res.setBody('{"actual":{"precipitation":"0.3","pressure":"1016","temp":"6","windSpeed":"0","windDirection":"0","description":{"id":"0006","icon":"&#xe644;","value":"Mist"}},"forecast":[{"date":"2015-04-10","minTemp":"8","maxTemp":"20","description":{"id":"0009","icon":"&#xe647;","value":"Light rain shower"}},{"date":"2015-04-11","minTemp":"6","maxTemp":"16","description":{"id":"0002","icon":"&#xe641;","value":"Partly Cloudy"}},{"date":"2015-04-12","minTemp":"7","maxTemp":"14","description":{"id":"0001","icon":"&#xe640;","value":"Sunny"}},{"date":"2015-04-13","minTemp":"12","maxTemp":"17","description":{"id":"0001","icon":"&#xe640;","value":"Sunny"}},{"date":"2015-04-14","minTemp":"9","maxTemp":"17","description":{"id":"0001","icon":"&#xe640;","value":"Sunny"}}],"averages":[{"month":"January","absMaxTemp":"22.2","absMinTemp":"-19.4","avgMaxTemp":"6.1","avgMinTemp":"-2.3"},{"month":"February","absMaxTemp":"22.2","absMinTemp":"-15.6","avgMaxTemp":"6.9","avgMinTemp":"-2.3"},{"month":"March","absMaxTemp":"28.3","absMinTemp":"-11","avgMaxTemp":"10.9","avgMinTemp":"1.1"},{"month":"April","absMaxTemp":"34.4","absMinTemp":"-3.9","avgMaxTemp":"17.3","avgMinTemp":"6.6"},{"month":"May","absMaxTemp":"36.1","absMinTemp":"0.7","avgMaxTemp":"22.9","avgMinTemp":"11.8"},{"month":"June","absMaxTemp":"38","absMinTemp":"10","avgMaxTemp":"28.2","avgMinTemp":"17.7"},{"month":"July","absMaxTemp":"39.4","absMinTemp":"14.4","avgMaxTemp":"30.4","avgMinTemp":"20.9"},{"month":"August","absMaxTemp":"40","absMinTemp":"13.9","avgMaxTemp":"29.6","avgMinTemp":"20.6"},{"month":"September","absMaxTemp":"35","absMinTemp":"7","avgMaxTemp":"25.7","avgMinTemp":"16.9"},{"month":"October","absMaxTemp":"31.7","absMinTemp":"1.7","avgMaxTemp":"19.8","avgMinTemp":"10.7"},{"month":"November","absMaxTemp":"26.7","absMinTemp":"-5","avgMaxTemp":"13.7","avgMinTemp":"5.3"},{"month":"December","absMaxTemp":"23.3","absMinTemp":"-13.9","avgMaxTemp":"8.1","avgMinTemp":"0"}]}');
        }
        res.setStatusCode(200);
        return res;
    }
}