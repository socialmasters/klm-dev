@IsTest
/**********************************************************************
 Name:  KLMFeedbackBeforeInsert_TEST.cls
======================================================
Purpose: Test class for the KLMFeedbackBeforeInsert_TEST class
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     A.J. van Kesteren	4/4/2013		Initial development
***********************************************************************/
public with sharing class KLMFeedbackBeforeInsert_TEST {

	static testMethod void testCreateCase() {
		// Set-up a feedback
		Account account = new Account();
		account.Name = 'Test Account';
		insert account;
		
		KLM_Feedback__c feedback = new KLM_Feedback__c();	
		feedback.Flight_Date__c = Date.today();
		feedback.Flight_Number__c = '641';
		feedback.Feedback__c = 'bla';	
		feedback.Account__c = account.Id;
		insert feedback;
		
		Account account2 = [select Last_Contact_Moment__c from Account where Id = :account.Id limit 1];
		
		System.assertNotEquals(null, account2.Last_Contact_Moment__c);
	}
}