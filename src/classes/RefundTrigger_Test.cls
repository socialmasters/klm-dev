/**********************************************************************
 Name:  RefundTrigger_Test.cls
======================================================
Purpose: Test class for:
1. RefundTrigger
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma    22 Jul 2014		Initial development
***********************************************************************/  
@isTest
private class RefundTrigger_Test {
	
	/*
	 * Test method for RefundTrigger Trigger
	 */ 	
	@isTest
	static void RefundTriggerTest(){

		AFKLM_WS_TestUtil.createWSSetting('IssueRefundRequest-v1');

		Account tstAccnt = AFKLM_WS_TestUtil.createAccount();
		List<Case> listOfCase = new List<Case>();
		for (Integer i=0; i<200; i++){
			Case tstCase = AFKLM_WS_TestUtil.createCase(tstAccnt, false);
			listOfCase.add(tstCase);
		}
		insert listOfCase;
		List<Id> listOfCaseId = new List<Id>();
		for (Case thisCase : listOfCase){
			listOfCaseId.add(thisCase.Id);
		}
		
		List<Refund__c> listOfRefund = bl_Refund.createRefundFromCase(listOfCaseId);
		insert listOfRefund;

		List<Id> listOfRefundId = new List<Id>();
		for (Refund__c thisRefund : listOfRefund){
			listOfRefundId.add(thisRefund.Id);
		}

		AFKLM_WS_MockTest tstWSMock = new AFKLM_WS_MockTest();
		tstWSMock.listOfRefundId = listOfRefundId;
		Test.setMock(WebServiceMock.class, tstWSMock);		

		Integer i=0;
		for (Refund__c thisRefund : listOfRefund){
			thisRefund.Status__c = 'Submitted';
			thisRefund.Case__c = listOfCase[i].Id;
			i++;
		}

		Test.startTest();
		
		AFKLM_Utility.hasRunRefundTrigger = false;
		update listOfRefund;

		Test.stopTest();

		listOfRefund = [select Id, CSR_Refund_Request_Id__c, Status__c, Error_Message__c from Refund__c where Id in :listOfRefundId];
		for (Refund__c thisRefund : listOfRefund){
			if (thisRefund.Error_Message__c == null){
				System.assertEquals('Requested', thisRefund.Status__c);
				System.assertEquals('1234567890', thisRefund.CSR_Refund_Request_Id__c);
			} else {
				System.assertEquals(null, thisRefund.CSR_Refund_Request_Id__c);
				System.assertEquals('Failed', thisRefund.Status__c);
				System.assertEquals('Reason - some exception', thisRefund.Error_Message__c);
			}
		}		
	}

	
}