/**
 * @author (s)    :	David van 't Hooft
 * @description   : Fox service class to retrieve flight information
 * @log: 	12May2014: version 1.0
 */
public class LtcFoxService {
	/*
	Flight call:
	http://fox.klm.com/fox/json/flights/byFlightCode?type=departure&positions=true&date=03-02-2014&flightCode=KL0744&dayPattern=E%20dd%20MMM&jsoncallback=jQuery172036410952289673626_1391519060086&language=nl&country=nl&_=1391519082369
	
    All flights call:
	http://fox.klm.com/fox/json/flights/current?nearestAirport=AMS&positions=true&language=nl&country=nl
	
    All airports call    
	http://fox.klm.com/fox/json/data/airports?jsoncallback=jQuery17206332485675166633_1391519326381&language=nl&country=nl&_=1391519326986
	http://fox.klm.com/fox/json/flights/byFlightCode?type=departure&positions=true&date=06-05-2014&flightCode=kl785&dayPattern=E%20dd%20MMM&language=nl&country=nl;
	*/
 	public LtcFoxService() {
 	}

    private String result = '';
    
    /**
     * Get current flight information 
     */
    public LtcFoxResponseModel getFoxFlightInfo() {
		return getFoxFlightInfo('allflights', 'departure', '', '');    	
    }    

    /**
     * Get specific flight  
     */
    public LtcFoxResponseModel getFoxFlightInfo(String flightType, String flightNumber, String travelDate) {
		return getFoxFlightInfo('flight', flightType, flightNumber, travelDate);    	
    }    
    
   /**
    * @callType String flight/allflights
    * @flightType: departure, arrival
    * @ travelDate String format '12-05-2014'
    **/
    private LtcFoxResponseModel getFoxFlightInfo(String callType, String flightType, String flightNumber, String travelDate) {    
    	LtcFoxResponseModel ltcFrm;
        String fPositions = 'false';
        String fDate;
        if (travelDate != null && travelDate.length() >= 10) {
        	fDate = travelDate.subString(8,10) + '-' + travelDate.subString(5,7) + '-' + travelDate.subString(0,4);   //format day-month-year
        }
        String fDateFormat = 'yyyy-MM-dd';	//E%20 puts day in text in front, - can be replaced by anything, and will return that
        //String fDate = '12-05-2014';
        String fLanguage= 'nl';
        String fCountry= 'nl';
        String url = '';
        if ('flight'.equalsIgnoreCase(callType)) {
        	url = 'http://fox.klm.com/fox/json/flights/byFlightCode?type=' + flightType 
        		+ '&positions=' + fPositions 
        		+ '&date=' + fDate
        		+ '&flightCode=' + flightNumber 
        		+ '&dayPattern=' + fDateFormat
        		+ '&language=' + fLanguage 
        		+ '&country=' + fCountry;
        } else {
        	url = 'http://fox.klm.com/fox/json/flights/current?nearestAirport=AMS&positions=true&language=nl&country=nl';
        }
        //we'll call the REST API here
        Http h = new Http();
        Httprequest req = new Httprequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json; charset=UTF-8');
        req.setTimeout(60000);
        if (Test.isRunningTest()) {        	
	        //Rest call is not performed during test, so send back mock data
        	result = '{\"specialFlight\":null,\"flights\":[{\"registrationCode\":\"PH-BVI\",\"totalScheduledFlyingTime\":54000000,\"totalScheduledTransferTime\":4500000,\"currentLeg\":0,\"legs\":[{\"origin\":\"AMS\",\"status\":\"DELAYED\",\"destination\":\"SIN\",\"legNumber\":0,\"timeToArrival\":8592321,\"scheduledArrivalDate\":\"2014-05-12\",\"scheduledArrivalTime\":\"15:20\",\"actualArrivalTime\":null,\"estimatedArrivalTime\":\"15:59\",\"arrivalDelay\":2340000,\"scheduledDepartureDate\":\"2014-05-11\",\"scheduledDepartureTime\":\"21:00\",\"actualDepartureTime\":\"21:34\",\"estimatedDepartureTime\":null,\"timeToDeparture\":-36107679,\"departureDelay\":2040000,\"actualArrivalDate\":null,\"actualDepartureDate\":\"2014-05-11\",\"arrivalDate\":\"12-05-2014\",\"departureDate\":\"11-05-2014\",\"estimatedArrivalDate\":\"2014-05-12\",\"estimatedDepartureDate\":null},{\"origin\":\"SIN\",\"status\":\"DELAYED\",\"destination\":\"DPS\",\"legNumber\":1,\"timeToArrival\":22272321,\"scheduledArrivalDate\":\"2014-05-12\",\"scheduledArrivalTime\":\"19:15\",\"actualArrivalTime\":null,\"estimatedArrivalTime\":\"19:47\",\"arrivalDelay\":1920000,\"scheduledDepartureDate\":\"2014-05-12\",\"scheduledDepartureTime\":\"16:35\",\"actualDepartureTime\":null,\"estimatedDepartureTime\":null,\"timeToDeparture\":12672321,\"departureDelay\":1920000,\"actualArrivalDate\":null,\"actualDepartureDate\":null,\"arrivalDate\":\"12-05-2014\",\"departureDate\":\"11-05-2014\",\"estimatedArrivalDate\":\"2014-05-12\",\"estimatedDepartureDate\":null}],\"operatingFlightCode\":\"KL0835\",\"codeShares\":[\"KL6035\"],\"positions\":[{\"when\":1399872345056,\"latitude\":\"17.7768\",\"longitude\":\"92.8689\"},{\"when\":1399872405056,\"latitude\":\"17.6707\",\"longitude\":\"92.9451\"}]}]}';        	
        } else {
        	httpresponse res = h.send(req);
        	result = res.getBody();
        }
        ltcFrm = LtcFoxResponseModel.parse(result);
        return ltcFrm;
    }

   /**
    * @callType String flight/allflights
    * @flightType: departure, arrival
    * @travelDate String format '12-05-2014'
    **/
    public List<LtcFoxAirportResponseModel> getFoxAirportInfo(String language) {    
    	List<LtcFoxAirportResponseModel> ltcFrmList;
        String url = 'http://fox.klm.com/fox/json/data/airports?language='+language;
        
        Http h = new Http();
        Httprequest req = new Httprequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json; charset=UTF-8');
        req.setTimeout(60000);
        if (Test.isRunningTest()) {        	
	        //Rest call is not performed during test, so send back mock data
        	result = '[{\"name\":\"Kigali International Airport\",\"country\":\"Rwanda\",\"latitude\":\"-1.966667\",\"longitude\":\"30.133333\",\"timeZoneId\":\"Africa/Kigali\",\"label\":\"Kigali - Kigali International Airport (KGL), Rwanda\",\"iataCode\":\"KGL\",\"utcOffset\":120,\"city\":\"Kigali\",\"destinationGuide\":null}]';        	
        } else {
        	httpresponse res = h.send(req);
        	result = res.getBody();
        }
        system.debug(result);
        ltcFrmList = LtcFoxAirportResponseModel.parse(result);
        return ltcFrmList;
    }
 
 
   /**
    * @callType String flight/allflights
    * @flightType: departure, arrival
    * @travelDate String format '12-05-2014'
    **/
    public List<LtcFoxAirplanesResponseModel> getFoxAirplanesInfo() {    
    	List<LtcFoxAirplanesResponseModel> ltcFrmList;
        String fLanguage= 'nl';
        String fCountry= 'NL';
        String url = 'http://fox.klm.com/fox/json/data/airplanes?language='+fLanguage+'&country='+fCountry;
        
        Http h = new Http();
        Httprequest req = new Httprequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json; charset=UTF-8');
        req.setTimeout(60000);
        if (Test.isRunningTest()) {        	
	        //Rest call is not performed during test, so send back mock data
        	result = '[{"name":"Robin","type":"Boeing 737-800","registrationCode":"PH-BXV","imageUrl":"/fox/img/aircraft/Boeing-737-800.png"},{"name":"Leifur Eiriksson","type":"Boeing 737-400","registrationCode":"PH-BDW","imageUrl":"/fox/img/aircraft/Boeing-737-400.png"}]';        	
        } else {
        	httpresponse res = h.send(req);
        	result = res.getBody();
        }
        system.debug(result);
        ltcFrmList = LtcFoxAirplanesResponseModel.parse(result);
        return ltcFrmList;
    }
 
 	
    public LtcFoxTextSearchResultModel searchFoxText(String searchText) {    
 	//{"searchResult":[{"origin":"MSP","scheduledDepartureTime":"15:19","scheduledDepartureDate":"zo 07 dec","destination":"AMS","scheduledArrivalTime":"06:35","scheduledArrivalDate":"ma 08 dec","status":"ARRIVED","operatingFlightCode":"DL0160","codeShares":[],"departureDate":"07-12-2014"},{"origin":"MSP","scheduledDepartureTime":"15:19","scheduledDepartureDate":"ma 08 dec","destination":"AMS","scheduledArrivalTime":"06:35","scheduledArrivalDate":"di 09 dec","status":"ON_SCHEDULE","operatingFlightCode":"DL0160","codeShares":[],"departureDate":"08-12-2014"}]}
    	LtcFoxTextSearchResultModel ltcFrm;
        String fDateFormat = 'yyyy-MM-dd';	//E%20 puts day in text in front, - can be replaced by anything, and will return that
        String fLanguage= 'nl';
        String fCountry= 'nl';
        String url = 'http://fox.klm.com/fox/json/flights/bySearchText?searchText=' + searchText 
        		+ '&dayPattern=' + fDateFormat
        		+ '&language=' + fLanguage 
        		+ '&country=' + fCountry;
        //we'll call the REST API here
        Http h = new Http();
        Httprequest req = new Httprequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json; charset=UTF-8');
        req.setTimeout(60000);
        if (Test.isRunningTest()) {        	
	        //Rest call is not performed during test, so send back mock data
       		result = '{\"searchResult\":[{\"origin\":\"MSP\",\"scheduledDepartureTime\":\"15:19\",\"scheduledDepartureDate\":\"zo 07 dec\",\"destination\":\"AMS\",\"scheduledArrivalTime\":\"06:35\",\"scheduledArrivalDate\":\"ma 08 dec\",\"status\":\"ARRIVED\",\"operatingFlightCode\":\"DL0160\",\"codeShares\":[],\"departureDate\":\"07-12-2014\"},{\"origin\":\"MSP\",\"scheduledDepartureTime\":\"15:19\",\"scheduledDepartureDate\":\"ma 08 dec\",\"destination\":\"AMS\",\"scheduledArrivalTime\":\"06:35\",\"scheduledArrivalDate\":\"di 09 dec\",\"status\":\"ON_SCHEDULE\",\"operatingFlightCode\":\"DL0160\",\"codeShares\":[],\"departureDate\":\"08-12-2014\"}]}';
        } else {
        	httpresponse res = h.send(req);
        	result = res.getBody();
        }
        ltcFrm = LtcFoxTextSearchResultModel.parse(result);
        return ltcFrm;
    }
    
 	/**
 	 * Get Result for the visual force page
 	 **/
    public string getResult() {
        return this.result;
    }
    
    /**
 	 * Se Result for the Test class
 	 **/
    public void setResult(String result) {
        this.result = result;
    } 
     
}