/**                     
 * @author (s)      : David van 't Hooft
 * @description     : class to process the flight info
 * @log:   6JUN2014: version 1.0
 */
public with sharing class LtcFoxAirportResponseModel {
	public String name;
	public String country;
	public String latitude;
	public String longitude;
	public String timeZoneId;
	public String label;
	public String iataCode;
	public Integer utcOffset;
	public String city;
	public String destinationGuide;

	public static List<LtcFoxAirportResponseModel> parse(String json) {
		return (List<LtcFoxAirportResponseModel>) System.JSON.deserialize(json, List<LtcFoxAirportResponseModel>.class);
	}	
}