/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman
 * @description     : Model class to contain Rating Post objects
 * @log             :  2MAY2014: version 1.0
 * @log             : 21JUL2015: version 1.1
 * @log             : 27AUG2015: version 1.2 added img url properties
 */ 
global class LtcRatingPostModel {
    public Rating rating { get; set; }
    global class Rating {
        public String rating { get; set; }
        public String positiveComment { get; set; }
        public String positiveImage { get; set; }
        public String positiveImageLarge { get; set; }
        public String negativeComment { get; set; }
        public String negativeImage { get; set; }
        public String negativeImageLarge { get; set; }        
        public String isPublished { get; set; }        
        public String language { get; set; }        
        public String country { get; set; }        
        public LtcRatingPostModel.Flight flight { get; set; }
        public LtcRatingPostModel.Passenger passenger { get; set; }
    
        public Rating(String rating, String positiveComment, String negativeComment, String isPublished, LtcRatingPostModel.Flight flight, LtcRatingPostModel.Passenger passenger) {
            this.rating = rating;
            this.positiveComment = positiveComment;
            this.negativeComment = negativeComment;
            this.isPublished = isPublished;
            this.flight = flight;
            this.passenger = passenger;
        }
        
        public override String toString() {
        	String str = 
        		'{' +
            	'"rating": "' + rating +  '", ' +
            	'"positiveComment": "' + positiveComment +  '", ' +
            	'"negativeComment": "' + negativeComment +  '", ' +
            	'"isPublished": "' + isPublished + '"';
            	if (flight != null) {
            		str += ', "flight": ' + flight.toString();
            	}  
            	if (passenger != null) {
	            	str += ', "passenger": ' + passenger.toString();  
            	}  
                str += '}';
            return str;
        }
    }
    
    global class Passenger {
        public String firstName { get; set; }
        public String familyName { get; set; }
        public String email { get; set; }
        public String allowEmailContact { get; set; }
        public Seat seat { get; set; }
    
        public Passenger(String firstName, String familyName, Seat seat, String email, String allowEmailContact) {
            this.firstName = firstName;
            this.familyName = familyName;
            this.seat = seat;
            this.email = email;
            this.allowEmailContact = allowEmailContact;
        }
        
        public override String toString() { 
        	String str = '{' +
            	'"firstName": "' + firstName +  '", ' +
            	'"familyName": "' + familyName +  '", ' +
            	'"email": "' + email +  '", ' +
            	'"allowEmailContact": "' + allowEmailContact +  '" ';
            	if (seat != null) {
            		str += ', "seat": ' + seat;
            	}
                str += '}';
            return str;
        }
    }
	
	
    global class Flight {
        public String flightNumber;
        public String travelDate { get; set; }
        public String origin { get; set; }
        public String destination { get; set; }
    
        public Flight(String flightNumber, String travelDate) {
            this.flightNumber = flightNumber;
            this.travelDate = travelDate;
        }
        
        public override String toString() { 
            return '{' +
            	'"flightNumber": "' + flightNumber +  '", ' +
            	'"travelDate": "' + travelDate + '"' +
            '}';
        }
    }
    
    global class Seat {
        public String seatNumber { get; set; }
    
        public Seat(string seatNumber) {
            this.seatNumber = seatNumber;
        }
        
        public override String toString() { 
            return '{' +
            	'"seatNumber": "' + seatNumber + '"' + 
            '}';
        }
    }
}