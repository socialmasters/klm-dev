/**                     
 * @author (s)      : David van 't Hooft
 * @description     : Model class to contain the Airplane info from fox
 * @log:   6JUN2014: version 1.0
 */
public class LtcFoxAirplanesResponseModel {

	public String name;
	public String type;
	public String registrationCode;
	public String imageUrl;

	
	public static List<LtcFoxAirplanesResponseModel> parse(String json) {
		return (List<LtcFoxAirplanesResponseModel>) System.JSON.deserialize(json, List<LtcFoxAirplanesResponseModel>.class);
	}
	
}