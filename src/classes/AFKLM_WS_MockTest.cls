/**********************************************************************
 Name:  AFKLM_WS_MockTest.cls
======================================================
Purpose: WebServiceMock class for SOAP Web Services
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma    ?      			Initial development
    1.1		Patrick Brinksma    21 Jul 2014		Added Refund Request
***********************************************************************/  
@isTest
global class AFKLM_WS_MockTest implements WebServiceMock {

	global List<Id> listOfRefundId;

	global void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {

		System.debug('responseType: ' + responseType);
		if (responseType == 'AFKLM_WS_BPL_BookedPassengersListParam.DataOut'){
	
			// Create reponse element
			AFKLM_WS_BPL_BookedPassengersListParam.DataOut out = new AFKLM_WS_BPL_BookedPassengersListParam.DataOut();
			
			// List of Pax
			List<AFKLM_WS_BPL_BookedPassengersListParam.Pax> listOfPax = new List<AFKLM_WS_BPL_BookedPassengersListParam.Pax>(); 
		
			// Populate response element
			// Create 10 pax entries
			for (Integer x=0; x < 10; x++){
				// Create pax
				AFKLM_WS_BPL_BookedPassengersListParam.Pax p = new AFKLM_WS_BPL_BookedPassengersListParam.Pax();
				p.firstName = 'PATRICK' + x;
				p.lastName = 'BRINKSMA' + x;
				p.type_x = 'ADT';
				p.uniqueId = '001C5505AE17EAF' + x;
				
				// paxSpecificity List
				AFKLM_WS_BPL_BookedPassengersListParam.PaxSpecificity ps = new AFKLM_WS_BPL_BookedPassengersListParam.PaxSpecificity();
				ps.specificity = 'PASSPORT';
				ps.code = 'DOCS';
				p.paxSpecificiesList = new List<AFKLM_WS_BPL_BookedPassengersListParam.PaxSpecificity>{ps};
				
				// Service List
				AFKLM_WS_BPL_BookedPassengersListParam.Service pss = new AFKLM_WS_BPL_BookedPassengersListParam.Service();
				pss.type_x = 'SPECIALMEAL';
				pss.code = 'TRML';
				p.servicesList = new List<AFKLM_WS_BPL_BookedPassengersListParam.Service>{pss};
				
				// Classification List
				AFKLM_WS_BPL_BookedPassengersListParam.Classification pc = new AFKLM_WS_BPL_BookedPassengersListParam.Classification();
				pc.type_x = 'FQT';
	        	pc.code = 'FQTV';
	        	pc.level = 'COTE';
	        	pc.airline = 'KL';
	        	pc.contractNumber = '012345678' + x;

				AFKLM_WS_BPL_BookedPassengersListParam.Classification pc1 = new AFKLM_WS_BPL_BookedPassengersListParam.Classification();
				pc1.type_x = 'FQT';
	        	pc1.code = 'FQTR';
	        	pc1.level = 'IVOR';
	        	pc1.airline = 'AF';
	        	pc1.contractNumber = '012345678' + x;

	        	p.classificationsList = new List<AFKLM_WS_BPL_BookedPassengersListParam.Classification>{pc, pc1};			
				
				// Booking
				AFKLM_WS_BPL_BookedPassengersListParam.Booking b = new AFKLM_WS_BPL_BookedPassengersListParam.Booking();
				b.airlineCode = 'KL';
				b.flightNumber = '1111';
				b.flightSuffix = 'a';
				b.originAirportCode = 'AMS';
				b.departureDate = System.Now();
				b.arrivalDate = System.Now() + 1;
				b.destinationAirportCode = 'CDG';
				b.familyStatusCode = 'CONFIRMED';
				b.bookingClass = 'N';
				b.transportClass = 'Y';
				b.operatingAirlineCode = 'AF';
				b.operatingFlightNumber = '1212';
				b.operatingFlightSuffix = 'b';
				b.firstSegmentAirportCode = 'MAD';
				b.recordLocator = '4LYF3' + x;
				b.isGroup = false;
				b.ticketNumber = '006231084313' + x;
			
				// Point of Sales
				AFKLM_WS_BPL_Common.PointOfSale s = new AFKLM_WS_BPL_Common.PointOfSale();
				s.officeId = 'VLCI1250' + x;
				s.corporateCode = '1A';
				s.cityCode = 'VLC';
				s.countryCode = 'NL';
				s.iataCode = '78254713';
				s.agentId = '1979CA';
				s.agentType = 'T';
				b.creator = s; 
				
				// inbound connecting flight
				AFKLM_WS_BPL_BookedPassengersListParam.Flight i = new AFKLM_WS_BPL_BookedPassengersListParam.Flight();
				i.airlineCode = 'AF';
				i.flightNumber = '222';
				i.departureDate = System.Now () - 1;
				b.inboundConnectingFlight = i;
				
				AFKLM_WS_BPL_BookedPassengersListParam.Flight o = new AFKLM_WS_BPL_BookedPassengersListParam.Flight();
				i.airlineCode = 'KL';
				i.flightNumber = '2222';
				i.departureDate = System.Now () - 2;
				b.outboundConnectingFlight = o;		
				
				p.booking = b;
				
				listOfPax.add(p);	
			}
			// Add pax list to out
			out.paxList = listOfPax;

			response.put('response_x', out);

		} else if (responseType == 'AFKLM_WS_PLP_DeparturePassengerListParam.DataOut') {
			
			AFKLM_WS_PLP_DeparturePassengerListParam.DataOut out = new AFKLM_WS_PLP_DeparturePassengerListParam.DataOut();
			
			// List of Pax
			List<AFKLM_WS_PLP_DeparturePassengerListParam.Pax> listOfPax = new List<AFKLM_WS_PLP_DeparturePassengerListParam.Pax>(); 
		
			// Populate response element
			// Create 10 pax entries
			for (Integer x=0; x < 10; x++){
				// Create pax
				AFKLM_WS_PLP_DeparturePassengerListParam.Pax p = new AFKLM_WS_PLP_DeparturePassengerListParam.Pax();
				p.hasInfant = false;
				p.typology = 'ADT';
				p.firstName = 'PATRICK' + x;
				p.lastName = 'BRINKSMA' + x;
				p.uniqueId = '001C5505AE17EAF' + x;

				// Classification List
				AFKLM_WS_PLP_DeparturePassengerListParam.Classification pc = new AFKLM_WS_PLP_DeparturePassengerListParam.Classification();
				pc.type_x = 'FQT';
	        	pc.code = 'FQTV';
	        	pc.level = 'IVOR';
	        	pc.airline = 'AF';
	        	pc.contractNumber = '012345678' + x;
	        	p.classificationsList = new List<AFKLM_WS_PLP_DeparturePassengerListParam.Classification>{pc};
				
				// paxSpecificity List
				AFKLM_WS_PLP_DeparturePassengerListParam.Specificity ps = new AFKLM_WS_PLP_DeparturePassengerListParam.Specificity();
				ps.specificity = 'PASSPORT';
				ps.code = 'DOCS';
				p.specificitiesList = new List<AFKLM_WS_PLP_DeparturePassengerListParam.Specificity>{ps};
				
				// Service List
				AFKLM_WS_PLP_DeparturePassengerListParam.Service pss = new AFKLM_WS_PLP_DeparturePassengerListParam.Service();
				pss.type_x = 'SPECIALMEAL';
				pss.code = 'TRML';
				p.servicesList = new List<AFKLM_WS_PLP_DeparturePassengerListParam.Service>{pss};
				
				// Booking
				AFKLM_WS_PLP_DeparturePassengerListParam.Travel t = new AFKLM_WS_PLP_DeparturePassengerListParam.Travel();
				t.uniqueId = '0014351E66908765';
				t.recordLocator = '4LYF3' + x;
				t.bookingClass = 'N';
				t.bookedTransportClass = 'N';
				t.bookingStatus = 'CONFIRMED';
				
				AFKLM_WS_PLP_DeparturePassengerListParam.Flight f = new AFKLM_WS_PLP_DeparturePassengerListParam.Flight();
				f.airlineCode = 'KL';
				f.flightNumber = '1111';
				f.originAirportCode = 'AMS';
				f.destinationAirportCode = 'CDG';
				f.departureDate = System.Now();
				f.arrivalDate = System.Now() + 1;				
				f.headerDepartureDateLT = System.Now();
				f.bookingStatus = 'CONFIRMED';
				t.flight = f;				
				t.outboundConnectingFlight = f;
				t.inboundConnectingFlight = f;
				
				AFKLM_WS_PLP_DeparturePassengerListParam.Leg l = new AFKLM_WS_PLP_DeparturePassengerListParam.Leg();
				l.seatNumber = '3' + x + 'D';
				l.originAirportCode = 'SIN';
				l.destinationAirportCode = 'AMS';
				t.legsList = new List<AFKLM_WS_PLP_DeparturePassengerListParam.Leg>{l};
							
				p.travel = t;
				
				listOfPax.add(p);	
			}
			// Add pax list to out
			out.paxList = listOfPax;

			response.put('response_x', out);			
			
		} else if (responseType == 'AFKLM_WS_FBR_PassengerFBRecognitionX.FBRecognitionResponse') {
			
			// Create reponse element
			AFKLM_WS_FBR_PassengerFBRecognitionX.FBRecognitionResponse out = new AFKLM_WS_FBR_PassengerFBRecognitionX.FBRecognitionResponse();
			
			out.returnCode = 'OK';
			out.cin = 1234567890;
			out.gin = 1234567890;
			out.civility = 'MR';
			out.firstName = 'PATRICK';
			out.lastName = 'BRINKSMA';
			out.birthDate = System.Now() - 100;
			out.status = 'V';
			out.codeLanguage = 'NL';
			
			AFKLM_WS_FBR_PassengerFBRecognitionX.EmailData email = new AFKLM_WS_FBR_PassengerFBRecognitionX.EmailData();
			email.status = 'V';
			email.address = 'pbrinksma@salesforce.com';
			email.mailingAuthorized = 'Y';
			out.email = email;
			
			AFKLM_WS_FBR_PassengerFBRecognitionX.PhoneData phone = new AFKLM_WS_FBR_PassengerFBRecognitionX.PhoneData();
			phone.status = 'V';
			phone.mediumType = 'P';
			phone.phoneType = 'T';
			phone.countryCode = '31';
			phone.areaCode = '073';
			phone.number_x = '1234567890';

			AFKLM_WS_FBR_PassengerFBRecognitionX.PhoneData phone1 = new AFKLM_WS_FBR_PassengerFBRecognitionX.PhoneData();
			phone1.status = 'V';
			phone1.mediumType = 'P';
			phone1.phoneType = 'M';
			phone1.countryCode = '31';
			phone1.areaCode = '073';
			phone1.number_x = '1234567890';

			out.phoneList = new List<AFKLM_WS_FBR_PassengerFBRecognitionX.PhoneData>{phone, phone1};
			
			AFKLM_WS_FBR_PassengerFBRecognitionX.AddressData addr = new AFKLM_WS_FBR_PassengerFBRecognitionX.AddressData();
			addr.status = 'V';
			addr.addressType = 'D';
			addr.name = 'name';
			addr.street = 'straatnaam 1';
			addr.addressCompletion = 'T';
			addr.place = 'Plaats';
			addr.zipcode = '1111AA';
			addr.city = 'Utrecht';
			addr.provinceCode = 'UT';
			addr.countryCode = 'NL';
			out.mailingAddress = addr;
			
			AFKLM_WS_FBR_PassengerFBRecognitionX.FBContractData f = new AFKLM_WS_FBR_PassengerFBRecognitionX.FBContractData();
			f.startMemberShipDate = System.Now() - 100;
			f.endMemberShipDate = System.Now() + 100;
			f.amexContract = false;
			f.tierLevel = 'P';
			f.startTierLevelDate = System.Now() - 50;
			f.endTierLevelDate = System.Now() + 100;
			f.lastActivityDate = System.Now();
			f.oneCardStatus = null;
			f.oneCardMereCIN = 0;
			f.memberType = null;
			f.qualificationStatus = null;
			f.nbYearsPlatinum = 0;
			f.milesExpiryType = null;
			f.milesExpiryProtection = false;
			f.milesStatusAmount = 0;
			f.milesStatusPostponed = 0;
			f.milesStatusPostponedNextYear = 0;
			f.qSegmentNb = 0;
			f.qSegmentNbPostponed = 0;
			f.qSegmentNbPostponedNxtYr = 0;
			f.status = 'C';
			f.milesAmount = 1000;
			f.statusFB = '*';
			f.ptc = 'ADT';
			f.eliteLevel = 'P';
			f.milesToBuy = 0;
			f.poolingAuthorized = false;
			f.communicationMedia = 'T';
			f.lastModificationDate = System.Now();
			out.fbContract = f;
			
			// Response
			response.put('response_x', out);

		} else if (responseType == 'AFKLM_WS_SHI_Search.SearchHomonymsResponse'){

			AFKLM_WS_SHI_Search.SearchHomonymsResponse out = new AFKLM_WS_SHI_Search.SearchHomonymsResponse();

			out.individual = new List<AFKLM_WS_SHI_Search.Individu>();

			// Build individual
			for (Integer x=0; x<10; x++){
				AFKLM_WS_SHI_Search.Individu ind = new AFKLM_WS_SHI_Search.Individu();
				AFKLM_WS_SHI_SicIndividuType.IndividualInformations info = new AFKLM_WS_SHI_SicIndividuType.IndividualInformations();
				info.clientNumber = '123456789' + x;
				info.version = '1';
				info.lastName = 'Lastname' + x;
				info.gender = 'M';
				info.personnalIdentity = '' + x;
				info.dateOfBirth = System.now() - (30 * 365);
				info.nationality = 'NL';
				info.otherNationality = 'TR';
				info.firstName = 'Firstname' + x;
				info.flagNoFusion = true;
				info.status = 'V';
				info.flagBankFraud = false;
				info.flagThirdTrap = false;
				info.civility = 'MR';
				info.managingCompany = 'KL';

				ind.individualInformations = info;

				AFKLM_WS_SHI_SicIndividuType.ContractType c1 = new AFKLM_WS_SHI_SicIndividuType.ContractType();
				c1.contractNumber = '123456789' + x;
				c1.contractType = 'FP';
				c1.companyContractType = 'AF';
				c1.version = '1';
				c1.contractStatus = 'C';
				c1.validityStartDate = System.now() - (100);
				c1.validityEndDate = System.now() + (100);

				AFKLM_WS_SHI_SicIndividuType.ContractType c2 = new AFKLM_WS_SHI_SicIndividuType.ContractType();
				c2.contractNumber = '123456789' + x;
				c2.contractType = 'FP';
				c2.companyContractType = 'MA';
				c2.version = '1';
				c2.contractStatus = 'C';
				c2.validityStartDate = System.now() - (100);
				c2.validityEndDate = System.now() + (100);

				AFKLM_WS_SHI_SicIndividuType.ContractType c3 = new AFKLM_WS_SHI_SicIndividuType.ContractType();
				c3.contractNumber = '123456789' + x;
				c3.contractType = 'FP';
				c3.companyContractType = 'AX';
				c3.version = '1';
				c3.contractStatus = 'C';
				c3.validityStartDate = System.now() - (100);
				c3.validityEndDate = System.now() + (100);				

				ind.contract = new List<AFKLM_WS_SHI_SicIndividuType.ContractType>{c1, c2, c3};

				AFKLM_WS_SHI_SicIndividuType.PostalAddressType p1 = new AFKLM_WS_SHI_SicIndividuType.PostalAddressType();
				p1.addressKey = '123456789' + x;
				p1.version = '1';
				p1.mediumCode = 'D';
				p1.mediumStatus = 'V';
				p1.city = 'AMSTERDAM';
				p1.numberAndStreet = '10 straatnaam';
				p1.zipCode = '1111AA';
				p1.countryCode = 'NL';

				ind.postalAddress = new List<AFKLM_WS_SHI_SicIndividuType.PostalAddressType>{p1};

				AFKLM_WS_SHI_SicIndividuType.TelecomType t1 = new AFKLM_WS_SHI_SicIndividuType.TelecomType();
				t1.telecomKey = '123456789' + x;
				t1.version = '1';
				t1.mediumStatus = 'V';
				t1.mediumCode = 'D';
				t1.terminalType = 'T';
				t1.phoneNumber = '123456789';
				t1.countryCode = '31';

				ind.telecom = new List<AFKLM_WS_SHI_SicIndividuType.TelecomType>{t1};

				AFKLM_WS_SHI_SicIndividuType.EmailType e1 = new AFKLM_WS_SHI_SicIndividuType.EmailType();
				e1.emailKey = '123456789' + x;
				e1.version = '1';
				e1.mediumStatus = 'V';
				e1.mediumCode = 'D';				
				e1.email = 'test' + x + '@testingemail.test';
				e1.fbOptin = 'A';

				ind.email = new List<AFKLM_WS_SHI_SicIndividuType.EmailType>{e1};

				out.individual.add(ind);
			}

			// Response
			response.put('response_x', out);
			
		} else if (responseType == 'AFKLM_WS_PNR_PNRListForACustomerParam.DataOut'){

			AFKLM_WS_PNR_PNRListForACustomerParam.DataOut out = new AFKLM_WS_PNR_PNRListForACustomerParam.DataOut();

			List<AFKLM_WS_PNR_PNRListForACustomerParam.TravelRecord> listOfTR = new List<AFKLM_WS_PNR_PNRListForACustomerParam.TravelRecord>();

			for (Integer x=0; x<10; x++){

				AFKLM_WS_PNR_PNRListForACustomerParam.TravelRecord tr = new AFKLM_WS_PNR_PNRListForACustomerParam.TravelRecord();
				tr.reclocAmd = '0123456789' + x;
				tr.creationDate = System.now();
				tr.purgeDateAmd = System.now();

				AFKLM_WS_PNR_Common.PointOfSale p = new AFKLM_WS_PNR_Common.PointOfSale();
				p.officeId = '12345' + x;
				p.corporateCode = 'KL';
				p.cityCode = 'AMS';
				p.countryCode = 'NL';
				p.agentType = 'A';
				
				tr.creator = p;
				tr.lastUpdater = p;
				tr.owner = p;

				tr.responsibleOfficeId = '123456' + x;
				tr.nbPassengers = 1;
				tr.isGroup = false;
				tr.nbNoNamedPassengers = 0;

				AFKLM_WS_PNR_PNRListForACustomerParam.DetailedPassenger dp = new AFKLM_WS_PNR_PNRListForACustomerParam.DetailedPassenger();
				dp.firstName = 'firstName';
				dp.lastName = 'lastName';
				dp.passengerType = 'ADT';
				dp.tattoo = '1';
				dp.uci = '123456789' + x;

				AFKLM_WS_PNR_PNRListForACustomerParam.Segment sg = new AFKLM_WS_PNR_PNRListForACustomerParam.Segment();
				sg.tattoo = '1';
				sg.airlineCode = 'KL';
				sg.flightNumber = '777';
				sg.bookingStatus = 'CONFIRMED';
				sg.boardingPoint = 'AMS';
				sg.offPoint = 'AMS';
				sg.departureDate = System.now() - 100;
				sg.arrivalDate = System.now() + 100;
				sg.bookingClass = 'C';
				sg.transportClass = 'M';
				sg.isOpen = false;

				tr.segmentsList = new List<AFKLM_WS_PNR_PNRListForACustomerParam.Segment>{sg};

				listOfTR.add(tr);

			}

			// Response
			response.put('response_x', out);

		} else if (responseType == 'AFKLM_WS_IssueRefundRequest.AddCSRSRecordsResponse_element'){
			// Response instance
			AFKLM_WS_IssueRefundRequest.AddCSRSRecordsResponse_element out = new AFKLM_WS_IssueRefundRequest.AddCSRSRecordsResponse_element();
			AFKLM_WS_IssueRefundRequest.AddCSRSRecordsResponse outData = new AFKLM_WS_IssueRefundRequest.AddCSRSRecordsResponse();
			// Payload
			List<AFKLM_WS_IssueRefundRequest.RefundResponse> resp = new List<AFKLM_WS_IssueRefundRequest.RefundResponse>();
			Integer i=0;
			for (Id thisId : listOfRefundId){
				AFKLM_WS_IssueRefundRequest.RefundResponse thisResp = new AFKLM_WS_IssueRefundRequest.RefundResponse();
				if (i == 0){
					thisResp.CSRRefNumber = '1234567890';
					i++;
				} else {
					thisResp.CSRRefNumber = 'Reason - some exception';
					i=0;
				}
				thisResp.SalesForceID = thisId;
				resp.add(thisResp);
			}

			outData.AddCSRSRecordsResultData = resp;
			out.AddCSRSRecordsResult = outData;

			// Response
			response.put('response_x', out);
		}

	}

}