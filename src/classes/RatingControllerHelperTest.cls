/********************************************************************** 
 Name:  RatingControllerHelperTest
 Task:    N/A
 Runs on: RatingControllerHelper
====================================================== 
Purpose: 
    This class contains unit tests for validating the behavior of Apex classes and triggers. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     David van 't Hooft  21/01/2015      Initial Development
***********************************************************************/
@isTest
private class RatingControllerHelperTest {

	private static ApexPages.StandardSetController myStandardSetController() {
	        List<Rating__c> ratingList = getRatingList();
	        ApexPages.StandardSetController ratingListController = 
	        	new ApexPages.StandardSetController(ratingList);
	        
	        return ratingListController;
    }
    
    private static List<Rating__c> getRatingList() {
        List<Rating__c> ratingList = new List<Rating__c>();
    	
        Case tstCase = new Case(
        
        	Priority = 'Normal',
            Status = 'New'
        );
        
        insert tstCase; 
    	
		String flightNumber = 'KL1234';
        Date trDate = System.now().date();
		String firstName = 'Jan';
		String familyName = 'Jansen';
		String negativeFeedBack = 'Negative';
		String positiveFeedback = 'Positive';
		Boolean pub = false;
		Integer ratingNumber = 5;
		String seatNumber = '50G';

        //Create Flight
        Flight__c flight = new Flight__c(Flight_Number__c = flightNumber, Scheduled_Departure_Date__c = trDate);
        insert flight;
            
        //Create flight date
	    Flight_Date__c flight_Date = new Flight_Date__c(Full_Date__c = trDate);
        insert flight_Date;
        	
        Passenger__c passenger;	
        Rating__c rating;		
        
    	for (integer i = 0; i < 2; i++) {
    	
			familyName += String.valueOf(i);
		
	        //Store the passenger
	        passenger = new Passenger__c(First_Name__c = firstName, Family_Name__c = familyName );
	        insert passenger;
	        
	        //Store the Rating with links to the passenger and the flight information
	        rating = new Rating__c(Flight_Info__c = flight.Id, Flight_Date__c = flight_Date.Id, Passenger__c = passenger.Id, Negative_comments__c = negativeFeedBack, 
	        		Positive_comments__c = positiveFeedback, Publish__c = pub, Rating_Number__c = ratingNumber, Seat_Number__c = seatNumber, caseId__c = tstCase.Id );
	        insert rating;

            ratingList.add(rating);
        }
        return ratingList;
    }

    static testMethod void markReviewedTest() {        
        test.startTest();
        ApexPages.StandardSetController ratingSetController = myStandardSetController(); 
        List<Rating__c> rl = getRatingList();
        
        List<RatingWrapper> ratingWrapList = new List<RatingWrapper>();
        RatingListController myController = new RatingListController(ratingSetController);
        for (Rating__c r : rl) {
        	RatingWrapper rw = new RatingWrapper(r, ratingSetController);
        	rw.isSelected=true;
            ratingWrapList.add(rw);
        }
        RatingControllerHelper contHelper = new RatingControllerHelper();
        contHelper.markReviewed(ratingWrapList);
        test.stopTest();
    }
       
}