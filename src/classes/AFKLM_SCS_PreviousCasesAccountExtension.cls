public with sharing class AFKLM_SCS_PreviousCasesAccountExtension {

	private Id accntId;

	public ApexPages.StandardSetController ssc {
		get{
			if (ssc == null){
				ssc = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Id, Subject, CaseNumber, CreatedDate, Status, Case_Topic__c, Case_Detail__c, Sentiment_at_close_of_case__c 
                     FROM Case 
                     WHERE AccountId = :accntId 
                     ORDER BY CreatedDate DESC LIMIT 5
                    ]));
			}
			return ssc;
		}
		set;
	}

	public AFKLM_SCS_PreviousCasesAccountExtension(ApexPages.StandardController stdCtrl) {
		accntId = stdCtrl.getId();
	}

	public List<Case> getCases(){
		return (List<Case>)ssc.getRecords();
	}
}