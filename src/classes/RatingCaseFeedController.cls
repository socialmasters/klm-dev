/**********************************************************************
 Name:  RatingCaseFeedController.cls
======================================================
Purpose: Controller class for VF Pages:
		 RatingCaseFeed page
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     David van 't Hooft  16/02/2015      Initial development
    2.0     Mees Witteman       01/04/2015      Implement actions
***********************************************************************/
public with sharing class RatingCaseFeedController {
	public String ratingComment {get; set;}
	public Case ratingCase {get; set;}
	
	public RatingCaseFeedController() {
		
	}
	
	public RatingCaseFeedController(ApexPages.StandardController stdC){
		this.ratingCase = (Case) stdC.getRecord();
	}
	
	public void commitRatingComment() {
		FeedItem fi = new FeedItem(parentId = ratingCase.Id);
		fi.body = 'KLM reply: ' + ratingCase.comments__c;
		fi.title = 'title';
		insert fi;
		
		// find and update the Rating KLM reply field
		List<Rating__c> ratings = [
			select r.id, r.language__c, r.country__c, r.Email__c, 
				r.AllowEmailContact__c, r.flight_number__c, r.travel_date__c, r.KLM_Reply__c
			from Rating__c r
			where r.CaseId__c =: ratingCase.Id
		];
		if (ratings.size() > 0) {
			Rating__c rating = ratings[0];
			Boolean noKlmReplyYet = rating.KLM_Reply__c == null || ''.equalsIgnoreCase(rating.KLM_Reply__c);
	
			// fill with comment box tekst
			rating.KLM_Reply__c = ratingCase.Comments__c;
			update rating;
			
			Boolean sendMail = noKlmReplyYet 
				&& rating.KLM_Reply__c != null 
				&& !''.equalsIgnoreCase(rating.KLM_Reply__c)
				&& rating.AllowEmailContact__c 
				&& rating.Email__c != null 
				&& !''.equals(rating.Email__c);
			
			if (sendMail) {
				sendMail(rating);
			}
		}
		ratingCase.Comments__c = '';
	}
	
	private void sendMail(Rating__c rating) {
  		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setUseSignature(false);
		mail.setBccSender(false);
		mail.setSaveAsActivity(true);
		mail.setWhatId(ratingCase.Id);
		
		String language = determineSmhLanguage(rating.language__c);
		language = language.toLowerCase();
		EmailTemplate template;
		List<EmailTemplate> ets = [Select Id, Subject, HtmlValue, Body from EmailTemplate where DeveloperName=:'Ltc_Rating_KLM_Reply_' + language];
		if (ets == null || ets.size() == 0) {
			System.debug(LoggingLevel.WARN, 'template Ltc_Rating_KLM_Reply_' + language + ' could not be found, default to EN');
			ets = [Select Id, Subject, HtmlValue, Body from EmailTemplate where DeveloperName =: 'Ltc_Rating_KLM_Reply_en'];
		}
		if (ets != null && ets.size() == 1) {
			template = ets[0];
		}

		String htmlBody = template.htmlValue;
		String urlReplacement = getAllRatingsUrl(rating);
		htmlBody = htmlBody.replace('{ALL_RATINGS_URL}', urlReplacement);
		mail.setHtmlBody(htmlBody);
		mail.setSubject(template.subject);
		
		Contact contact = [SELECT Contact.Id FROM Case WHERE Id =: ratingCase.Id].Contact;
		if (contact != null) {
			mail.setTargetObjectId(contact.Id);
		}
		
		List<OrgWideEmailAddress> lstOrgWideEmailId = [Select id from OrgWideEmailAddress where address = 'noreply.flightguide@klm.com'];
		if (lstOrgWideEmailId.size() == 0) {
		   System.debug(LoggingLevel.ERROR, 'There is no correct Organization wide email address setup in the org. Please set noreply.flightguide@klm.com');
		} else {
			String orgWideEmailAddressID = lstOrgWideEmailId[0].id;    
			mail.setOrgWideEmailAddressId(orgWideEmailAddressID);
		}
		
		try {
			Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
		} catch (Exception e) {
			System.debug(LoggingLevel.ERROR, 'send mail failed msg=' + e.getMessage());
		}
	}
	
	/**
	 *  return base-url + NgFlightGuide/#/gb/en/kl1001/2015-02-13/2015-02-13/2015-02-13/kl1001/ratingoverview
	 **/
	public static String getAllRatingsUrl(Rating__c rating){
		String instanceUrl = getInstanceUrl();
		String travelDate = String.valueOf(rating.travel_date__c);
		String country = rating.country__c == null ? 'nl' : rating.country__c.toLowerCase();
		String language = rating.language__c == null ? 'nl' : rating.language__c.toLowerCase();
		String result = (instanceUrl == null ? '' : instanceUrl)
				+ '/NgFlightGuide/#/' + country + '/' + language
				+ '/' + rating.flight_number__c 
				+ '/' + travelDate
				+ '/' + travelDate 
				+ '/' + travelDate
				+ '/' + rating.flight_number__c 
				+ '/ratingoverview';
	    return result;
	}
	
	public static String getInstanceUrl(){
	    RatingService__c ratingCustomSettings = RatingService__c.getOrgDefaults();
	    return ratingCustomSettings.LTC_Base_Url__c;
	}
	
	private String determineSmhLanguage(String language) {
		String result = language;
		if (language == null || ''.equals(language)) {
			result = 'EN';
		} else if ('PL'.equals(language.toUpperCase())) {
			result = 'EN';
		} else if ('HU'.equals(language.toUpperCase())) {
			result = 'EN';
		} else if ('CZ'.equals(language.toUpperCase())) {
			result = 'EN';
		} else if ('DA'.equals(language.toUpperCase())) {
			result = 'EN';
		} else if ('FI'.equals(language.toUpperCase())) {
			result = 'EN';
		} else if ('HK'.equals(language.toUpperCase())) {
			result = 'EN';
		} else if ('ID'.equals(language.toUpperCase())) {
			result = 'EN';
		} else if ('IN'.equals(language.toUpperCase())) {
			result = 'EN';
		} else if ('RO'.equals(language.toUpperCase())) {
			result = 'EN';
		} else if ('SK'.equals(language.toUpperCase())) {
			result = 'EN';
		} else if ('SV'.equals(language.toUpperCase())) {
			result = 'EN';
		} else if ('TH'.equals(language.toUpperCase())) {
			result = 'EN';
		} else if ('TW'.equals(language.toUpperCase())) {
			result = 'EN';
		} else if ('BR'.equals(language.toUpperCase())) {
			result = 'PT';
		} else if ('UK'.equals(language.toUpperCase())) {
			result = 'RU';
		}
		return result.toUpperCase();
	}
}