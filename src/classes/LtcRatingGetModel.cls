/**                     
 * @author (s)      : Edwin van Schaick / David van 't Hooft
 * @description     : Model class to contain Rating domain objects
 * @log:   28APR2014: version 1.0
 * 
 */
global class LtcRatingGetModel { 
	 
    public Flight getRatingResponse(String flightNumber, String startDate, String endDate, List<String> ratingIds, List<Rating__c> ratingList, Integer limitTo, Integer offset) {
        return new Flight(flightNumber, startDate, endDate, ratingIds, ratingList, limitTo, offset);
    }
   
    global class Flight {
        public String flightNumber;
        public String startDate { get; set; }
        public String endDate { get; set; }
        public RatingStatistics ratingStatistics { get; set; }
        public RatingContainer ratings { get; set; }
        public Integer total  { get; set; }
    
        public Flight(String flightNumber, String startDate, String endDate, List<String> ratingIds, List<Rating__c> ratingList, Integer limitTo, Integer offset) {
            Boolean showSeatNumber = hasMoreFlightDays(ratingList);
            this.flightNumber = flightNumber;
            this.startDate = startDate;
            this.endDate = endDate;
            this.ratingStatistics = new RatingStatistics(ratingList);
            this.total = ratingList.size();
            List<Rating__c> reducedRatingList = LtcUtil.getLimitedList(ratingList, limitTo, offset);
            this.ratings = new RatingContainer(ratingIds, reducedRatingList, showSeatNumber);
        }
        
        private Boolean hasMoreFlightDays(List<Rating__c> ratingList) {
        	Boolean result = false;
        	Set<Date> dates = new Set<Date>();
        	for (Rating__c rating : ratingList) {
        		dates.add(rating.Flight_Info__r.Scheduled_Departure_Date__c);
        	}
        	System.debug(LoggingLevel.DEBUG, 'hasMoreFlightDays : number of dates ' + dates.size());
        	return dates.size() > 1;
        }
        
        public override String toString() { 
            return '{' +
                '"flightNumber": "' + flightNumber +  '", ' +
                '"startDate": "' + startDate +  '", ' +
                '"endDate": "' + endDate +  '", ' +
                '"total": ' + total +  ', ' +
                '"ratingStatistics": ' + ratingStatistics.toString() +  ', ' +
                '"ratings": ' + ratings.toString() +  
			'}';
        }
    }
    
    global class RatingStatistics {
        public Integer numberOfRatings { get; set; }
        public String averageRating { get; set; }
        public RatingCategoryContainer ratingCategories { get; set; }
    
        public RatingStatistics(List<Rating__c> ratingList) {
            this.numberOfRatings = ratingList.size();
            this.calculateAverage(ratingList);
            this.createRatingCategories(ratingList);
        }
        
        private void CalculateAverage(List<Rating__c> ratingList) {
            Decimal total = 0;
            
            if (ratingList != null && ratingList.size()>0) {
                for (Rating__c rating: ratingList) {
                    total += rating.Rating_Number__c;
                }
                
                averageRating = String.ValueOf(total/ratingList.size());
            } else {
                averageRating = '';     
            }       
        }
        
        private void CreateRatingCategories(List<Rating__c> ratingList) {
            // Create categories first.
            // TODO: translation for category name to be added.
            List<RatingCategory> ratingCategoryList = new List<RatingCategory>();
            ratingCategoryList.add(new RatingCategory('Horrible', 1));
            ratingCategoryList.add(new RatingCategory('So-so', 2));
            ratingCategoryList.add(new RatingCategory('Ok', 3));
            ratingCategoryList.add(new RatingCategory('Good', 4));
            ratingCategoryList.add(new RatingCategory('Excellent', 5));
            
            // Now count all the ratings.
            if (ratingList != null) {
                for (Rating__c rating: ratingList) {
                    if (rating.Rating_Number__c > 0 && rating.Rating_Number__c < 6) {
                        ratingCategoryList[Integer.ValueOf(rating.Rating_Number__c) - 1].count++;
                    }
                }
            }
            ratingCategories = new RatingCategoryContainer(ratingCategoryList);
        }
        
        public override String toString() {
            return '{' +
                '"numberOfRatings": "' + numberOfRatings +  '", ' +
                '"averageRating": "' + averageRating +  '", ' +
                '"ratingCategory": ' + ratingCategories.toString() +  
                '}';
        }
    }
    
    global class RatingCategoryContainer {
        public List<RatingCategory> itemList { get; set; }
    
        public RatingCategoryContainer(List<RatingCategory> itemList) {
            this.itemList = itemList;
        }
        
        public override String toString() { 
            String result = '[';
            String separator = '';
            
            for (RatingCategory item: itemList) {
                result += separator;
                separator = ', ';               
                result += item.toString();
            }
            
            result += ']';
            
            return result;
        }
    }
    
    global class RatingCategory {
        public String categoryName { get; set; }
        public Integer level { get; set; }
        public Integer count { get; set; }
    
        public RatingCategory(String categoryName, Integer level) {
            this.categoryName = categoryName;
            this.level = level;
            this.count = 0;
        }
        
        public override String toString() { 
            return '{' +
                //'"name": "' + categoryName +  '", ' +
                '"level": "' + level +  '", ' +
                '"count": "' + count +  
                '"}';
        }
    }
    
    global class RatingContainer {
        public List<Rating> itemList { get; set; }
    
        public RatingContainer(List<String> ratingIds, List<Rating__c> objectList, Boolean showSeatNumber) {
            String tmpId = '';
            this.itemList = new List<Rating>();
            LtcCrypto ltcc = new LtcCrypto();
            Set<String> unEncryptedOwnIds = new Set<String>();
            if (objectList != null) {
                //First add the ratings that matches the ratingIds in the list
                if (ratingIds != null) {
                	for (String ratingId: ratingIds) {
	                    tmpId = ltcc.decodeText(ratingId);
		                for (Rating__c item: objectList) {
		                    if (tmpId.equals(item.Id)) {
		                        //Only store the own rating Id, leave the rest blank!
	                    		System.debug(LoggingLevel.INFO, 'decoded tmpId=' + tmpId + ' item.id=' + item.id + ' added rating with ratingId=' + ratingId);
		                        itemList.add(new Rating(item, ratingId, showSeatNumber));
		                        unEncryptedOwnIds.add(tmpId);
		                        break;
		                    }
		                }
                	}
                }
                for (Rating__c item: objectList) {
                    if (!unEncryptedOwnIds.contains(item.Id) && item.Publish__c == true) {
                        //Only store the own rating Id, leave the rest blank!
                        itemList.add(new Rating(item, '', showSeatNumber));
                        System.debug(LoggingLevel.DEBUG, 'add rating with item.id=' + item.id);
                    }
                }
            }
        }
        
        public override String toString() { 
            String result = '[';
            String separator = '';
            
            for (Rating item: itemList) {
                result += separator;
                separator = ', ';
                
                result += item.toString();
            }
            
            result += ']';
            
            return result;
        }
    }
    
    global class Rating {
    	private LtcCrypto ltcc=  new LtcCrypto();        
    	public Rating__c rating { get; set; }
        public String ownRatingId {get; set; }
        public Passenger passenger { get; set; }
        public String flightNumber { get; set; }
        public String travelDate { get; set; }
        
        public Rating(Rating__c rating, String ownRatingId, Boolean showSeatNumber) { 
            this.rating = rating;
            this.ownRatingId = ownRatingId;
            this.passenger = new Passenger(rating.Passenger__r, showSeatNumber ? rating.Seat_Number__c : '', rating.Hidden_Seat_Number__c);
            this.flightNumber = rating.Flight_Info__r.Flight_Number__c;
            if (showSeatNumber) {
                this.travelDate = String.ValueOf(rating.Flight_Info__r.Scheduled_Departure_Date__c).substring(0, 7);
            } else {
                this.travelDate = String.ValueOf(rating.Flight_Info__r.Scheduled_Departure_Date__c);
            }
        }
        
        public override String toString() {
            LtcDateTimeFormatter ltcDateTimeFormatter = new LtcDateTimeFormatter();
            String positiveComment = '';
            String negativeComment = '';
            String klmReply = '';
            if (!rating.Hidden_Comments__c) {
                positiveComment = EncodingUtil.urlEncode(rating.Positive_comments__c, 'UTF-8').replaceAll('\\+', '%20');
                negativeComment = EncodingUtil.urlEncode(rating.Negative_comments__c, 'UTF-8').replaceAll('\\+', '%20');
                klmReply = EncodingUtil.urlEncode(rating.KLM_Reply__c, 'UTF-8').replaceAll('\\+', '%20');
            }
            String ratingId = ''.equals(ownRatingId) ? 'null' : '"' + ownRatingId + '"';
            return '{' +
                '"ratingId": ' + ratingId +  ', ' +
                '"rating": "' + rating.Rating_Number__c +  '", ' +
                '"positiveComment": "' + positiveComment +  '", ' +
                '"negativeComment": "' + negativeComment +  '", ' +
                '"klmReply": "' + klmReply +  '", ' +
                '"flightNumber": "' + flightNumber + '", ' +  
                '"travelDate": "' + travelDate + '", ' +  
                '"isPublished": "' + String.valueOf(rating.Publish__c) + '", ' + 
                '"hiddenComments": "' + String.valueOf(rating.Hidden_Comments__c) + '", ' + 
                '"passenger": ' + passenger.toString() +  
			'}';
        }
    }
    
    global class Passenger {
        public Passenger__c passenger { get; set; }
        public Seat seat { get; set; }
        publiC Boolean hiddenSeatNumber { get; set; }
        
        public Passenger(Passenger__c passenger, string seatNumber, boolean hiddenSeatNumber) {
            this.passenger = passenger;
            this.seat = new Seat(seatNumber, hiddenSeatNumber);
            this.hiddenSeatNumber = hiddenSeatNumber;
        }
        
        public override String toString() { 
            String firstName = '';
            if (!passenger.Hidden_Name__c && !hiddenSeatNumber) {
                firstName = EncodingUtil.urlEncode(passenger.First_Name__c, 'UTF-8').replaceAll('\\+', '%20');
            }
            return '{' +
                '"firstName": "' + firstName +  '", ' +
                '"hiddenName": "' + String.valueOf(passenger.Hidden_Name__c) + '", ' + 
                '"seat": ' + seat.toString() +
                '}';
        }
    }
    
    global class Seat {
        public String seatNumber { get; set; }
        public Boolean hiddenSeatNumber;
    
        public Seat(string seatNumber, Boolean hiddenSeatNumber) {
            this.seatNumber = seatNumber;
            this.hiddenSeatNumber = hiddenSeatNumber;
        }
        
        public override String toString() { 
            String seatNum = '';
            if (!hiddenSeatNumber) {
                seatNum = EncodingUtil.urlEncode(seatNumber, 'UTF-8').replaceAll('\\+', '%20');
            }
            return '{' +
                '"seatNumber": "' + seatNum + '", ' +
                '"hiddenSeatNumber": "' + String.valueOf(hiddenSeatNumber) + '"' + 
            '}';
        }
    }
 }