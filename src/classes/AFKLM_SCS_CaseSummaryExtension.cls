/**********************************************************************
 Name:  AFKLM_SCS_CaseSummaryExtension
 Task:    N/A
 Runs on: SCS_CaseHighlightConsole
======================================================
Purpose: 
        The Case highlight console extension which is custom Visualforce build
        
        Note: Performance issue on formula fields 'sf4twitter__Fcbk_User_Id__pc'
        - It is possible to ask SF to create Custom Index: https://developer.salesforce.com/blogs/engineering/2013/02/force-com-soql-best-practices-nulls-and-formula-fields.html
        - Reference: http://help.salesforce.com/apex/HTViewSolution?id=000181277&language=en_US

======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta          Unknown         INITIAL DEVELOPMENT
    1.1     Stevano Cheung      07/08/2014      Fixed query execution. First use the parent Id for getting the profileURL. After the non-indexed formula field sf4twitter__Fcbk_User_Id__pc
    1.2     Stevano Cheung      02/12/2014      Added Klout feature score  
***********************************************************************/
public class AFKLM_SCS_CaseSummaryExtension {
    private final Case cs;
    private final Account acc;
    private Integer kloutScr = 0;

    public AFKLM_SCS_CaseSummaryExtension (ApexPages.StandardController stdController) {
        if(!Test.isRunningTest()) {
            stdController.addFields(new List<String>{'AccountId'});
        }
        this.cs = (Case) stdController.getRecord();
    }

    public String getProfileURL(){
        String profurl;
        List<Account> acc = [SELECT id,sf4twitter__Fcbk_User_Id__pc FROM Account WHERE id=:cs.AccountId LIMIT 1];   
        try{    
            if(cs.AccountId != null) {
                profurl = [SELECT ProfileURL FROM SocialPersona WHERE parentId =: cs.AccountId].ProfileURL;
            } else {
                profurl = [SELECT ProfileURL FROM SocialPersona WHERE FB_User_Id__c=: acc[0].sf4twitter__Fcbk_User_Id__pc].ProfileURL;
            }
        } catch(Exception e){
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'No Social Persona attached to the account or incorrect ID. ' ) );
            System.Debug(e.getMessage());
        }
        return profurl;
    }
    
    public /*Integer*/void getKloutScore() {
        Integer kloutScore = 0;
        List<Account> acc = [SELECT id, sf4twitter__Twitter_User_Id__pc, sf4twitter__Twitter_Username__pc,Klout_Score__c FROM Account WHERE id=:cs.AccountId LIMIT 1];
        
        try {
            if(cs.AccountId != null) {
                String kloutId = '';
                if(acc[0].sf4twitter__Twitter_User_Id__pc != null) {
                    kloutId = (String) kloutObjects('http://api.klout.com/v2/identity.json/tw/'+acc[0].sf4twitter__Twitter_User_Id__pc+'?key=us4k68rumkgwwnqkk797hsm4').get('id');
                    
                } else if(acc[0].sf4twitter__Twitter_Username__pc != null) {
                    kloutId = (String) kloutObjects('http://api.klout.com/v2/identity.json/twitter?screenName='+acc[0].sf4twitter__Twitter_Username__pc+'&key=us4k68rumkgwwnqkk797hsm4').get('id');
                }
                
                if(!''.equals(kloutId)) {
                	Decimal kloutDecimal = Decimal.valueOf(Double.valueOf(kloutObjects('http://api.klout.com/v2/user.json/'+kloutId+'/score?key=us4k68rumkgwwnqkk797hsm4').get('score')));
                	kloutScore = Integer.valueOf(kloutDecimal.divide(1, 0, System.RoundingMode.UP));
                	kloutScr = kloutScore;
                	saveKloutScore();
                    // kloutScore = Integer.valueOf(kloutObjects('http://api.klout.com/v2/user.json/'+kloutId+'/score?key=us4k68rumkgwwnqkk797hsm4').get('score'));
                }
            }
        } catch(Exception e) {
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'Klout Score error. ' ) );
            System.Debug(e.getMessage());
        }

        //return kloutScore;
    }
    
    private Map<String, Object> kloutObjects(String endpoint) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setHeader('Content-Type', 'application/json');
        req.setMethod('GET');
        
        HttpResponse res = h.send(req);
        return (Map<String, Object>) JSON.deserializeUntyped(res.getbody());
    }
    
    private void saveKloutScore(){
    	
    	Account account = [SELECT Klout_Score__c, Influencer__c FROM Account WHERE id =: cs.AccountId LIMIT 1];
    	if(account.Klout_Score__c != kloutScr){
    		account.Klout_Score__c = kloutScr;
    		if(kloutScr >= 60 && account.Influencer__c == false){
    			account.Influencer__c = true;
    		}
    		update account;
    	}
    }   
    
    public Integer getKloutScoreNumber(){
    	return kloutScr;
    }
}