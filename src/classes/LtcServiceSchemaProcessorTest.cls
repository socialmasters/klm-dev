/**
 * Test the service schema processor
 */
@isTest
private class LtcServiceSchemaProcessorTest {

    static testMethod void testServiceItemCreation() {
        
        CabinClass__c economyClass = new CabinClass__c();
        economyClass.name = 'economy';
        economyClass.label__c = 'economy';
        insert economyClass;
        
        CabinClass__c businessClass = new CabinClass__c();
        businessClass.name = 'business';
        businessClass.label__c = 'business';
        insert businessClass;
        
        StaticResource sr = [
			Select Name, Id, Description 
			From StaticResource 
			where name =: 'LTCServiceSchedule'
		];

        
        TestProcessor processor = new TestProcessor();
        processor.processServiceSchema();
        
        List<ServiceItem__c> sis = [
            select id from ServiceItem__c
        ];
        
        System.assertEquals(15, sis.size());
    }
    
    private class TestProcessor extends LtcServiceSchemaProcessor {
        
        private virtual Boolean isEuroSchema() {
        	
        	return false;
        }
        
        private virtual String[] getLinesFromResource() {
            String lines = 'Service Schedules' +
            '/nSelected Date:  09-Sep-2014 ' +
            '/nRegion: Africa,  Far East,  Middle East,  North America,  South America", Date: 09-09-2014, , , , , , , , , ' +
            '/nKL 0409, leg:    1, AMS, ALA, Days of Operation:    1234567, Category of duration:    07HRS, Time of day departing:    15-17, Catering station:    AMS, , Avg duration in minutes:    395, Flight type:    I-NA-ME' +
            '/nCREW-D0 OB - 3,  30-Mar-2014,  25-Oct-2014, Standard,  ,  ,  ,  ,  ,  ,  ' +
            '/n,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ' +
            '/n,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ' +
            '/nE431-2 - 431CO,  2 - cockpit box ICA,  ,  ,  ,  ,  ,  ,  ,  ,  ' +
            '/nE451-2 - 451CA,  2 - dr.rolls in bulk ICA,  ,  ,  ,  ,  ,  ,  ,  ,  ' +
            '/nE411-2 - 411CW,  1 - hml ICA,  ,  ,  ,  ,  ,  ,  ,  ,  ' +
            '/n,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ' +
            '/nI-NA 07H00 1 - 10,   01-Jul-2014,   25-Oct-2014,  Standard,  ,  ,  ,  ,  ,  ,  ' +
            '/n,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ' +
            '/n,  ,  ,  ,  ,  ,  ,  , , , ' +
            '/nE261 - , Pre take off beverage,  - , Drink Service, , , , , , , ' +
            '/n - , Hot Towel,  - , Garniture, , , , , , , ' +
            '/n - , Drink Service,  - , Hot Towel, , , , , , , ' +
            '/n - , Garniture, E311 - , Hot Meal, , , , , , , ' +
            '/nE212 - , Hot Meal,  - , Wine Service, , , , , , , ' +
            '/n - , Wine Service,  - , Coffee /Liquer service, , , , , , , ' +
            '/nE297 - , Dessert,  - , Sales, , , , , , , ' +
            '/nE210 - , Friandises,  - , Water/tray drinks, , , , , , , ' +
            '/n - , Sales,  - , Hot Towel, , , , , , , ' +
            '/n - , Mineral Water Bottles, E382 - , Hot Snack, , , , , , , ' +
            '/n - , Tray Drinks,  - , Drink Service, , , , , , , ' +
            '/n - , Hot Towel, , , , , , , , , ' +
            '/nE284 - , Cold Snack, , , , , , , , , ' +
            '/n - , Drink Service, , , , , , , , , ' +
            '/n, , , , , , , , , , ' +
            '/nKL 0410, leg:    1, ALA, AMS, Days of Operation:    1234567, Category of duration:    08HRS, Time of day departing:    00-05, Catering station:    ALA, , Avg duration in minutes:    435, Flight type:    I-DA-ALA' +
            '/nCREW-N1 HB - 3,  30-Mar-2014,  25-Oct-2014, Standard, , , , , , , ' +
            '/n, , , , , , , , , , ' +
            '/n, , , , , , , , , , ' +
            '/nE441-2 - 442CW, 1 - hot snack, , , , , , , , , ' +
            '/nE433-4 - 434CW, 2 - crewbox menu, , , , , , , , , ' +
            '/n, , , , , , , , , , ' +
            '/nI-DA 07H00 1 - 3,  30-Mar-2014,  25-Oct-2014, Standard, , , , , , , ' +
            '/n, , , , , , , , , , ' +
            '/n, , , , , , , , , , ' +
            '/nE261 - , Pre take off beverage,  - , Hot Towel, , , , , , , ' +
            '/nE291 - , Bulk, E337 - , Cold Breakfast, , , , , , , ' +
            '/n - , Mineral Water Bottles,  - , Coffee/tea/milk/juice/water, , , , , , , ' +
            '/n - , Hot Towel,  - , Sales, , , , , , , ' +
            '/nE246 - , Hot Snack,  - , Water/tray drinks, , , , , , , ' +
            '/n - , Wine Service,  - , Hot Towel, , , , , , , ' +
            '/n - , Sales, E331 - , Hot Breakfast, , , , , , , ' +
            '/n - , Tray Drinks,  - , Coffee/tea/milk/juice/water, , , , , , , ' +
            '/n - , Hot Towel, , , , , , , , , ' +
            '/n - , Juice Service, , , , , , , , , ' +
            '/nE235 - , Hot Breakfast, , , , , , , , , ' +
            '/n, , , , , , , , , ,';
            
            return lines.split('/n');
        }

    }
}