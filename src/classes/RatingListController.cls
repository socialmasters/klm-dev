/********************************************************************** 
 Name:  RatingListController
 Task:    N/A
 Runs on: RatingListConsole
====================================================== 
Purpose: 
    RatingListController Class to utilize existing List View in Apex with Pagination Support.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     David van 't Hooft  19/01/2015      Initial Development
    2.0     Mees Witteman / Maarten Lapere      Added All open / closed and refactored
    2.1     Mees Witteman       31-07-2015      Fixed SelectOption bug for language queues
*********************************************************************************************/
global with sharing class RatingListController {
	public static final String ALL_OPEN_IGT = 'All Open Cases RYF IGT';
	public static final String ALL_CLOSED_IGT = 'All Closed Cases RYF IGT';
	public static final String ALL_OPEN_SMH = 'All Open Cases SMH';
	public static final String ALL_CLOSED_SMH = 'All Closed Cases SMH';

    public Double totalPageNo;
    public String filterId {get; set;}
    public String newCaseId {get; set;}
    public String newCaseNumber {get; set;}
    public String selectedCaseId {get; set;}
    public String selectedRatingId {get; set;}
    public Boolean selectAll {get; set;}
    public Boolean autoRefresh {get; set;}
    public String fieldValues{get;set;}
    public ApexPages.StandardSetController sRatingListController {get; set;}

    public List<MetadataService.ListView> listViews = null;
    public List<SelectOption> listViewOptions = null;

    private MetaDataParser mdp = new MetaDataParser('Rating__c');
    private Integer pageSizeValue;

    private transient List<RatingWrapper> ratingWrap = new List<RatingWrapper>();

    public RatingListController(){
        this(null);
    }

    public RatingListController(ApexPages.StandardSetController c) {
        this.fieldValues = 'SLA__C DESC, Rating_Number__c ASC';
        this.pageSizeValue = determinePageSize();
        this.sRatingListController = buildStandardSetController();
    }

    private Integer determinePageSize() {
        Integer result = 10;
        if (!Test.isRunningTest()) {
            RatingService__c ratingCustomSettings = RatingService__c.getOrgDefaults();
            result = Integer.valueof(ratingCustomSettings.RatingListSize__c);
        }
        return result;
    }
    
    /**
     * The StandardSetController for the list handling
     */
    public ApexPages.StandardSetController buildStandardSetController() {
        List<Rating__c> rating = new List<Rating__c>();
        String queryFilter = '';
        if (Test.isRunningTest()) {
            if (!String.isEmpty(filterId) && filterId.startsWith('All ')) {
                queryFilter = getAllQueryFilter(filterId);
            }
        } else {
            if (!String.isEmpty(filterId) && filterId.startsWith('All ')) {
                queryFilter = getAllQueryFilter(filterId);
            } else {
                queryFilter = mdp.getQueryFilter(filterId);
                if (''.equals(queryFilter)) {
                    queryFilter = ' WHERE Has_Comments__c = true ';
               } else {
                	queryFilter = queryFilter + ' AND Has_Comments__c = true AND CaseId__c = null ';
                }
                System.debug('queryFilter=' + queryFilter);
            }
        }
        
        readListViews();
        
        buildListViewOptionsList();
        System.debug('filterId=' + filterId);
        String baseQuery = 'Select email__c, allowEmailContact__c, country__c, language__c, Travel_Date__c, SLA__c, Last_Name__c, '
        	+ ' Seat_Number__c, Rating_Number__c, Publish__c, Positive_comments__c, Passenger__c, OwnerId, Negative_comments__c, KLM_Reply__c , '
       		+ ' Name, Moderation_Done__c, Id, Hidden_Seat_Number__c, Has_Comments__c, Hidden_Comments__c, Flight_Number__c, Flight_Info__c, ' 
       		+ ' Flight_Date__c, First_Name__c, CaseId__c, CreatedDate '
            + ' FROM Rating__c '+ queryFilter 
            + ' ORDER BY ' + fieldValues + ', CreatedDate ASC LIMIT 10000';
        System.debug('baseQuery=' + baseQuery);
        
        sRatingListController = new ApexPages.StandardSetController(Database.getQueryLocator(baseQuery));
        sRatingListController.setPageSize(pageSizeValue);
        return sRatingListController;

    }
    
    private String getAllQueryFilter(String filterId) {
    	String closedCaseValues = '(\'Closed\',\'Closed - No Response\',\'Closed with repair\',\'Closed without repair\',\'Closed Customer Care\',\'Closed Feedback only\')';
    	String result = ' WHERE caseId__c <> null ';
		if (ALL_OPEN_IGT.equals(filterId)) {
			result += ' AND (language__c <> \'en\' AND language__c <> \'nl\' AND language__c <> \'\' AND language__c != null) ';
			result += ' AND caseId__r.status not in ' + closedCaseValues + ' ';
		} else if (ALL_OPEN_SMH.equals(filterId)) {
			result += ' AND (language__c = \'en\' OR language__c = \'nl\' OR language__c = \'\' OR language__c = null) ';
			result += ' AND caseId__r.status not in ' + closedCaseValues + ' ';
		} else if (ALL_CLOSED_IGT.equals(filterId)) {
			result += ' AND (language__c <> \'en\' AND language__c <> \'nl\' AND language__c <> \'\' AND language__c != null) ';
			result += ' AND caseId__r.status in ' + closedCaseValues + ' ';
		} else if (ALL_CLOSED_SMH.equals(filterId)) {
			result += ' AND (language__c = \'en\' OR language__c = \'nl\' OR language__c = \'\' OR language__c = null) ';
			result += ' AND caseId__r.status in ' + closedCaseValues + ' ';
		}
    	return result;
    }

    /**
    * Getting the Rating as a wrapper to add the isSelected parameter and display on page.
    *
    * @return List<RatingWrapper> a List of Rating__c wrapped with 'isSelected' field
    */
    public List<RatingWrapper> getRatings() {
        ratingWrap = new List<RatingWrapper>();
        if (sRatingListController == null) {
            sRatingListController = buildStandardSetController();
        }
        for(Rating__c r : (List<Rating__c>) sRatingListController.getRecords()) {
            System.debug('getRatings rating.id=' + r.id);
            System.debug('sRatingListController=' +sRatingListController);
            ratingWrap.add(new RatingWrapper(r, sRatingListController));
        }
        System.debug('ratingWrap=' + ratingWrap);
        return ratingWrap;
    }
   
    /** 
    * Create cases for single/multiple Social Posts when 'Create Case' button is selected
    *
    * @return PageReference to create cases page if selected else return nothing
    */
    public PageReference createCases() {
		this.newCaseId = null;
        Integer MAX_NR_OF_CASES = 1;
        Integer countSelectedRecords = 0;
    	
    	RatingWrapper wrapSelectedRating;
        for(RatingWrapper wrapperRating: ratingWrap){
            if (wrapperRating.isSelected == true) {
                wrapSelectedRating = wrapperRating;
                countSelectedRecords++;
            }
        }   
        
        if (countSelectedRecords == MAX_NR_OF_CASES) {
        	if (wrapSelectedRating.wRating.CaseId__c != null) {
        		ApexPages.addmessage( new ApexPages.message( ApexPages.severity.INFO, 'This rating already has a Case') );
        		return null;
        	}
            if((wrapSelectedRating.wRating.Moderation_Done__c == true)) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The Rating is already "Marked As Done"! ' ) );
                return null;
            }
        	try {
        		LtcFlightInfo fi = new LtcFlightInfo();
        		Rating__c rating = wrapSelectedRating.wRating;
        		Flight__c flight = getFlight(rating);
        		LtcAircraft__c aircraft = fi.getAircraft(flight.registrationCode__c);
        		LtcAircraftType__c acType = fi.getAircraftType(aircraft);
        		
    			String recTypeId = [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id;
    			String fn = rating.First_Name__c;
    			String ln = rating.Last_Name__c;
    			String ratingEmail = rating.Email__c;
    			Boolean optOut = !rating.AllowEmailContact__c;
    			
	            Account newPersonAccount = new Account(
	                FirstName = String.isBlank(fn) ? 'unknown' : fn,
	                LastName = String.isBlank(ln) ? 'unknown' : ln,
	                RecordTypeId = recTypeId,
	            	PersonEmail = ratingEmail,
					PersonHasOptedOutOfEmail = optOut,
					Country_of_Residence__c = rating.country__c
	            );
       			insert newPersonAccount;
        
	            String newPersonAccountId = [select Id from Contact where accountId = :newPersonAccount.Id].Id;
	            
	            recTypeId = [select Id from RecordType where (Name = 'Rating Case') and (SobjectType = 'Case')].Id;
	            Case newCase = new Case(
	            	recordTypeId = recTypeId,
	                accountId = newPersonAccount.Id,
	                contactId = newPersonAccountId,
	                subject = rating.Rating_Number__c + ' rating',
	                ownerId = userInfo.getUserId(),
	                country__c = rating.country__c,
	                language__c = rating.language__c,
	                suppliedName = userInfo.getUserName(),
	                sentiment_at_start_of_case__c = ratingToSentiment(Integer.ValueOf(rating.Rating_Number__c)),
	                sentiment_at_close_of_case__c = ratingToSentiment(Integer.ValueOf(rating.Rating_Number__c)),
	                origin = 'Rate the flight',
	                aircraft_Registration__c = flight.registrationCode__c,
	                aircraft_Type__c = acType.name,
	                destination_Airport__c = flight.legs__r[0].destination__c,
	                Email__c = ratingEmail,
	                last_Name__c = String.isBlank(ln) ? 'unknown' : ln,
	                first_Name__c = String.isBlank(fn) ? 'unknown' : fn,
	                flight_Date__c = rating.Travel_Date__c,
	                flight_Number__c = flight.flight_number__c,
	                flight_Prefix__c = flight.flight_number__c.substring(0, 2),
	                origin_Airport__c = flight.legs__r[0].origin__c,
	                seat_Nr__c = rating.Seat_Number__c,
	            	rating__c = rating.id,
	           		klm_com_link__c = RatingCaseFeedController.getAllRatingsUrl(rating)
	            );
	            insert newCase;
	            
	            rating.CaseId__c = newcase.Id;
	            update rating;

				String pos = 'pos: ' + (rating.Positive_comments__c == null ? '' : rating.Positive_comments__c);
	            String neg = 'neg: '+ (rating.Negative_comments__c == null ? '' : rating.Negative_comments__c);
	            			            
				FeedItem feedItem = new FeedItem(parentId = newCase.Id);
				feedItem.body = pos;
				insert feedItem;
				
				feedItem = new FeedItem(parentId = newCase.Id);
				feedItem.body = neg;
				insert feedItem;
				
	            this.newCaseId = newcase.Id;

    			
            } catch (Exception e){

                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
                System.debug(LoggingLevel.ERROR, e);
            }
        } else if (countSelectedRecords > MAX_NR_OF_CASES) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, '' + countSelectedRecords + ' records selected, please select only one for engage'));
        }
        
        return null;
    }

	private Flight__c getFlight(Rating__c rating) {
		Flight__c flight = null;
		List<Flight__c> flights = [
			SELECT f.registrationCode__c, f.operatingFlightCode__c, f.Scheduled_Departure_Date__c, f.currentLeg__c, f.Flight_Number__c,
				(SELECT origin__c, status__c, destination__c, legNumber__c, timeToArrival__c, scheduledArrivalDate__c, 
					scheduledArrivalTime__c, actualArrivalTime__c, estimatedArrivalTime__c, arrivalDelay__c, scheduledDepartureDate__c, 
					scheduledDepartureTime__c, actualDepartureTime__c, estimatedDepartureTime__c, timeToDeparture__c, departureDelay__c, 
					actualArrivalDate__c, actualDepartureDate__c, arrivalDate__c, departureDate__c, estimatedArrivalDate__c, 
					estimatedDepartureDate__c, Flight__c 
			  	FROM Legs__r) 
			FROM Flight__c f
			WHERE f.Id =: rating.Flight_Info__c
		];


		// only one 
		if (flights.size() > 0) {
			flight = flights[0];
		}
		return flight;
	}

	private String ratingToSentiment(Integer rating) {
		if (rating == 5) {
			return 'Very Positive';
		} else if (rating == 4) {
			return 'Positive';
		} else if (rating == 3) {
			return 'Neutral';
		} else if (rating == 2) {
			return 'Negative';
		} else if (rating == 1) {
			return 'Very Negative';
		} else {
			return '' + rating;
		}
	}

   /** 
    * Update the field MarkAsReviewed__c to 'true'
    */
    public void markAsReviewed() {
        RatingControllerHelper controllerHelper = new RatingControllerHelper(); 
        controllerHelper.markReviewed(ratingWrap);
        ratingWrap = null;
        sRatingListController = null;
        controllerHelper = null;
    }

    /** 
     * Get the standard Social Post List Views options
     *
     * @return SelectOption[] of List Views
     */
    public SelectOption[] getRatingExistingViews() {
        return listViewOptions;
    }

    /** 
     * Refresh the Social Post View when 'refresh' is selected
     */
    public void refreshRatingView() {
        selectAll = false;
        sRatingListController = null;
        if (ratingWrap != null) {
            ratingWrap.clear();
        }
    }
    
    public List<SelectOption> getFieldItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('SLA__C DESC, CreatedDate ASC','Action State'));
        options.add(new SelectOption('CreatedDate ASC','Create date'));
        options.add(new SelectOption('Rating_Number__c ASC','Rating'));
        options.add(new SelectOption('Flight_Number__c ASC','Flight Number'));
        return options;
    }
    
    /**
    * Reset List View
    *
    * @return null
    */
    public void resetFilter() {
        selectAll = false;
        sRatingListController = null;
    }
    
    /**
    * Navigate to the First Page
    */
    public void firstPage() {
        selectAll = false;
        sRatingListController.first();
    }

    /**
    * Navigate to the First Page
    */
    public void lastPage() {
        selectAll = false;
        sRatingListController.last();
    }

    /**
    * Navigate to the Next Page
    */
    public void next() {
        selectAll = false;
        if(sRatingListController.getHasNext()) {
            sRatingListController.next();
        }
    }

    /**
    * Navigate to the Previous Page
    */
    public void prev() {
        selectAll = false;
        if(sRatingListController.getHasPrevious()) {
            sRatingListController.previous();
        }
    }
    
    /**
    * Get the total number of pages in pagination
    */
    public Integer getTotalPageNo() {
        totalPageNo = Double.valueOf(sRatingListController.getResultSize()) / Double.valueOf(pageSizeValue);
        Integer totalPageNum;
        Integer tempTotalPageNo = Integer.valueOf(totalPageNo);
        
        if(totalPageNo > tempTotalPageNo){
            totalPageNum = tempTotalPageNo + 1;
        } else {
            totalPageNum = tempTotalPageNo;
        }
        
        if(totalPageNo <= 1){
            totalPageNum = 1;
        }
        
        return totalPageNum;
    }

    public Integer getSize() {
        return pageSizeValue;
    }

    public void setSize(Integer size) {
        this.pageSizeValue = pageSizeValue;
    }
    
    /**
     * read the SocialPost custom object to read the listview metadata
     * only do this ones duuring its livecycle 
     **/
    public void readListViews() {
        if(!Test.isRunningTest()) {
            this.listViews =  mdp.readListViews();
        }
    }   
    
    /**
     * Get All non private listViews and sort them by label and not API fullName
     */
    public void buildListViewOptionsList() {
        listViewOptions = new List<SelectOption>();
        
        // Cannot execute Websrv callout on Test classes
        if (Test.isRunningTest()) {
            listViewOptions.add(new SelectOption('Test View Label', 'Test View FullName'));
        } else {
            for (MetadataService.ListView listView : this.listViews) {
                //System.debug(LoggingLevel.INFO, 'listView.label=' + listView.label + ' listView.fullName' + listView.fullName);
                if (!listView.label.startsWith('My')) {
                    listViewOptions.add(new SelectOption(listView.fullName, listView.label));
                }
            }
            listViewOptions.sort();
            listViewOptions.add(new SelectOption(ALL_OPEN_IGT, ALL_OPEN_IGT));
            listViewOptions.add(new SelectOption(ALL_OPEN_SMH, ALL_OPEN_SMH));
            listViewOptions.add(new SelectOption(ALL_CLOSED_IGT, ALL_CLOSED_IGT));
            listViewOptions.add(new SelectOption(ALL_CLOSED_SMH , ALL_CLOSED_SMH));
        }
        
        this.filterId = listViewOptions[0].getValue();
    }
}