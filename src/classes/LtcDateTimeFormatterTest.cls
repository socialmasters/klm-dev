/**
 * @author (s)      : David van 't Hooft
 * @description     : Date formatter Test class
 * @log:   8MAY2014: version 1.0
 *
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LtcDateTimeFormatterTest {

    /**
     * Test the date time formatter by locale
     **/
    static testMethod void dateTimeFormatterUnitTest() {
        LtcDateTimeFormatter ltcDateTimeFormatter = new LtcDateTimeFormatter();
        
        DateTime dt = DateTime.newInstance(2014, 8, 5, 12, 30, 10);
        String formattedDateTime = ltcDateTimeFormatter.formatDateTime(dt, 'en-GB');
        system.assertEquals('05/08/2014 12:30', formattedDateTime);
        formattedDateTime = ltcDateTimeFormatter.formatDateTime(dt, 'nl-NL');
        system.assertEquals('5-8-2014 12:30', formattedDateTime);
        formattedDateTime = ltcDateTimeFormatter.formatDateTime(dt, 'lt');
        system.assertEquals('2014.8.5 12.30', formattedDateTime);
        formattedDateTime = ltcDateTimeFormatter.formatDateTime(dt, '');
        system.assertEquals('05/08/2014 12:30', formattedDateTime);        
    }
    
    /**
     * Test the date formatter by locale
     **/
    static testMethod void dateFormatterUnitTest() {
        LtcDateTimeFormatter ltcDateTimeFormatter = new LtcDateTimeFormatter();
        
        Date d = Date.newInstance(2014, 8, 5);
        String formattedDate = ltcDateTimeFormatter.formatDate(d, 'en-GB');
        system.assertEquals('05/08/2014', formattedDate);
        formattedDate = ltcDateTimeFormatter.formatDate(d, 'nl-NL');
        system.assertEquals('5-8-2014', formattedDate);
        formattedDate = ltcDateTimeFormatter.formatDate(d, 'nl-UNKNOWN'); //default to nl
        system.assertEquals('5-8-2014', formattedDate);
        formattedDate = ltcDateTimeFormatter.formatDate(d, 'zz-UNKNOWN'); // default to en
        system.assertEquals('05/08/2014', formattedDate);
        formattedDate = ltcDateTimeFormatter.formatDate(d, null);
        system.assertEquals('05/08/2014', formattedDate);
    }
    
}