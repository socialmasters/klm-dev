/**							
 * @author      : Mees Witteman
 * @description : Class to process the static resource LtcServiceSchema (service schema summer / winter)
 *				  and create a ServicePeriod with ServiceItems, LegClassPeriods and LegClassItems.
 *
 *				  If the name of the resource contains 'EUR' we initialize the column- and skipnumbers 
 * 				  for reading the file conform the needs of the EUR file.
 *
 * LtcServiceSchemaProcessor processor = new LtcServiceSchemaProcessor();
 * processor.processServiceSchema();
 *                
 * If you run into a governer limit, break the process in two parts:
 * LtcServiceSchemaProcessor processor = new LtcServiceSchemaProcessor();
 * processor.createItemsAndPeriods();
 *
 * LtcServiceSchemaProcessor processor = new LtcServiceSchemaProcessor();
 * processor.createLegClassItems();
 *                
 */
public with sharing virtual class LtcServiceSchemaProcessor {
	
	// the period to handle
	public ServicePeriod__c servicePeriod;
	
	// existing items
	public Map<String, ServiceItem__c> serviceItems = new Map<String, ServiceItem__c>();
	public CabinClass__c economyClass;
	public CabinClass__c businessClass;
	
	public List<LegClassPeriod__c> legClassPeriods = new List<LegClassPeriod__c>();
	public List<LegClassItem__c> legClassItems = new List<LegClassItem__c>();
	
	// defaults for non EUR file               // EUR file values
	public Integer flightNrColumn = 0;         //  0
	public Integer legNrColumn = 1;            //  3
	public Integer businessItemCodeColumn = 0; //  1
	public Integer businessItemColumn = 1;     //  6 
	public Integer economyItemCodeColumn = 2;  // 12 
	public Integer economyItemColumn = 3;      // 17 
	public Integer skipLinesForPeriod = 1;     //  2 
	public Integer startDateColumn = 1;        //  7
	public Integer endDateColumn = 2;          // 12 
	public String itemName;
	public String itemCode;
	public String type;
	
	// run both iteration methods at once
	public void processServiceSchema() {
		createItemsAndPeriods();
		createLegClassItems();
	}

	// first iteration to insert all ServiceItems and LegClassPeriods
	public void createItemsAndPeriods() {
		initialize();		
		loadCurrentRecords();

		String[] lines = getLinesFromResource();
		String[] record;
		String[] nextRecord;
		LegClassPeriod__c economyClassPeriod;
		LegClassPeriod__c businessClassPeriod;
		ServiceItem__c serviceItem;
		LegClassItem__c legClassItem;
		Integer sortOrder = 0;
		Boolean reachedServiceItems = false; // only after line starting with I / EXL / ES / EL / EM the si's will follow
		for (Integer i = 3; i < lines.size(); i++) {
			System.debug(Logginglevel.INFO, 'process line ' + lines[i]);
			record = lines[i].split(',');
			
			if (record[flightNrColumn].startswith('KL')) {
				System.debug(Logginglevel.INFO, 'KL line: new flight / leg' + lines[i]);
				// new leg, nextline after skipLinesForPeriod contains the dates for the leg period
				i = i + skipLinesForPeriod;
				System.debug(Logginglevel.INFO, 'dates for leg period ' + lines[i]);
				nextRecord = lines[i].split(',');
				businessClassPeriod = createLegClassPeriod(record, nextRecord, businessClass);
				legClassPeriods.add(businessClassPeriod);
				economyClassPeriod = createLegClassPeriod(record, nextRecord, economyClass);
				legClassPeriods.add(economyClassPeriod);
				if (nextRecord[0].startsWith('I-T')) {
					reachedServiceItems = true;
				} else {
					reachedServiceItems = false;
				}
			} 
			else if (record[flightNrColumn].startsWith('I') 
					|| record[flightNrColumn].startsWith('EXL')
					|| record[flightNrColumn].startsWith('EL')
					|| record[flightNrColumn].startsWith('EM')
					|| record[flightNrColumn].startsWith('ES')) {
				System.debug(Logginglevel.INFO, 'I / EXL / ES / EL / EM record, start service items ' + lines[i]);
				// after this line si's will follow
				reachedServiceItems = true;
			}
			else if (reachedServiceItems && isServiceLine(record)) {
				System.debug(Logginglevel.INFO, 'process service record ' + lines[i]);
				itemcode = stripCode(record[businessItemCodeColumn]);
				itemName = stripItemName(record[businessItemColumn]);
				type = getTypeFromName(itemName);
				itemName = (!itemCode.equals('') && 'meal'.equals(type)) ? itemCode : itemName;
				if (isValidItem(itemName)) {
					serviceItem = createServiceItem(itemName, type);
					serviceItems.put(serviceItem.name__c, serviceItem);
				} 
				itemcode = stripCode(record[economyItemCodeColumn]);
				itemName = stripItemName(record[economyItemColumn]);
				type = getTypeFromName(itemName);
				itemName = (!itemCode.equals('') && 'meal'.equals(type)) ? itemCode : itemName;
				if (isValidItem(itemName)) {
					serviceItem = createServiceItem(itemName, type);
					serviceItems.put(serviceItem.name__c, serviceItem);
				}
			} else {
				System.debug(Logginglevel.INFO, 'skipped line ' + lines[i]);
			}
			if (legClassPeriods.size() > 100) {
				upsert legClassPeriods;
				legClassPeriods = new List<LegClassPeriod__c>();
			}
		}
		upsert serviceItems.values();
		upsert legClassPeriods;
	}
	
	// second iteration to insert all LegClassItem objects
	public void createLegClassItems() {
		initialize();
		loadCurrentRecords();
		String[] lines = getLinesFromResource();
		String[] record;
		String[] nextRecord;
		LegClassPeriod__c economyClassPeriod;
		LegClassPeriod__c businessClassPeriod;
		ServiceItem__c serviceItem;
		LegClassItem__c legClassItem;
		Boolean reachedServiceItems = false;
		Integer sortOrder = 0;
		
		for (Integer i = 3; i < lines.size(); i++) {
			System.debug(Logginglevel.INFO, 'process line' + lines[i]);
			record = lines[i].split(',');
			if (record[flightNrColumn].startswith('KL')) {
				sortOrder = 0;
				// new leg, nextline contains the dates for the period
				i = i + skipLinesForPeriod;
				nextRecord = lines[i].split(',');
				String flightNumber = record[flightNrColumn].replaceAll(' ', '');
				Integer legNr = Integer.valueOf(record[legNrColumn].replaceAll('leg:', '').trim()) - 1;
				Date startDate = parseCsvDate(nextRecord[startDateColumn]);
				Date endDate = parseCsvDate(nextRecord[endDateColumn]);
				economyClassPeriod = findLegClassPeriod(flightNumber ,legNr, startDate, endDate, economyClass);
				businessClassPeriod = findLegClassPeriod(flightNumber ,legNr, startDate, endDate, businessClass);
				if (nextRecord[0].startsWith('I-T')) {
					reachedServiceItems = true;
				} else {
					reachedServiceItems = false;
				}
			} 
			else if (record[flightNrColumn].startsWith('I') 
					|| record[flightNrColumn].startsWith('EXL')
					|| record[flightNrColumn].startsWith('EL')
					|| record[flightNrColumn].startsWith('EM')
					|| record[flightNrColumn].startsWith('ES')) {
				System.debug(Logginglevel.INFO, 'I / EXL / ES / EL / EM record, start service items ' + lines[i]);
				reachedServiceItems = true;
			}
			else if (reachedServiceItems && isServiceLine(record)) {
				itemcode = stripCode(record[businessItemCodeColumn]);
				itemName = stripItemName(record[businessItemColumn]);
				type = getTypeFromName(itemName);
				itemName = 'meal'.equals(type) ? itemCode : itemName;
				
				if (isValidItem(itemName)) {
					legClassItem = createLegClassItem(itemName, businessClassPeriod, sortOrder);
					legClassItems.add(legClassItem);
				}
				itemcode = stripCode(record[economyItemCodeColumn]);
				itemName = stripItemName(record[economyItemColumn]);
				type = getTypeFromName(itemName);
				itemName = 'meal'.equals(type) ? itemCode : itemName;
				if (isValidItem(itemName)) {
					legClassItem = createLegClassItem(itemName, economyClassPeriod, sortOrder);
					legClassItems.add(legClassItem);
				}
				sortOrder = sortOrder + 1;
			} 
			if (legClassItems.size() > 100) {
				upsert legClassItems;
				legClassItems = new List<LegClassItem__c>();
			}
		}
		upsert legClassItems;
	}
	
	private Boolean isServiceLine(String[] record) {
		Boolean result = false;
		if (record.size() >= economyItemCodeColumn) {
			if (record[businessItemCodeColumn].startsWith('E') 
				|| record[businessItemCodeColumn].trim().startsWith('-') // - 
				|| record[economyItemCodeColumn].startsWith('E')
				|| record[economyItemCodeColumn].trim().startsWith('-')) {
				result = true;
			}
			//System.debug(LoggingLevel.DEBUG, record[businessItemCodeColumn] + '-' + record[economyItemCodeColumn] + ' isServiceLine=' + result);
		}
		return result;
	}
	
	private Boolean isValidItem(String itemName) {
		Boolean result = true;
		if (itemName == null) {
			result = false;
		} 
		else if (''.equals(itemName)) {
			result = false;
		}
		else if (itemName.toUppercase().contains('GARNITURE')) {
			result = false;
		}
		else if (itemName.toUppercase().contains('NOSERVICE')) {
			result = false;
		}
		else if (itemName.toUppercase().contains('WINESERVICE')) {
			result = false;
		}
		else if (itemName.toUppercase().contains('BONBONS')) {
			result = false;
		}
		else if (itemName.toUppercase().contains('FRIANDIS')) {
			result = false;
		}
		else if (itemName.toUppercase().contains('VIENNOIS')) {
			result = false;
		}
		else if (itemName.toUppercase().contains('HOTROLL')) {
			result = false;
		}
		else if (itemName.toUppercase().equals('BULK')) {
			result = false;
		}
		else if (itemName.toUppercase().contains('COFFEE')) {
			result = false;
		}
		else if (itemName.toUppercase().startsWith('DESSERT')) {
			result = false;
		}
		else if (itemName.toUppercase().contains('E210')) {
			result = false;
		}
		else if (itemName.toUppercase().contains('E295')) {
			result = false;
		}
		else if (itemName.toUppercase().contains('E297')) {
			result = false;
		}
		return result;
	}
	
	private LegClassPeriod__c createLegClassPeriod(String[] record, String[] nextRecord, CabinClass__c cabinClass) {
		String flightNumber = record[flightNrColumn].replaceAll(' ', '');
		Integer legNr = Integer.valueOf(record[legNrColumn].replaceAll('leg:', '').trim()) - 1;
		Date startDate = parseCsvDate(nextRecord[startDateColumn]);
		Date endDate = parseCsvDate(nextRecord[endDateColumn]);
		LegClassPeriod__c lcp = findLegClassPeriod(flightNumber, legNr, startDate, endDate, cabinClass);
		if (lcp == null) {
			lcp = new LegClassPeriod__c();
			lcp.CabinClass__c = cabinClass.id;
			lcp.FlightNumber__c = flightNumber;
			lcp.legNumber__c = legNr;
			lcp.StartDate__c = startDate;
			lcp.EndDate__c = endDate;
			lcp.ServicePeriod__c = servicePeriod.id;
		}
		System.debug(Logginglevel.INFO, 'leg/class/period ' + lcp.flightnumber__c + ' ' + lcp.legnumber__c + ' ' + cabinClass.name + ' ' + lcp.startDate__c  + ' ' + lcp.endDate__c);
		return lcp;
	}
	
	private LegClassPeriod__c findLegClassPeriod(String flightNumber, Integer legNr, Date startDate, Date endDate,  CabinClass__c cabinClass) {
		for (LegClassPeriod__c lcp : legClassPeriods) {
			if (lcp.cabinClass__c.equals(cabinClass.id)
					&& lcp.flightNumber__c.equals(flightNumber)
					&& lcp.legNumber__c == legNr
					&& lcp.StartDate__c.isSameDay(startDate)
					&& lcp.EndDate__c.isSameDay(endDate)) {
				return lcp;
			}
		}
		return null;
	}		
	
	private ServiceItem__c createServiceItem(String serviceItemName, String type) {
		ServiceItem__c si = findServiceItem(serviceItemName);
		if (si == null) {
			si = new ServiceItem__c();
			si.name__c = 'ServiceItem_' + serviceItemName + '_name';
			si.description__c = 'ServiceItem_' + serviceItemName + '_description';
			// log the label translation sheet entries for new created service items
			System.debug(Logginglevel.INFO, 'si:\t\t{');
			System.debug(Logginglevel.INFO, 'si:\t\t\t"@key":"klmmj.' + si.name__c + '",');
			System.debug(Logginglevel.INFO, 'si:\t\t\t"#text":"' + serviceItemName + '"');
			System.debug(Logginglevel.INFO, 'si:\t\t},');
			System.debug(Logginglevel.INFO, 'si:\t\t{');
			System.debug(Logginglevel.INFO, 'si:\t\t\t"@key":"klmmj.' + si.description__c + '",');
			System.debug(Logginglevel.INFO, 'si:\t\t\t"#text":"' + serviceItemName + '"');
			System.debug(Logginglevel.INFO, 'si:\t\t},');
			
		} 
		si.type__c = type;
		return si;
	}
	
	private ServiceItem__c findServiceItem(String serviceItemName) {
		String siName = 'ServiceItem_' + serviceItemName + '_name';
		ServiceItem__c si = serviceItems.get(siName);
		return si;
	}
		
	private LegClassItem__c createLegClassItem(String serviceItemName, LegClassPeriod__c legClassPeriod, Integer order) {
		ServiceItem__c si = findServiceItem(serviceItemName);
		LegClassItem__c lci = new LegClassItem__c();
		lci.LegClassPeriod__c = legClassPeriod.id;
		lci.serviceItem__c = si.id;
		lci.order__c = order;
		return lci;
	}

 	private String stripItemName(String input) {
 		if (input == null) return '';
 		String output = input.trim();
 		if (output.contains('-')) {
 			output = output.substring(output.indexOf('-') + 1).trim();
 		}
 		output = output.replaceAll(' ', '');
 		output = output.replaceAll('/', '-');
 		output = output.replaceAll('%', '');
 		output = output.toLowerCase();
 		return output;
 	}
 	
 	private String stripCode(String input) {
 		if (input == null) return '';
 		String output = input.trim();
 		output = output.replaceAll(' ', '');
 		if ('-'.equals(output)) {
 			output = '';
 		}
 		if (output.indexOf('-') != -1) {
 			output = output.subString(0, output.indexOf('-'));
 		}
 		output = output.toLowerCase(); 
 		return output;
 	}
 	
 	private String getTypeFromName(String serviceItemName) {
 		String result = 'other';
 		if (serviceItemName.contains('drink')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('drk')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('meal')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('tidbi')) {
 			result = 'tidbits';
 		} else if (serviceItemName.contains('water')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('juice')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('bonbon')) {
 			result = 'snack';
 		} else if (serviceItemName.contains('mnml')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('dinner')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('lun')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('wine')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('beverage')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('juice')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('coffee')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('towel')) {
 			result = 'towel';
 		} else if (serviceItemName.contains('icecream')) {
 			result = 'icecream';
 		} else if (serviceItemName.contains('ice cream')) {
 			result = 'icecream';
 		} else if (serviceItemName.contains('breakfast')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('bkf')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('dess')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('noodle')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('friandise')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('garniture')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('combi')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('yours')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('sand')) {
 			result = 'snack';
 		} else if (serviceItemName.contains('cookie')) {
 			result = 'snack';
 		} else if (serviceItemName.contains('snack')) {
 			result = 'snack';
 		} else if (serviceItemName.contains('sale')) {
 			result = 'sales';
 		} else {
 			result = 'other';
 		}
 		System.debug(LoggingLevel.INFO, 'name=' + serviceItemName + ' type=' + result);
 		return result;
 
 	}
 	
	// convert dd-MMM-yyyy to yyyy-MM-dd HH:mm:ss format
	private Date parseCsvDate(String inputDate) {
		inputDate = inputDate.trim();
		String outputDate = inputDate.substring(7,11);
		outputDate = outputDate + '-';
		outputDate = outputDate + getMonthNumber(inputDate.substring(3,6));
		outputDate = outputDate + '-';
		outputDate = outputDate + inputDate.substring(0,2);
		outputDate = outputDate + ' 12:00:00';
		return Date.valueOf(outputDate);
	}
	
	private String getMonthNumber(String monthName) {
		String result = '01';
		if ('Feb'.equals(monthName)) {
			result = '02';
		} else if ('Mar'.equals(monthName)) {
			result = '03';
		} else if ('Apr'.equals(monthName)) {
			result = '04';
		} else if ('May'.equals(monthName)) {
			result = '05';
		} else if ('Jun'.equals(monthName)) {
			result = '06';
		} else if ('Jul'.equals(monthName)) {
			result = '07';
		} else if ('Aug'.equals(monthName)) {
			result = '08';
		} else if ('Sep'.equals(monthName)) {
			result = '09';
		} else if ('Oct'.equals(monthName)) {
			result = '10';
		} else if ('Nov'.equals(monthName)) {
			result = '11';
		} else if ('Dec'.equals(monthName)) {
			result = '12';
		}
		return result;
	}

  	private virtual String[] getLinesFromResource() {
  		StaticResource sr = [
			Select  s.Name, s.Id, s.Body 
			From StaticResource s  
			where name =: 'LTCServiceSchedule'
		];
		blob tempBlob = sr.Body;
		String tempString = tempBlob.toString();
		tempString = tempSTring.replace('"', '');
		String[] lines = tempString.split('\n');
		return lines;
  	}
  	
	private void loadCurrentRecords() {
		economyClass = [
			Select Name, Id, label__c
			From CabinClass__c	
			where name =: 'economy'
		];
		businessClass= [
			Select Name, Id, label__c
			From CabinClass__c 
			where name =: 'business'
		];
		
		List<ServiceItem__c> sis = [select id, name__c, description__c from ServiceItem__c limit 5000];
		for (ServiceItem__c si : sis) {
			serviceItems.put(si.name__c, si);
		}
		
		legClassPeriods = [
			select id, CabinClass__c, EndDate__c, FlightNumber__c, LegNumber__c, ServicePeriod__c, StartDate__c
			from LegClassPeriod__c
			where ServicePeriod__c =: servicePeriod.id
		];

	}
	
	private virtual Boolean isEuroSchema() {
		StaticResource sr = [
			Select Name, Id, Description 
			From StaticResource 
			where name =: 'LTCServiceSchedule'
		];
		return sr.description.contains('EUR');
	}
	
	private void initialize() {
		StaticResource sr = [
			Select Name, Id, Description 
			From StaticResource 
			where name =: 'LTCServiceSchedule'
		];
		
		List<ServicePeriod__c> sps = [
			Select Id, name__c
			From ServicePeriod__c
			where name__c =: sr.description
		];
		
		if (sps != null && !sps.isEmpty()) {
			servicePeriod = sps[0];
        } 
        else {
        	servicePeriod = new ServicePeriod__c();
			servicePeriod.name__c = sr.description;
			insert servicePeriod;
        }
		
		if (isEuroSchema()) {
			System.debug(Logginglevel.INFO, 'use settings for the EUR schedule');
			flightNrColumn = 0;
			legNrColumn = 3;
			businessItemCodeColumn = 1;
			businessItemColumn = 6;
			economyItemCodeColumn = 12;
			economyItemColumn = 17;
			skipLinesForPeriod = 2;
			startDateColumn = 7;
			endDateColumn = 12;
		} 
		
	}
}