/********************************************************************** 
 Name:  AFKLM_SCS_SocialPostWrapperClsTest
 Task:    N/A
 Runs on: N/A
====================================================== 
Purpose: 
    This class contains unit tests for validating the behavior of Apex classes and triggers. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      05/02/2014      Initial Development
***********************************************************************/
@isTest
private class AFKLM_SCS_SocialPostWrapperClsTest {

	// Test empty controller
	static testMethod void myControllerTest() {
		Test.startTest();
		AFKLM_SCS_SocialPostWrapperCls tstWrapperController = new AFKLM_SCS_SocialPostWrapperCls(null);
		Test.stopTest();
		
		System.assertEquals(tstWrapperController.cSocialPost, null);
		System.assertEquals(tstWrapperController.isSelected, null);
		System.assertEquals(tstWrapperController.scsSocialPostController, null);
		System.assertEquals(tstWrapperController.socialPostController, null);
	}
	
	// Test negative date sort
    static testMethod void myNegativeDateSortTest() {
		List<SocialPost> socialPostList = new List<SocialPost>();
		socialPostList = getSocialPostList(socialPostList, 'substract');
		insert socialPostList;
		
		Test.startTest();
		wrapperInitiateAndSort(socialPostList);
		Test.stopTest();
		
		System.assert(socialPostList[0].Posted > socialPostList[1].Posted,'\n\nWARNING: Posted date from SocialPost 1 is not later than posted date from SocialPost 2');
    }

	// Test positive date sort
    static testMethod void myPositiveDateSortTest() {
		List<SocialPost> socialPostList = new List<SocialPost>();
		socialPostList = getSocialPostList(socialPostList, '');
		insert socialPostList;
		
		Test.startTest();
		wrapperInitiateAndSort(socialPostList);
		Test.stopTest();
		
		System.assert(socialPostList[0].Posted < socialPostList[1].Posted,'\n\nWARNING: Posted date from SocialPost 1 is later than posted date from SocialPost 2');
    }

	// Helper test method for initialize objects and sort
	private static void wrapperInitiateAndSort(List<SocialPost> socialPostList) {
		ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(socialPostList);
		List<AFKLM_SCS_SocialPostWrapperCls> socialPostWrapper = new List<AFKLM_SCS_SocialPostWrapperCls>();
		
        for(SocialPost socialPost : socialPostList) {
			socialPostWrapper.add(new AFKLM_SCS_SocialPostWrapperCls(socialPost, ssc));
        }
		socialPostWrapper.sort();
	}

	// Helper test method for initialize and insert SocialPost objects
	private static List<SocialPost> getSocialPostList(List<SocialPost> socialPostList, String operator) {
 		Datetime postedDate = null;
 		
 		for (Integer x=0; x < 5; x++){
			if(operator == 'substract') {
 				postedDate = Datetime.now()-x;
 			} else {
 				postedDate = Datetime.now()+x;
 			}
 			
			SocialPost tstSocialPost = new SocialPost(
										Name = 'Test '+x,
										Posted = postedDate
									);
			socialPostList.add(tstSocialPost);
		}
		return socialPostList;
	}
}