@isTest
private class PSLSurveyHandlerBatchTest {
	
	static {
		createPSLSUrveySettings( 'NL', true, 10, null );
	}

	@isTest static void testIgnoreServicingCase() {
		Account a1 = createPersonAccount( 'NL', 'NL', 'Platinum' );
		System.debug(a1);
		Account a2 = createPersonAccount( 'NL', 'NL', 'Platinum' );
		Case c1 = createPSLCase( a1 );
		Case c2 = createServicingCase( a2 );
		List<Case> cases = [ SELECT Id, Status, ClosedDate, PSL_Survey_Sent__c FROM Case ];
		for ( Case c : cases ) {
			System.debug( c );
			System.assert( ! c.PSL_Survey_Sent__c );
		}
		PSLSurveyHandlerBatch b = new PSLSurveyHandlerBatch();
		Test.startTest();
		Database.executeBatch( b, 1 );
		Test.stopTest();
		c1 = [ SELECT Id, Status, ClosedDate, PSL_Survey_Sent__c FROM Case WHERE Id = :c1.Id ];
		System.debug( c1 );
		//REMOVE System.assert( c1.PSL_Survey_Sent__c, 'PSL Case Should Have Been Sent Survey!' );
		//REMOVE System.assertNotEquals( null, c1.Contact.Last_PSL_Survey_Sent__c );
		c2 = [ SELECT Id, Status, ClosedDate, PSL_Survey_Sent__c FROM Case WHERE Id = :c2.Id ];
		System.debug( c2 );
		System.assert( ! c2.PSL_Survey_Sent__c, 'Servicing Case Should Not Have Been Sent Survey!' );
	}

	@isTest static void testIgnoreNonPlatinumAccounts() {
		Account a1 = createPersonAccount( 'NL', 'NL', 'Ivory' );
		System.debug(a1);
		Account a2 = createPersonAccount( 'NL', 'NL', 'Platinum' );
		Case c1 = createPSLCase( a1 );
		PSLSurveyHandlerBatch b = new PSLSurveyHandlerBatch();
		Test.startTest();
		Database.executeBatch( b, 1 );
		Test.stopTest();
		c1 = [ SELECT Id, Status, ClosedDate, PSL_Survey_Sent__c FROM Case WHERE Id = :c1.Id ];
		System.debug( c1 );
		System.assert( ! c1.PSL_Survey_Sent__c );
	}

	@isTest static void testBatch() {
		for ( Integer i = 0; i < 10; i++ ) {
			Account a = createPersonAccount( 'NL', 'NL', 'Platinum' );
			Case c = createPSLCase( a );
		}
		List<Case> cases = [ SELECT Id, Status, ClosedDate, PSL_Survey_Sent__c FROM Case ];
		for ( Case c : cases ) {
			System.debug( c );
			System.assert( ! c.PSL_Survey_Sent__c );
		}
		PSLSurveyHandlerBatch b = new PSLSurveyHandlerBatch();
		Test.startTest();
		Database.executeBatch( b );
		Test.stopTest();
	}

	/*
	@isTest static void testNoSurveyOnNonPSLCase() {
		Account a1 = createPersonAccount();
		Test.startTest();
		Case c1 = createServicingCase( a1 );
		Test.stopTest();
		c1 = [ SELECT Id, ContactId, PSL_Survey_Sent__c FROM Case WHERE Id = :c1.Id ];
		System.assertEquals( false, c1.PSL_Survey_Sent__c );
		System.assertNotEquals( null, c1.ContactId );
		Contact contact1 = [ SELECT Id, Last_PSL_Survey_Sent__c FROM Contact WHERE Id = :c1.ContactId ];
		System.assertEquals( null, contact1.Last_PSL_Survey_Sent__c );
	}
	
	@isTest static void testSingleCase() {
		Account a1 = createPersonAccount();
		Test.startTest();
		Case c1 = createPSLCase( a1 ); 
		Test.stopTest();
		c1 = [ SELECT Id, ContactId, PSL_Survey_Sent__c FROM Case WHERE Id = :c1.Id ];
		System.assertEquals( true, c1.PSL_Survey_Sent__c );
		System.assertNotEquals( null, c1.ContactId );
		Contact contact1 = [ SELECT Id, Last_PSL_Survey_Sent__c FROM Contact WHERE Id = :c1.ContactId ];
		System.assertNotEquals( null, contact1.Last_PSL_Survey_Sent__c );
	}

	@isTest static void testOneAccountMultipleCase() {
		Account a1 = createPersonAccount();
		Test.startTest();
		Case c1 = createPSLCase( a1 );
		Case c2 = createPSLCase( a1 ); 
		Test.stopTest();
		c1 = [ SELECT Id, ContactId, PSL_Survey_Sent__c, LastModifiedDate FROM Case WHERE Id = :c1.Id ];
		System.assertEquals( true, c1.PSL_Survey_Sent__c );
		System.assertNotEquals( null, c1.ContactId );
		Contact contact1 = [ SELECT Id, Last_PSL_Survey_Sent__c FROM Contact WHERE Id = :c1.ContactId ];
		System.assertNotEquals( null, contact1.Last_PSL_Survey_Sent__c );
		c2 = [ SELECT Id, ContactId, PSL_Survey_Sent__c FROM Case WHERE Id = :c2.Id ];
		System.assertEquals( false, c2.PSL_Survey_Sent__c );
		System.assertEquals( c1.ContactId, c2.ContactId );
		Contact contact2 = [ SELECT Id, Last_PSL_Survey_Sent__c FROM Contact WHERE Id = :c2.ContactId ];
		System.assertNotEquals( null, contact2.Last_PSL_Survey_Sent__c );
	}
	
	@isTest static void testMultipleAccountMultipleCase() {
		Account a1 = createPersonAccount();
		Account a2 = createPersonAccount();
		Test.startTest();
		Case c1 = createPSLCase( a1 );
		Case c2 = createPSLCase( a2 ); 
		Test.stopTest();
		c1 = [ SELECT Id, ContactId, PSL_Survey_Sent__c FROM Case WHERE Id = :c1.Id ];
		System.assertEquals( true, c1.PSL_Survey_Sent__c );
		System.assertNotEquals( null, c1.ContactId );
		Contact contact1 = [ SELECT Id, Last_PSL_Survey_Sent__c FROM Contact WHERE Id = :c1.ContactId ];
		System.assertNotEquals( null, contact1.Last_PSL_Survey_Sent__c );
		c2 = [ SELECT Id, ContactId, PSL_Survey_Sent__c FROM Case WHERE Id = :c2.Id ];
		System.assertEquals( true, c2.PSL_Survey_Sent__c );
		System.assertNotEquals( null, c2.ContactId );
		Contact contact2 = [ SELECT Id, Last_PSL_Survey_Sent__c FROM Contact WHERE Id = :c2.ContactId ];
		System.assertNotEquals( null, contact2.Last_PSL_Survey_Sent__c );
	}
	*/

	public static Account createPersonAccount( String country, String language, String flyingBlueLevel ) {
		RecordType personAccountRecordType =  [ SELECT Id FROM RecordType WHERE DeveloperName = 'PersonAccount' AND SObjectType = 'Account'];
		Account newPersonAccount = new Account();
		newPersonAccount.FirstName = 'Fred';
		newPersonAccount.LastName = 'Smith';
		newPersonAccount.RecordTypeId = personAccountRecordType.Id;
		// the following values are ignored in production, as they are retrieved through Customer API
		newPersonAccount.Flying_Blue_Number__c = '1234567890';
		newPersonAccount.PersonEmail = 'ernst.eeldert+psl@gmail.com';
		newPersonAccount.Flying_Blue_Level__pc = flyingBlueLevel;
		newPersonAccount.Country__pc = country;
		newPersonAccount.Preferred_Language__pc = language;
		insert newPersonAccount;
		System.assertNotEquals( null, newPersonAccount.Id );
		return newPersonAccount;
	}

	public static Case createPSLCase( Account a ) {
		RecordType caseRecordType =  [ SELECT Id FROM RecordType WHERE DeveloperName = 'Platinum_Service_Line' AND SObjectType = 'Case' ];
		Case c = new Case();
		c.RecordTypeId = caseRecordType.Id;
		c.AccountId = a.Id;
		Contact contact = [ SELECT Id FROM Contact WHERE AccountId = :a.Id LIMIT 1 ];
		c.ContactId = contact.Id;
		c.Status = 'Closed';
		insert c;
		System.assertNotEquals( null, c.Id );
		System.debug('CASE:' + c );
		return c;
	}

	public static Case createServicingCase( Account a ) {
		RecordType caseRecordType =  [ SELECT Id FROM RecordType WHERE DeveloperName = 'Servicing' AND SObjectType = 'Case' ];
		Case c = new Case();
		c.RecordTypeId = caseRecordType.Id;
		c.AccountId = a.Id;
		Contact contact = [ SELECT Id FROM Contact WHERE AccountId = :a.Id LIMIT 1 ];
		c.ContactId = contact.Id;
		c.Case_Detail__c = 'something';
		c.Sentiment_at_start_of_case__c = 'neutral';
		c.Sentiment_at_close_of_case__c = 'neutral';
		c.Status = 'Closed';
		insert c;
		System.assertNotEquals( null, c.Id );
		System.debug('CASE:' + c );
		return c;
	}

	public static void createPSLSUrveySettings( String countryCode, Boolean isActive, Integer surveyLimit, String bccAddress ) {
		AFKLM_PSL_Survey_Settings__c s = new AFKLM_PSL_Survey_Settings__c();
		s.Name = countryCode;
		s.Active__c = isActive;
		s.Daily_Survey_Limit__c = surveyLimit;
		s.BCC_Email__c = bccAddress;
		insert s;
	}

}