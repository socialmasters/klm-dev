/********************************************************************** 
 Name:  AFKLM_SCS_AccessTokenRefreshCustAPI
 Task:    N/A
 Runs on: N/A
====================================================== 
Purpose: 
    Refresh the Customer API token within the Custom Settings
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      21/05/2014      Initial Development
***********************************************************************/
public with sharing class AFKLM_SCS_AccessTokenRefreshCustAPI {
	// Add these kind of information in the Custom Settings later on
	private static final string GRANT_TYPE='grant_type=client_credentials';
	private static final string REDIRECT_URL='https://emea.salesforce.com/apex/AFKLM_SCS_AccessTokenRefreshCustAPI'; // redirect URL
    private Blob key; //This is the accessToken below
	private String accessToken;
	
	/**
	 * Reprocess Customer API OAuth service to get new access token 
	 *  and save it in the Custom Settings 'SCS_CustomerAPIOAuthAccessToken__c'
	 **/
	 // Description: You can't do a DML insert/update/upsert within a call-out to update the Custom Settings Customer API Access Token.
	public void reprocessOAuthToken() {
		try {
			String newAccessToken = '';
			
			AFKLM_SocialCustomerService__c custSetting = AFKLM_SocialCustomerService__c.getOrgDefaults();
			newAccessToken = generateAccessToken(custSetting);
			custSetting.SCS_CustomerAPIOAuthAccessToken__c = EncodingUtil.base64Encode(Blob.valueOf(newAccessToken));
			upsert custSetting;
		} catch(Exception e) {
			throw new AFKLM_SCS_RestException('Unable to generate and set the Customer API Access token', e);
		}
	}

	/**
	 * Generate the new expired accessToken from the Customer API OAuth service
	 **/
	private String generateAccessToken(AFKLM_SocialCustomerService__c custSetting) {
        Http h = new Http();
        String body = GRANT_TYPE + 
        	'&client_id=' + custSetting.SCS_CustomerAPIOAuthClientId__c +
        	'&client_secret='+ EncodingUtil.base64Decode(custSetting.SCS_CustomerAPIOAuthClientSecret__c).toString() +
        	'&redirect_uri=' + REDIRECT_URL;

        // Instantiate a new HTTP request, specify the method (POST) as well as the OAuth 2.0 endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(custSetting.SCS_CustomerAPIOAuthService__c);
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setMethod('POST');
        req.setBody(body);
        
        HttpResponse res = h.send(req);
        
        // Parse JSON response to get all the Access Token values.
        // {"token_type":"bearer","mapi":"f2ywujh23b9nucqhewpck72s","access_token":"<Generated token>","expires_in":86400}
        // TODO: Create separate parser method
        JSONParser parser = JSON.createParser(res.getbody());
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
                // Get the next Token value which is the Access Token.
                parser.nextToken();
                this.accessToken = parser.getText();
            }
        }

		return this.accessToken;
	}

	/**
	 * Encode the the text 
	 **/
	private String encodeText(String text) {
	    Blob cipherText = Crypto.encryptWithManagedIV('AES256', key, Blob.valueOf(text));
	    return EncodingUtil.base64Encode(cipherText); 		
	}
	
	/**
	 * Decode the text
	 **/
	private String decodeText(String text) {
	    Blob encodedEncryptedBlob = EncodingUtil.base64Decode(text);
	    Blob decryptedBlob = Crypto.decryptWithManagedIV('AES256', key, encodedEncryptedBlob);
	    return decryptedBlob.toString();		
	}
}