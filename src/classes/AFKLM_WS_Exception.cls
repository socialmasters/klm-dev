/**********************************************************************
 Name:  AFKLM_WS_Exception.cls
======================================================
Purpose: Custom exception class for Web Services
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma    18/11/2013      Initial development
***********************************************************************/  
public class AFKLM_WS_Exception extends Exception{

}