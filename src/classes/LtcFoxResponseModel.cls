/**                     
 * @author (s)      : David van 't Hooft
 * @description     : Model class to capture the FOX services response
 * @log:   10MAY2014: version 1.0
 */
global class LtcFoxResponseModel {
	global Object specialFlight { get; set; }
	global List<Flight> flights { get; set; }
	
	public static LtcFoxResponseModel parse(String json) {
		return (LtcFoxResponseModel) System.JSON.deserialize(json, LtcFoxResponseModel.class);
	}
	
	global class Flight {
		public String registrationCode { get; set; }
		public Integer totalScheduledFlyingTime { get; set; }
		public Integer totalScheduledTransferTime { get; set; }
		public Integer currentLeg { get; set; }
		public List<Leg> legs { get; set; }
		public String operatingFlightCode { get; set; }
		public List<String> codeShares { get; set; }
		public List<Position> positions { get; set; }
		
		public void populateFlight(Flight__c flight) {
			flight.registrationCode__c = this.registrationCode;
			flight.totalScheduledFlyingTime__c = this.totalScheduledFlyingTime;
			flight.totalScheduledTransferTime__c = this.totalScheduledTransferTime;
			flight.currentLeg__c = this.currentLeg;
			flight.operatingFlightCode__c = this.operatingFlightCode;
		}
	}
	
	global class Leg {
		public String origin { get; set; }
		public String status { get; set; }
		public String destination { get; set; }
		public Integer legNumber { get; set; }
		public Integer timeToArrival { get; set; }
		public String scheduledArrivalDate { get; set; }
		public String scheduledArrivalTime { get; set; }
		public String actualArrivalTime { get; set; }
		public String estimatedArrivalTime { get; set; }
		public Integer arrivalDelay { get; set; }
		public String scheduledDepartureDate { get; set; }
		public String scheduledDepartureTime { get; set; }
		public String actualDepartureTime { get; set; }
		public String estimatedDepartureTime { get; set; }
		public Integer timeToDeparture { get; set; }
		public Integer departureDelay { get; set; }
		public String actualArrivalDate { get; set; }
		public String actualDepartureDate { get; set; }
		public String arrivalDate { get; set; }
		public String departureDate { get; set; }
		public String estimatedArrivalDate { get; set; }
		public String estimatedDepartureDate { get; set; }
		
		public void populateLeg(Leg__c leg) {
			leg.origin__c = this.origin;
			leg.status__c = this.status;
			leg.destination__c = this.destination;
			leg.legNumber__c = this.legNumber;
			leg.timeToArrival__c = this.timeToArrival;
			leg.scheduledArrivalDate__c = this.scheduledArrivalDate == null ? null : Date.valueof(this.scheduledArrivalDate);
			leg.scheduledArrivalTime__c = this.scheduledArrivalTime;
			leg.actualArrivalTime__c = this.actualArrivalTime;
			leg.estimatedArrivalTime__c = this.estimatedArrivalTime;
			leg.arrivalDelay__c = this.arrivalDelay;
			leg.scheduledDepartureDate__c = this.scheduledDepartureDate == null ? null : Date.valueof(this.scheduledDepartureDate);
			leg.scheduledDepartureTime__c = this.scheduledDepartureTime;
			leg.actualDepartureTime__c = this.actualDepartureTime;
			leg.estimatedDepartureTime__c = this.estimatedDepartureTime;
			leg.timeToDeparture__c = this.timeToDeparture;
			leg.departureDelay__c = this.departureDelay;
			leg.actualArrivalDate__c = this.actualArrivalDate == null ? null : Date.valueof(this.actualArrivalDate);
			leg.actualDepartureDate__c = this.actualDepartureDate == null ? null : Date.valueof(this.actualDepartureDate);
			leg.arrivalDate__c = this.arrivalDate == null ? null : 
				Date.valueof(this.arrivalDate.substring(6, 10) + '-' + this.arrivalDate.substring(3, 5) + '-' + this.arrivalDate.substring(0, 2));
			leg.departureDate__c = this.departureDate == null ? null : 
				Date.valueof(this.departureDate.substring(6, 10) + '-' + this.departureDate.substring(3, 5) + '-' + this.departureDate.substring(0, 2));
			leg.estimatedArrivalDate__c = this.estimatedArrivalDate == null ? null : Date.valueof(this.estimatedArrivalDate);
			leg.estimatedDepartureDate__c = this.estimatedDepartureDate == null ? null : Date.valueof(this.estimatedDepartureDate);
		}
	}

	global class Position {
		public Long whenn { get; set; }
		public String latitude { get; set; }
		public String longitude { get; set; }
	}
	
}