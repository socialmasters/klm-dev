/**********************************************************************
 Name:  AFKLM_Utility_Test.cls
======================================================
Purpose: Test class for AFKLM_Utility

======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma    01/12/2013      Initial development
***********************************************************************/
@isTest
private class AFKLM_Utility_Test {

    static testMethod void testAFKLM_Utility() {
        AFKLM_Utility.startTimeForType('Test1');
        AFKLM_Utility.startTimeForType('Test2');
        AFKLM_Utility.endTimeForType('Test1');
        AFKLM_Utility.endTimeForType('Test2');
        AFKLM_Utility.endTimeForType('Test3');
        
        String timeSummary = AFKLM_Utility.getTimeSummary('<br/>');
        
        System.debug(timeSummary);
    }
}