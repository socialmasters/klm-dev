/**********************************************************************
 Name:  AFKLM_SCS_InboundTwitterHandlerImpl
 Task:    N/A
 Runs on: AFKLM_SCS_InboundSocialPostHandlerImpl
======================================================
Purpose: 
   Inbound Social Handler for Twitter based on Social Hub rules and data sources. 
   Creates Social Posts and handles business logic with Social Customer Service enablement.
======================================================
History                                                            
-------                                                            
VERSION		AUTHOR				DATE			DETAIL                                 
	1.0		Stevano Cheung		19/03/2014		INITIAL DEVELOPMENT
	1.1		Stevano Cheung		19/03/2014		FindParentCase() method has multiple similar if. Is refactored for seperate method.
***********************************************************************/
global virtual class AFKLM_SCS_InboundTwitterHandlerImpl implements Social.InboundSocialPostHandler {
    // Reopen case if it has not been closed for more than this number.
    // SLA reopen the case and attach the reply to a Case after 1 day (Value 1 = 24 hours). 
    global virtual Integer getMaxNumberOfDaysClosedToReopenCase() {
        return 1;
    }

	//not using this method. Can be deleted?
    global virtual Boolean usePersonAccount() {
        return false;
    }
	
	//not using this method. Can be deleted?
    global virtual String getDefaultAccountId() {
        return null;
    }

    global Social.InboundSocialPostResult handleInboundSocialPost(SocialPost post, 
                                                        SocialPersona persona,
                                                        Map<String, Object> rawData) {

        Social.InboundSocialPostResult twitterResult = new Social.InboundSocialPostResult();
        twitterResult.setSuccess(true);

        if (post.Id != null) {
            	update post;
            if (persona.id != null) {
                try{
                	update persona;
                } catch (Exception e) {
                	System.debug(e.getMessage());
                }
            }
            return twitterResult;
        }

        findReplyTo(post, rawData);

        List<Account> twitterWithPersonAccount = [SELECT Id, sf4twitter__Twitter_Username__pc FROM Account WHERE sf4twitter__Twitter_Username__pc = :persona.Name LIMIT 1];

        if (persona.Id == null) {
            // Logic to check if "Person Account" exist first, else create a Persona with default temporary "Person Account"
            if(!twitterWithPersonAccount.isEmpty()) {
                persona.parentId = twitterWithPersonAccount[0].Id;
                insert persona;

                // Fix for adding the Social Post to an existing personAccount
                post.WhoId = TwitterWithPersonAccount[0].Id;
            } else {
                createPersona(persona);
            }
        }
        else {
            // Fix for adding the Social Post to an existing personAccount
            if(!twitterWithPersonAccount.isEmpty()) {
                post.WhoId = TwitterWithPersonAccount[0].Id;
            }
            try{
            	update persona;
            } catch (Exception e) {
            	System.debug(e.getMessage());
            }
        }

        post.PersonaId = persona.Id;
		findParentCase(post, persona, rawData);

        if(post.Content != null && post.Content.length() > 144){
				post.Short_Content__c = post.Content.substring(0,144);
		} else {
				post.Short_Content__c = post.Content;
		}
		
        insert post;
        return twitterResult;   
    }
    
    private Case findParentCase(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        SocialPost replyToPost = null;
        List<RecordType> recordTypeIds = [SELECT Id FROM RecordType WHERE (Name = 'AF Servicing' OR Name = 'Servicing') AND SobjectType = 'Case'];
        
        if ('Retweet'.equals(post.MessageType)){
        	post.parentId = null;
        	return null;
        }

        if (post.ReplyTo != null && (post.isOutbound || post.ReplyTo.PersonaId == persona.Id)) {
            replyToPost = post.ReplyTo;
        } else if (post.MessageType == 'Direct' && String.isNotBlank(post.Recipient)) {
            // Find the latest outbound post that the DM is responding to
            List<SocialPost> posts = [SELECT Id, ParentId FROM SocialPost WHERE  OutboundSocialAccount.ProviderUserId = :post.Recipient AND ReplyTo.Persona.Id = :persona.Id ORDER BY CreatedDate DESC LIMIT 1];

            if (!posts.isEmpty()) { 
                replyToPost = posts[0];
            }

            // 2014-01-23: DM should also be attach to the same case
            List<Case> cs = [SELECT Id, ClosedDate, isClosed FROM Case WHERE Delete_Case__c = false AND AccountId = :persona.ParentId AND RecordTypeId IN: recordTypeIds ORDER BY CreatedDate DESC LIMIT 1];
			if (!cs.isEmpty()) {
				cs[0] = reopenClosedCaseLogic(cs, post);
				
				if(cs[0] != null) {
					return cs[0];
				}
			}
        }

		// TODO: Assumption Can be removed because Twitter doesn't have replyToPost
        if (replyToPost != null) {
            List<Case> cases = [SELECT Id, IsClosed, Status, ClosedDate FROM Case WHERE Delete_Case__c = false AND Id = :replyToPost.ParentId AND RecordTypeId IN: recordTypeIds];
			if (!cases.isEmpty()) {
				cases[0] = reopenClosedCaseLogic(cases, post);
				
				if(cases[0] != null) {
					return cases[0];
				}
			}
        } else { 
            // Fix for additonal cases to be attached when engaging with the customer.
            List<Case> cs = [SELECT Id, ClosedDate, isClosed FROM Case WHERE Delete_Case__c = false AND AccountId = :persona.ParentId AND RecordTypeId IN: recordTypeIds ORDER BY CreatedDate DESC LIMIT 1];
			if (!cs.isEmpty()) {
				cs[0] = reopenClosedCaseLogic(cs, post);
				
				if(cs[0] != null) {
					return cs[0];
				}
			}
        }
        return null;
    }

	private Case reopenClosedCaseLogic(List<Case> cases, SocialPost post) {
		Case returnCase = null;
		
		if (!cases[0].IsClosed) { 
			post.ParentId = cases[0].Id;
			returnCase = cases[0];
		}

		if (cases[0].ClosedDate > System.now().addDays(-getMaxNumberOfDaysClosedToReopenCase())) {
			reopenCase(cases[0]);
			post.ParentId = cases[0].Id;
			returnCase = cases[0];
		}
		
		return returnCase;
	}

    private void reopenCase(Case parentCase) {
        parentCase.Status = 'Reopened'; 
        parentCase.SCS_Reopened_Date__c = Datetime.now();
        update parentCase;
    }

    private void findReplyTo(SocialPost post, Map<String, Object> rawData) {
        String replyToId = (String)rawData.get('replyToExternalPostId');

        if (String.isBlank(replyToId)) 
            return;

        List<SocialPost> postList = [SELECT Id, Status, ParentId, PersonaId FROM SocialPost WHERE ExternalPostId = :replyToId LIMIT 1];

        if (!postList.isEmpty()) {
            post.ReplyToId = postList[0].id;
            post.ReplyTo = postList[0];
            //post.ParentId = postList[0].ParentId;
        }
    }

    private void createPersona(SocialPersona persona) {
        if (persona == null || persona.Id != null || String.isBlank(persona.ExternalId) || String.isBlank(persona.Name) ||
            String.isBlank(persona.Provider)) 
            return;

        // Select/Create by personAccount.Name "TemporaryPersonAccount"?
        //  this is the "TemporaryPersonAccount" since the creation of new "Person Account" should be done when "Create Case" is selected by agents. 
        List<Account> accountList = [SELECT Id FROM Account WHERE Name = 'TemporaryPersonAccount' LIMIT 1];

        if ( !accountList.isEmpty()) {
            persona.parentId = accountList[0].Id;
        }
        
        /*String TemporaryPersonAccountID = '';
        AFKLM_SocialCustomerService__c custSettings = AFKLM_SocialCustomerService__c.getValues('TemporaryPersonAccount');
        TemporaryPersonAccountID = custSettings.SCS_TemporaryPersonAccount__c;

		if (TemporaryPersonAccountID != null) {
            persona.parentId = TemporaryPersonAccountID;
        }*/
        insert persona;
    }
}