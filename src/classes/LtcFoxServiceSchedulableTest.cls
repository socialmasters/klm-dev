/**
 * @author (s)    :	m.lapere@gen25.com
 * @description   : Test class for LtcFoxServiceSchedulable
 *
 * @log: 	2015/04/13: version 1.0
 */

@isTest
public with sharing class LtcFoxServiceSchedulableTest {
	
	public static testMethod void testLtcFoxServiceSchedulable() {
		
		// executing the class should insert a bunch of QueueItems
		(new LtcFoxServiceSchedulable()).execute(null);
		
		QueueItem__c[] qItems = [
			SELECT Id, Class__c, Status__c, Message__c, Started__c, Finished__c,
				BatchId__c, Parameters__c, ScopeSize__c, Run_After__c, Run_Before__c, Priority__c
			FROM QueueItem__c
		];

		// there should be 5 QueueItems
		System.assertEquals(5, qItems.size());

		for (QueueItem__c qi : qItems) {
			
			// the class name should be filled in and the status should be queued
			System.assertEquals('LtcFoxServiceBatch', qi.Class__c);
			System.assertEquals(LtcQueueItemAbstractBatchable.status.QUEUED.name(), qi.Status__c);
			
			// the following values should all be null
			System.assertEquals(null, qi.Message__c);
			System.assertEquals(null, qi.Started__c);
			System.assertEquals(null, qi.Finished__c);
			System.assertEquals(null, qi.BatchId__c);
			System.assertEquals(null, qi.Run_After__c);			
			System.assertEquals(null, qi.Priority__c);			

			// the given parameter should range from -2 to 2
			System.assert(Integer.valueOf(qi.Parameters__c) < 3);
			System.assert(Integer.valueOf(qi.Parameters__c) > -3);

			// scopesize of LtcFoxServiceBatch should be under 101
			// so it won't run into callout limits
			System.assert(qi.ScopeSize__c < 101);

			// the RunBefore should have a value of tomorrow
			Time midnight = Time.newInstance(0, 0, 0, 0);
        	Datetime tomorrow = Datetime.newInstance(Date.today().addDays(1), midnight);
			System.assertEquals(tomorrow, qi.Run_Before__c);		
		}
	}
}