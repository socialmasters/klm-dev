/********************************************************************** 
 Name:  AFKLM_SCS_SocialPostFacebookController
 Task:    N/A
 Runs on: AFKLM_SCS_SocialPosController
====================================================== 
Purpose: 
    Social Post Controller Class to utilize existing List View in Apex with Pagination Support with logic used for Facebook.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta          22/07/2014      Initial Development
    
***********************************************************************/
public with sharing class AFKLM_SCS_SocialPostFacebookController {

    public String createCasesFacebook(List<AFKLM_SCS_SocialPostWrapperCls> socPostList, Map<String, SocialPost> postMap, Boolean error, ApexPages.StandardSetController SocPostSetController, String newCaseId, String newCaseNumber, List<SocialPost> listSocialPosts, SocialPost post, String tempSocialHandle) {
        AFKLM_SCS_ControllerHelper controllerHelper = new AFKLM_SCS_ControllerHelper(); 
        List<String> returnedCaseId = new List<String>();


        if(post.ParentId == null) {
            //IBO: Code below needs to be changes as only social post on which it was replied on should be "Actioned"
            //After engaging all social posts from same handle should be MAR (plus other fields: Ignore_for_SLA__c,SCS_Status__c,SCS_MarkAsReviewed__c, SCS_MarkAsReviewedBy__c, etc)
            //Trigger SCS_SocialPostAfterInsertUpdate (or other name) will change the one social post to Actioned 
            if((('Actioned').equals(post.SCS_Status__c) || ('Mark as reviewed').equals(post.SCS_Status__c) || post.SCS_MarkAsReviewed__c == true)) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The Social Post is already "Mark As Reviewed" OR "Actioned" and being handled by another agent. ' ) );
                return null;
            }

            post.SCS_Status__c = 'Mark as reviewed';
            post.SCS_MarkAsReviewed__c = true;
            post.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
            post.Ignore_for_SLA__c = true;
            post.OwnerId = userInfo.getUserId();

            listSocialPosts.add(post);
            postMap.put( post.Id, post );
        } else {

            newCaseId = post.Parentid;

            if((('Actioned').equals(post.SCS_Status__c) || ('Mark as reviewed').equals(post.SCS_Status__c) || post.SCS_MarkAsReviewed__c == true)) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The case number  "' + post.SCS_CaseNumber__c + '" already exists OR is actioned and being handled by another agent. ' ) );
                return null;
            }
 
            post.SCS_Status__c = 'Mark as reviewed';
            post.SCS_MarkAsReviewed__c = true;
            post.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
            post.Ignore_for_SLA__c = true;
            post.OwnerId = userInfo.getUserId();
                
            listSocialPosts.add(post);
        }

        if( !postMap.IsEmpty() ){
            returnedCaseId = controllerHelper.createNewCases(postMap, newCaseId, newCaseNumber, error);

            if(returnedCaseId.size() > 0) {
                               
                newCaseId = returnedCaseId[0];
                if('Private'.equals(post.MessageType)){
                    if(tempSocialHandle != null){
                        this.getAllRelatedPosts(socPostList, newCaseId, tempSocialHandle, SocPostSetController);
                    }
                }
            }

        } else {
            if('Private'.equals(post.MessageType)){
                if(tempSocialHandle != null){
                    this.getAllRelatedPosts(socPostList, newCaseId, tempSocialHandle, SocPostSetController); 
                }
            }       
        }

        if( !error ){
            return newCaseId;
        }
        return null;
    }


    // Engage on case view
    public String createCasesFacebookCaseView(List<AFKLM_SCS_SocialPostWrapperCls> socPostList, Map<String, SocialPost> postMap, Boolean error, ApexPages.StandardSetController SocPostSetController, String newCaseId, String newCaseNumber, List<SocialPost> listSocialPosts, SocialPost post, String tempSocialHandle) {
        
        AFKLM_SCS_ControllerHelper controllerHelper = new AFKLM_SCS_ControllerHelper(); 
        List<String> returnedCaseId = new List<String>();

        if((('Actioned').equals(post.SCS_Status__c) || ('Mark as reviewed').equals(post.SCS_Status__c) || post.SCS_MarkAsReviewed__c == true)) {

            if(post.ParentId == null) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The Social Post is already "Mark As Reviewed" OR "Actioned" and being handled by another agent. ' ) );
            } else {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The case number  "' + post.SCS_CaseNumber__c + '" already exists OR is actioned and being handled by another agent. ' ) );                
            }
            
            return null;
        }
        
        post.SCS_Status__c = 'Mark as reviewed';
        post.SCS_MarkAsReviewed__c = true;
        post.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
        post.Ignore_for_SLA__c = true;
        post.OwnerId = userInfo.getUserId();
                        
        if(post.ParentId == null) {

            postMap.put( post.Id, post );
        } else {

            newCaseId = post.Parentid;
        }

        listSocialPosts.add(post);

        if( !postMap.IsEmpty() ){
            returnedCaseId = controllerHelper.createNewCases(postMap, newCaseId, newCaseNumber, error);

            if(returnedCaseId.size() > 0) {
                               
                newCaseId = returnedCaseId[0];
                if('Private'.equals(post.MessageType) && tempSocialHandle != null){
                        
                    this.getAllRelatedPostsCaseView(post, newCaseId, tempSocialHandle, SocPostSetController);
                }
            }

        } else {
            if('Private'.equals(post.MessageType) && tempSocialHandle != null){
                    
                this.getAllRelatedPostsCaseView(post, newCaseId, tempSocialHandle, SocPostSetController); 
            }       
        }

        if( !error ) return newCaseId;

        return null;
    }

    private void getAllRelatedPostsCaseView(SocialPost post, String newCaseId, String tempSocialHandle, ApexPages.StandardSetController SocPostSetController) {

        Case personAccountId = [SELECT accountId FROM Case WHERE id =: newCaseId LIMIT 1];
        List<SocialPost> toUpdate = new List<SocialPost>();

        List<SocialPost> relatedPosts = [SELECT id, Handle, SCS_Status__c, SCS_MarkAsReviewed__c, SCS_MarkAsReviewedBy__c, Ignore_for_SLA__c, OwnerId, ParentId, WhoId FROM SocialPost WHERE Handle=:tempSocialHandle AND SCS_CaseNumber__c=:post.SCS_CaseNumber__c AND id!=:post.Id];
        if(relatedPosts.size() > 0) {

            // The non selected to merge by Handle   
            for(SocialPost sp : relatedPosts) {
                        
                if(tempSocialHandle.equals(sp.Handle) && !('Actioned').equals(sp.SCS_Status__c)) {
                    
                    if(sp.ParentId == null) sp.ParentId = newCaseId;
                                
                    sp.SCS_Status__c = 'Mark as reviewed';
                    sp.SCS_MarkAsReviewed__c = true;
                    sp.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                    sp.Ignore_for_SLA__c = true;
                    sp.WhoId = personAccountId.accountId;
                    sp.OwnerId = userInfo.getUserId();

                    toUpdate.add(sp);
                }
                            
            }

            update toUpdate;
        }
    }

    private void getAllRelatedPosts(List<AFKLM_SCS_SocialPostWrapperCls> socPostList, String newCaseId, String tempSocialHandle, ApexPages.StandardSetController SocPostSetController) {
              
        Case personAccountId = [SELECT accountId FROM Case WHERE id =: newCaseId LIMIT 1]; 
        List<SocialPost> listWrapperSocialPost = new List<SocialPost>();

               
        while(SocPostSetController.getHasNext()) {
            SocPostSetController.next();
            for(SocialPost c : (List<SocialPost>)SocPostSetController.getRecords()) {
                socPostList.add(new AFKLM_SCS_SocialPostWrapperCls(c, SocPostSetController));
            }
        }
        //IBO:This needs to be modified as it takes only social posts on only one page ("SocPostList" which size is 200)
        //other posts from the same customer are not taken and not MAR (will not disappear from the view)
        //also logic should not take mark as reviewed posts... now it checks only if the post is actioned
        

        // The non selected to merge by Handle   
        for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : socPostList) {
        //for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : testingList) {
                    
            if(tempSocialHandle.equals(wrapperSocialPost.cSocialPost.Handle) && !('Actioned').equals(wrapperSocialPost.cSocialPost.SCS_Status__c)) {
                
                if(wrapperSocialPost.cSocialPost.ParentId == null) {
                    wrapperSocialPost.cSocialPost.ParentId = newCaseId;
                } 
                            
                wrapperSocialPost.cSocialPost.SCS_Status__c = 'Mark as reviewed';
                wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                // SC: This should not be done after test. The TemporaryPersonAccount will be on the SocialPost instead the user Account.
                wrapperSocialPost.cSocialPost.WhoId = personAccountId.accountId;
                wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();

                listWrapperSocialPost.add(wrapperSocialPost.cSocialPost);
            }
                        
        }

        update listWrapperSocialPost;
    }

    
}