/**
 * @author (s)    	: David van 't Hooft
 * @requirement id  : 
 * @description   	: Test class to test the SocialPostDeletebatch
 * @log:            : 20JUL2015 v1.0 
 */
@isTest
private class SocialPostDeleteTest {

    static testMethod void socialPostDeleteTestOk() {
        SocialPost sp = new SocialPost(name = 'Test social post to delete', Delete_Social_Post__c = true);
        insert sp;
		
        List <SocialPost> spList = [Select s.id From SocialPost s limit 10];		     
        system.assertEquals(1, spList.size());
        
        test.starttest();
        SocialPostDeleteScheduler spds = new SocialPostDeleteScheduler();
        spds.execute(null);
        test.stoptest();
        
        spList = [Select s.id From SocialPost s limit 10];		     
        system.assertEquals(0, spList.size(), 'SocialPost should be deleted');        
    }
    
    static testMethod void socialPostDeleteTestNotOk() {
        SocialPost sp = new SocialPost(name = 'Test social post to delete', Delete_Social_Post__c = false);
        insert sp;
		
        List <SocialPost> spList = [Select s.id From SocialPost s limit 10];		     
        system.assertEquals(1, spList.size());
        
        test.starttest();
        SocialPostDeleteScheduler spds = new SocialPostDeleteScheduler();
        spds.execute(null);
        test.stoptest();
        
        spList = [Select s.id From SocialPost s limit 10];		     
        system.assertEquals(1, spList.size(), 'SocialPost should *not* be deleted');        
    }
}