/**********************************************************************
 Name:  AFKLM_CFT_CustomerCareEmailSender
======================================================
Purpose: 

Send an email to Customer Care
======================================================
History                                                            
-------                                                            
VERSION		AUTHOR				DATE			DETAIL                                 
	1.0		AJ van Kesteren		22/03/2013		INITIAL DEVELOPMENT
	1.1		Patrick Brinksma	18/12/2013		Updated to use new way of calling Web Service
***********************************************************************/	
global class AFKLM_CFT_CustomerCareEmailSender {

	@future (callout=true)
	public static void sendEmail(Id caseId) {
		
		Case c = [select Id, Status, CaseNumber, RecordTypeId, PNR__c, Description, Subject, Reason, Sub_Reason__c,
								Account.Id, Account.Flying_Blue_Number__c, Account.Official_First_Name__c, Account.Official_Last_Name__c,
								Account.PersonEmail, Account.Country_of_Residence__c
					from Case 
					where Id = :caseId];
		
		// EmailTemplate template = [Select Id
    	//							From EmailTemplate 
    	//							Where Name = 'Customer Feedback Tool Customer Care Email' limit 1];
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(new List<String>{AFKLM_CFT_Emails__c.getValues('customer_care').email__c});
					
		mail.setSubject('Customer Feedback Tool: Please enter complaint');
		
		String email = c.Account.PersonEmail;
		String countryOfResidence = c.Account.Country_of_Residence__c;
		if (c.Account.Flying_Blue_Number__c != null){
			try {			
				AFKLM_WS_Manager.flyingBlueMember fbMember = AFKLM_WS_Manager.searchOnFBNumber(c.Account.Flying_Blue_Number__c, '62.17.146.154');
				if (fbMember.emailAddress != null){
					email = fbMember.emailAddress;
				}
				if (fbMember.country != null){
					countryOfResidence = fbMember.country;	
				}
			} catch (Exception e) {
				// We cannot obtain email & country. Bad luck, we just send an e-mail without. 
				System.debug('####: ' + e);
			}
		}			
		String s = '<html>';
					
		s += '<table border="1">';
					
		s += '<tr>';
		s += '<td>Flying Blue Number</td>';
		s += '<td>' + c.Account.Flying_Blue_Number__c + '</td>';
		s += '</tr>';
					
		s += '<tr>';
		s += '<td>First Name</td>';
		s += '<td>' + c.Account.Official_First_Name__c + '</td>';
		s += '</tr>';
					
		s += '<tr>';
		s += '<td>Last Name</td>';
		s += '<td>' + c.Account.Official_Last_Name__c + '</td>';
		s += '</tr>';
					
		s += '<tr>';
		s += '<td>Email</td>';
		s += '<td>' + email + '</td>';
		s += '</tr>';
		
		s += '<tr>';
		s += '<td>Country of residence</td>';
		s += '<td>' + countryOfResidence + '</td>';
		s += '</tr>';
					
		s += '<tr>';
		s += '<td>PNR</td>';
		s += '<td>' + c.PNR__c + '</td>';
		s += '</tr>';
					
		s += '<tr>';
		s += '<td>Feedback Category</td>';
		s += '<td>' + c.Reason + '</td>';
		s += '</tr>';					
					
		s += '<tr>';
		s += '<td>Feedback Subcategory</td>';
		s += '<td>' + c.Sub_Reason__c + '</td>';
		s += '</tr>';
					
		s += '<tr>';
		s += '<td>Subject</td>';
		s += '<td>' + c.Subject + '</td>';
		s += '</tr>';
					
		s += '<tr>';
		s += '<td>Description</td>';
		s += '<td>' + c.Description + '</td>';
		s += '</tr>';						
					
		s += '</table>';
					
		s += '</html>';
					
		mail.setHtmlBody(s);
					
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
}