/**********************************************************************
 Name:  ctrl_Refund
======================================================
Purpose: Logic for handling Refund from Visualforce Page
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma    20 Jul 2014      Initial development
***********************************************************************/  
global with sharing class ctrl_Refund {

    // Indicator if Refund can be submitted
    public Boolean canSubmit {
        get{
            // Submit is only allowed if status is New or Failed
            return (thisRefund != null && 
                    (thisRefund.Status__c == 'New' || thisRefund.Status__c == 'Failed')) ? true : false;            
        }
        private set;
    }

    // Refund instance
	public Refund__c thisRefund {get;set;}

    // Case instance
    private final Case thisCase;

    // Constructor
    public ctrl_Refund(ApexPages.StandardController stdController) {
        Id thisId = stdController.getId();
        if (thisId.getSobjectType() == Refund__c.sObjectType){
            if (!Test.isRunningTest()){
                // Make sure all fields are included
                stdController.addFields(new List<String>{
                    'Id',
                    'CSR_Refund_Request_Id__c',
                    'Error_Message__c',
                    'Email__c',
                    'First_Name__c',
                    'Flight_Date__c',
                    'Flight_Number__c',
                    'Flying_Blue_Number__c',
                    'Language__c',
                    'Last_Name__c',
                    'Paid_Service__c',
                    'PNR__c',
                    'Remarks__c',
                    'Seat_Number__c',
                    'Status__c'     
                    });
            }
            this.thisRefund = (Refund__c) stdController.getRecord();
        } else if (thisId.getSobjectType() == Case.sObjectType){
            thisCase = (Case) stdController.getRecord();
        }
    }

    /*
     * Method to create a Refund out of a Case
     */
    public PageReference createRefund(){
        List<Refund__c> listOfRefund = bl_Refund.createRefundFromCase(new List<Id>{thisCase.Id});
        if (listOfRefund.size() == 1){
            insert listOfRefund;
            return new ApexPages.StandardController(listOfRefund[0]).view();
        }
        // Something went wrong
        return null;
    }

    /*
     * Method to submit Refund to CSR
     * Input: thisRefund - instance of Refund
     * Output: String - 'Ok' in case of no error, otherwise error message
     */
    @RemoteAction
    global static String submitRefund(Id thisRefundId){
        Refund__c thisRefund = [
            select 
                Id, 
                Case__c,
                CSR_Refund_Request_Id__c,
                Error_Message__c,
                Email__c,
                First_Name__c,
                Flight_Date__c,
                Flight_Number__c,
                Flying_Blue_Number__c,
                Language__c,
                Last_Name__c,
                Paid_Service__c,
                PNR__c,
                Remarks__c,
                Seat_Number__c,
                Status__c
            from Refund__c        
            where Id = :thisRefundId];
        if (bl_Refund.validateRefundForRequest(thisRefund)){
            bl_Refund.issueRefundRequests(new List<Id>{thisRefund.Id});
            return 'Ok';
        } else {
            return System.Label.Refund_Validation_Error;
        }
    }

}