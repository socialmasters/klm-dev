/**
 * @author (s)      : David van 't Hooft
 * @description     : Test class to test the LtcUtil class
 * @log:   14MAY2014: version 1.0 
 * */
@isTest
private class LtcUtilTest {

    static testMethod void cleanFlightNumberTest() {
        System.assertEquals('KL0012', LtcUtil.cleanFlightNumber('KL012'));
        System.assertEquals('KL0012', LtcUtil.cleanFlightNumber('kl000012'));
        System.assertEquals('KL1234', LtcUtil.cleanFlightNumber('KL1234'));
    }
    
    static testMethod void testLimitedList() {
   		List<Rating__c> bigList = new List<Rating__c>();
   		for (integer i = 0; i < 35; i++) {
   			Rating__c rat = new Rating__c();
   			bigList.add(rat);
   		}
   		List<Rating__c> limitedList = LtcUtil.getLimitedList(bigList, 10, 0);
   		System.assertEquals(10, limitedList.size());
   		
   		limitedList = LtcUtil.getLimitedList(bigList, 10, 30);
   		System.assertEquals(5, limitedList.size());
    }
    
    static testMethod void testLimitedListSmall() {
   		List<Rating__c> bigList = new List<Rating__c>();
   		for (integer i = 0; i < 5; i++) {
   			Rating__c rat = new Rating__c();
   			bigList.add(rat);
   		}
   		List<Rating__c> limitedList = LtcUtil.getLimitedList(bigList, 10, 0);
   		System.assertEquals(5, limitedList.size());
   		
   		limitedList = LtcUtil.getLimitedList(bigList, 10, 1);
   		System.assertEquals(4, limitedList.size());
    }
   
    	
    	
}