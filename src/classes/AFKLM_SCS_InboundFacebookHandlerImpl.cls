global virtual class AFKLM_SCS_InboundFacebookHandlerImpl {
    // Reopen case if it has not been closed for more than this number
    global virtual Integer getMaxNumberOfDaysClosedToReopenCase() {
        // SCH: SLA reopen the case and attach the reply to a case after 24 hours = 1 day
        return 1;
    }
    
    /**
    * Method to check if wall post is from Global page, and set Is campaign to true on SocialPost object.
    */
    public void checkCampaign(SocialPost post) {
    	
    	AFKLM_SocialCustomerService__c custSettings = AFKLM_SocialCustomerService__c.getValues('SCS_Campaing_Social_Handles');
    	//takes social handles from custom setting SocialCustomerService -> SCS_CampaignSocialHandles
    	Set<String> handles = new Set<String>(custSettings.SCS_CampaignSocialHandles__c.split(';',custSettings.SCS_CampaignSocialHandles__c.length()));
    	if(custSettings.SCS_CampaignSocialHandles2__c != null){
    		handles.addAll(custSettings.SCS_CampaignSocialHandles2__c.split(';',custSettings.SCS_CampaignSocialHandles2__c.length()));
    	}
        if(custSettings.SCS_CampaignSocialHandles3__c != null){
            handles.addAll(custSettings.SCS_CampaignSocialHandles3__c.split(';',custSettings.SCS_CampaignSocialHandles3__c.length()));
        }
    	//Set<String> handles2 = new Set<String>(custSettings.SCS_CampaignSocialHandles2__c.split(';',custSettings.SCS_CampaignSocialHandles2__c.length()));
    	
    	//hardcoded social handles
     // Set<String> handles = new Set<String>{'Social Airline Italian','KLM', 'KLM Italia', 'KLM Kazakhstan', 'KLM Russia', 'KLM Japan', 'KLM Brasil', 'KLM Norway','KLM Dutch Caribbean','KLM Suriname','KLM Philippines', 'Air France', 
      //	'KLM Portugal', 'KLM China'};
        if(handles.contains(post.Handle) && post.Provider.equals('Facebook')){
              post.Is_campaign__c = true;
              update post;
        }
    }
    
    /**
    * This method deletes the last case feed item. 
    * Restrict: creation double case feed
    */
    private void deleteLastCaseFeed(SocialPost post){
        List<FeedItem> fi=[SELECT id FROM FeedItem WHERE parentId=:post.Id ORDER BY CreatedDate DESC LIMIT 1];
        try{
            delete fi[0];
        } catch(Exception e) {
            System.debug('************'+e.getMessage());
        }
    }
    
    global virtual Boolean usePersonAccount() {
        return false;
    }

    global virtual String getDefaultAccountId() {
        return null;
    }
    
    /** 
    * Method for automatic creation of cases for Wall Posts and Private Messages
    * Use "createNewCases" method in ControllerHelper class
    * 
    */
    private void autoCreateCase(SocialPost post){
        Case newCase=null;
        //we can delete this if, since the separation based on message type is handled in line:128
        if('Private'.equals(post.MessageType) || 'Post'.equals(post.MessageType)){
            AFKLM_SCS_ControllerHelper helper = new AFKLM_SCS_ControllerHelper();
            
            Map<String, SocialPost> postMap = new Map<String, SocialPost>();
            postMap.put(post.Id, post);
            helper.createNewCases(postMap, '', '', false);
            
            if('Post'.equals(post.MessageType)) {
                checkCampaign(post);
            }
        }
    }
    
    private void handleNotCorrectPersonas(SocialPersona persona){
    	if(persona.ProfileUrl.length()>255){
    		persona.ProfileUrl = 'https://www.facebook.com/'+persona.ProfileUrl.substring(persona.ProfileUrl.lastIndexOf('/')+1,persona.ProfileUrl.length());
    	}
    }
    
    /**
    * Inbound Social Handler for 'Social Post' and 'Social Persona' inserted by Radian6 / Social Hub rules and data sources.
    *
    */
    global Social.InboundSocialPostResult handleInboundSocialPost(SocialPost post, 
                                                        SocialPersona persona,
                                                        Map<String, Object> rawData) {
        Social.InboundSocialPostResult facebookResult = new Social.InboundSocialPostResult();
        facebookResult.setSuccess(true);
        
        handleNotCorrectPersonas(persona);
		
        if (post.Id != null) {
            	update post;
            if (persona.id != null) {
                try{
                	update persona;
                } catch (Exception e) {
                	System.debug(e.getMessage());
                }
            }
            return facebookResult;
        }
        
        findReplyTo(post, rawData);

        // List<Account> facebookWithPersonAccount = [SELECT Id, sf4twitter__Fcbk_Username__pc FROM Account WHERE sf4twitter__Fcbk_Username__pc = :persona.Name LIMIT 1];
        String personaExternalIdWithPrefix = 'FB_' + persona.ExternalId;
        
        List<Account> facebookWithPersonAccount = [SELECT Id FROM Account WHERE sf4twitter__Fcbk_User_Id__pc = :personaExternalIdWithPrefix LIMIT 1];
        // Case parentCase = null;

        if (persona.Id == null) {
            // Logic to check if "Person Account" exist first, else create a Persona with default temporary "Person Account"
            if(!facebookWithPersonAccount.isEmpty()) {
                persona.parentId = facebookWithPersonAccount[0].Id;
                insert persona;

                // Fix for adding the Social Post to an existing personAccount
                post.WhoId = facebookWithPersonAccount[0].Id;
            } else {
                createPersona(persona);
            }
        }
        else {
            // Fix for adding the Social Post to an existing personAccount
            if(!facebookWithPersonAccount.isEmpty()) {
                post.WhoId = facebookWithPersonAccount[0].Id;
            }
            try{
            	update persona;
            } catch (Exception e) {
            	System.debug(e.getMessage());
            }
            //update persona;//?? need this here ??
        }
        
        post.PersonaId = persona.Id;
        
        if('Post'.equals(post.MessageType)){
            // parentCase = null;
        } else {
            /* parentCase = */ findParentCase(post, persona, rawData);
        }
        
        
        if(post.Content != null && post.Content.length()> 255){
        	if(post.Content.length()>5000){
        		post.Content = post.Content.substring(0,5000);
        	}
        	post.Short_Content__c = post.Content.substring(0,255);
    	} else {
        	post.Short_Content__c = post.Content;
    	}
    	
    	

        insert post;
        
        if(('Private'.equals(post.MessageType) || 'Post'.equals(post.MessageType))){
             autoCreateCase(post);
             deleteLastCaseFeed(post);
        }
        
        return facebookResult;  
    }

    /** 
    * Method for finding parent cases and attaching related comments and replies to the same case
    * 
    */
    private Case findParentCase(SocialPost post, SocialPersona persona, Map<String, Object> rawData) {
        SocialPost replyToPost = null;
        List<RecordType> recordTypeIds = [SELECT Id FROM RecordType WHERE (Name = 'AF Servicing' OR Name = 'Servicing') AND SobjectType = 'Case'];

        system.debug('--+ recordTypeIds: '+recordTypeIds);
        system.debug('--+ post: '+post);
        
        if (('Comment'.equals(post.MessageType) || 'Reply'.equals(post.MessageType)) && String.isNotBlank(post.Recipient)) {
            replyToPost = post.ReplyTo;
            
            if(replyToPost != null) {
                if(replyToPost.ParentId != null) {
                    List<Case> cs = [SELECT Id, Status, isClosed, ClosedDate FROM Case WHERE Id =: replyToPost.ParentId AND RecordTypeId IN: recordTypeIds LIMIT 1];
                    
                    if(!cs[0].isClosed){
                        post.ParentId = replyToPost.ParentId;
                        if(post.replyTo.Is_Campaign__c == true){
                            post.Is_Campaign__c = true;
                        }
                        return cs[0];
                    }
                
                    if(cs[0].isClosed || 'Closed - No Response'.equals(cs[0].Status)) {
                        if(post.Is_Campaign__c == true){
                        	post.parentId = cs[0].id;
                            return cs[0];
                        }else{
                            reopenCase(cs[0]);
                            post.ParentId = cs[0].Id;
                            return cs[0];
                        }
                    }
                }
            }
        } else if ('Private'.equals(post.MessageType) && String.isNotBlank(post.Recipient)) {
            String personaPrefixFBId = 'FB_'+persona.ExternalId;
            //IBO:Fix for persona with "TemporaryPersonAccount"
            List<Account> acc=[SELECT id FROM Account WHERE sf4twitter__Fcbk_User_Id__pc =: personaPrefixFBId LIMIT 1];
            if(!acc.isEmpty()){
                List<Case> cs = [SELECT Id, ClosedDate, isClosed FROM Case WHERE AccountId =: acc[0].Id AND RecordTypeId IN: recordTypeIds ORDER BY CreatedDate DESC LIMIT 1];
                
                if(!cs.isEmpty()) {
                        if(!cs[0].isClosed){
                            post.ParentId = cs[0].Id;
                            //post.ParentId = cs[0].ParentId;
                            return cs[0];
                        }
                        
                    if(cs[0].isClosed || 'Closed - No Response'.equals(cs[0].Status)){
                        if (cs[0].ClosedDate > System.now().addDays(-getMaxNumberOfDaysClosedToReopenCase())) {
                            reopenCase(cs[0]);
                            post.ParentId = cs[0].Id;
                            return cs[0];
                        }
                    }
                }
            }
        }
        return null;
    } 
    
    /** 
    * Method for reopening case
    * IF case status is closed and user replies on social post within 24 hours, case status should be set to "Reopened"
    *
    */
    private void reopenCase(Case parentCase) {
        parentCase.Status = 'Reopened'; 
        parentCase.SCS_Reopened_Date__c = Datetime.now();
        parentCase.Sentiment_at_close_of_case__c = '';
        update parentCase;
    }

    /** 
    * findReplyTo
    * 
    */ 
    private void findReplyTo(SocialPost post, Map<String, Object> rawData) {
        String replyToId = (String)rawData.get('replyToExternalPostId');
        if (String.isBlank(replyToId)) 
            return;

        List<SocialPost> postList = [SELECT Id, Status, ParentId, IsOutbound, PersonaId, ExternalPostId, Is_Campaign__c FROM SocialPost WHERE ExternalPostId = :replyToId LIMIT 1];
        if (!postList.isEmpty()) {
            post.ReplyToId = postList[0].id;
            post.ReplyTo = postList[0];
            //post.ParentId = postList[0].ParentId;
            post.Is_Campaign__c = postList[0].Is_Campaign__c;
        }
    }
        
    /** 
    * Method for creating SocialPersona
    * SocialPersona is created automatically with Parent "TemporaryPersonAccount"
    * 
    */  
    private void createPersona(SocialPersona persona) {
        if (persona == null || persona.Id != null || String.isBlank(persona.ExternalId) || String.isBlank(persona.Name) ||
            String.isBlank(persona.Provider)) 
            return;

        // Select/Create by personAccount.Name "TemporaryPersonAccount"?
        //  this is the "TemporaryPersonAccount" since the creation of new "Person Account" should be done when "Create Case" is selected by agents. 
        List<Account> accountList = [SELECT Id FROM Account WHERE Name = 'TemporaryPersonAccount' LIMIT 1];
        
        if ( !accountList.isEmpty()) {
            persona.parentId = accountList[0].Id;
        }
        
        /*String TemporaryPersonAccountID = '';
        AFKLM_SocialCustomerService__c custSettings = AFKLM_SocialCustomerService__c.getValues('TemporaryPersonAccount');
        TemporaryPersonAccountID = custSettings.SCS_TemporaryPersonAccount__c;

		if (TemporaryPersonAccountID != null) {
            persona.parentId = TemporaryPersonAccountID;
        }*/
        
        insert persona;
    }
}