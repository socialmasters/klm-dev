/**
 * @author (s)    :	David van 't Hooft, m.lapere@gen25.com
 * @description   : Apex Batch scheduler for the class LtcFoxServiceBatch
 *
 * @log: 	12May2014: version 1.0
 *          2015/04/13: version 1.1 (m.lapere@gen25.com) adapted to utilize QueueItem__c and LtcQueueItemRunner
 */
	
global class LtcFoxServiceSchedulable implements Schedulable {

	private static final Integer SCOPE_SIZE = 100;
    private static final String CLASS_NAME = 'LtcFoxServiceBatch';
	private static final Integer FROM_DAY = -2; // e.g. -1 = yesterday. inclusive!
	private static final Integer UNTIL_DAY = 2; // e.g. 1 = tomorrow. inclusive!

    global void execute(SchedulableContext scMain) {

    	// create a QueueItem for each day between FROM_DAY and UNTIL_DAY (inclusive)
    	QueueItem__c[] toInsert = new QueueItem__c[]{};
    	for (Integer d = FROM_DAY; d <= UNTIL_DAY; d++) {
            toInsert.add(LtcFoxServiceSchedulable.createQueueItem(d));
    	}
        insert toInsert;
    }

    public static QueueItem__c createQueueItem(Integer param) {
        // create a single QueueItem
        Time midnight = Time.newInstance(0, 0, 0, 0);
        Datetime tomorrow = Datetime.newInstance(Date.today().addDays(1), midnight);
        return new QueueItem__c(
            Class__c = CLASS_NAME,
            Parameters__c = String.valueOf(param),
            ScopeSize__c = SCOPE_SIZE,            
            Run_Before__c = tomorrow
        );
    }
}