/**
 * @author (s)    : Archana Murthy Wuntakal Laxman, David van 't Hooft
 * @requirement id:     
 * @description   : Batch Scheduler Class to delete transscripts 
 */ 	
global class DeleteWebChatTranscriptsScheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        DeleteWebChatTranscripts delBatch = new DeleteWebChatTranscripts();
        Id BatchProcessId = Database.ExecuteBatch(delBatch, 10000);
        System.debug('records deleted : ' + BatchProcessId );
    }
}