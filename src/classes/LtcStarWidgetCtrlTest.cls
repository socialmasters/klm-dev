/**
 * This class contains unit tests for the star widget controller
 */
@isTest
private class LtcStarWidgetCtrlTest {

    static testMethod void getScript() {
        LtcStarWidgetCtrl ctr = new LtcStarWidgetCtrl();
        System.assert(ctr.getScript().startsWith('define'));
    }
}