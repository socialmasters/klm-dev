/**
 * @author (s)    	: David van 't Hooft
 * @requirement id  : 
 * @description   	: Scheduler class for the SocialPostDeletebatch
 * @log:            : 20JUL2015 v1.0 
 */
global class SocialPostDeleteScheduler implements Schedulable {
  global void execute(SchedulableContext sc) {
    SocialPostDeleteBatch socialPostDeleteBatchTmp = new SocialPostDeleteBatch();
    Database.executeBatch(socialPostDeleteBatchTmp, 200);
  }
}