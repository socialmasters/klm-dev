/********************************************************************** 
 Name:  AFKLM_SCS_SocialPostController
 Task:    N/A
 Runs on: SCS_SocialPostListConsole
====================================================== 
Purpose: 
    Social Post Controller Class to utilize existing List View in Apex with Pagination Support.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      16/01/2014      Initial Development
    1.1     Karan Shekhar       22/01/2014      Fixed the total number of Social Post and reset of the filter
            Vikram Rathore      22/01/2014      Fixed the opening of tabs in SF Console during creation of case
                                                Recommendation: use utility class for 'Create classes' as such
    1.2     DvtH
***********************************************************************/
global /* with sharing */ class AFKLM_SCS_SocialPostController{
    public Double totalPageNo;
    public AFKLM_SCS_SocialPostController(){}
    @TestVisible private transient List<AFKLM_SCS_SocialPostWrapperCls> socPostList = new List<AFKLM_SCS_SocialPostWrapperCls>();
    
    public String socPostFilterId {get;set;}
    public String newCaseId {get;set;}
    public String newCaseNumber {get;set;}
    public Boolean error {get; set;}
    public String selectedCaseId { get; set; }
    public Boolean autoRefresh {get;set;}
    public Boolean selectAll {get;set;}
    private Integer pageSizeValue = 0;

    public User user {
        get{
            if(user == null){
                user = [SELECT CompanyName FROM User WHERE Id = :UserInfo.getUserId()];
            }

            return user;
        }
        set;
    }

    @TestVisible private String fieldValues {
        get{

            if(fieldValues == null) {
                if('AirFrance'.equals(user.CompanyName)) {

                    fieldValues = 'Elite__c';
                } else {
                    fieldValues = 'Posted';
                }
            }

            return fieldValues;
        }
        set;
    }
    private String orderType = 'ASC';

    public String pagegoto {get;set;}

    public String addFilter {get;set;}

    public Set<ID> spids = new Set<Id>();
    public String filter;

    public List<SocialPost> listSocialPosts = new List<SocialPost>();
    @TestVisible private Set<String> casesOverviewViews = new Set<String>();

    public AFKLM_SCS_SocialPostController(ApexPages.StandardSetController c) {}
    public List<MetadataService.ListView> listViews = null;
    public List<SelectOption> listViewOptions = null;
    @TestVisible private MetaDataParser mdp = new MetaDataParser('SocialPost');
    
    /**
    * The StandardSetController for the Social Post based on query
    */
    public ApexPages.StandardSetController SocPostSetController {
        get{
            if(SocPostSetController == null) {
                AFKLM_SocialCustomerService__c customSettingsSCS = AFKLM_SocialCustomerService__c.getOrgDefaults();
                List<SocialPost> socialPost = new List<SocialPost>();
                pageSizeValue = Integer.valueof(customSettingsSCS.SCS_SocialPostListSize__c);
                
                // Get the 'Custom Settings' query and pagesize to use                
                String customSettingBaseQuery = customSettingsSCS.SCS_SocialPostQuery__c;
                String customSettingFilterId = customSettingsSCS.SCS_SocialPostFilterId__c;
                //@DvtH
                readListViews();
                buildListViewOptionsList();

                String queryFilter = mdp.getQueryFilter(socPostFilterId);

                AFKLM_SocialCustomerService__c casesOverview = AFKLM_SocialCustomerService__c.getValues('Settings');
                if(casesOverview.SCS_CasesOverViewViews__c != null){
                    casesOverviewViews.addAll(casesOverview.SCS_CasesOverviewViews__c.split(';',casesOverview.SCS_CasesOverviewViews__c.length()));
                }
                if(casesOverview.SCS_CasesOverViewViews2__c != null){
                    casesOverviewViews.addAll(casesOverview.SCS_CasesOverviewViews2__c.split(';',casesOverview.SCS_CasesOverviewViews2__c.length()));
                }

                if(addFilter != null && addFilter != '') {
                    //queryFilter = queryFilter + ' AND SCS_CaseNumber__c LIKE \'%'+addFilter+'%\'';

                    Integer fl = addFilter.length();
                    if(fl < 8) {
                        addFilter = '0'.repeat( 8 - fl ) + addFilter; 
                    }
                    queryFilter = queryFilter + ' AND SCS_CaseNumber__c=\''+addFilter+'\'';
                }

                filter = queryFilter;
                System.debug('************* queryFilter:'+queryFilter);

                if(casesOverviewViews.contains(socPostFilterId)){
                    
                    checkSocialPosts();
                    if(spids != null){

                        customSettingBaseQuery = 'SELECT Id,SCS_CaseNumber__c,Handle,MessageType,Content,SLA__c,Elite__c,Influencers_SP__c,TopicProfileName,SCS_MarkAsReviewed__c,Persona.Followers,Persona.NumberOfFriends,Social_Media_Image__c,Persona.ExternalPictureURL,Status,SCS_Status__c,Reply_speed__c,ParentId,Provider,Posted,CreatedDate,Name FROM SocialPost WHERE ID IN: spids ORDER BY '+fieldValues+' '+orderType+', Posted ASC LIMIT 10000';
                    }
                } else {
                    customSettingBaseQuery = 'SELECT Id,SCS_CaseNumber__c,Handle,MessageType,Content,SLA__c,Elite__c,Influencers_SP__c,TopicProfileName,SCS_MarkAsReviewed__c,Persona.Followers,Persona.NumberOfFriends,Social_Media_Image__c,Persona.ExternalPictureURL,Status,SCS_Status__c,Reply_speed__c,ParentId,Provider,Posted,CreatedDate,Name FROM SocialPost '+queryFilter+' ORDER BY '+fieldValues+' '+orderType+', Posted ASC LIMIT 10000';
                }
                System.debug('************* Custom Based query: '+customSettingBaseQuery);
                
                SocPostSetController = new ApexPages.StandardSetController(Database.getQueryLocator(customSettingBaseQuery));
                SocPostSetController.setPageSize(pageSizeValue); //DvtH
            }
            return SocPostSetController;
        }set;
    }

    public Integer currentPageNumber {
        get {
            return SocPostSetController.getPageNumber();
        }
        set;
    }

    //@DvtH>
    //<@DvtH
    /**
    * Getting the Social Post as a wrapper to add the isSelected parameter and display on page.
    * Afterwards use the Comparable interface when doing the sort()
    *
    * @return List<AFKLM_SCS_SocialPostWrapperCls> a List of SocialPost wrapped with 'isSelected' field
    */
    public List<AFKLM_SCS_SocialPostWrapperCls> getSocialPosts() {
        socPostList = new List<AFKLM_SCS_SocialPostWrapperCls>();

        if(socPostList != null) {
            for(SocialPost c : (List<SocialPost>)SocPostSetController.getRecords()) {
                socPostList.add(new AFKLM_SCS_SocialPostWrapperCls(c, SocPostSetController));
            }
        }

        // socPostList.sort();
        return socPostList;
    }

    private void checkSocialPosts(){

        Map<id, SocialPost> spMap = new Map<Id, SocialPost>();
        Map<String, DateTime> dateMap = new Map<String, DateTime>();
        dateMap.clear();
        spids.clear();

        List<SocialPost> lsp = new List<SocialPost>();
        Set<String> casenumbers = new Set<String>();
        Datetime deleteValue;

        system.debug('--+ filter: ' + filter);
        if(filter != null){

            lsp = database.query('SELECT Id, SCS_CaseNumber__c, Posted FROM SocialPost '+filter+' ORDER BY Id ASC LIMIT 10000');
        }

        for(SocialPost sp : lsp){

            if(!dateMap.containsKey(sp.SCS_CaseNumber__c)){

                dateMap.put(sp.SCS_CaseNumber__c, sp.Posted);
                spids.add(sp.id);
            }
        }
    }
   
    /** 
    * Create cases for single/multiple Social Posts when 'Create Case' button is selected
    *
    * @return PageReference to create cases page if selected else return nothing
    */
    public PageReference createCases() {
        String existingCaseNumber = '';
        String tempSocialHandle = '';
        String selectedRecords = '';
        Integer MAX_NR_OF_CASES = 1;
        Integer numberSelectedSocialPosts = 0;
        Integer countSelectedRecords = 0;
        Integer pagegoto = currentPageNumber;

        for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost: socPostList){
            if(wrapperSocialPost.isSelected == true) {
                countSelectedRecords++;
            }
        }

        if(countSelectedRecords == 1) {
            for(AFKLM_SCS_SocialPostWrapperCls wrapSelectedSocialPost: socPostList) {
                if(wrapSelectedSocialPost.isSelected == true){
                    // Selected records: Saves the selected SocialPost with existing Case number for Twitter to use on multi attach
                    if(wrapSelectedSocialPost.cSocialPost.ParentId != null && ('Twitter'.equals(wrapSelectedSocialPost.cSocialPost.Provider) || 'WeChat'.equals(wrapSelectedSocialPost.cSocialPost.Provider) /*|| 'WhatsApp'.equals(wrapSelectedSocialPost.cSocialPost.Provider)*/)){
                        existingCaseNumber = wrapSelectedSocialPost.cSocialPost.ParentId; 
                        
                    }
                    
                    // Currently only one Case number can be created
                    tempSocialHandle = wrapSelectedSocialPost.cSocialPost.Handle;
                    selectedRecords = wrapSelectedSocialPost.cSocialPost.Id;
                    this.newCaseId = wrapSelectedSocialPost.cSocialPost.ParentId;
                }
            }
            
            // Check if records has been selected else do nothing
            if(!''.equals(selectedRecords)) {
                Map<String, SocialPost> postMap = new Map<String, SocialPost>();
                this.error = false;

                try {
                    List<SocialPost> listSocialPosts = new List<SocialPost>();
                    for( SocialPost post : [SELECT Id, SCS_Status__c, SCS_CaseNumber__c, Name,MessageType, ParentId, ReplyToId, Content, Headline, Persona.ParentId, Provider, SCS_MarkAsReviewed__c FROM SocialPost WHERE Id = :selectedRecords FOR UPDATE] ) {                       
                    // Verify if the ParentId already has been created with dual screen
                        if('Twitter'.equals(post.Provider)){
                            AFKLM_SCS_SocialPostTwitterController twitterController = new AFKLM_SCS_SocialPostTwitterController();
                            this.newCaseId = twitterController.createCasesTwitter(socPostList, postMap, error, SocPostSetController, this.newCaseId, this.newCaseNumber, existingCaseNumber, listSocialPosts, post, tempSocialHandle);
                            twitterController = null;
                        } else if('Facebook'.equals(post.Provider)){

                            AFKLM_SCS_SocialPostFacebookController facebookController = new AFKLM_SCS_SocialPostFacebookController();
                            if(casesOverviewViews.contains(socPostFilterId)){
                                
                                this.newCaseId = facebookController.createCasesFacebookCaseView(socPostList, postMap, error, SocPostSetController, this.newCaseId, this.newCaseNumber, listSocialPosts, post, tempSocialHandle);
                            } else {

                                this.newCaseId = facebookController.createCasesFacebook(socPostList, postMap, error, SocPostSetController, this.newCaseId, this.newCaseNumber, listSocialPosts, post, tempSocialHandle);
                            }

                            facebookController = null;
                        } else if('WeChat'.equals(post.Provider)){
                            AFKLM_SCS_SocialPostWeChatController weChatController = new AFKLM_SCS_SocialPostWeChatController();
                            this.newCaseId = weChatController.createCasesWeChat(socPostList, postMap, error, SocPostSetController, this.newCaseId, this.newCaseNumber, existingCaseNumber, listSocialPosts, post, tempSocialHandle);
                            weChatController = null;
                        } /*else if('WhatsApp'.equals(post.Provider)){
                            system.debug('--+ Whatsapp: '+post);
                            AFKLM_SCS_SocialPostWhatsAppController whatsAppController = new AFKLM_SCS_SocialPostWhatsAppController();
                            this.newCaseId = whatsAppController.createCasesWhatsApp(socPostList, postMap, error, SocPostSetController, this.newCaseId, this.newCaseNumber, existingCaseNumber, listSocialPosts, post, tempSocialHandle);
                            whatsAppController = null;
                        }*//*else if('Other'.equals(post.Provider)){
                            AFKLM_SCS_SocialPostMessengerController messengerController = new AFKLM_SCS_SocialPostMessengerController();
                            this.newCaseId = messengerController.createCasesMessenger(socPostList, postMap, error, SocPostSetController, this.newCaseId, this.newCaseNumber, existingCaseNumber, listSocialPosts, post, tempSocialHandle);
                            messengerController = null;
                        } else if('SinaWeibo'.equals(post.Provider)){
                            AFKLM_SCS_SocialPostSinaWeiboController weiboController = new AFKLM_SCS_SocialPostSinaWeiboController();
                            this.newCaseId = weiboController.createCasesSinaWeibo(socPostList, postMap, error, SocPostSetController, this.newCaseId, this.newCaseNumber, listSocialPosts, post);
                            weiboController = null;
                        }*/
                    }

                    update listSocialPosts; 
                } catch (Exception e){
                    this.error = true;
                    ApexPages.addmessage( new ApexPages.message( ApexPages.severity.ERROR, e.getMessage() ) );
                }

                this.SocPostSetController = SocPostSetController;
                this.SocPostSetController.setPageNumber( pagegoto );
                return null;   
            }
        } else {

            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.INFO, 'Please only select one record for creating one Case.') );
        }
        
        this.newCaseId = '';
        socPostList = null;
        SocPostSetController.setPageNumber( pagegoto );
        return null;
    }

    /** 
    *  It uses a Helper class to make the code more compact
    *
    * Update single/multiple Social Posts when 'Reviewed' button is selected
    *  Update the field SCS_MarkAsReviewed__c to 'true'
    *  Update the field SCS_MarkAsReviewedBy__c to '<Firstname>' '<LastName>'
    *  Update the field OwnerId to the user selected the 'Reviewed' button
    *
    * @return PageReference
    */
    public PageReference markAsReviewed() {
        AFKLM_SCS_ControllerHelper controllerHelper = new AFKLM_SCS_ControllerHelper();
        Integer pagegoto = currentPageNumber;
        List<String> currentCaseNumber = new List<String>();

        if(casesOverviewViews.contains(socPostFilterId)){
            for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost: socPostList){
                if(wrapperSocialPost.isSelected == true) {
                    currentCaseNumber.add(wrapperSocialPost.cSocialPost.SCS_CaseNumber__c);
                }
            }

            listSocialPosts = [SELECT id,SCS_MarkAsReviewed__c,SCS_MarkAsReviewedBy__c,SCS_Status__c,Ignore_for_SLA__c,OwnerId,SCS_Spam__c FROM SocialPost WHERE SCS_CaseNumber__c IN :currentCaseNumber AND (SCS_Status__c != 'Mark as reviewed' OR SCS_Status__c != 'Actioned')];

            controllerHelper.markReviewed(socPostList, listSocialPosts, false);
        } else {

            controllerHelper.markReviewed(socPostList, listSocialPosts, false);
        }

        socPostList = null;
        SocPostSetController = null;
        controllerHelper = null;

        this.SocPostSetController.setPageNumber( pagegoto );
        return null;
    }

    /** 
    *  It uses a Helper class to make the code more compact
    *
    * Update single/multiple Social Posts when 'Spam' button is selected
    *  Update the field SCS_MarkAsReviewed__c to 'true'
    *  Update the field SCS_Spam__c to 'true'
    *  Update the field SCS_MarkAsReviewedBy__c to '<Firstname>' '<LastName>'
    *  Update the field OwnerId to the user selected the 'Reviewed' button
    *
    * @return PageReference
    */
    public PageReference markAsSpam() {
        AFKLM_SCS_ControllerHelper controllerHelper = new AFKLM_SCS_ControllerHelper();
        Integer pagegoto = currentPageNumber;
        List<String> currentCaseNumber = new List<String>();

        if(casesOverviewViews.contains(socPostFilterId)){
            for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost: socPostList){
                if(wrapperSocialPost.isSelected == true) {
                    currentCaseNumber.add(wrapperSocialPost.cSocialPost.SCS_CaseNumber__c);
                }
            }

            listSocialPosts = [SELECT id,SCS_MarkAsReviewed__c,SCS_MarkAsReviewedBy__c,SCS_Status__c,Ignore_for_SLA__c,OwnerId,SCS_Spam__c FROM SocialPost WHERE SCS_CaseNumber__c IN :currentCaseNumber AND (SCS_Status__c != 'Mark as reviewed' OR SCS_Status__c != 'Actioned')];

            controllerHelper.markReviewed(socPostList, listSocialPosts, true);
        } else {
            controllerHelper.markReviewed(socPostList, listSocialPosts, true);
        }

        socPostList = null;
        SocPostSetController = null;
        controllerHelper = null;

        this.SocPostSetController.setPageNumber( pagegoto );
        return null;
    }

    public PageReference changeToFlyingBlue(){
        AFKLM_SocialCustomerService__c flyingBlueQueueId = AFKLM_SocialCustomerService__c.getValues('Settings');

        Map<String, SocialPost> postMap = new Map<String, SocialPost>();
        AFKLM_SCS_ControllerHelper controllerHelper = new AFKLM_SCS_ControllerHelper();
        String newCaseId = '';
        String newCaseNumber = '';
        List<Case> casesToUpdate = new List<Case>(); 
        List<Case> handledCases = new List<Case>();
        Set<String> returnedCaseIds = new Set<String>();
        Set<SocialPost> setSocialPosts = new Set<SocialPost>(); 
        List<SocialPost> socialPostsToUpdate = new List<SocialPost>();  
        Set<SocialPost> excludeSocialPosts = new Set<SocialPost>();  
        Set<SocialPost> selectedSocialPosts = new Set<SocialPost>(); 
        Set<SocialPost> reviewedSocialPosts = new Set<SocialPost>();
        Set<ID> caseIdsSet = new Set<ID>();
        Set<String> existingCases = new Set<String>();
        //Map<String, String> tmpHandle = new Map<String, String>();
        Set<String> tmpHandle = new Set<String>();
        Boolean isNew = false;
        //Case flyingBlueCase = new Case();

        //List<String> currentCaseNumber = new List<String>();
        Set<String> caseIds = new Set<String>();

        Integer pagegoto = currentPageNumber;
                          
        // Get list of selected
        for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : socPostList){

            if(wrapperSocialPost.isSelected == true) {
                if(!('Actioned').equals(wrapperSocialPost.cSocialPost.SCS_Status__c) && !('Mark as reviewed').equals(wrapperSocialPost.cSocialPost.SCS_Status__c)){
                    selectedSocialPosts.add(wrapperSocialPost.cSocialPost);
                } else {
                    reviewedSocialPosts.add(wrapperSocialPost.cSocialPost);
                     //return null;
                }
            }
        }

        if(selectedSocialPosts.size() <= 5){
            for(SocialPost selSocialPost : selectedSocialPosts){

                postMap.put(selSocialPost.id, selSocialPost);

                if(selSocialPost.ParentId != null){
                    if(!existingCases.contains(selSocialPost.ParentId)){
                        existingCases.add(selSocialPost.ParentId);
                    }
                }
                
                //update selected social posts
                selSocialPost.SCS_Status__c = 'Actioned';
                selSocialPost.isFlyingBlue__c = true;
                selSocialPost.OwnerId = userInfo.getUserId();

                returnedCaseIds.addAll(controllerHelper.createNewCases(postMap, newCaseId, newCaseNumber, false));

                if(casesOverviewViews.contains(socPostFilterId)){
                    caseIds.add(selSocialPost.ParentId);
                    excludeSocialPosts.add(selSocialPost);
                }

                for(AFKLM_SCS_SocialPostWrapperCls wrapSocialPost : socPostList){
                    if(wrapSocialPost.cSocialPost.Handle == selSocialPost.Handle && wrapSocialPost.cSocialPost.id != selSocialPost.Id && !('Actioned').equals(wrapSocialPost.cSocialPost.SCS_Status__c) && !('Mark as reviewed').equals(wrapSocialPost.cSocialPost.SCS_Status__c)){//&& wrapSocialPost.cSocialPost.ParentId == null){
                        //if(wrapperSocialPost.isSelected == true)
                        wrapSocialPost.cSocialPost.SCS_Status__c = 'Mark as reviewed';
                        wrapSocialPost.cSocialPost.isFlyingBlue__c = true;
                        wrapSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                        wrapSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                        wrapSocialPost.cSocialPost.OwnerId = userInfo.getUserId();

                        if(wrapSocialPost.cSocialPost.ParentId == null){
                            wrapSocialPost.cSocialPost.ParentId = selSocialPost.ParentId;
                        }

                        if(!caseIdsSet.contains(wrapSocialPost.cSocialPost.id)){
                            caseIdsset.add(wrapSocialPost.cSocialPost.id);
                            
                            if(!setSocialPosts.contains(wrapSocialPost.cSocialPost)){
                                setSocialPosts.add(wrapSocialPost.cSocialPost);
                            }
                        }
                    }
                }
             
                if(!returnedCaseIds.contains(selSocialPost.ParentId)){
                    returnedCaseIds.add(selSocialPost.ParentId);
                }
            
                if(!caseIdsSet.contains(selSocialPost.id)){
                    caseIdsSet.add(selSocialPost.id);

                    if(!setSocialPosts.contains(selSocialPost)){
                        setSocialPosts.add(selSocialPost);
                    }
                }
            
            }
        } else {
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'Too many related cases, please select social posts related to max 5 different cases.') );
            return null;
        }
        

        if(!caseIds.isEmpty() && caseIds.size()>0){

            List<SocialPost> lsp = [SELECT id, SCS_Status__c, isFlyingBlue__c, Ignore_for_SLA__c, SCS_MarkAsReviewed__c, OwnerId FROM SocialPost WHERE ParentId IN: caseIds AND Id NOT IN: excludeSocialPosts ];

            if(!lsp.isEmpty() && lsp.size()>0){
                for(SocialPost sp : lsp){
                    sp.SCS_Status__c = 'Mark as reviewed';
                    sp.isFlyingBlue__c = true;
                    sp.Ignore_for_SLA__c = true;
                    sp.SCS_MarkAsReviewed__c = true;
                    sp.OwnerId = userInfo.getUserId();

                    if(!setSocialPosts.contains(sp)){
                        setSocialPosts.add(sp);
                    }
                }
            }
            

            returnedCaseIds.addAll(caseIds);
        }

        handledCases = [SELECT id, OwnerId, Status FROM Case WHERE id IN: returnedCaseIds];
        List<SocialPost> listPostToCheck = [SELECT id, Status, CreatedDate FROM SocialPost WHERE parentId IN: existingCases AND Id NOT IN: postMap.keySet() ORDER BY CreatedDate DESC];
        //List<Group> flyingBlueQueue = [SELECT id FROM Group WHERE DeveloperName =: 'SCS_KLM_FlyingBlue' AND Type = 'Queue' LIMIT 1];

        if(!handledCases.isEmpty() && handledCases.size() > 0){
            for(Case cs : handledCases){
               if(existingCases.contains(cs.id)){
                    isNew = checkIsNew(listPostToCheck,selectedSocialPosts,cs);
                    if(isNew){
                        //'waiting internal answer Flying Blue'
                        cs.OwnerId = flyingBlueQueueId.SCS_FlyingBlueQueueId__c;
                        cs.Status = 'Waiting for Internal answer Flying Blue';
                    } else {
                        cs.OwnerId = flyingBlueQueueId.SCS_FlyingBlueQueueId__c;
                        cs.Status = 'New'; 
                    }
                } else {
                    //newly created case
                    //cs.OwnerId = flyingBlueQueue[0].id;
                    cs.OwnerId = flyingBlueQueueId.SCS_FlyingBlueQueueId__c;
                    cs.Status = 'New';

                }

                casesToUpdate.add(cs);
            }
        }

        if((!setSocialPosts.isEmpty() && setSocialPosts.size() > 0) && (!casesToUpdate.isEmpty() && casesToUpdate.size() > 0)){
                socialPostsToUpdate.addAll(setSocialPosts);
            try{
                update casesToUpdate;
                update socialPostsToUpdate;
            } catch (Exception ex) {
                System.debug('^^^AFKLM_SCS_SocialPostController - change To flying blue: Exception in updating Social Post: '+ex.getMessage());
            }
        }

        if(!reviewedSocialPosts.isEmpty() && reviewedSocialPosts.size() > 0){

            
            ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'Please note, that some social posts were not changed to "Flying Blue" ') );
            return null;
        }
        
        
        //controllerHelper.changeToFlyingBlue(socPostList, SocPostSetController, tmpHandle, caseIds);
        
        socPostList = null;
        SocPostSetController = null;
        controllerHelper = null;
        SocPostSetController.setPageNumber( pagegoto );
        return null;        
    }

    private Boolean checkIsNew(List<SocialPost> listPostToCheck, Set<SocialPost> selectedSocialPosts, Case cs){
        //Set<SocialPost> setOfPostsToCheck = new Set<SocialPost>();

       /* for(SocialPost socialPostToCheck : listPostToCheck){
            if(!setOfPostsToCheck.contains(socialPostToCheck.PersonaId)){
                setOfPostsToCheck.add(socialPostToCheck);
            }
        }*/

        for(SocialPost sp : selectedSocialPosts){
            if(sp.ParentId == cs.Id){
                for(SocialPost spost : listPostToCheck){
                    if(sp.ParentId == spost.ParentId && sp.CreatedDate > spost.CreatedDate && sp.Id != spost.Id && spost.Status == 'Replied'){
                        return true;
                    } /*else {
                        return false;
                    }*/
                }
            }
        }

        return false;

    }

    /** 
    * Get the standard Social Post List Views options
    *
    * @return SelectOption[] of List Views
    */
    public SelectOption[] getSocialPostExistingViews() {
        return listViewOptions;
    }

    /** 
    * Filter results based on search
    */
    public void filterSocialPostView() {

        SocPostSetController = null;
        this.SocPostSetController = SocPostSetController;
        socPostList.clear();
    }

    public PageReference resetCaseFilter(){
        addFilter = null;
        SocPostSetController = null;
        this.SocPostSetController = SocPostSetController;
        socPostList.clear();

        return null;
    }

    /** 
    * Refresh the Social Post View when 'refresh' is selected
    */
    public void refreshSocialPostView() {
        autoRefresh = true;
        selectAll = false;

        Integer pno = pageNumber;

        SocPostSetController = null;
        this.SocPostSetController = SocPostSetController;
        socPostList.clear();

        this.SocPostSetController.setPageNumber(pno);
    }
    
    public List<SelectOption> getFieldItems() {
        List<SelectOption> options = new List<SelectOption>();        
        
        if('AirFrance'.equals(user.CompanyName)) {
            options.add(new SelectOption('Elite__c','Priority Order'));
        }        

        options.add(new SelectOption('Posted','Posted'));
        options.add(new SelectOption('MessageType','Message Type'));
        options.add(new SelectOption('Handle','Handle'));
        options.add(new SelectOption('SCS_CaseNumber__c','Case Number'));
        options.add(new SelectOption('Language__c','Language'));

        return options;
    }
    
    public String getFieldValues() {
        return fieldValues;
    }

    public void setFieldValues(String fieldValues) {
        
        if('Elite__c'.equals(fieldValues)) {
            this.orderType = 'DESC'; 
            this.fieldValues = fieldValues+' NULLS LAST, Influencers_SP__c';
        } else {
            this.orderType = 'ASC';
            this.fieldValues = fieldValues;
        }
    }
    
    /**
    * Reset List View
    *
    * @return null
    */
    public PageReference resetFilter() {
        autoRefresh = false;
        selectAll = false;
        SocPostSetController = null;
        return null;
    }
    
    /**
    * Navigate to the First Page
    */
    public void firstPage() {
        selectAll = false;
        SocPostSetController.first();
    }

    /**
    * Navigate to the First Page
    */
    public void lastPage() {
        autoRefresh = false;
        selectAll = false;
        SocPostSetController.last();
    }

    /**
    * Navigate to the Next Page
    */
    public void next() {
        autoRefresh = false;
        selectAll = false;
        if(SocPostSetController.getHasNext()) {
            SocPostSetController.next();
        }
    }

    /**
    * Navigate to the Previous Page
    */
    public void prev() {
        selectAll = false;
        if(SocPostSetController.getHasPrevious()) {
            SocPostSetController.previous();
        }
    }

    /**
    * Navigate to the specific page
    */
    public void goToPage() {
        autoRefresh = false;
        selectAll = false;

        pagegoto = Apexpages.currentPage().getParameters().get('pagegoto');
        // remove all non-numeric characters
        pagegoto = pagegoto.replaceAll('[^0-9]','');

        SocPostSetController.setPageNumber( integer.valueof(pagegoto) );
    }
    
    /**
    * Calculate page number and total number of records fetched by SOQL
    */
    public Integer pageNumber {
        get {
            return SocPostSetController.getPageNumber();
        }
        set;
    }

    public Integer count {
        get {
            return SocPostSetController.getResultSize();
        }
        set;
    }
    
    public String getCustomMessage{
        get{
          String FloatingMessage = 'FloatingMessage';
            AFKLM_SocialCustomerService__c custSettings = AFKLM_SocialCustomerService__c.getValues(FloatingMessage);
            String customMessage = custSettings.SCS_FloatingMessage__c;
            return customMessage;
        }set;
    }

    /**
    * Get the total number of pages in pagination
    */
    public Integer getTotalPageNo() {
        totalPageNo = Double.valueOf(SocPostSetController.getResultSize()) / Double.valueOf(pageSizeValue);
        Integer totalPageNum;
        Integer tempTotalPageNo = Integer.valueOf(totalPageNo);
        
        if(totalPageNo > tempTotalPageNo){
            totalPageNum = tempTotalPageNo + 1;
        } else {
            totalPageNum = tempTotalPageNo;
        }
        
        if(totalPageNo <= 1){
            totalPageNum = 1;
        }
        
        return totalPageNum;
    }

    public integer getSize() {
        return pageSizeValue;
    }

    public void setSize(integer size) {
        this.pageSizeValue = pageSizeValue;
    }
    
    // https://developer.salesforce.com/forums?id=906F0000000988zIAA
    // http://wiki.developerforce.com/page/Creating_Compound_Views_Using_Visualforce
    // http://salesforce.stackexchange.com/questions/5553/why-is-the-view-command-link-working-only-the-first-time-i-click-it-how-do-i
    public PageReference getCasenumber() {
        // String caseId = ApexPages.currentPage().getParameters().get('caseId');
        selectedCaseId = [SELECT Id FROM Case WHERE Id = :ApexPages.currentPage().getParameters().get('caseId')].Id;
        
        return null;
    }
    
    /**
     * read the SocialPost custom object to read the listview metadata
     * only do this ones duuring its livecycle 
     **/
    @TestVisible private void readListViews() {
        if(!Test.isRunningTest()) {
            this.listViews =  mdp.readListViews();
        }
    }   
    
    /**
     * Get All non private listViews and sort them by label and not API fullName
     */
    @TestVisible private void buildListViewOptionsList() {
        if (listViewOptions==null) { 
            List<SelectOption> tmplistViewOptions = new List<SelectOption>();
            listViewOptions = new List<SelectOption>();
            
            // Cannot execute Websrv callout on Test classes
            if(Test.isRunningTest()) {
                tmplistViewOptions.add(new SelectOption('Test View Label','Test View FullName'));
                tmplistViewOptions.sort();
                listViewOptions.add(new SelectOption(tmplistViewOptions[0].getLabel(), tmplistViewOptions[0].getValue()));
            } else {
                for(MetadataService.ListView listView : this.listViews) {
                    tmplistViewOptions.add(new SelectOption(listView.label,listView.fullName));
                }
                tmplistViewOptions.sort();          
            
                User user = [SELECT SocialPostViewValues__c FROM User WHERE Id = :UserInfo.getUserId()];
                
                if(user.SocialPostViewValues__c != null) {
                    for(SelectOption tmpSelectOption : tmplistViewOptions) {
                        if(user.SocialPostViewValues__c.contains(tmpSelectOption.getLabel())) {
                            listViewOptions.add(new SelectOption(tmpSelectOption.getLabel(),tmpSelectOption.getValue()));
                        }
                    }
                } else {
                    for(SelectOption tmpSelectOption : tmplistViewOptions) {
                        listViewOptions.add(new SelectOption(tmpSelectOption.getLabel(),tmpSelectOption.getValue()));
                    }
                }
            }
            
            if(listViewOptions.size() > 0) {
                this.socPostFilterId = listViewOptions[0].getValue();
            } else {
                listViewOptions.add(new SelectOption('All Posts. You removed to much','All Posts. You removed to much'));
                this.socPostFilterId = listViewOptions[0].getValue();
            }
        }
    }
}