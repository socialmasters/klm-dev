/**********************************************************************
 Name:  RESTFeedbackControllerTest 
======================================================
Purpose: 

1. Test class for RESTFeedbackController 

======================================================
History                                                            
-------                                                            
VERSION    AUTHOR            DATE          DETAIL                                 
  1.0      Martin van Rhijn  12/10/2012    INITIAL DEVELOPMENT
***********************************************************************/
@isTest
private class RESTFeedbackControllerTest {
    static testMethod void testFeedbackStats() {
        List<RESTFeedbackController.FeedbackStat> feedbackStats = RESTFeedbackController.getFeedbackStats();
        System.assertEquals(0, feedbackStats.size());
        //persist mock feedback object
        Feedback__c fb = new Feedback__c(airportCode__c = 'AMS',
                                       area__c = 'checkin',
                                       subArea__c = 'kiosk',
                                       thumbsUp__c = true,
                                       deviceDetails__c = 'Android...',
                                       keyword__c = 'fast',
                                       // comment__c = '',
                                       fbNumber__c = '',
                                       fbTierLevel__c = '',
                                       feedbackDate__c = System.now());
        Feedback__c fb2 = new Feedback__c(airportCode__c = 'AMS',
                                       area__c = 'checkin',
                                       subArea__c = 'kiosk',
                                       thumbsUp__c = true,
                                       deviceDetails__c = 'Android...',
                                       keyword__c = 'fast',
                                       // comment__c = '',
                                       fbNumber__c = '',
                                       fbTierLevel__c = '',
                                       feedbackDate__c = System.now());
        Feedback__c fb3 = new Feedback__c(airportCode__c = 'AMS',
                                       area__c = 'arrival',
                                       subArea__c = 'arrival',
                                       thumbsUp__c = false,
                                       deviceDetails__c = 'Android...',
                                       keyword__c = 'information',
                                       // comment__c = '',
                                       fbNumber__c = '',
                                       fbTierLevel__c = '',
                                       feedbackDate__c = System.now());
        Feedback__c fb4 = new Feedback__c(airportCode__c = 'YYC',
                                       area__c = 'arrival',
                                       subArea__c = 'arrival',
                                       thumbsUp__c = false,
                                       deviceDetails__c = 'Android...',
                                       // keyword__c = 'information',
                                       // comment__c = '',
                                       fbNumber__c = '',
                                       fbTierLevel__c = '',
                                       feedbackDate__c = System.now());
        Test.startTest();
        insert fb;
        insert fb2;
        insert fb3;
        insert fb4;
        Test.stopTest();
        
        feedbackStats = RESTFeedbackController.getFeedbackStats();
        System.assertEquals(3, feedbackStats.size());
    }
    
    //String airportCode, String area, String subArea, Boolean thumbsUp, String deviceDetails, String keyword, String comment, String fbNumber, String fbTierLevel, DateTime feedbackDate
    static testMethod void testPersistFeedback() {
        Test.startTest();
        RESTFeedbackController.persistFeedback('AMS','checkin','kiosk',true,'iPhone 5...','fast','no comment','1234567890','Platinum',System.now());
        Test.stopTest();
        List<RESTFeedbackController.FeedbackStat> feedbackStats = RESTFeedbackController.getFeedbackStats();
        System.assertEquals(1, feedbackStats.size());
    }
}