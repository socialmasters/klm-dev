/**                     
 *  Response model to return a list of comments and statistics per flightnumber / travel date.
 */ 
global class LtcRatingFlightStatisticsResponseModel {

	public List<Flight> flights = new List<Flight>();
	
    public override String toString() { 
        String result = '{"flights" : ';
        String separator = '';
        if (flights.isEmpty()) {
        	result += 'null';
        } else {
        	result += '[';
        	for (Flight flight : flights) {
        		result += separator;
        		result += flight.toString();
        		separator = ' ,';
        	}
        	result += ']';
        }
        result += '}';
		return result;
    }
	
	global class Flight {
        public String flightNumber;
        public String travelDate;
        public LtcRatingGetModel.RatingStatistics ratingStatistics;
    
        public Flight(String flightNumber, String travelDate, List<Rating__c> ratingList) {
            this.flightNumber = flightNumber;
            this.travelDate = travelDate;
            if (ratingList != null && ratingList.size() > 0) { 
            	this.ratingStatistics = new LtcRatingGetModel.RatingStatistics(ratingList);
            }
        }
        
        public override String toString() { 
            return '{' +
                '"flightNumber": "' + flightNumber +  '", ' +
                '"travelDate": "' + travelDate +  '", ' +
                '"ratingStatistics": ' + (ratingStatistics == null ? 'null' : ratingStatistics.toString()) +  
			'}';
        }
	}
}