/**********************************************************************
 Name:  SocialMonitoringDataControllerTest.cls
=======================================================================
Purpose: Test methods for SocialMonitoringDataController
=======================================================================
History                                                            
-------                                                            
VERSION     AUTHOR                DATE            DETAIL                                 
    1.0     Aard-Jan van Kesteren 26/09/2013      Initial development
***********************************************************************/ 
@isTest
public with sharing class SocialMonitorDataControllerTest {

	static testMethod void testGetJsonData() {
		SocialMonitoringDataController controller = new SocialMonitoringDataController();
		String result = controller.getJsonData();
		System.assertEquals(3 * 6, ((Map<String, Object>)JSON.deserializeUntyped(result)).keySet().size());
	}

}