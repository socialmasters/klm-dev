/**
 * Example class taken from: 
 * http://blog.edlconsulting.com/salesforce-com/template-for-global-configuration-variables-in-apex/
 *
 * Access the configuration variables in your code via the following construct:
 * <code>
 	  GlobalConfigurationVariables.getInstance().maxRatingPeriodSelectionInDays
   </code>
 */
global class GlobalConfigurationVariables {
    global static final String MAX_RATING_PERIOD_SELECTION_IN_DAYS = 'maxRatingPeriodSelectionInDays';
    global static final String FIRST_RATING_PERIOD_SELECTION_IN_DAYS = 'firstRatingPeriodSelectionInDays';
    global static final String MINIMAL_NUMBER_OF_RATINGS = 'minimalNumberOfRatings';
    global static final String RATINGS_FROM_LATER_GROUP = 'ratingsFromLaterGroup';

    global static GlobalConfigurationVariables instance;
 
    global static GlobalConfigurationVariables getInstance() {
        if (instance == null) {
            instance = new GlobalConfigurationVariables();
        }
        return instance;
    }

	global Integer maxRatingPeriodSelectionInDays {get; private set;}
    global Integer firstRatingPeriodSelectionInDays {get; private set;}
    global Integer minimalNumberOfRatings {get; private set;}
    global Integer ratingsFromLaterGroup {get; private set;}

    private GlobalConfigurationVariables() {
        final Map<String,GlobalConfigurationVariable__c> all = GlobalConfigurationVariable__c.getAll();

        maxRatingPeriodSelectionInDays = retrieveInteger(MAX_RATING_PERIOD_SELECTION_IN_DAYS, all);
        firstRatingPeriodSelectionInDays = retrieveInteger(FIRST_RATING_PERIOD_SELECTION_IN_DAYS, all);
        minimalNumberOfRatings = retrieveInteger(MINIMAL_NUMBER_OF_RATINGS, all);
        ratingsFromLaterGroup = retrieveInteger(RATINGS_FROM_LATER_GROUP, all);

        if (maxRatingPeriodSelectionInDays == null || maxRatingPeriodSelectionInDays < 0) {
            maxRatingPeriodSelectionInDays = 365;
        }
        if (firstRatingPeriodSelectionInDays == null || firstRatingPeriodSelectionInDays < 0) {
            firstRatingPeriodSelectionInDays = 30;
        }
        if (minimalNumberOfRatings == null || minimalNumberOfRatings < 0) {
            minimalNumberOfRatings = 9;
        }
        if (ratingsFromLaterGroup == null || ratingsFromLaterGroup < 0) {
            ratingsFromLaterGroup = 9;
        }
    }
    
    private static Integer retrieveInteger(String key, Map<String,GlobalConfigurationVariable__c> all) {
        Integer returnValue = null;
        if (all != null && !isBlank(key) && all.get(key) != null) {
            try{
                if (all.get(key).value__c != null) {
                    returnValue = Integer.valueOf(all.get(key).value__c);
                }
            } catch (System.TypeException ex) {
            	System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
            }
        }
        return returnValue;
    }
    
    private static String retrieveString(String key, Map<String,GlobalConfigurationVariable__c> all) {
        String returnValue = null;
        if (all != null && !isBlank(key) && all.get(key) != null) {
            returnValue = all.get(key).value__c;
        }
        return returnValue;
    }
    
    private static boolean isBlank(String str) {
        return str == null || str.trim() == null || str.trim().length() == 0;
    }

    
}