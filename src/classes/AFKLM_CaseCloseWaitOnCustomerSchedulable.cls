/********************************************************************** 
 Name:  AFKLM_CaseCloseWaitOnCustomerSchedulable
 Task:    N/A
 Runs on: N/A
====================================================== 
Purpose: 
    This schedulable class will be able to schedule the CaseCloseWaitingOnCustomerBatch apex batch.
        e.g. every hour: (Run in anonymous on Force.com or Developer mode)
            AFKLM_CaseCloseWaitOnCustomerSchedulable cwb = new AFKLM_CaseCloseWaitOnCustomerSchedulable();
            String cronStr = '0 0 * * * ?';
            System.schedule('Case Waiting On Customer Close Batch', cronStr, cwb);
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      21/01/2014      Initial Development (based on Jelle Terpstra - jelle.terpstra@accenture.com)
***********************************************************************/
global class AFKLM_CaseCloseWaitOnCustomerSchedulable implements Schedulable {
    global void execute(SchedulableContext scMain) {
        AFKLM_CaseCloseWaitOnCustomerBatch cwb = new AFKLM_CaseCloseWaitOnCustomerBatch();
        ID idBatch = Database.executeBatch(cwb, 100);
        
        AFKLM_CaseCloseWaitOnCustomerAFBatch cwbAf = new AFKLM_CaseCloseWaitOnCustomerAFBatch();
        ID idAfBatch = Database.executeBatch(cwbAf, 100);
    }
}