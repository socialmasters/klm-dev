/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman
 * @description     : Rest API interface for the rating feedback of a flight
 * @log             : 13MAR2014: version 1.0
 * @log             : 21JUL2015: version 2.0 LTC-743 do JSON deserializing try catch block and prevent detailed error reporting to the clients
 * @log             : 27AUG2015: version 2.1 added image url's
 * @log             : 14SEP2015: version 2.2 added max nr of ratings per flight check
 */
@RestResource(urlMapping='/Ratings/*')
global class LtcRatingRestInterface { 
    public static Integer MAX_RATINGS_PER_FLIGHT = 200;

    /**
     * Create a rating
     * Expected Json object:
     * { "rating": {"rating": "2", "positiveComment": "Excellent feedback1", "negativeComment": "Negative feedback1",
     *      "positiveImage": "http://fotosite/posurl.jpg", 
     *      "positiveImageLarge" : "http://fotosite/poslargeurl.jpg", 
     *      "negativeImage" : "", "negativeImageLarge" : "",
     *      "isPublished": "True", 
     *      "flight": {"flightNumber": "KL1234", "travelDate": "2014-01-01"}, 
     *      "passenger": {"firstName": "Kees", "familyName": "Jansen", 
     *      "seat": {"seatNumber": "1A"}}}});     
     **/
    @HttpPost
    global static String doPost(){
        String result = '';
        RestResponse res = RestContext.response;
        try{
            String requestBody = RestContext.request.requestBody.toString();
            System.debug('requestBody=' + requestBody);
            LtcRatingPostModel ratingpm = (LtcRatingPostModel) System.JSON.deserialize(requestBody, LtcRatingPostModel.class);
            result = createRating(ratingpm.rating);
        } catch (Exception ex) {
            res.statusCode = 500;
            if (LtcUtil.isDevOrg()) {
                result = ex.getMessage() + '   '  + ex.getStackTraceString();
            }
            System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        return result;
    }

    global static String createRating(LtcRatingPostModel.Rating rating) {
        String ratingId = '';  
        String response = '';
        RestResponse res = RestContext.response;

        LtcRating ltcRating = new LtcRating();
        if (ltcRating.countRatings(rating.Flight.flightNumber, Date.valueOf(rating.Flight.travelDate)) > MAX_RATINGS_PER_FLIGHT) {
            response = '{"ratingStatus": "Closed"}';
            res.statusCode = 403;
            System.debug(LoggingLevel.ERROR, 'max nr of ratings reached for flight ' + rating.Flight.flightNumber + ' ' + rating.Flight.travelDate); 
        } else {
            String acceptLanguage = RestContext.request.headers.get('Accept-Language');
            LtcFlightInfo ltcFlightInfo = new LtcFlightInfo();
            LtcFlightResponseModel responseModel = ltcFlightInfo.getFlightInfo(rating.Flight.flightNumber, rating.Flight.travelDate, acceptLanguage); 
            if (responseModel.isEmpty()) {
                res.statusCode = 404;
            } else if ('Inactive'.equalsIgnoreCase(responseModel.flight.ratingStatus)) {
                response = '{"ratingStatus": "Inactive"}';
                res.statusCode = 403;
            } else if ('Closed'.equalsIgnoreCase(responseModel.flight.ratingStatus)) {
                response = '{"ratingStatus": "Closed"}';
                res.statusCode = 403;
            }  else {
                ratingId = ltcRating.setRating(rating, acceptLanguage);
                response = '{"rating": {"ratingId":"' + ratingId + '"}}';
            }            
        }
        return response;
    }
    
    /**
     * Update the publish parameter for a specific rating id
     * Expected Json object:
     * { "rating": {"ratingId": "a10110000009a09", "isPublished": "False"}});
     **/
    @HttpPut
    global static String doPut(){
        String result = '';
        RestResponse res = RestContext.response;
        try{
            String requestBody = RestContext.request.requestBody.toString();
            System.debug('requestBody=' + requestBody);
            LtcRatingPutModel ratingPutModel = (LtcRatingPutModel) System.JSON.deserialize(requestBody, LtcRatingPutModel.class);
            result = updateRating(ratingPutModel.rating);
        } catch (Exception ex) {
            res.statusCode = 500;
            if (LtcUtil.isDevOrg()) {
                result = ex.getMessage() + '   '  + ex.getStackTraceString();
            }
            System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        return result;
    }

    global static String updateRating(LtcRatingPutModel.Rating rating){
        String ratingId = '';
        String response = '';  
        try {
            LtcRating ltcRating = new LtcRating();
            ratingId = ltcRating.updateRating(rating);
            response = '{"rating": {"ratingId":"' + ratingId + '"}}';
        } catch(Exception ex) {
            if (LtcUtil.isDevOrg()) {
                response = ex.getMessage() + '   '  + ex.getStackTraceString();
            }
            System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        return response;
    }
 
}