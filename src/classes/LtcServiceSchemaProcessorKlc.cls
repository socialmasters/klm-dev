/**							
 * @author      : Mees Witteman
 * @description : Class to process the static resource LtcServiceSchema (service schema summer / winter)
 *				  and create a ServicePeriod with ServiceItems, LegClassPeriods and LegClassItems
 *                for KLC flights.
 *
 *
 * LtcServiceSchemaProcessorKlc processor = new LtcServiceSchemaProcessorKlc();
 * processor.processServiceSchema();
 *                
 * If you into governer limit, split the process in two parts:
 * LtcServiceSchemaProcessorKlc processor = new LtcServiceSchemaProcessorKlc();
 * processor.createItemsAndPeriods();
 *
 * LtcServiceSchemaProcessorKlc processor = new LtcServiceSchemaProcessorKlc();
 * processor.createLegClassItems();
 *                
 */
public with sharing virtual class LtcServiceSchemaProcessorKlc {
	public ServicePeriod__c servicePeriod;
	
	public Map<String, ServiceItem__c> serviceItems = new Map<String, ServiceItem__c>();
	public CabinClass__c economyClass;
	public CabinClass__c businessClass;
	
	public List<LegClassPeriod__c> legClassPeriods = new List<LegClassPeriod__c>();
	public List<LegClassItem__c> legClassItems = new List<LegClassItem__c>();

	String[] lines;
	
	public Integer flightNrColumn = 0;
	public Integer businessMealColumn = 9; 
	public Integer businessDrinksColumn = 10;
	public Integer economyMealColumn = 11;
	public Integer economyDrinksColumn = 12; 
	public Integer extraServiceColumn = 13;
	public Integer startDateColumn = 1; 
	public Integer endDateColumn = 2; 
	public String itemName;
	public String type;
	
	public LtcServiceSchemaProcessorKlc() {
		initialize();		
		loadCurrentRecords();
		lines = getLinesFromResource();
	}

	// complete import
	public void processServiceSchema() {
		createItemsAndPeriods();
		createLegClassItems();
	}

	// first iteration to insert all ServiceItems and LegClassPeriods
	public void createItemsAndPeriods() {
		String[] record;
		String[] nextRecord;
		LegClassPeriod__c businessClassPeriod;
		LegClassPeriod__c economyClassPeriod;
		ServiceItem__c serviceItem;
		LegClassItem__c legClassItem;
		Integer sortOrder = 0;
		Boolean reachedServiceItems = false;
		String newFlightNumber = '';
		String flightNumber;
		Date startDate;
		Date endDate;
		String extraService;
		Map<String, ServiceItem__c> serviceItemsToInsert = new Map<String, ServiceItem__c>();
		for (Integer i = 0, ls = lines.size(); i < ls; i++) {
			System.debug(Logginglevel.INFO, 'process line ' + lines[i]);
			record = lines[i].split(',');
			System.debug(Logginglevel.INFO, 'record len=' + record.size());
			if (record[flightNrColumn].startswith('KL')) {
				flightNumber = record[flightNrColumn].replaceAll(' ', '');
				if (newFlightNumber != flightNumber) {
					System.debug(Logginglevel.INFO, 'KL line: ' + lines[i]);

					startDate = determineStartDate(i, record, lines);
					endDate = determineEndDate(i, record, lines);

					businessClassPeriod = createLegClassPeriod(flightNumber, startDate, endDate, businessClass);
					legClassPeriods.add(businessClassPeriod);
					economyClassPeriod = createLegClassPeriod(flightNumber, startDate, endDate, economyClass);
					legClassPeriods.add(economyClassPeriod);

					itemName = stripItemName(record[businessMealColumn]);
					type = getTypeFromName(itemName);
					serviceItem = createServiceItem(itemName, type);
					serviceItems.put(serviceItem.name__c, serviceItem);
					if (serviceItem.id == null) {
						serviceItemsToInsert.put(serviceItem.name__c, serviceItem);
					}

					itemName = stripItemName(record[businessDrinksColumn]);
					type = getTypeFromName(itemName);
					serviceItem = createServiceItem(itemName, type);
					serviceItems.put(serviceItem.name__c, serviceItem);
					if (serviceItem.id == null) {
						serviceItemsToInsert.put(serviceItem.name__c, serviceItem);
					}
					itemName = stripItemName(record[economyMealColumn]);
					type = getTypeFromName(itemName);
					serviceItem = createServiceItem(itemName, type);
					serviceItems.put(serviceItem.name__c, serviceItem);
					if (serviceItem.id == null) {
						serviceItemsToInsert.put(serviceItem.name__c, serviceItem);
					}
					itemName = stripItemName(record[economyDrinksColumn]);
					type = getTypeFromName(itemName);
					serviceItem = createServiceItem(itemName, type);
					serviceItems.put(serviceItem.name__c, serviceItem);
					if (serviceItem.id == null) {
						serviceItemsToInsert.put(serviceItem.name__c, serviceItem);
					}
					extraService = record[extraServiceColumn];
					if (String.isNotBlank(extraService)) {
						itemName = stripItemName(extraService);
						type = getTypeFromName(itemName);
						serviceItem = createServiceItem(itemName, type);
						serviceItems.put(serviceItem.name__c, serviceItem);
						if (serviceItem.id == null) {
							serviceItemsToInsert.put(serviceItem.name__c, serviceItem);
						}
					}
				} else {
					System.debug(Logginglevel.INFO, 'skipped line no match' + lines[i]);
				}
			} else {
				System.debug(Logginglevel.INFO, 'skipped line no KL' + lines[i]);
			}
			
			if (legClassPeriods.size() > 200) {
				upsert legClassPeriods;
				legClassPeriods = new List<LegClassPeriod__c>();
			}		
			if (serviceItemsToInsert.size() > 200) {
				insert serviceItemsToInsert.values();
				serviceItemsToInsert = new Map<String, ServiceItem__c>();
			}
			newFlightNumber = record[flightNrColumn].replaceAll(' ', '');
		}
		if (serviceItemsToInsert.size() > 0) {
			insert serviceItemsToInsert.values();
		}
		
		if (legClassPeriods.size() > 0) {
			upsert legClassPeriods;
		}
		
	}

	private Date determineStartDate(Integer j, String[] currentRecord, String[] lines) {
		Date result = parseCsvDate(currentRecord[startDateColumn]);
		Date compareDate;
		String[] nextRecord;
		while (j + 1 < lines.size()) {
			j = j + 1;
			nextRecord = lines[j].split(',');
			if (currentRecord[flightNrColumn] == nextRecord[flightNrColumn]) {
				compareDate = parseCsvDate(nextRecord[startDateColumn]);
				// date that calls is after => negative
				if (result.daysBetween(compareDate) < 0) {
					result = compareDate;
				}
			} else {
				return result;
			}
		}
		return result;
	}
	private Date determineEndDate(Integer j, String[] currentRecord, String[] lines) {
		Date result = parseCsvDate(currentRecord[endDateColumn]);
		Date compareDate;
		String[] nextRecord;
		while (j + 1 < lines.size()) {
			j = j + 1;
			nextRecord = lines[j].split(',');
			if (currentRecord[flightNrColumn] == nextRecord[flightNrColumn]) {
				compareDate = parseCsvDate(nextRecord[endDateColumn]);
				// date that calls is after => negative
				if (result.daysBetween(compareDate) > 0) {
					result = compareDate;
				}
			} else {
				return result;
			}
		}
		return result;
	}
	
	// second iteration to insert all LegClassItem objects
	public void createLegClassItems() {
		loadCurrentRecords();
		String[] record;
		String[] nextRecord;
		LegClassPeriod__c economyClassPeriod;
		LegClassPeriod__c businessClassPeriod;
		ServiceItem__c serviceItem;
		LegClassItem__c legClassItem;
		Boolean reachedServiceItems = false;
		Integer legNr = 0;
		String newFlightNumber = '';
		String flightNumber;
		Integer sortOrder = 0;
		Date startDate;
		Date endDate;
		String extraService;
		for (Integer i = 0, ls = lines.size(); i < ls; i++) {
			System.debug(Logginglevel.INFO, 'process line' + lines[i]);
			record = lines[i].split(',');
			if (record[flightNrColumn].startswith('KL')) {
				flightNumber = record[flightNrColumn].replaceAll(' ', '');
				if (newFlightNumber != flightNumber) {
					startDate = determineStartDate(i, record, lines);
					endDate = determineEndDate(i, record, lines);
					economyClassPeriod = findLegClassPeriod(flightNumber ,legNr, startDate, endDate, economyClass);
					businessClassPeriod = findLegClassPeriod(flightNumber ,legNr, startDate, endDate, businessClass);

					sortOrder = 0;
					itemName = stripItemName(record[businessMealColumn]);
					legClassItem = createLegClassItem(itemName, businessClassPeriod, sortOrder);
					legClassItems.add(legClassItem);
					
					sortOrder = sortOrder + 1;
					itemName = stripItemName(record[businessDrinksColumn]);
					legClassItem = createLegClassItem(itemName, businessClassPeriod, sortOrder);
					legClassItems.add(legClassItem);

					extraService = record[extraServiceColumn];
					if (String.isNotBlank(extraService)) {
						sortOrder = sortOrder + 1;
						itemName = stripItemName(record[extraServiceColumn]);
						legClassItem = createLegClassItem(itemName, businessClassPeriod, sortOrder);
						legClassItems.add(legClassItem);
					}					

					sortOrder = 0;
					itemName = stripItemName(record[economyMealColumn]);
					legClassItem = createLegClassItem(itemName, economyClassPeriod, sortOrder);
					legClassItems.add(legClassItem);

					sortOrder = sortOrder + 1;
					itemName = stripItemName(record[economyDrinksColumn]);
					legClassItem = createLegClassItem(itemName, economyClassPeriod, sortOrder);
					legClassItems.add(legClassItem);

					extraService = record[extraServiceColumn];
					if (String.isNotBlank(extraService)) {
						sortOrder = sortOrder + 1;
						itemName = stripItemName(record[extraServiceColumn]);
						legClassItem = createLegClassItem(itemName, economyClassPeriod, sortOrder);
						legClassItems.add(legClassItem);
					}
				}
			} 
			if (legClassItems.size() > 200) {
				upsert legClassItems;
				legClassItems = new List<LegClassItem__c>();
			}
			newFlightNumber = record[flightNrColumn].replaceAll(' ', '');
		}
		if (legClassItems.size() > 0) {
			upsert legClassItems;
		}
	}
	
	
	private LegClassPeriod__c createLegClassPeriod(String flightNumber, Date startDate, Date endDate, CabinClass__c cabinClass) {
		Integer legNr = 0;
		LegClassPeriod__c lcp = findLegClassPeriod(flightNumber, legNr, startDate, endDate, cabinClass);
		if (lcp == null) {
			lcp = new LegClassPeriod__c();
			lcp.CabinClass__c = cabinClass.id;
			lcp.FlightNumber__c = flightNumber;
			lcp.legNumber__c = legNr;
			lcp.StartDate__c = startDate;
			lcp.EndDate__c = endDate;
			lcp.ServicePeriod__c = servicePeriod.id;
		}
		System.debug(Logginglevel.INFO, 'leg/class/period ' + lcp.flightnumber__c + ' ' + lcp.legnumber__c + ' ' + cabinClass.name + ' ' + lcp.startDate__c  + ' ' + lcp.endDate__c);
		return lcp;
	}
	
	private LegClassPeriod__c findLegClassPeriod(String flightNumber, Integer legNr, Date startDate, Date endDate,  CabinClass__c cabinClass) {
		for (LegClassPeriod__c lcp : legClassPeriods) {
			if (lcp.cabinClass__c.equals(cabinClass.id)
					&& lcp.flightNumber__c.equals(flightNumber)
					&& lcp.legNumber__c == legNr
					&& lcp.StartDate__c.isSameDay(startDate)
					&& lcp.EndDate__c.isSameDay(endDate)) {
				return lcp;
			}
		}
		return null;
	}		
	
	private ServiceItem__c createServiceItem(String serviceItemName, String type) {
		ServiceItem__c si = findServiceItem(serviceItemName);
		if (si == null) {
			si = new ServiceItem__c();
			si.name__c = 'ServiceItem_' + serviceItemName + '_name';
			si.description__c = 'ServiceItem_' + serviceItemName + '_description';
			// log the label translation sheet entries for new created service items
			System.debug(Logginglevel.INFO, 'si:\t\t{');
			System.debug(Logginglevel.INFO, 'si:\t\t\t"@key":"klmmj.' + si.name__c + '",');
			System.debug(Logginglevel.INFO, 'si:\t\t\t"#text":"' + serviceItemName + '"');
			System.debug(Logginglevel.INFO, 'si:\t\t},');
			System.debug(Logginglevel.INFO, 'si:\t\t{');
			System.debug(Logginglevel.INFO, 'si:\t\t\t"@key":"klmmj.' + si.description__c + '",');
			System.debug(Logginglevel.INFO, 'si:\t\t\t"#text":"' + serviceItemName + '"');
			System.debug(Logginglevel.INFO, 'si:\t\t},');
			
		} else {
			System.debug(LoggingLevel.INFO, 'found service item ' + si.name__c + ' ' + si.description__c);
		}
		si.type__c = type;
		return si;
	}
	
	private ServiceItem__c findServiceItem(String serviceItemName) {
		String siName = 'ServiceItem_' + serviceItemName + '_name';
		ServiceItem__c si = serviceItems.get(siName);
		return si;
	}
		
	private LegClassItem__c createLegClassItem(String serviceItemName, LegClassPeriod__c legClassPeriod, Integer order) {
		ServiceItem__c si = findServiceItem(serviceItemName);
		LegClassItem__c lci = new LegClassItem__c();
		lci.LegClassPeriod__c = legClassPeriod.id;
		lci.serviceItem__c = si.id;
		lci.order__c = order;
		return lci;
	}

 	private String stripItemName(String input) {
 		if (input == null) return '';
 		String output = input.trim();
 		output = output.replaceAll(' ', '');
 		output = output.replaceAll('/', '-');
 		output = output.replaceAll(':', '');
 		output = output.replaceAll('\\+', '');
 		output = output.replaceAll('%', ''); 		
 		output = output.replaceAll('\\*', '');
 		output = output.toLowerCase();
 		output = 'klc_' + output;
 		return output;
 	}
 	
 	private String getTypeFromName(String serviceItemName) {
 		String result = 'other';
 		if (serviceItemName.contains('drink')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('drk')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('choice1')) {
 			result = 'drinks'; 		
 		} else if (serviceItemName.contains('choice2')) {
 			result = 'drinks';
  		} else if (serviceItemName.contains('choice3')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('meal')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('tidbi')) {
 			result = 'tidbits';
 		} else if (serviceItemName.contains('water')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('juice')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('snack')) {
 			result = 'snack';
 		} else if (serviceItemName.contains('muffin')) {
 			result = 'snack';
 		} else if (serviceItemName.contains('dinner')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('lun')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('wine')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('beverage')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('juice')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('coffee')) {
 			result = 'drinks';
 		} else if (serviceItemName.contains('towel')) {
 			result = 'towel';
 		} else if (serviceItemName.contains('icecream')) {
 			result = 'icecream';
 		} else if (serviceItemName.contains('ice cream')) {
 			result = 'icecream';
 		} else if (serviceItemName.contains('breakfast')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('bkf')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('dess')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('noodle')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('friandise')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('garniture')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('combi')) {
 			result = 'meal';
 		} else if (serviceItemName.contains('croissant')) {
 			result = 'snack';
 		} else if (serviceItemName.contains('sand')) {
 			result = 'snack';
 		} else if (serviceItemName.contains('cookie')) {
 			result = 'snack';
 		} else if (serviceItemName.contains('sale')) {
 			result = 'sales';
 		} else {
 			result = 'other';
 		}
 		System.debug(LoggingLevel.INFO, 'name=' + serviceItemName + ' type=' + result);
 		return result;
 
 	}
 	
	// convert dd-MMM-yyyy to yyyy-MM-dd HH:mm:ss format
	private Date parseCsvDate(String inputDate) {
		inputDate = inputDate.trim();
		String[] parts = inputdate.split('-');
		String outputDate = '20' + parts[2];
		outputDate = outputDate + '-';
		outputDate = outputDate + getMonthNumber(parts[1]);
		outputDate = outputDate + '-';
		outputDate = outputDate + parts[0];
		outputDate = outputDate + ' 12:00:00';
		return Date.valueOf(outputDate);
	}
	
	private String getMonthNumber(String monthName) {
		String result = '01';
		if ('Feb'.equals(monthName)) {
			result = '02';
		} else if ('Mar'.equals(monthName)) {
			result = '03';
		} else if ('Apr'.equals(monthName)) {
			result = '04';
		} else if ('May'.equals(monthName)) {
			result = '05';
		} else if ('Jun'.equals(monthName)) {
			result = '06';
		} else if ('Jul'.equals(monthName)) {
			result = '07';
		} else if ('Aug'.equals(monthName)) {
			result = '08';
		} else if ('Sep'.equals(monthName)) {
			result = '09';
		} else if ('Oct'.equals(monthName)) {
			result = '10';
		} else if ('Nov'.equals(monthName)) {
			result = '11';
		} else if ('Dec'.equals(monthName)) {
			result = '12';
		}
		return result;
	}

  	private virtual String[] getLinesFromResource() {
  		StaticResource sr = [
			Select  s.Name, s.Id, s.Body 
			From StaticResource s  
			where name =: 'LTCServiceSchedule'
		];
		blob tempBlob = sr.Body;
		String tempString = tempBlob.toString();
		tempString = tempSTring.replace('"', '');
		String[] lines = tempString.split('\n');
		return lines;
  	}
  	
	private void loadCurrentRecords() {
		economyClass = [
			Select Name, Id, label__c
			From CabinClass__c	
			where name =: 'economy'
		];
		businessClass= [
			Select Name, Id, label__c
			From CabinClass__c 
			where name =: 'business'
		];
		
		List<ServiceItem__c> sis = [select id, name__c, description__c from ServiceItem__c];
		for (ServiceItem__c si : sis) {
			serviceItems.put(si.name__c, si);
		}
		
		loadLegClassPeriods();

	}

	private void loadLegClassPeriods() {
		legClassPeriods = [
			select id, CabinClass__c, EndDate__c, FlightNumber__c, LegNumber__c, ServicePeriod__c, StartDate__c
			from LegClassPeriod__c
			where ServicePeriod__c =: servicePeriod.id
		];
	}
	

	
	private void initialize() {
		StaticResource sr = [
			Select Name, Id, Description 
			From StaticResource 
			where name =: 'LTCServiceSchedule'
		];
		
		List<ServicePeriod__c> sps = [
			Select Id, name__c
			From ServicePeriod__c
			where name__c =: sr.description
		];
		
		if (sps != null && !sps.isEmpty()) {
			servicePeriod = sps[0];
        } 
        else {
        	servicePeriod = new ServicePeriod__c();
			servicePeriod.name__c = sr.description;
			insert servicePeriod;
        }
		

		
	}
}