public with sharing class LtcLocationsApiService {
	
	private static final string GRANT_TYPE='grant_type=client_credentials';
	
	public static LtcLocationsWeatherResponse getWeatherForCity(String cityCode) {
		LtcLocationsWeatherResponse weatherResponse;
        LtcLocationsApiSettings__c setting = LtcLocationsApiSettings__c.getOrgDefaults();
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(setting.endPoint__c + '/locations/cities/' + cityCode + '/weather'); //https://api.klm.com/travel/locations/cities/AMS/weather
        req.setHeader('Accept','application/json');
        req.setHeader('Authorization', 'Bearer ' + setting.oAuthAccessToken__c);
        req.setMethod('GET');
        req.setTimeout(3000);

        HttpResponse res;
        String responseBody = '';

	    Http h = new Http(); 
        res = h.send(req);
        if (res.getStatusCode() == 401) {
        	reprocessOAuthToken();
        } else if (res.getStatusCode() == 200) {
        	System.debug(LoggingLevel.INFO, res.getBody());
	        weatherResponse = LtcLocationsWeatherResponse.parse(res.getbody());
	    } else {
        	System.debug(LoggingLevel.ERROR, 'weather could not be determined: status=' + res.getStatus() + ' statuscode=' + res.getStatusCode());
        }
		return weatherResponse;
	}
	
	public static String getCityCodeForAirport(String airportCode) {
		String cityCode = airportCode;

        LtcLocationsApiSettings__c setting = LtcLocationsApiSettings__c.getOrgDefaults();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(setting.endPoint__c + '/locations/airports/' + airportCode);
        req.setHeader('Accept','application/json');
        req.setHeader('Authorization', 'Bearer ' + setting.oAuthAccessToken__c);
        req.setMethod('GET');
        req.setTimeout(2000);
        
        HttpResponse res;
        String responseBody = '';
        
        LtcLocationsAirportResponse laResponse;
        
        Http h = new Http(); 
        res = h.send(req);
        if (res.getStatusCode() == 401) {
        	reprocessOAuthToken();
       	} else if (res.getStatusCode() == 200) {
	        System.debug(LoggingLevel.INFO, 'res.getbody()=' + res.getbody());
	        laResponse = LtcLocationsAirportResponse.parse(res.getbody());
	        System.debug(LoggingLevel.INFO, 'laResponse=' + laResponse);
 			System.debug(LoggingLevel.INFO, 'laResponse.parent=' + laResponse.parent);
	        cityCode = laResponse.parent.code;
        } else {
			System.debug(LoggingLevel.ERROR, 'city could not be determined for airport ' + airportCode + ', status=' + res.getStatus() + ' statuscode=' + res.getStatusCode());
        }   	
		return cityCode;
	}
	
	/**
	 * Reprocess Locations API OAuth service to get new access token 
	 * and save it in the LtcLocationsApiSettings__c 'oAuthAccessToken__c'
	 **/
	public static void reprocessOAuthToken() {
		try {
			System.debug(LoggingLevel.DEBUG, 'Reprocess oauth token');
			LtcLocationsApiSettings__c setting = LtcLocationsApiSettings__c.getOrgDefaults();
			setting.oAuthAccessToken__c = generateAccessToken(setting);
			upsert setting;
		} catch (Exception e) {
			System.debug(LoggingLevel.ERROR, e);
			throw e;
		}
	}

	/**
	 * Generate the new expired accessToken from the Locations API oAuth service
	 **/
	private static String generateAccessToken(LtcLocationsApiSettings__c setting) {
        Http h = new Http();
        String body = GRANT_TYPE;

        HttpRequest req = new HttpRequest();
        req.setEndpoint(setting.endPoint__c + '/oauth/token');
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setHeader('Authorization', setting.authorization__c);
        req.setMethod('POST');
        req.setBody(body);
        
        HttpResponse res;
        if (Test.isRunningTest()) {
			res = new HttpResponse(); 
			res.setBody('{"token_type":"bearer","access_token":"This is a test","expires_in":86400}');
        } else {
        	res = h.send(req);
        }
        
        // Parse JSON response to get all Access Token value.
        // {"token_type":"bearer","access_token":"<Generated token>","expires_in":86400}
        System.debug(LoggingLevel.DEBUG, 'res.getbody()=' + res.getbody());
        JSONParser parser = JSON.createParser(res.getbody());
        
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
                // Get the next Token value which is the Access Token.
                parser.nextToken();
                return parser.getText();
            }
        }
		
		return '';
	}
}