//Copyright (c) 2010, Mark Sivill, Sales Engineering, Salesforce.com Inc.
//All rights reserved.
//
//Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
//Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
//Neither the name of the salesforce.com nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
//INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
//CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// History
//
// Version      Date            Author          Comments
// 1.0.0        26-04-2010      Mark Sivill     Initial version
//
// Overview
//
// Test batch process for Weather Chatter 
// need to ensure that no more records than batch size is queried (in this case 1)
//
@isTest
private class WeatherChatterBatchTest {

	//
	// test with default which is process active chatters
	//
    static testMethod void testDefault() {

		for (Weather_Chatter__c[] recs : [SELECT id FROM Weather_Chatter__c WHERE Chatter_Active__c = true])
		{
			delete recs;
		}
		
        List<Weather_Chatter__c> yahooWeather = new List<Weather_Chatter__c>
            { 
                new Weather_Chatter__c( Name='Twickenham', Where_on_Earth_Identifier__c='38395', Temperature_Scale__c='c', Chatter_Active__c=true)
            };
        upsert yahooWeather Where_on_Earth_Identifier__c;       

		Test.StartTest();
        WeatherChatterBatch b = new WeatherChatterBatch();
        ID batchprocessid = Database.executeBatch(b,1);   	
		Test.StopTest();
    }

	//
	// test all records (regardless of chatter active or not)
	//
    static testMethod void testCustomQuery() {

		for (Weather_Chatter__c[] recs : [SELECT id FROM Weather_Chatter__c])
		{		
			delete recs;
		}

        List<Weather_Chatter__c> yahooWeather = new List<Weather_Chatter__c>
            { 
                new Weather_Chatter__c( Name='Twickenham', Where_on_Earth_Identifier__c='38395', Temperature_Scale__c='c', Chatter_Active__c=false)
            };
        upsert yahooWeather Where_on_Earth_Identifier__c;       

		Test.StartTest();
        WeatherChatterBatch b = new WeatherChatterBatch('SELECT Id, Where_on_Earth_Identifier__c, Temperature_Scale__c FROM Weather_Chatter__c');
        ID batchprocessid = Database.executeBatch(b,1);   	
		Test.StopTest();
    }
    
    
}