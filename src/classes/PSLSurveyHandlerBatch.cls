global class PSLSurveyHandlerBatch implements Schedulable, Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

	final static String PSL_RECORDTYPE_DEV_NAME = 'Platinum_Service_Line';
	final static Integer LAST_N_MONTHS = 6;
	final static String PSL_FROM_ADDRESS = 'noreply.psl@klm.com';

	private static Map<String, Id> templateMap;
	private static final String SEARCH_CUSTOMER_URL = 'https://api.klm.com/customerapi/customer/';
    private static final String SEARCH_CUSTOMER_URL_BY_ID = 'https://api.klm.com/customerapi/customers/';
    private static final Map<String, AFKLM_PSL_Survey_Settings__c> surveySettingMap = AFKLM_PSL_Survey_Settings__c.getAll();

    private final Map<String, Integer> surveyCountryLimitCounterMap = new Map<String, Integer>();
    private Integer processedCases = 0;
    private final Set<String> processedFlyingBlueMembers = new Set<String>();

	// schedulable interface
	global void execute( SchedulableContext sc ) {
		PSLSurveyHandlerBatch batch = new PSLSurveyHandlerBatch();
		// limit is 100 callouts, so with 40 * 2 we're on the safe side
		Database.executeBatch( batch, 40 );
	}

	// batchable interface
	global Database.QueryLocator start( Database.BatchableContext bc ) {
		Datetime rangeStart, rangeStop;
		// as we can't control Case.ClosedDate, we specify a different range for Unit Tests
		if ( Test.isRunningTest() ) {
			rangeStart = System.now().addHours( -2 );
			rangeStop = System.now().addMinutes( 1 );
		} else {
			rangeStart = System.today().addDays( -2 );
			rangeStop = System.today().addDays( -1 );
		}
		System.debug( 'rangeStart: ' + rangeStart );
		System.debug( 'rangeStop: ' + rangeStop );
		String query =
			'SELECT ' +
			'	Id, ' +
			'   Account.Id, ' +
			'	Account.Flying_Blue_Number__c, ' +
			'   Account.PersonEmail, ' +
			'   Account.Preferred_Language__pc, ' +
			'   Account.Country__pc, ' +
			'   Account.Flying_Blue_Level__pc ' +
			'FROM ' +
			'	Case ' +
			'WHERE ' +
			'	ClosedDate >= :rangeStart AND ClosedDate < :rangeStop ' +
			'   AND Account.Flying_Blue_Number__c != null ' +
			'	AND IsClosed = true ' +
			'	AND PSL_Survey_Sent__c = false ' +
			'	AND RecordType.DeveloperName = :PSL_RECORDTYPE_DEV_NAME ' +
			'	AND Account.Personal_Assistance__pc = false ' +
			'	AND ( ' +
			'		Account.PSL_Survey_Last_Sent__c = null ' +
			'		OR Account.PSL_Survey_Last_Sent__c < LAST_N_MONTHS:' + LAST_N_MONTHS +
			' 	) ';
		System.debug( 'QUERY: ' + query );
		return Database.getQueryLocator( query );
	}

	global void execute( Database.BatchableContext bc, List<Case> cases ) {
		if ( cases != null ) {
			processedCases += cases.size();
		}
		List<Case> casesToUpdate = new List<Case>();
		List<Account> accountsToUpdate = new List<Account>();
		for ( Case c : cases ) {
			try {
				String flyingBlueNumber = c.Account.Flying_Blue_Number__c;
				String language = 'EN';
				String country;
				String flyingBlueLevel;
				String emailAddress;
				if ( ! Test.isRunningTest() ) {
					// retrieve customer id by fb number
					String sb = '?flying-blue-number=' + flyingBlueNumber;
					AFKLM_SCS_CustomerAPIClient customerAPIClient = new AFKLM_SCS_CustomerAPIClient();
					AFKLM_SCS_RestResponse rspCustomerId = customerAPIClient.request( SEARCH_CUSTOMER_URL + sb, 'GET', new Map<String,String>());
					if ( ! rspCustomerId.error ) {
						System.debug ( rspCustomerId.getResponseText() );
						CustomerIdInfo cidInfo = ( CustomerIdInfo ) JSON.deserializeStrict( rspCustomerId.getResponseText(), CustomerIdInfo.class );
						// retrieve customer details by id
						AFKLM_SCS_RestResponse rspCustomer = customerAPIClient.request( SEARCH_CUSTOMER_URL_BY_ID + cidInfo.id, 'GET', new Map<String,String>());
						if ( ! rspCustomer.error ) {
							// manually parse response, this really needs to be fixed properly by the customer api client
							CustomerInfo cInfo = ( CustomerInfo ) JSON.deserialize( rspCustomer.getResponseText(), CustomerInfo.class );
							System.debug( cInfo );
							if ( cInfo != null ) {
								if ( cInfo.individual != null ) {
									// retrieve email address, if valid
									if ( cInfo.individual.emailAccount != null && 'Valid'.equals( cInfo.individual.emailAccount.status ) ) {
										emailAddress = cInfo.individual.emailAccount.address;
									}
									// retrieve country
									if ( cInfo.individual.passengerInformation != null && cInfo.individual.passengerInformation.countryOfResidence != null ) {
										country = cInfo.individual.passengerInformation.countryOfResidence;
									}
									// retrieve country - take 2
									if ( String.isBlank( country ) && cInfo.individual.postalAddresses != null && cInfo.individual.postalAddresses.size() > 0 ) {
										country = cInfo.individual.postalAddresses[0].country;
									}
								}
							}
							if ( cInfo.preference != null && cInfo.preference.communicationPreference != null && cInfo.preference.communicationPreference.language != null ) {
								language = cInfo.preference.communicationPreference.language;
							}
							if ( cInfo.membership != null && cInfo.membership.flyingBlueMembership != null && cInfo.membership.flyingBlueMembership.level != null ) {
								flyingBlueLevel = cInfo.membership.flyingBlueMembership.level;
							}
						}
					}
				} else {
					// this is only used during unit tests
					country = c.Account.Country__pc;
					flyingBlueLevel = c.Account.Flying_Blue_Level__pc;
					emailAddress = c.Account.PersonEmail;
					language = c.Account.Preferred_Language__pc;
				}
				System.debug( 'email: ' + emailAddress );
				System.debug( 'country: ' + country );
				System.debug( 'language: ' + language );
				System.debug( 'flyingBlueLevel: ' + flyingBlueLevel );
				if ( processedFlyingBlueMembers.contains( flyingBlueNumber ) ) {
					System.debug( 'This Flying Blue member has already been surveyed in this batch.' );
					continue;
				} else {
					System.debug( 'Processing Flying Blue member: ' + flyingBlueNumber );
					processedFlyingBlueMembers.add( flyingBlueNumber );
				}
				if ( String.isBlank( country ) ) {
					System.debug( 'Unable to retrieve Country via Customer API.' );
					continue;
				}
				if ( String.isBlank( flyingBlueLevel ) ) {
					System.debug( 'Unable to determine Flying Blue membership level via Customer API.' );
					continue;
				}
				if ( ! flyingBlueLevel.equals( 'Platinum' ) ) {
					System.debug( 'Incorrect Flying Blue level: ' + flyingBlueLevel );
					continue;
				}
				if ( String.isBlank( emailAddress ) ) {
					System.debug( 'Unable to determine email address via Customer API.' );
					continue;
				}
				AFKLM_PSL_Survey_Settings__c surveySettings = surveySettingMap.get( country );
				if ( surveySettings == null ) {
					System.debug( 'Unable to determine survey configuration for country: ' + country );
					continue;
				}
				if ( ! surveySettings.Active__c ) {
					System.debug( 'Survey settings are not active.' );
					continue;
				}
				Integer x = 0;
				if ( surveyCountryLimitCounterMap.get( country ) == null ) {
					System.debug( 'Initialize country survey counter in map to zero.' );
					surveyCountryLimitCounterMap.put( country, x );
				} else {
					// pull counter from map
					x = surveyCountryLimitCounterMap.get( country );
				}
				if ( x < surveySettings.Daily_Survey_Limit__c ) {
					// save data into Person Account, so it can be picked up by a workflow rule which
					// will send out the email
					Account a = new Account();
					a.Id = c.Account.Id;
					a.PSL_Survey_Email_Address__c = emailAddress;
					a.PSL_Survey_Language__c = language;
					a.PSL_Survey_Send_Email_Flag__c = true;
					accountsToUpdate.add( a );

					c.PSL_Survey_Sent__c = true;
					casesToUpdate.add( c );
					// increase counter
					x++;
					// save in map
					surveyCountryLimitCounterMap.put( country, x );
				} else {
					System.debug( 'Survey country limit reached: ' + surveySettings.Daily_Survey_Limit__c );
				}
			} catch ( Exception e ) {
				throw new PSLSurveyHandlerException( 'Unable to send PSL survey: ' + e.getMessage(), e );
			}
		}
		System.debug( 'Cases processed this batch: ' + cases.size() );
		System.debug( 'Emails sent this batch: ' + casesToUpdate.size() );
		if ( casesToUpdate.size() > 0 ) {
			update casesToUpdate;
			update accountsToUpdate;
		}
	}

	global void finish( Database.BatchableContext bc ) {
		System.debug( 'Survey Statistics' );
		System.debug( 'Total Cases Processed: ' + processedCases );
		System.debug( 'Total Unique Customers: ' + processedFlyingBlueMembers.size() );
		for ( String country : surveyCountryLimitCounterMap.keySet() ) {
			System.debug ( country + ': ' + surveyCountryLimitCounterMap.get( country ) );
		}
	}

	public class PSLSurveyHandlerException extends Exception {}

}