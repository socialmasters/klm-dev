public with sharing class AFKLM_SCS_PreviousCasesPersonAccount {

	private Id accntId;

	public ApexPages.StandardSetController ssc {
		get{
			if (ssc == null){
				ssc = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Id, casenumber, createddate, status,case_topic__c, case_detail__c, Sentiment_at_close_of_case__c, PNR__c 
                     FROM Case 
                     WHERE AccountId = :accntId 
                     ORDER BY CreatedDate DESC LIMIT 5
                    ]));
			}
			return ssc;
		}
		set;
	}
	
	  /** 
    * Get the count of cases based on Person Account
    *
    * @return number of cases
    */
	public Integer getCaseCount(){
    	Integer casecount=[SELECT COUNT() FROM Case WHERE AccountID=:accntId];
    	return casecount;
    }

	public AFKLM_SCS_PreviousCasesPersonAccount(ApexPages.StandardController stdCtrl) {
		accntId = stdCtrl.getId();
	}

	public List<Case> getCases(){
		return (List<Case>)ssc.getRecords();
	}
}