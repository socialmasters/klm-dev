@isTest
public with sharing class LtcRatingTest {
	
	static testMethod void singleFlightCall() {
		Monitor_Flight__c monFlight = new Monitor_Flight__c();
    	monFlight.Flight_Number__c = 'KL1234';
    	insert monFlight;
    	
        DateTime aDate = DateTime.now();
    	LtcRating rating = new LtcRating();
    	
    	System.assert(!LtcRatedFlightsRestInterface.getRatedFlightNumbers().contains('KL1234'));
    	rating.setRating('3', '', '', 'true', 'fn', 'ln', 'pietje@puk.nl', 'KL1234', '11a', aDate.format('yyyy-MM-dd'), 'ORG', 'DST',  'nl_NL');
    	System.assert(LtcRatedFlightsRestInterface.getRatedFlightNumbers().contains('KL1234'));
    	
        //setup request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/Ratings/FlightStatistics/';  
        req.addParameter('flightNumber', 'KL1234');
        req.addParameter('travelDate', aDate.format('yyyy-MM-dd'));
        
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

		// do request
        String result = LtcRatingFlightStatisticsRestInterface.searchRatedFlight();
        System.debug(LoggingLevel.INFO, 'result=' + result);
    	System.assert(result.contains('{"level": "3", "count": "1"}'));
	}
	
	static testMethod void oneRatedFlight() { 
		Monitor_Flight__c monFlight = new Monitor_Flight__c();
    	monFlight.Flight_Number__c = 'KL1234';
    	insert monFlight;
    	
        DateTime aDate = DateTime.now();
    	LtcRating rating = new LtcRating();
    	
    	System.assert(!LtcRatedFlightsRestInterface.getRatedFlightNumbers().contains('KL1234'));
    	rating.setRating('3', '', '', 'true', 'fn', 'ln', 'pietje@puk.nl',  'KL1234', '11a', aDate.format('yyyy-MM-dd'), 'ORG', 'DST',  'nl_NL');
    	System.assert(LtcRatedFlightsRestInterface.getRatedFlightNumbers().contains('KL1234'));
    	 
    	// check one rating
    	LtcRatingFlightStatisticsPostModel.Flight flight = new LtcRatingFlightStatisticsPostModel.Flight('KL1234', aDate.format('yyyy-MM-dd'));
    	System.assert(flight.toString().contains('flightNumber: KL1234'));
    	List<LtcRatingFlightStatisticsPostModel.Flight> flights = new List<LtcRatingFlightStatisticsPostModel.Flight>();
    	flights.add(flight);
    	
    	//setup request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/Ratings/FlightsStatistics/';  
        req.addParameter('data', 'KL1234!' + aDate.format('yyyy-MM-dd'));
        
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
    	
    	String result = LtcRatingFlightsStatisticsRestInterface.searchRatedFlights();
    	System.debug(LoggingLevel.INFO, 'result=' + result);
    	System.assert(result.contains('{"numberOfRatings": "1", "averageRating": "3"'));
    	System.assert(result.contains('{"level": "3", "count": "1"}'));
	}
	
	static testMethod void oneWeekFlightsOneRatingADay() { 
		Monitor_Flight__c monFlight = new Monitor_Flight__c();
    	monFlight.Flight_Number__c = 'KL1234';
    	insert monFlight;
    	
        DateTime aDate = DateTime.now();
    	LtcRating rating = new LtcRating();
    	
   		rating.setRating('3', '', '', 'true', 'fn', 'ln', 'pietje@puk.nl',  'KL1234', '11a', aDate.format('yyyy-MM-dd'), 'ORG', 'DST',  'nl_NL');
    	for (integer x = 1; x < 6; x++) {
    		aDate = aDate.addDays(-1);
    		rating.setRating('3', '', '', 'true', 'fn', 'ln', 'pietje@puk.nl',  'KL1234', '11a', aDate.format('yyyy-MM-dd'), 'ORG', 'DST',  'nl_NL');
    	}
    	
    	//setup request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/Ratings/FlightsStatistics/';  
        req.addParameter('data', 'KL1234!' + aDate.format('yyyy-MM-dd'));
        
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
    	
    	String result = LtcRatingFlightsStatisticsRestInterface.searchRatedFlights();
    	System.debug(LoggingLevel.INFO, 'result=' + result);
    	System.assert(result.contains('{"numberOfRatings": "6", "averageRating": "3"'));
    	System.assert(result.contains('{"level": "3", "count": "6"}'));
   }
	
	static testMethod void moreRatings() { 
		Monitor_Flight__c monFlight = new Monitor_Flight__c();
    	monFlight.Flight_Number__c = 'KL1234';
    	insert monFlight;
    	
        DateTime aDate = DateTime.now();
    	LtcRating rating = new LtcRating();
    	
   		rating.setRating('3', '', '', 'true', 'fn', 'ln', 'pietje@puk.nl',  'KL1234', '11a', aDate.format('yyyy-MM-dd'), 'ORG', 'DST',  'nl_NL');
    	for (integer x = 1; x < 10; x++) {
    		aDate = aDate.addDays(-7);
    		rating.setRating('4', '', '', 'true', 'fn', 'ln', 'pietje@puk.nl',  'KL1234', '11a', aDate.format('yyyy-MM-dd'), 'ORG', 'DST',  'nl_NL');
    	}
    	 
    	//setup request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/Ratings/FlightsStatistics/';  
        req.addParameter('data', 'KL1234!' + aDate.format('yyyy-MM-dd'));
        
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
    	
    	String result = LtcRatingFlightsStatisticsRestInterface.searchRatedFlights();
    	System.debug(LoggingLevel.INFO, 'result=' + result);
    	System.assert(result.contains('{"numberOfRatings": "10", "averageRating": "3.9"'));
    	System.assert(result.contains('{"level": "3", "count": "1"}'));
    	System.assert(result.contains('{"level": "4", "count": "9"}'));
   }
}