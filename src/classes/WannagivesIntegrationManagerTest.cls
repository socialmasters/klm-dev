/**********************************************************************
 Name:  WannagivesIntegrationManagerTest
======================================================
Purpose: 

Tests WannagivesIntegrationManager and WannagivesCaseTrigger

======================================================
History                                                            
-------                                                            
VERSION    AUTHOR             DATE          DETAIL                                 
    1.0    AJ van Kesteren    06/09/2013    Initial Development
***********************************************************************/  

@isTest
public with sharing class WannagivesIntegrationManagerTest {

	private Static Account a;
	private Static Case c;
	private Static Case c1;
	private Static Case c2;
	private Static Case c3;

	static testMethod void testHandleNewCases() {
		loadTestData();		
		Case tstCase1 = [Select AccountId from Case where Id = :c.Id limit 1];
		System.assertEquals(tstCase1.AccountId, a.Id);
		
		Case tstCase2 = [Select AccountId, Account.LastName from Case where Id = :c3.Id limit 1];
		System.assertNotEquals(null, tstCase2.accountId);
		system.assertEquals('aap', tstCase2.Account.LastName);
	}

	static testMethod void testCreateLoginMessageBody() {
		loadTestData();
		WannagivesIntegrationManager mgr = new WannagivesIntegrationManager();
		String body = mgr.createLoginMessageBody();
		System.assert(body.contains(':login'));		
	}
	
	static testMethod void testCreateSalesOrderConfirmOrderMessageBody() {
		loadTestData();
		WannagivesIntegrationManager mgr = new WannagivesIntegrationManager();
		String body = mgr.createSalesOrderConfirmOrderMessageBody('ABCDE', '12345', c);
		System.assert(body.contains('>ABCDE<'));
		System.assert(body.contains('>12345<'));		
	}	

	static testMethod void testGetSessionIdFromLoginResponse() {
		loadTestData();
		WannagivesIntegrationManager mgr = new WannagivesIntegrationManager();		
		String response = '<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:Magento" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"><SOAP-ENV:Body><ns1:loginResponse><loginReturn xsi:type="xsd:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">3fa1a487f44a527ba1b698e22b4304eb</loginReturn></ns1:loginResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>';
		DOM.Document doc = new DOM.Document();  
		doc.load(response);
		String sessionId = mgr.getSessionIdFromLoginResponse(doc);
		System.assertNotEquals(null, sessionId);
	}

	static testMethod void testGetIsSuccessFromConfirmOrderResponse() {
		loadTestData();
		WannagivesIntegrationManager mgr = new WannagivesIntegrationManager();			
		String response = '<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:Magento" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"><SOAP-ENV:Body><ns1:salesOrderConfirmOrderResponse><result xsi:type="xsd:boolean" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">true</result></ns1:salesOrderConfirmOrderResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>';
		DOM.Document doc = new DOM.Document();  
		doc.load(response);
		Boolean result = mgr.getIsSuccessFromConfirmOrderResponse(doc, c);
		System.assert(result);
	}

	static testMethod void testHandleClosedCase() {
		loadTestData();
		WannagivesIntegrationManager mgr = new WannagivesIntegrationManager();
		mgr.handleClosedCase(c.Id, 'Ok');
		Case tstCase = [Select Wannagives_Integration_Status__c from Case where Id = :c.Id limit 1];
		System.assertEquals('Fault', tstCase.Wannagives_Integration_Status__c);	
	}

	private static void loadTestData() {
		Wannagives_Integration_Settings__c settings = new Wannagives_Integration_Settings__c();
		settings.Name = 'Default';
		settings.Endpoint__c = 'ENDPOINT';
		settings.Username__c = 'USERNAME';
		settings.API_Key__c = 'API_KEY';
		insert settings;
		
        RecordType recordType = [ select Id, Name, DeveloperName from RecordType where SObjectType = 'Account' and IsPersonType = true limit 1 ];

        a = new Account();
        a.FirstName = 'Test';
        a.LastName = 'Test123';
        a.RecordTypeId = recordType.id;
        a.Flying_Blue_Number__c = '1234567';
        insert a;
        	
		c = new Case();
		c.Flying_Blue_Number__c = '1234567';
		c.Last_Name__c = 'Last Name';
		c.RecordTypeId = [
			select Id 
    		from RecordType 
    		where SObjectType = 'Case'
    		and DeveloperName = 'Wannagives'
    		limit 1].Id;
		insert c;

		c1 = new Case();
		c1.Requestor_Social_Id__c = 'fb_12345678';
		c1.Last_Name__c = 'Last Name 1';
		c1.RecordTypeId = [
			select Id 
    		from RecordType 
    		where SObjectType = 'Case'
    		and DeveloperName = 'Wannagives'
    		limit 1].Id;
		insert c1;

		c2 = new Case();
		c2.Requestor_Social_Id__c = 'li_12345678';
		c2.Last_Name__c = 'Last Name 2';
		c2.RecordTypeId = [
			select Id 
    		from RecordType 
    		where SObjectType = 'Case'
    		and DeveloperName = 'Wannagives'
    		limit 1].Id;
		insert c2;
		
		c3 = new Case();
		c3.Last_Name__c = 'aap';
		c3.RecordTypeId = [
			select Id 
    		from RecordType 
    		where SObjectType = 'Case'
    		and DeveloperName = 'Wannagives'
    		limit 1].Id;
		insert c3;		
		
		
	}

}