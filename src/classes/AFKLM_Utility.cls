/**********************************************************************
 Name:  AFKLM_Utility.cls
======================================================
Purpose: 
	1. Utility class to time execution of code
	2. Static to prevent recursive triggers
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma    01/12/2013      Initial development
***********************************************************************/
public with sharing class AFKLM_Utility {

    public static Boolean hasRunRefundTrigger = false;
    
	// Map of total time per type of timing
	private static Map<String, Long> mapOfTypeToTotalTime = new Map<String, Long>();
	// Map of type of timing to start time of timing
	private static Map<String, Long> mapOfTypeToStartTime = new Map<String, Long>(); 

	// Build a local cache so that we don't request this multiple times
	private static Map<Schema.SObjectType,Map<String,Id>> rtypesCache;

	static {
		rtypesCache = new Map<Schema.SObjectType, Map<String,Id>>();
	}

	// Start timing for specific type
	// If timing was already started, it will be overwritten
	public static void startTimeForType(String sType){
		mapOfTypeToStartTime.put(sType, System.now().getTime());
	} 
	
	// End timing for specific type 
	// Add to total for this type
	// Remove from start map
	public static void endTimeForType(String sType){
		// End timing
		Long endTime = System.now().getTime();
		// Get start time
		Long startTime = mapOfTypeToStartTime.remove(sType);
		if (startTime == null){
			startTime = endTime;	
		}
		// Get total time
		Long totalTime = mapOfTypeToTotalTime.get(sType);
		if (totalTime == null){
			totalTime = 0;
		}
		// Add timing to total
		totalTime += (endTime - startTime);
		// Add total to map
		mapOfTypeToTotalTime.put(sType, totalTime);
	}
	
	// Create summary
	public static String getTimeSummary(String lineBreak){
		String sReturn = '';
		for (String sType : mapOfTypeToTotalTime.keySet()){
			sReturn += sType + ': ' + String.valueof(mapOfTypeToTotalTime.get(sType)) + 'ms' + lineBreak; 
		}
		return sReturn;
	}

	// Returns a map of active, user-available RecordType IDs for a given SObjectType,
	// keyed by each RecordType's unique, unchanging DeveloperName 
	public static Map<String, Id> getRecordTypeIdsByDeveloperName( Schema.SObjectType token ) {

	    // Do we already have a result? 
	    Map<String, Id> mapRecordTypes = rtypesCache.get(token);
	    // If not, build a map of RecordTypeIds keyed by DeveloperName
	    if (mapRecordTypes == null) {
	        mapRecordTypes = new Map<String, Id>();
	        rtypesCache.put(token,mapRecordTypes);
	    } else {
	       // If we do, return our cached result immediately!
	       return mapRecordTypes;
	    }

	    // Get the Describe Result
	    Schema.DescribeSObjectResult obj = token.getDescribe();

	    // Obtain ALL Active Record Types for the given SObjectType token
	    // (We will filter out the Record Types that are unavailable
	    // to the Running User using Schema information)
	    String soql = 
	        'SELECT Id, Name, DeveloperName '
	        + 'FROM RecordType '
	        + 'WHERE SObjectType = \'' + String.escapeSingleQuotes(obj.getName()) + '\' '
	        + 'AND IsActive = TRUE';
	    List<SObject> results;
	    try {
	        results = Database.query(soql);
	    } catch (Exception ex) {
	        results = new List<SObject>();
	    }

	    // Obtain the RecordTypeInfos for this SObjectType token
	    Map<Id,Schema.RecordTypeInfo> recordTypeInfos = obj.getRecordTypeInfosByID();

	    // Loop through all of the Record Types we found,
	    //      and weed out those that are unavailable to the Running User
	    for (SObject rt : results) {  
	        if (recordTypeInfos.get(rt.Id).isAvailable()) {
	            // This RecordType IS available to the running user,
	            //      so add it to our map of RecordTypeIds by DeveloperName
	            mapRecordTypes.put(String.valueOf(rt.get('DeveloperName')),rt.Id);
	        }
	    }
	    return mapRecordTypes;

	}

}