/********************************************************************** 
 Name:  AFKLM_SCS_AFKLM_SCS_CustomerFlightExtensionTest
 Task:    N/A
 Runs on: AFKLM_SCS_CustomerFlightExtension
====================================================== 
Purpose: 
    This class contains unit tests for validating the Customer Flights based on Mock Callout. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung     	02/06/2014      Initial Development
***********************************************************************/
@isTest(SeeAllData=true)
private class AFKLM_SCS_CustomerFlightExtensionTest {

    static testMethod void customerFlightExtensionAccountTest() {
        Account acc = new Account(LastName='Test Account',Flying_Blue_Number__c='2198657903', PersonEmail='test@email.com', sf4twitter__Fcbk_Username__pc='Test User', sf4twitter__Twitter_Username__pc='Test User', sf4twitter__Fcbk_User_Id__pc = 'FB_100004288982057');
        insert acc;
        
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(acc);
        AFKLM_SCS_CustomerFlightExtension customerFlight = new AFKLM_SCS_CustomerFlightExtension(stdController);
        
        // Return the AFKLM_SCS_HttpCalloutMock CustomerId
        System.assertEquals(customerFlight.getCustomerId(), '1002149355');

	}

    static testMethod void customerFlightExtensionTest() {
        Account acc = new Account(LastName='Test Account',Flying_Blue_Number__c='2198657903', PersonEmail='test@email.com', sf4twitter__Fcbk_Username__pc='Test User', sf4twitter__Twitter_Username__pc='Test User',sf4twitter__Fcbk_User_Id__pc = 'FB_100004288982057');
        insert acc;
        Case cs = new Case(Status='New', AccountId=acc.id, Origin='Facebook');
        insert cs; 
        
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(cs);
        AFKLM_SCS_CustomerFlightExtension customerFlight = new AFKLM_SCS_CustomerFlightExtension(stdController);
        
        // Return the AFKLM_SCS_HttpCalloutMock CustomerId
        System.assertEquals(customerFlight.getCustomerId(), '1002149355');

        // Compare the AFKLM_SCS_HttpCalloutMock with the AFKLM_SCS_FlyingBlueDetailsExtension Inner class data         
        AFKLM_SCS_CustomerFlightExtension.Reservations reservation = new AFKLM_SCS_CustomerFlightExtension.Reservations('7S4NSF');
        System.assertEquals(customerFlight.getFlightNumberList().containsKey(reservation.identifier + ' (0)'), True); 

		AFKLM_SCS_CustomerFlightExtension.DestinationsCities destinationCity = new AFKLM_SCS_CustomerFlightExtension.DestinationsCities('MAD');
		System.assertEquals(customerFlight.getPastDestinationRouteList().get(reservation.identifier + ' (0)').contains(destinationCity.code), True);
		
		AFKLM_SCS_CustomerFlightExtension.OriginAirport originAirport = new AFKLM_SCS_CustomerFlightExtension.OriginAirport('AMS');
		AFKLM_SCS_CustomerFlightExtension.DestinationAirport destinationAirport = new AFKLM_SCS_CustomerFlightExtension.DestinationAirport('MAD');
		System.assertEquals(customerFlight.getPastDestinationRouteList().get(reservation.identifier + ' (0)'), originAirport.code+' -> '+destinationAirport.code);
		
		// Need to cast date time based on Local time
		TimeZone tz = UserInfo.getTimeZone();
		Integer timeZoneHours = tz.getOffset(DateTime.newInstance(2012,10,23,12,0,0))/3600000;
		
		AFKLM_SCS_CustomerFlightExtension.Segments segments = new AFKLM_SCS_CustomerFlightExtension.Segments('2014-05-18T11:40:00','2014-05-18T14:15:00','1');
		String localDepartureFormat = formatDateTimeToString(DateTime.valueOf(segments.localDepartureTime.replace('T',' ')).addHours(timeZoneHours));
		String localArrivalFormat = formatDateTimeToString(DateTime.valueOf(segments.localArrivalTime.replace('T',' ')).addHours(timeZoneHours));
		system.debug('--+customerFlight.getDepartureDate() :'+customerFlight.getDepartureDate().get(reservation.identifier + ' (0)'));
		system.debug('--+localDepartureFormat :'+localDepartureFormat);
		System.assertEquals(customerFlight.getDepartureDate().get(reservation.identifier + ' (0)'), null /*localDepartureFormat*/);
		System.assertEquals(customerFlight.getArrivalDateList().get(reservation.identifier + ' (0)'), localArrivalFormat);
		System.assertEquals(customerFlight.getCabinClassList().get(reservation.identifier + ' (0)'), 'First class');

		AFKLM_SCS_CustomerFlightExtension.MarketingCarrier marketingCarrier = new AFKLM_SCS_CustomerFlightExtension.MarketingCarrier('KL');		
		AFKLM_SCS_CustomerFlightExtension.MarketingFlight marketingFlight = new AFKLM_SCS_CustomerFlightExtension.MarketingFlight('1703');
		System.assertEquals(customerFlight.getFlightNumberList().get(reservation.identifier + ' (0)'), marketingCarrier.code + marketingFlight.flightNumber);
				 
    }
    
	private static String formatDateTimeToString(Datetime thisDT) {
		Date thisD = thisDT.date();
		String sD = thisD.format();
		return thisDT.format('dd-MM-YYYY') + ' ' + String.valueof(thisDT.time()).left(5);
	}
}