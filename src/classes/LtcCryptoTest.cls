/**
 * @author (s)      : David van 't Hooft
 * @description     : Test class to test the LtCCrypto class
 * @log:   2MAY2014: version 1.0
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LtcCryptoTest {

	/**
	 * Check encode and decode function
	 **/
    static testMethod void ltcCryptoUnitTest() {
    	LtcCrypto ltcc =  new LtcCrypto();
    	String text1 = 'MySecretText';
    	System.debug('start text: '+text1);
    	String text2 = ltcc.encodeText(text1);
    	System.debug('encrypted text: '+text2);
    	System.assertNotEquals(text1, text2, 'Should be different');
    	String text3 = ltcc.decodeText(text2);
    	System.debug('end text: '+text3);
    	System.assertEquals(text1, text3, 'After encode and decode it should be the same');
    }
}