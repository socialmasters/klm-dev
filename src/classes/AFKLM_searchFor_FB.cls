global with sharing class AFKLM_searchFor_FB {
/**********************************************************************
 Name:  AFKLM_searchFor_FB.cls
======================================================
Purpose: Data construct for FB Member data shown in AFKLM_searchFor page
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma	11/11/2012		Initial development
***********************************************************************/

	// Class properties
	public String fbNumber {get;set;}
	public Long cinNumber {get;set;}
	public String civility {get;set;}
	public String firstName {get;set;}
	public String lastName {get;set;}
    public String tierLevel {get;set;}
    public Long awardMiles {get;set;}
    public Long levelMiles {get;set;}
    public String clubMembership {get;set;}
    public String evolutionLevel {get;set;}
    public String startCurrentLevel {get;set;}
    public Integer yearsMembership {get;set;}
    public String nationality {get;set;}
    public String dateBirth {get;set;}
    public String language {get;set;}
    public String city {get;set;}
    public String country {get;set;}
    public String corporate {get;set;}
    public String company {get;set;}
    public String Mobile {get;set;}
    public String homePhone {get;set;}
    public String email {get;set;}
    public String emailOptOut {get;set;}
    public String addrStreet {get;set;}
    public String addrZipcode {get;set;}
    public String addrCity {get;set;}    
    public String addrCountry {get;set;}	
    public AFKLM_WS_FBR_PassengerFBRecognitionX.PhoneData[] phoneList {get;set;}
    
    // Empty constructor
    public AFKLM_searchFor_FB() {}
	
}