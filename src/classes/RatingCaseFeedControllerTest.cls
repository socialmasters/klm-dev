/**
 * @author (s)      : David van 't Hooft
 * @description     : Test class to test the RatingCaseFeedController class
 * @log:   16FEB2015: version 1.0
 *
 */
@isTest
private class RatingCaseFeedControllerTest {

    static testMethod void commitRatingComment() {
    	Case cs = new Case();
    	cs.comments__c = 'test';
    	insert cs;
    	
    	Passenger__c pass = new Passenger__c();
    	pass.AllowEmailContact__c = true;
    	pass.Email__c = 'pietje@puk.nl';
    	insert pass;
    	
    	Rating__c rating = new Rating__c();
    	rating.CaseId__c = cs.Id;
    	rating.language__c = 'RU';
    	rating.country__c = 'AS';
    	rating.passenger__c = pass.Id;
    	insert rating;
    	
        RatingCaseFeedController ratingCaseFeedController = new RatingCaseFeedController();
        ratingCaseFeedController.ratingCase = cs;
        ratingCaseFeedController.commitRatingComment();
        List<FeedItem> items = [
        	select id from FeedItem
        ];
        
        System.assertEquals(1, items.size());
    }
    
    static testMethod void standardConstructorTestDummyTest() {
    	Case cs = new Case();
    	cs.comments__c = 'hi there';
        RatingCaseFeedController ratingCaseFeedController = new RatingCaseFeedController(new ApexPages.StandardController(cs));
        System.assertEquals(ratingCaseFeedController.ratingCase.comments__c, cs.comments__c);
    }    
}