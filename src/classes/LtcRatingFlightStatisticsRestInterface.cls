/**                     
 * Rest API interface for retrieving ratings and rating statistic for the star pages on EBT.
 */
@RestResource(urlMapping='/Ratings/FlightStatistics/*')
global class LtcRatingFlightStatisticsRestInterface { 

	/**                     
	 * Get all ratings that are allowed to display for the specified flightnumber and traveldate.
	 */	
	@HttpGet
    global static String searchRatedFlight() {
    	String response = ''; 
		RestResponse res = RestContext.response;
		if (res != null) {
			res.addHeader('Access-Control-Allow-Origin', '*');
		}
        try {
	    	String flightNumber = RestContext.request.params.get('flightNumber');
            String travelDateString = RestContext.request.params.get('travelDate');
            
            // defaults
            Integer limitTo = 15;
            Integer offset = 0;
            String limitString = RestContext.request.params.get('limit');
            String offsetString = RestContext.request.params.get('offset'); 
            
            if (limitString != null && !''.equals(limitString)) {
            	limitTo = Integer.valueOf(limitString);
            }            
            if (offsetString != null && !''.equals(offsetString)) {
            	offset = Integer.valueOf(offsetString);
            }
            
	    	Date travelDate = Date.valueof(travelDateString);
	    	
	    	LtcRating rating = new LtcRating();
	    	List<Rating__c> flightRatings = rating.getFlightRatingsForStarInterface(flightNumber, travelDate);
			Integer total = flightRatings.size();
			List<String> ratingIds = new List<String>();
			
	    	LtcRatingGetModel.Flight flight = new LtcRatingGetModel.Flight(flightNumber, travelDateString, travelDateString, ratingIds, flightRatings, limitTo, offset);
 			response = '{ "flight": ' + flight.toString() + '}';

        } catch (Exception ex) {
 			if (LtcUtil.isDevOrg()) {
 				response = ex.getMessage() + ' ' + ex.getStackTraceString();    
        	}
 			System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        
//        res.addHeader('Content-Type', 'application/json');
//        res.responseBody = Blob.valueOf(response);
    	return response;
    }
    

}