/********************************************************************** 
 Name:  MetaDataParserTest
 Task:    N/A
 Runs on: MetaDataParser
====================================================== 
Purpose: 
    This class contains unit tests for validating the behavior of Apex classes and triggers. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     David van't Hooft 	31/07/2014      Initial Development
    2.0		Ivan Botta			07/08/2014		Finishing class - creation of test scenarios
***********************************************************************/
@isTest(SeeAllData=true)
private class MetaDataParserTest {

	/**
	 * Test the metadata parser for the Listviews and the QueryFilter
	 *
    static testMethod void myUnitTest() {
        MetaDataParser mdp = new MetaDataParser('Case');
        List<MetadataService.ListView> listviews = mdp.readListViews();
        System.assert(listviews!=null);
  	    System.debug('listviews'+listviews);
        
        String query;
		for(MetadataService.ListView listView : listViews) {
	        query = mdp.getQueryFilter(listview.fullName);
    	    System.assert(query!=null);
    	    System.debug('queryfilter'+query);
			//break;
		}        
    }*/
    
    
    
	//@isTest(SeeAllData=true)
	// Check if the default controller and overriden controller
    static testMethod void myControllerMultipleUnorderedTest1() { 
    	
		Test.setMock(WebServiceMock.class, new AFKLM_SCS_MetadataServiceMockClassTest(AFKLM_SCS_MetadataServiceMockClassTest.MULTIPLE_UNORDERED_TEST1));
		Group grp = new Group(DeveloperName = 'DummyName', Name = 'Dummy Name', type = 'Queue');
		insert grp;
		
		Test.startTest();
	
			
        MetaDataParser mdp = new MetaDataParser('Case');
        List<MetadataService.ListView> listviews = mdp.readListViews();
        System.assert(listviews!=null);
  	    System.debug('listviews'+listviews);
	
        String query;
        Integer cnt = 0;
		for(MetadataService.ListView listView : listViews) {
	        query = mdp.getQueryFilter(listview.fullName);
    	    System.assert(query!=null);
    	    System.debug('queryfilter'+query);
			//break;
		}        
		
		Test.stopTest();		
    }

    static testMethod void myControllerMultipleUnorderedTest2() { 
    	
		Test.setMock(WebServiceMock.class, new AFKLM_SCS_MetadataServiceMockClassTest(AFKLM_SCS_MetadataServiceMockClassTest.MULTIPLE_UNORDERED_TEST2));
		Group grp = new Group(DeveloperName = 'DummyName', Name = 'Dummy Name', type = 'Queue');
		insert grp;
		
		Test.startTest();
	
			
        MetaDataParser mdp = new MetaDataParser('Case');
        List<MetadataService.ListView> listviews = mdp.readListViews();
        System.assert(listviews!=null);
  	    System.debug('listviews'+listviews);
	
        String query;
        Integer cnt = 0;
		for(MetadataService.ListView listView : listViews) {
	        query = mdp.getQueryFilter(listview.fullName);
    	    System.assert(query!=null);
    	    System.debug('queryfilter'+query);
			//break;
		}        
		
		Test.stopTest();		
    }
    
    static testMethod void myControllerSingleTest() { 
    	
		Test.setMock(WebServiceMock.class, new AFKLM_SCS_MetadataServiceMockClassTest(AFKLM_SCS_MetadataServiceMockClassTest.SINGLE_TEST1));
		Group grp = new Group(DeveloperName = 'DummyName', Name = 'Dummy Name', type = 'Queue');
		insert grp;
		
		Test.startTest();
	
			
        MetaDataParser mdp = new MetaDataParser('Case');
        List<MetadataService.ListView> listviews = mdp.readListViews();
        System.assert(listviews!=null);
  	    System.debug('listviews'+listviews);
	
        String query;
        Integer cnt = 0;
		for(MetadataService.ListView listView : listViews) {
	        query = mdp.getQueryFilter(listview.fullName);
    	    System.assert(query!=null);
    	    System.debug('queryfilter'+query);
			//break;
		}        
		
		Test.stopTest();		
    }    

    static testMethod void myControllerQueueTest() { 
    	
		Test.setMock(WebServiceMock.class, new AFKLM_SCS_MetadataServiceMockClassTest(AFKLM_SCS_MetadataServiceMockClassTest.QUEUE_TEST1));
		Group grp = new Group(DeveloperName = 'DummyName', Name = 'Dummy Name', type = 'Queue');
		insert grp;
		
		Test.startTest();
	
			
        MetaDataParser mdp = new MetaDataParser('Case');
        List<MetadataService.ListView> listviews = mdp.readListViews();
        System.assert(listviews!=null);
  	    System.debug('listviews'+listviews);
	
        String query;
        Integer cnt = 0;
		for(MetadataService.ListView listView : listViews) {
	        query = mdp.getQueryFilter(listview.fullName);
    	    System.assert(query!=null);
    	    System.debug('queryfilter'+query);
			//break;
		}        
		
		Test.stopTest();		
    }    
      
      
    static testMethod void myControllerPicklistTest() { 
        MetaDataParser mdp = new MetaDataParser('Case');
        List<Schema.PicklistEntry> ple = mdp.getPicklistValues('Origin');
   	    System.assert(ple!=null);
    }
}