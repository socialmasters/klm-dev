/********************************************************************** 
 Name:  SCS_SocialPostReplyParentIdNotChangedTest
====================================================== 
Purpose: Test class: ParentId from SocialPost cannot be modified after auto creation of Case for Facebook Reply.

====================================================== */
@isTest 
private class SCS_SPReplyParentIdNotChangedTest {
    static testMethod void validateSocialPostInsertReply() {
        Case cs = new Case(Last_Name__c = '0222222');
        insert cs;
       
        SocialPost sp = new SocialPost(Name='SocialPost with parent',MessageType = 'Reply', MediaType = 'Facebook', ParentId = cs.id);
        insert sp;
       
       sp = [SELECT Name FROM SocialPost WHERE Id =: sp.Id];
       System.assertEquals('SocialPost with parent', sp.Name);
    }
    
    static testMethod void validateSocialPostInsertError() {
        SocialPost sp = new SocialPost(Name='SocialPost without parent',MessageType = 'Reply', MediaType = 'Facebook');
        insert sp;
       
		try {
			sp.Name = 'Try to update SP without ParentId';
			update sp;
		} catch(Exception ex) {
			System.Assert(ex.getMessage().contains('Sorry, you are not allowed to create a case on Facebook Reply level.'));
		}
    }
}