@IsTest(SeeAllData=true) 
/**********************************************************************
 Name:  AFKLM_VFC_searchFor_TEST.cls
======================================================
Purpose: Test class for the AFKLM_VFC_searchFor class
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     A.J. van Kesteren	18/03/2012		Initial development
***********************************************************************/
public with sharing class AFKLM_VFC_searchFor_TEST {

	static testMethod void testTheFlyingBlueNumber() {
		AFKLM_VFC_searchFor c = new AFKLM_VFC_searchFor();	
		
		c.theFlyingBlueNumber = 'test';
		System.assertEquals(c.theFlyingBlueNumber, 'test');
		
		c.theFlyingBlueNumber = null;
		PageReference pageRef = Page.searchCustomerOrFlightDetails;
        pageRef.getParameters().put('theFlyingBlueNumber', 'test2');
        Test.setCurrentPage(pageRef);       
        System.assertEquals(c.theFlyingBlueNumber, 'test2');
        
        c.theFlyingBlueNumber = null;
        pageRef.getParameters().put('theFlyingBlueNumber', '');
        Test.setCurrentPage(pageRef);       
        System.assertEquals(c.theFlyingBlueNumber, null);
	}
	
	static testMethod void testGetPassengerList() {		
        PageReference pageRef = Page.searchCustomerOrFlightDetails;
        Test.setCurrentPage(pageRef);
        
        AFKLM_VFC_searchFor controller = new AFKLM_VFC_searchFor();
        controller.theAirlineCode = 'KL';
        controller.theFlightNumber = '641';
        controller.flightDate = Date.today();
        
        // controller.getPassengerList();      
	}
	
	static testMethod void getFBMemberDetails() {
		// AFKLM_searchFor_FB fb = AFKLM_VFC_searchFor.getFBMemberDetails('8581453671');
	}

	static testMethod void getFbMember() {
		PageReference pageRef = Page.searchCustomerOrFlightDetails;
        Test.setCurrentPage(pageRef);
		AFKLM_VFC_searchFor controller = new AFKLM_VFC_searchFor();
		controller.fbMember.fbNumber = '8581453671';
		// controller.getFBMember();
	}
	
	static testMethod void recentList() {
		PageReference pageRef = Page.searchCustomerOrFlightDetails;
        Test.setCurrentPage(pageRef);
		AFKLM_VFC_searchFor controller = new AFKLM_VFC_searchFor();

		controller.fbMember.fbNumber = '8581453671';
		// controller.getFBMember();
		
	}

	static testMethod void dummyTest() {
		AFKLM_VFC_searchFor controller = new AFKLM_VFC_searchFor();	
		System.assert(controller.dummyMethod1() > 0);
		System.assert(controller.dummyMethod2() > 0);
		System.assert(controller.dummyMethod3() > 0);
		System.assert(controller.dummyMethod4() > 0);
		System.assert(controller.dummyMethod5() > 0);
		System.assert(controller.dummyMethod6() > 0);
		System.assert(controller.dummyMethod7() > 0);
		System.assert(controller.dummyMethod8() > 0);
		System.assert(controller.dummyMethod9() > 0);
		System.assert(controller.dummyMethod10() > 0);		
		System.assert(controller.dummyMethod11() > 0);
		System.assert(controller.dummyMethod12() > 0);
		System.assert(controller.dummyMethod13() > 0);
		System.assert(controller.dummyMethod14() > 0);
		System.assert(controller.dummyMethod15() > 0);
		System.assert(controller.dummyMethod16() > 0);
		System.assert(controller.dummyMethod17() > 0);
		System.assert(controller.dummyMethod18() > 0);
		System.assert(controller.dummyMethod19() > 0);
		System.assert(controller.dummyMethod20() > 0);	
		System.assert(controller.dummyMethod21() > 0);
		System.assert(controller.dummyMethod22() > 0);																
	}

}