/**                     
 * @author (s)      : David van 't Hooft
 * @description     : Date formatter respecting the country and language code
 * @log:   8MAY2014: version 1.0
 * @log:   2JUN2014: version 1.1 Mees Witteman added handling for language in the Accept-language format
 */
public with sharing class LtcDateTimeFormatter {
	
	 static final String DEFAULT_LANGUAGE = 'en';
	 
	 static Map<String, String> mapDateTimeValues = new Map<String, String>();
	 static Map<String, String> mapDateValues = new Map<String, String>();
	 
	 static {
	 	mapDateTimeValues();
	 	mapDateValues();
	 }

	// returns the properly formatted datetime value
	public String formatDateTime(DateTime ddate_Time, String acceptLanguage) {
	    System.debug('user_locale' + acceptLanguage);
	    String date_time_format = findFormat(acceptLanguage, mapDateTimeValues);
	    return ddate_Time.format(date_time_format); //create a string with the proper format
	}
	
	// Deprecated preferred use Accept-Language header format in language parameter
	// returns the properly formatted datetime value
	public String formatDateTime(DateTime ddate_Time, String language, String country) {
	    String user_locale = '';
	    if (language!=null && country!='') {
	    	user_locale = language.toLowerCase();
		    if (country!=null && country!='') {
		    	user_locale += '-' + country.toUpperCase(); //format locale key
		    }
	    }
	    String date_time_format = 'M/d/yyyy h:mm a'; //variable for the datetime format defaulted to the US format
	    System.debug('user_locale'+user_locale);
	    if (mapDateTimeValues.containsKey(user_locale)) { //if the map contains the correct datetime format
	        date_time_format = mapDateTimeValues.get(user_locale); //grab the datetime format for the locale
	    }
	    System.debug('datetime_format'+date_time_format);
	    return ddate_Time.format(date_time_format); //create a string with the proper format
	}

	// returns the properly formatted datetime value
	public String formatDate(Date ddate, String acceptLanguage) {
	    System.debug('acceptLanguage' + acceptLanguage);
	    String date_format = findFormat(acceptLanguage, mapDateValues);
	    return DateTime.newInstance(ddate, Time.newInstance(0,0,0,0)).format(date_format);  //create a string with the proper format
	}

	// Deprecated preferred use Accept-Language header format in language parameter
	// returns the properly formatted datetime value 
	public String formatDate(Date ddate, String language, String country) {
	    String user_locale = '';
	    if (language!=null) {
	    	user_locale = language.toLowerCase();
		    if (country!=null && country!='') {
		    	user_locale += '-' + country.toUpperCase(); //format locale key
		    }
	    }
	    //String datetime_format = 'M/d/yyyy h:mm a'; //variable for the datetime format defaulted to the US format
	    String date_format = 'M/d/yyyy'; //variable for the datetime format defaulted to the US format
	    System.debug('user_locale'+user_locale);
	    if (mapDateValues.containsKey(user_locale)) { //if the map contains the correct datetime format
	        date_format = mapDateValues.get(user_locale); //grab the datetime format for the locale
	    }
	    System.debug('date_format'+date_format);
	    return DateTime.newInstance(ddate, Time.newInstance(0,0,0,0)).format(date_format);  //create a string with the proper format
	}
	
	private String findFormat(String language, Map<String, String> mappedValues) {
		String format;
		if (mappedValues.containsKey(language)) { //if the map contains the correct format
	        format = mappedValues.get(language); //grab the format for the locale
	    } else if (language != null && language.length() >= 2 && mappedValues.containsKey(language.substring(0, 2))) { // try finding with language part only
	    	format = mappedValues.get(language.substring(0, 2));
	    } else {
	    	format = mappedValues.get(DEFAULT_LANGUAGE); // fallback to default language
	    }
	    System.debug('format' + format);
	    return format;
	}

	/**
	 * populate a map with locale values and corresponding datetime formats
	 **/
	private static void MapDateTimeValues() {
	    MapDateTimeValues.put('ar', 'dd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('ar-AE', 'dd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('ar-BH', 'dd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('ar-JO', 'dd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('ar-KW', 'dd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('ar-LB', 'dd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('ar-SA', 'dd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('bg', 'yyyy-M-d H:mm');
	    MapDateTimeValues.put('bg-BG', 'yyyy-M-d H:mm');
	    MapDateTimeValues.put('ca', 'dd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('ca-ES', 'dd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('ca-ES_EURO', 'dd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('cs', 'd.M.yyyy H:mm');
	    MapDateTimeValues.put('cs-CZ', 'd.M.yyyy H:mm');
	    MapDateTimeValues.put('da', 'dd-MM-yyyy HH:mm');
	    MapDateTimeValues.put('da-DK', 'dd-MM-yyyy HH:mm');
	    MapDateTimeValues.put('de', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('de-AT', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('de-AT_EURO', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('de-CH', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('de-DE', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('de-DE_EURO', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('de-LU', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('de-LU_EURO', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('el', 'd/M/yyyy h:mm a');
	    MapDateTimeValues.put('el-GR', 'd/M/yyyy h:mm a');
	    MapDateTimeValues.put('en', 'dd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('en-AU', 'd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('en-B', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('en-BM', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('en-CA', 'dd/MM/yyyy h:mm a');
	    MapDateTimeValues.put('en-GB', 'dd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('en-GH', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('en-ID', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('en-IE', 'dd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('en-IE_EURO', 'dd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('en-NZ', 'd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('en-SG', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('en-US', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('en-ZA', 'yyyy/MM/dd hh:mm a');
	    MapDateTimeValues.put('es', 'd/MM/yyyy H:mm');
	    MapDateTimeValues.put('es-AR', 'dd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('es-BO', 'dd-MM-yyyy hh:mm a');
	    MapDateTimeValues.put('es-CL', 'dd-MM-yyyy hh:mm a');
	    MapDateTimeValues.put('es-CO', 'd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('es-CR', 'dd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('es-EC', 'dd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('es-ES', 'd/MM/yyyy H:mm');
	    MapDateTimeValues.put('es-ES_EURO', 'd/MM/yyyy H:mm');
	    MapDateTimeValues.put('es-GT', 'd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('es-HN', 'MM-dd-yyyy hh:mm a');
	    MapDateTimeValues.put('es-MX', 'd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('es-PE', 'dd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('es-PR', 'MM-dd-yyyy hh:mm a');
	    MapDateTimeValues.put('es-PY', 'dd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('es-SV', 'MM-dd-yyyy hh:mm a');
	    MapDateTimeValues.put('es-UY', 'dd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('es-VE', 'dd/MM/yyyy hh:mm a');
	    MapDateTimeValues.put('et-EE', 'd.MM.yyyy H:mm');
	    MapDateTimeValues.put('fi', 'd.M.yyyy H:mm');
	    MapDateTimeValues.put('fi-FI', 'd.M.yyyy H:mm');
	    MapDateTimeValues.put('fi-FI_EURO', 'd.M.yyyy H:mm');
	    MapDateTimeValues.put('fr', 'dd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('fr-BE', 'd/MM/yyyy H:mm');
	    MapDateTimeValues.put('fr-CA', 'yyyy-MM-dd HH:mm');
	    MapDateTimeValues.put('fr-CH', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('fr-FR', 'dd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('fr-FR_EURO', 'dd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('fr-LU', 'dd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('fr-MC', 'dd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('hr', 'yyyy.MM.dd HH:mm');
	    MapDateTimeValues.put('hr-HR', 'yyyy.MM.dd HH:mm');
	    MapDateTimeValues.put('hu', 'yyyy.MM.dd. H:mm');
	    MapDateTimeValues.put('hy', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('hy-AM', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('is', 'd.M.yyyy HH:mm');
	    MapDateTimeValues.put('is-IS', 'd.M.yyyy HH:mm');
	    MapDateTimeValues.put('it', 'dd/MM/yyyy H.mm');
	    MapDateTimeValues.put('it-CH', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('it-IT', 'dd/MM/yyyy H.mm');
	    MapDateTimeValues.put('iw', 'HH:mm dd/MM/yyyy');
	    MapDateTimeValues.put('iw-IL', 'HH:mm dd/MM/yyyy');
	    MapDateTimeValues.put('ja', 'yyyy/MM/dd H:mm');
	    MapDateTimeValues.put('ja-JP', 'yyyy/MM/dd H:mm');
	    MapDateTimeValues.put('kk', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('kk-KZ', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('km', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('km-KH', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('ko', 'yyyy. M. d a h:mm');
	    MapDateTimeValues.put('ko-KR', 'yyyy. M. d a h:mm');
	    MapDateTimeValues.put('lt', 'yyyy.M.d HH.mm');
	    MapDateTimeValues.put('lt-LT', 'yyyy.M.d HH.mm');
	    MapDateTimeValues.put('lv', 'yyyy.d.M HH:mm');
	    MapDateTimeValues.put('lv-LV', 'yyyy.d.M HH:mm');
	    MapDateTimeValues.put('ms', 'dd/MM/yyyy h:mm a');
	    MapDateTimeValues.put('ms-MY', 'dd/MM/yyyy h:mm a');
	    MapDateTimeValues.put('nl', 'd-M-yyyy H:mm');
	    MapDateTimeValues.put('nl-BE', 'd/MM/yyyy H:mm');
	    MapDateTimeValues.put('nl-NL', 'd-M-yyyy H:mm');
	    MapDateTimeValues.put('nl-SR', 'd-M-yyyy H:mm');
	    MapDateTimeValues.put('no', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('no-NO', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('pl', 'yyyy-MM-dd HH:mm');
	    MapDateTimeValues.put('pt', 'dd-MM-yyyy H:mm');
	    MapDateTimeValues.put('pt-AO', 'dd-MM-yyyy H:mm');
	    MapDateTimeValues.put('pt-BR', 'dd/MM/yyyy HH:mm');
	    MapDateTimeValues.put('pt-PT', 'dd-MM-yyyy H:mm');
	    MapDateTimeValues.put('ro', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('ro-RO', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('ru', 'dd.MM.yyyy H:mm');
	    MapDateTimeValues.put('sk', 'd.M.yyyy H:mm');
	    MapDateTimeValues.put('sk-SK', 'd.M.yyyy H:mm');
	    MapDateTimeValues.put('sl', 'd.M.y H:mm');
	    MapDateTimeValues.put('sl-SI', 'd.M.y H:mm');
	    MapDateTimeValues.put('sv', 'yyyy-MM-dd HH:mm');
	    MapDateTimeValues.put('sv-SE', 'yyyy-MM-dd HH:mm');
	    MapDateTimeValues.put('th', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('th-TH', 'd/M/yyyy, H:mm ?.');
	    MapDateTimeValues.put('tr', 'dd.MM.yyyy HH:mm');
	    MapDateTimeValues.put('ur', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('ur-PK', 'M/d/yyyy h:mm a');
	    MapDateTimeValues.put('vi', 'HH:mm dd/MM/yyyy');
	    MapDateTimeValues.put('vi-VN', 'HH:mm dd/MM/yyyy');
	    MapDateTimeValues.put('zh', 'yyyy-M-d ah:mm');
	    MapDateTimeValues.put('zh-CN', 'yyyy-M-d ah:mm');
	    MapDateTimeValues.put('zh-HK', 'yyyy-M-d ah:mm');
	    MapDateTimeValues.put('zh-TW', 'yyyy/M/d a h:mm');
	    
	}

	/**
	 * populate a map with locale values and corresponding datetime formats
	 **/
	private static void  MapDateValues() {
	    MapDateValues.put('ar', 'dd/MM/yyyy');
	    MapDateValues.put('ar-AE', 'dd/MM/yyyy');
	    MapDateValues.put('ar-BH', 'dd/MM/yyyy');
	    MapDateValues.put('ar-JO', 'dd/MM/yyyy');
	    MapDateValues.put('ar-KW', 'dd/MM/yyyy');
	    MapDateValues.put('ar-LB', 'dd/MM/yyyy');
	    MapDateValues.put('ar-SA', 'dd/MM/yyyy');
	    MapDateValues.put('bg', 'yyyy-M-d');
	    MapDateValues.put('bg-BG', 'yyyy-M-d');
	    MapDateValues.put('ca', 'dd/MM/yyyy');
	    MapDateValues.put('ca-ES', 'dd/MM/yyyy');
	    MapDateValues.put('ca-ES_EURO', 'dd/MM/yyyy');
	    MapDateValues.put('cs', 'd.M.yyyy');
	    MapDateValues.put('cs-CZ', 'd.M.yyyy');
	    MapDateValues.put('da', 'dd-MM-yyyy');
	    MapDateValues.put('da-DK', 'dd-MM-yyyy');
	    MapDateValues.put('de', 'dd.MM.yyyy');
	    MapDateValues.put('de-AT', 'dd.MM.yyyy');
	    MapDateValues.put('de-AT_EURO', 'dd.MM.yyyy');
	    MapDateValues.put('de-CH', 'dd.MM.yyyy');
	    MapDateValues.put('de-DE', 'dd.MM.yyyy');
	    MapDateValues.put('de-DE_EURO', 'dd.MM.yyyy');
	    MapDateValues.put('de-LU', 'dd.MM.yyyy');
	    MapDateValues.put('de-LU_EURO', 'dd.MM.yyyy');
	    MapDateValues.put('el', 'd/M/yyyy');
	    MapDateValues.put('el-GR', 'd/M/yyyy');
	    MapDateValues.put('en', 'dd/MM/yyyy');
	    MapDateValues.put('en-AU', 'd/MM/yyyy');
	    MapDateValues.put('en-B', 'M/d/yyyy');
	    MapDateValues.put('en-BM', 'M/d/yyyy');
	    MapDateValues.put('en-CA', 'dd/MM/yyyy');
	    MapDateValues.put('en-GB', 'dd/MM/yyyy');
	    MapDateValues.put('en-GH', 'M/d/yyyy');
	    MapDateValues.put('en-ID', 'M/d/yyyy');
	    MapDateValues.put('en-IE', 'dd/MM/yyyy');
	    MapDateValues.put('en-IE_EURO', 'dd/MM/yyyy');
	    MapDateValues.put('en-NZ', 'd/MM/yyyy');
	    MapDateValues.put('en-SG', 'M/d/yyyy');
	    MapDateValues.put('en-US', 'M/d/yyyy');
	    MapDateValues.put('en-ZA', 'yyyy/MM/dd');
	    MapDateValues.put('es', 'd/MM/yyyy');
	    MapDateValues.put('es-AR', 'dd/MM/yyyy');
	    MapDateValues.put('es-BO', 'dd-MM-yyyy');
	    MapDateValues.put('es-CL', 'dd-MM-yyyy');
	    MapDateValues.put('es-CO', 'd/MM/yyyy');
	    MapDateValues.put('es-CR', 'dd/MM/yyyy');
	    MapDateValues.put('es-EC', 'dd/MM/yyyy');
	    MapDateValues.put('es-ES', 'd/MM/yyyy');
	    MapDateValues.put('es-ES_EURO', 'd/MM/yyyy');
	    MapDateValues.put('es-GT', 'd/MM/yyyy');
	    MapDateValues.put('es-HN', 'MM-dd-yyyy');
	    MapDateValues.put('es-MX', 'd/MM/yyyy');
	    MapDateValues.put('es-PE', 'dd/MM/yyyy');
	    MapDateValues.put('es-PR', 'MM-dd-yyyy');
	    MapDateValues.put('es-PY', 'dd/MM/yyyy');
	    MapDateValues.put('es-SV', 'MM-dd-yyyy');
	    MapDateValues.put('es-UY', 'dd/MM/yyyy');
	    MapDateValues.put('es-VE', 'dd/MM/yyyy');
	    MapDateValues.put('et-EE', 'd.MM.yyyy');
	    MapDateValues.put('fi', 'd.M.yyyy');
	    MapDateValues.put('fi-FI', 'd.M.yyyy');
	    MapDateValues.put('fi-FI_EURO', 'd.M.yyyy');
	    MapDateValues.put('fr', 'dd/MM/yyyy');
	    MapDateValues.put('fr-BE', 'd/MM/yyyy');
	    MapDateValues.put('fr-CA', 'yyyy-MM-dd');
	    MapDateValues.put('fr-CH', 'dd.MM.yyyy');
	    MapDateValues.put('fr-FR', 'dd/MM/yyyy');
	    MapDateValues.put('fr-FR_EURO', 'dd/MM/yyyy');
	    MapDateValues.put('fr-LU', 'dd/MM/yyyy');
	    MapDateValues.put('fr-MC', 'dd/MM/yyyy');
	    MapDateValues.put('hr', 'yyyy.MM.dd');
	    MapDateValues.put('hr-HR', 'yyyy.MM.dd');
	    MapDateValues.put('hu', 'yyyy.MM.dd.');
	    MapDateValues.put('hy', 'M/d/yyyy');
	    MapDateValues.put('hy-AM', 'M/d/yyyy');
	    MapDateValues.put('is', 'd.M.yyyy');
	    MapDateValues.put('is-IS', 'd.M.yyyy');
	    MapDateValues.put('it', 'dd/MM/yyyy');
	    MapDateValues.put('it-CH', 'dd.MM.yyyy');
	    MapDateValues.put('it-IT', 'dd/MM/yyyy');
	    MapDateValues.put('iw', 'HH:mm dd/MM/yyyy');
	    MapDateValues.put('iw-IL', 'HH:mm dd/MM/yyyy');
	    MapDateValues.put('ja', 'yyyy/MM/dd');
	    MapDateValues.put('ja-JP', 'yyyy/MM/dd');
	    MapDateValues.put('kk', 'M/d/yyyy');
	    MapDateValues.put('kk-KZ', 'M/d/yyyy');
	    MapDateValues.put('km', 'M/d/yyyy');
	    MapDateValues.put('km-KH', 'M/d/yyyy');
	    MapDateValues.put('ko', 'yyyy. M. d');
	    MapDateValues.put('ko-KR', 'yyyy. M. d');
	    MapDateValues.put('lt', 'yyyy.M.d');
	    MapDateValues.put('lt-LT', 'yyyy.M.d');
	    MapDateValues.put('lv', 'yyyy.d.M');
	    MapDateValues.put('lv-LV', 'yyyy.d.M');
	    MapDateValues.put('ms', 'dd/MM/yyyy');
	    MapDateValues.put('ms-MY', 'dd/MM/yyyy');
	    MapDateValues.put('nl', 'd-M-yyyy');
	    MapDateValues.put('nl-BE', 'd/MM/yyyy');
	    MapDateValues.put('nl-NL', 'd-M-yyyy');
	    MapDateValues.put('nl-SR', 'd-M-yyyy');
	    MapDateValues.put('no', 'dd.MM.yyyy');
	    MapDateValues.put('no-NO', 'dd.MM.yyyy');
	    MapDateValues.put('pl', 'yyyy-MM-dd');
	    MapDateValues.put('pt', 'dd-MM-yyyy');
	    MapDateValues.put('pt-AO', 'dd-MM-yyyy');
	    MapDateValues.put('pt-BR', 'dd/MM/yyyy');
	    MapDateValues.put('pt-PT', 'dd-MM-yyyy');
	    MapDateValues.put('ro', 'dd.MM.yyyy');
	    MapDateValues.put('ro-RO', 'dd.MM.yyyy');
	    MapDateValues.put('ru', 'dd.MM.yyyy');
	    MapDateValues.put('sk', 'd.M.yyyy');
	    MapDateValues.put('sk-SK', 'd.M.yyyy');
	    MapDateValues.put('sl', 'd.M.y');
	    MapDateValues.put('sl-SI', 'd.M.y');
	    MapDateValues.put('sv', 'yyyy-MM-dd');
	    MapDateValues.put('sv-SE', 'yyyy-MM-dd');
	    MapDateValues.put('th', 'M/d/yyyy');
	    MapDateValues.put('th-TH', 'd/M/yyyy');
	    MapDateValues.put('tr', 'dd.MM.yyyy');
	    MapDateValues.put('ur', 'M/d/yyyy');
	    MapDateValues.put('ur-PK', 'M/d/yyyy');
	    MapDateValues.put('vi', 'dd/MM/yyyy');
	    MapDateValues.put('vi-VN', 'dd/MM/yyyy');
	    MapDateValues.put('zh', 'yyyy-M-d');
	    MapDateValues.put('zh-CN', 'yyyy-M-d');
	    MapDateValues.put('zh-HK', 'yyyy-M-d');
	    MapDateValues.put('zh-TW', 'yyyy/M/d');
	}

}