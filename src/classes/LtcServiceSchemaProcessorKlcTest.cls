/**
 * Test the KLC service schema processor
 */
@isTest
private class LtcServiceSchemaProcessorKlcTest {
	
	static testMethod void testServiceItemCreation() {
        CabinClass__c economyClass = new CabinClass__c();
        economyClass.name = 'economy';
        economyClass.label__c = 'economy';
        insert economyClass;
        
        CabinClass__c businessClass = new CabinClass__c();
        businessClass.name = 'business';
        businessClass.label__c = 'business';
        insert businessClass;
        
        StaticResource sr = [
			Select Name, Id, Description 
			From StaticResource 
			where name =: 'LTCServiceSchedule'
		];

        
        TestProcessor processor = new TestProcessor();
        processor.processServiceSchema();
        
        List<ServiceItem__c> sis = [
            select id from ServiceItem__c
        ];
        
        System.assertEquals(9, sis.size());
    }

	private class TestProcessor extends LtcServiceSchemaProcessorKlc {
      
        private virtual String[] getLinesFromResource() {
            String lines = '- 2nd service is voor c-class en m-class,,,,,,,,,,,,,,' +
			'/n- wanneer er geen keuze is aan drinken (orange juice of water) eten tegelijk. Wanneer er sprake is van choice 1/2/3 dan wordt eerst ,,,,,,,,,,,,,,,' +
			'/nFlt,Startper,Endper,Freq,Dep,Arr,Deptm,Arrtm,Service,Food c-class,Drinks c-class,Food m-class,Drinks m-class,,,' +
			'/nKL0947,18-May-15,24-Oct-15,1234567,AMS,BHD,8:00,9:40,L,Lunch*,Choice 3*,Choice: sweet or savoury snack,Choice 1*, , ,' +
			'/nKL0948,18-May-15,24-Oct-15,1234567,BHD,AMS,10:15,11:50,L,Lunch*,Choice 3*,Choice: sweet or savoury snack,Choice 1*, , ,' +
			'/nKL0953,29-Mar-15,5-Jul-15,1234567,AMS,NCL,6:25,7:40,L,Breakfast*,Choice 3*,Fresh sandwich,Choice 1*, , ,' +
			'/nKL0953,31-Aug-15,24-Oct-15,1234567,AMS,NCL,6:25,7:40,L,Breakfast*,Choice 3*,Fresh sandwich,Choice 1*, , ,' +
			'/nKL1163,29-Mar-15,23-Oct-15,1234567,AMS,GOT,19:15,20:45,L,Dinner*,Choice 3*,Choice: sweet or savoury snack,Choice 1*, , ,' +
			'/nKL1163,24-Oct-15,24-Oct-15,_____6_,AMS,GOT,19:15,20:45,L,Dinner*,Choice 3*,Choice: sweet or savoury snack,Choice 1*, , ,' +
			'/nKL1172,29-Mar-15,24-Oct-15,1234567,TRD,AMS,4:15,6:30,XL,Breakfast*,Choice 3*,Fresh sandwich,Choice 2*,+2nd service: cookie + coffee/tea, ,' +
			'/nKL1173,29-Mar-15,24-Oct-15,1234567,AMS,TRD,7:35,9:45,XL,Breakfast*,Choice 3*,Fresh sandwich,Choice 2*,+2nd service: cookie + coffee/tea, ,' +
			'/nKL1174,29-Mar-15,24-Oct-15,1234567,TRD,AMS,10:15,12:30,XL,Lunch*,Choice 3*,Fresh sandwich,Choice 2*,+2nd service: cookie + coffee/tea, ,' +
			'/nKL1175,29-Mar-15,18-Oct-15,______7,AMS,TRD,12:30,14:40,XL,Lunch*,Choice 3*,Fresh sandwich,Choice 2*,+2nd service: cookie + coffee/tea, ,' +
			'/nKL1175,30-Mar-15,3-Jul-15,12345__,AMS,TRD,12:30,14:40,XL,Lunch*,Choice 3*,Fresh sandwich,Choice 2*,+2nd service: cookie + coffee/tea, ,' +
			'/nKL1175,4-Apr-15,25-Jul-15,_____6_,AMS,TRD,12:30,14:40,XL,Lunch*,Choice 3*,Fresh sandwich,Choice 2*,+2nd service: cookie + coffee/tea, ,' +
			'/nKL1175,31-Aug-15,24-Oct-15,123456_,AMS,TRD,12:30,14:40,XL,Lunch*,Choice 3*,Fresh sandwich,Choice 2*,+2nd service: cookie + coffee/tea, ,' +
			'/nKL1175,6-Jul-15,28-Aug-15,12345__,AMS,TRD,12:25,14:35,XL,Lunch*,Choice 3*,Fresh sandwich,Choice 2*,+2nd service: cookie + coffee/tea, ,' +
			'/nKL1175,1-Aug-15,1-Aug-15,_____6_,AMS,TRD,12:30,14:40,XL,Lunch*,Choice 3*,Fresh sandwich,Choice 2*,+2nd service: cookie + coffee/tea, ,' +
			'/nKL1176,29-Mar-15,18-Oct-15,______7,TRD,AMS,15:10,17:25,XL,Dinner*,Choice 3*,Fresh sandwich,Choice 2*,+2nd service: cookie + coffee/tea, ,';
            return lines.split('/n');
        }

    }
}