/**********************************************************************
 Name:  SMHStatisticsManager.cls
=======================================================================
Purpose: Manage SMHStatistics custom object by calculating new Social
         Media Hub statistics. 
=======================================================================
History                                                            
-------                                                            
VERSION     AUTHOR                DATE            DETAIL                                 
    1.0     Aard-Jan van Kesteren 17/07/2013      Initial development
    1.1		Patrick Brinksma	  04/11/2013	  Changed calculation based on Response_Time_Published_Date__c to Response_Time__c
    1.2		Stevano Cheung	  	  22/05/2014	  Changed calculation based on new Social Customer Service (SCS) configuration on SocialPost object
***********************************************************************/    
global with sharing class SMHStatisticsManager implements Schedulable {

	public static final String TWITTER = 'Twitter';
	public static final String FACEBOOK = 'Facebook';
	
	public static final Id SERVICING_RECORD_TYPE_ID = 
		[select Id from RecordType where Name = 'Servicing' and sObjectType='Case' limit 1].Id;
 
	private Integer periodHours;
 
 	public SMHStatisticsManager(Integer periodHours) {
 		this.periodHours = periodHours;
 	}
 
	public static void scheduleJobs() {
		// Schedule a job each 5 minutes to create a rolling window effect. 
		System.schedule('SMH Statistics *:00, Last 1 Hour', '0 0 * * * ? *', new SMHStatisticsManager(1));
		System.schedule('SMH Statistics *:05, Last 1 Hour', '0 5 * * * ? *', new SMHStatisticsManager(1));		
		System.schedule('SMH Statistics *:10, Last 1 Hour', '0 10 * * * ? *', new SMHStatisticsManager(1));
		System.schedule('SMH Statistics *:15, Last 1 Hour', '0 15 * * * ? *', new SMHStatisticsManager(1));
		System.schedule('SMH Statistics *:20, Last 1 Hour', '0 20 * * * ? *', new SMHStatisticsManager(1));
		System.schedule('SMH Statistics *:25, Last 1 Hour', '0 25 * * * ? *', new SMHStatisticsManager(1));
		System.schedule('SMH Statistics *:30, Last 1 Hour', '0 30 * * * ? *', new SMHStatisticsManager(1));
		System.schedule('SMH Statistics *:35, Last 1 Hour', '0 35 * * * ? *', new SMHStatisticsManager(1));
		System.schedule('SMH Statistics *:40, Last 1 Hour', '0 40 * * * ? *', new SMHStatisticsManager(1));
		System.schedule('SMH Statistics *:45, Last 1 Hour', '0 45 * * * ? *', new SMHStatisticsManager(1));
		System.schedule('SMH Statistics *:50, Last 1 Hour', '0 50 * * * ? *', new SMHStatisticsManager(1));
		System.schedule('SMH Statistics *:55, Last 1 Hour', '0 55 * * * ? *', new SMHStatisticsManager(1));
		
		// Schedule a job for each day
		System.schedule('SMH Statistics End Of Day, Last 24 Hours', '0 0 0 * * ? *', new SMHStatisticsManager(24));	
		
		// Schedule a job for each week	
		System.schedule('SMH Statistics End Of Week, Last Week', '0 0 19 ? * 1 *', new SMHStatisticsManager(7 * 24));
		
		// Seconds Minutes Hours Day_of_month Month Day_of_week optional_year	
	}
	
   /*
	* Method called by the Apex scheduler.
	*/
	global void execute(SchedulableContext sc) {
		// Schedule response times jobs
		(new SMHStatisticsResponseTimesCalculator(periodHours, SMHStatisticsManager.FACEBOOK)).calculate();
		(new SMHStatisticsResponseTimesCalculator(periodHours, SMHStatisticsManager.TWITTER)).calculate();
		(new SMHStatisticsResponseTimesCalculator(periodHours, null)).calculate();
	
		// Schedule resolution times jobs
		(new SMHStatisticsResolutionTimesCalculator(periodHours, SMHStatisticsManager.FACEBOOK)).calculate();
		(new SMHStatisticsResolutionTimesCalculator(periodHours, SMHStatisticsManager.TWITTER)).calculate();
		(new SMHStatisticsResolutionTimesCalculator(periodHours, null)).calculate();
	}
	
   /*
    * Super class for all SMH Statistics calculators
    */
	public abstract class SMHStatisticsCalculator {
		
		private Integer periodHours;
		private String origin;
		private List<String> origins;
		private DateTime periodStartDate;
		private DateTime periodEndDate;
		
		public SMHStatisticsCalculator(Integer periodHours, String origin) {
			this.periodHours = periodHours;
			this.origin = origin;
			// If the origin is filled, we create a list containing that one origin.
			// If the origin is null, we create a list containing all possible origins. 
			// We use this approach to later be able to use "origin in :origins" in 
			// the SOQL queries. 
			if (origin != null) {
				this.origins = new List<String>{origin};
			} else {
				this.origins = new List<String>{
					SMHStatisticsManager.FACEBOOK, 
					SMHStatisticsManager.TWITTER,
					'Hyves',
					'Blog',
					'Other',
					'ВКонтакте'
				};	
			}
		}
		
		// This method performs the calculation and stores the results in a new
		// SMHStatistics__c record. 
		public void calculate() {
			System.debug('@@@ Start calculating SMH statistics: Origin => ' + origin +', Period Hours => ' + periodHours);
			periodEndDate = DateTime.now();
			periodStartDate = periodEndDate.addHours(-1 * periodHours);
			
			SMHStatistics__c record = new SMHStatistics__c();
			record.Period_Hours__c = periodHours;
			record.Type__c = getType();
			System.debug('@@@ Type => ' + record.Type__c);
			record.Period_Start_Date__c = periodStartDate;
			System.debug('@@@ Period Start Date => ' + record.Period_Start_Date__c);
			record.Period_End_Date__c = periodEndDate;
			System.debug('@@@ Period End Date => ' + record.Period_End_Date__c);
			record.Origin__c = origin;
			
			List<SObject> results = doQuery();
			System.debug('@@@ Results.size => ' + results.size());
			
			Double totalMins = 0;
			Integer counter = 0;
			for (SObject o : results) {
				counter += 1;
				
				Double mins = getMinutes(o);
				totalMins += mins;
				
				// Check if this record the longest time within the top 50%
				if (record.Top_50_Longest_Minutes__c == null && counter >= results.size() * 0.5) {
					record.Top_50_Longest_Minutes__c = mins;
					record.Top_50_Average_Minutes__c = totalMins / counter;		
				}
				// Check if this record the longest time within the top 60%
				if (record.Top_60_Longest_Minutes__c == null && counter >= results.size() * 0.6) {
					record.Top_60_Longest_Minutes__c = mins;		
					record.Top_60_Average_Minutes__c = totalMins / counter;		
				}	
				// Check if this record the longest time within the top 70%
				if (record.Top_70_Longest_Minutes__c == null && counter >= results.size() * 0.7) {
					record.Top_70_Longest_Minutes__c = mins;		
					record.Top_70_Average_Minutes__c = totalMins / counter;		
				}	
				// Check if this record the longest time within the top 80%
				if (record.Top_80_Longest_Minutes__c == null && counter >= results.size() * 0.8) {
					record.Top_80_Longest_Minutes__c = mins;		
					record.Top_80_Average_Minutes__c = totalMins / counter;		
				}					
				// Check if this record the longest time within the top 90%
				if (record.Top_90_Longest_Minutes__c == null && counter >= results.size() * 0.9) {
					record.Top_90_Longest_Minutes__c = mins;		
					record.Top_90_Average_Minutes__c = totalMins / counter;		
				}	
			}
			
			record.Amount__c = results.size();
			record.Total_Minutes__c = totalMins;
			
			// If there are no results in the query, we still need to give some 
			// fields a value. 
			if (results.size() == 0) {
				record.Top_50_Average_Minutes__c = 0;
				record.Top_60_Average_Minutes__c = 0;
				record.Top_70_Average_Minutes__c = 0;
				record.Top_80_Average_Minutes__c = 0;
				record.Top_90_Average_Minutes__c = 0;
				
				record.Top_50_Longest_Minutes__c = 0;
				record.Top_60_Longest_Minutes__c = 0;
				record.Top_70_Longest_Minutes__c = 0;
				record.Top_80_Longest_Minutes__c = 0;
				record.Top_90_Longest_Minutes__c = 0;
			}
			
			insert record;
			System.debug('@@@ Completed calculating SMH statistics');
		}
		
		// Implement to indicate the type of the calculation
		public abstract String getType();
		
		// Implement to define the results over which the statistics
		// will be calculated
		public abstract List<SObject> doQuery();
		
		// Implement to define what the length value of a result object is 
		public abstract Double getMinutes(SObject o);
	}
	
   /*
    * Class the implements the calculation of response times.
    */	
	public class SMHStatisticsResponseTimesCalculator extends SMHStatisticsCalculator {
		
		public SMHStatisticsResponseTimesCalculator(Integer periodHours, String origin) {
			super(periodHours, origin);
		}	

		public override String getType() {
			return 'Response';
		}
		
		// SC: Commented out for now
		/* public override List<SObject> doQuery() {
			return [
				select Response_Time__c
				from sf4twitter__Twitter_Conversation__c
				where sf4twitter__Status__c = 'Actioned'
				// Added where clauses on LastModifiedDate purely because of
				// performance reasons. Functionally they are not needed, but
				// they are allowed and the index on them helps a lot. 
				and   LastModifiedDate > :periodStartDate
				and   Actioned_Date__c > :periodStartDate
				and   Actioned_Date__c <= :periodEndDate
				and   sf4twitter__Origin__c in :origins
				and Response_Time__c != null
				order by Response_Time__c asc
			];
		} */
		
		// Robertjan: currently set engaged speed less than 180 to be used for the monitoring, because of KLM internal FB process
		// RJ: Can you please investigate whether we can filter out Tweets and posts with a response time of 180+ min (example) for the SLA monitor
		public override List<SObject> doQuery() {
			return [
				select Engage_speed__c, Reply_speed__c
				from SocialPost
				where SCS_Status__c = 'Actioned'
				and   Ignore_for_SLA__c = false				
				and   LastModifiedDate > :periodStartDate
				and   Replied_date__c > :periodStartDate
				and   Replied_date__c <= :periodEndDate
				and   Provider in :origins
				and   IncludeResponseTime__c = true
				and   Engage_speed__c < 180
				and   Engage_speed__c != null
				and   Company__c = 'KLM'
				order by Engage_speed__c asc
			];
		}
				
		// SC: Commented out for now
		/* public override Double getMinutes(SObject o) {
			//return ((sf4twitter__Twitter_Conversation__c)o).Response_Time_Published_Date__c;
			return ((sf4twitter__Twitter_Conversation__c)o).Response_Time__c;
		}*/ 
		
		public override Double getMinutes(SObject o) {
			//return ((sf4twitter__Twitter_Conversation__c)o).Response_Time_Published_Date__c;
			if(Test.isRunningTest()) {
				//DvtH24JUL2014 Cannot use the calculated time there created time == now during testing
				return ((SocialPost)o).Reply_speed__c;
			} else {	
				return ((SocialPost)o).Engage_speed__c;
			}
		}
		
	}

   /*
    * Class the implements the calculation of resolution times.
    */	
	public class SMHStatisticsResolutionTimesCalculator extends SMHStatisticsCalculator {
		
		public SMHStatisticsResolutionTimesCalculator(Integer periodHours, String origin) {
			super(periodHours, origin);
		}
	
		public override String getType() {
			return 'Resolution';
		}	

		public override List<SObject> doQuery() {
			return [
				select Resolution_Time__c
				from Case
				where Status = 'Closed'
				// Added where clauses on LastModifiedDate purely because of
				// performance reasons. Functionally they are not needed, but
				// they are allowed and the index on them helps a lot. 
				and   LastModifiedDate > :periodStartDate
				and   ClosedDate > :periodStartDate
				and   ClosedDate <= :periodEndDate
				and   Origin in :origins
				and   RecordTypeId = :SERVICING_RECORD_TYPE_ID
				and   Sub_Reason__c	not in (
					'Advertisement / Spam - removed',
					'Delete case',
					'Elfstedentocht',
					'Jobs',
					'KLM post / comment',
					'Other language',
					'Picture / Video',
					'Stewardress Yourself',
					'Ticket to campaign',
					'Unclear comment'
				)	
				and Resolution_Time__c != null	
				order by Resolution_Time__c asc
			];
		}	
		
		// SC: Commented out for now
		public override Double getMinutes(SObject o) {
			return ((Case)o).Resolution_Time__c;
		}	
	}
}