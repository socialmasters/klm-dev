/**
 * @author (s)    :	David van 't Hooft, m.lapere@gen25.com
 * @description   : Apex Batch class for retrieving fox Data
 *
 * @log: 	12May2014: version 1.0
 *
 *	2015/04/13 (m.lapere@gen25.com) adapted the class to extend LtcQueueItemAbstractBatchable
 *	Also note that since recently, Salesforce has increased the callout limit to 100,
 *	so this batch can be executed with scopesize 100 (1 callout per item)
 */
public class LtcFoxServiceBatch extends LtcQueueItemAbstractBatchable implements Database.AllowsCallouts {

	@testVisible
	private Integer preDays = 0;
	
	/**
	 * @param daysToFlight integer (Accepted values -2, -1, 0, 1, 2) 
	 *		-2 = day before yesterday, -1 = yesterday, 0 = today, 1 = tomorrow, 2 is overmorrow
	 */
	public override void initParams(String params) {
		Integer daysToFlight = Integer.valueOf(params);
		if (daysToFlight >= -2 && daysToFlight < 3) {
			preDays = daysToFlight;
		}
	}
	
    public override Database.querylocator actualStart(Database.BatchableContext bc){
        return Database.getQueryLocator('SELECT Flight_Number__c FROM Monitor_Flight__c');
	}
    
	public override void actualExecute(Database.BatchableContext bc, sObject[] scopeSObjects){
		System.debug('LtcFoxServiceBatch execute');
		Monitor_Flight__c[] scope = (Monitor_Flight__c[]) scopeSObjects;
	 	Set<String> flightNumberSet = new Set<String>();
	 	LtcFoxResponseModel ltcFoxResponseModel;
		List<LtcFoxResponseModel.Flight> ltcFoxFlights = new List<LtcFoxResponseModel.Flight>();
	 	List<Flight__c> sFlightsToUpdate = new List<Flight__c>();
	 	List<Leg__c> sLegsToUpdate = new List<Leg__c>();
	 	Map<String, Flight__c> flightMap = new Map<String, Flight__c>();
	 	LtcFoxService ltcFoxService =  new LtcFoxService();
		Date d = Datetime.now().addDays(preDays).date();
		String travelDate = String.valueOf(d);
		String flightType = 'departure';
		String flightNumber;
		
		System.debug('Scope size:' + scope.size());
	 	for (Monitor_Flight__c monitor_flight : scope) {
	 		flightNumber = LtcUtil.cleanFlightNumber(monitor_flight.Flight_Number__c);
	 		flightNumberSet.add(flightNumber);

    		// Call fox service to receive flight info  
	 		ltcFoxResponseModel = ltcFoxService.getFoxFlightInfo(flightType, flightNumber, travelDate);
	 		if (ltcFoxResponseModel != null && ltcFoxResponseModel.flights != null) {
	 			ltcFoxFlights.addAll(ltcFoxResponseModel.flights);
	 			
 				for(LtcFoxResponseModel.Flight flight : ltcFoxResponseModel.flights) {
 					System.debug('add flight with operatingFlightCodes: ' + flight.operatingFlightCode);
 				}
	 		}
	 	}
		
	 	//Get current stored flight information to prevent duplications	 
		List<Flight__c> sfdcFlights = [
		    select Id, Flight_Number__c,
		    (select Id, legNumber__c from Legs__r)
		    from Flight__c f 
		    where f.Flight_Number__c in: flightNumberSet 
			and f.scheduled_departure_date__c =: d
		    limit 1000
		];
	 	
	 	//Map the sfdc data on flight number
	 	if (sfdcFlights != null) {
		 	for (Flight__c flight: sfdcFlights) {
		 		flightMap.put(flight.Flight_Number__c, flight);		 		
		 	}
	 	}
	 	
	 	//Store the fox data in SFDC 
	 	if (ltcFoxFlights != null) {
		 	for (LtcFoxResponseModel.Flight foxFlight : ltcFoxFlights) {
		 		flightNumber = LtcUtil.cleanFlightNumber(foxFlight.operatingFlightCode);
		 		Flight__c flight = createFlight(flightNumber, flightMap, foxFlight);
		 		if (foxFlight.legs != null && foxFlight.legs.size() > 0 && foxFlight.legs[0] != null) {
		 			flight.scheduled_departure_date__c = foxFlight.legs[0].scheduledDepartureDate == null ? null : Date.valueof(foxFlight.legs[0].scheduledDepartureDate);
		 		}
	 			if (flight.id == null) {
	 				System.debug('flightnumber ' + flightNumber + ' inserted');
	 				insert flight; // legs need the id, so store per flight
	 			} else {
	 				System.debug('flightnumber ' + flightNumber + ' update');
		 			sFlightsToUpdate.add(flight);
	 			}
	 			
	 			for (Integer i = 0 ; i < foxFlight.legs.size(); i++) {
					LtcFoxResponseModel.Leg foxLeg = foxFlight.legs[i];
					Leg__c leg;
					if (flight.legs__r != null && flight.legs__r.size() > i && flight.legs__r[i] != null) {
						leg = flight.legs__r[i];
						System.debug('update leg ' + i);
					} else {
						System.debug('new leg ' + i);
						leg = new Leg__c();
						leg.flight__c = flight.id;
						flight.legs__r.add(leg);
					}
					foxLeg.populateLeg(leg);
					sLegsToUpdate.add(leg);
				}
	 					 		
			 	if (sLegsToUpdate.size() >= 100) {
			 		upsert sFlightsToUpdate;
			 		sFlightsToUpdate.clear();
			 		upsert sLegsToUpdate;
			 		sLegsToUpdate.clear();
			 	}	 		
		 	}
	 		
		 	if (!sLegsToUpdate.isEmpty()) {
		 		upsert sFlightsToUpdate;
		 		upsert sLegsToUpdate;
		 	}
	 	}
	}

 	/**
 	 * fill in the flight details
 	 */
    private Flight__c createFlight(String flightNumber, Map<String, Flight__c> flightMap, LtcFoxResponseModel.Flight foxFlight) {
	 	Flight__c tmpFlight;
 		if (flightMap.containsKey(flightNumber)) {
 			tmpFlight = flightMap.get(flightNumber);
 		} else {
 			tmpFlight = new Flight__c();
 		}
 		foxFlight.populateFlight(tmpFlight);
 		tmpFlight.Flight_Number__c = flightNumber;
 		
 		System.debug('createFlight nr:' + flightNumber + ' has id: ' + tmpFlight.id);
 		return tmpFlight;   	
    }

	public override void actualFinish(Database.BatchableContext BC){
		
	}
}