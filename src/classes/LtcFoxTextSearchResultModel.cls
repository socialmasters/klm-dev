/**                     
 * @author (s)      : David van 't Hooft
 * @description     : Model class to capture the FOX services response
 * @log:   10MAY2014: version 1.0
 */
global class LtcFoxTextSearchResultModel {
	public List<SearchResult> searchResult;

	global class SearchResult {
		public String origin;
		public String scheduledDepartureTime;
		public String scheduledDepartureDate;
		public String destination;
		public String scheduledArrivalTime;
		public String scheduledArrivalDate;
		public String status;
		public String operatingFlightCode;
		public List<String> codeShares;
		public String departureDate;
	}

	
	public static LtcFoxTextSearchResultModel parse(String json) {
		return (LtcFoxTextSearchResultModel) System.JSON.deserialize(json, LtcFoxTextSearchResultModel.class);
	}
	
}