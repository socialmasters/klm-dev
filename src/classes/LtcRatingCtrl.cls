public class LtcRatingCtrl {
    
    @RemoteAction
    public static String postRating(String url, String ratingNum,String positiveFeedback,String negativeFeedback,String familyName,String flightNumber,String seatNumber,String travelDate) {    
        String result = '';
        //we'll call the REST API here
        http h = new http();
 
        httprequest req = new httprequest();
        //note that 'na1' may be different for your organization
        //req.setEndpoint('https://na1.salesforce.com/services/data/v20.0/' + action);
        //post task
        req.setEndpoint(url + '/services/apexrest/Ratings');
        req.setMethod('POST');
        req.setBody('{"ratingNum":ratingNum,"positiveFeedback":positiveFeedback,"negativeFeedback":negativeFeedback,"familyName":familyName,"flightNumber":flightNumber,"seatNumber":seatNumber,"travelDate":travelDate}');
        //req.setHeader('Authorization', 'OAuth ' + session);
        req.setHeader('Content-Type', 'application/json; charset=UTF-8');
        req.setTimeout(60000);
        /**
        //get task 
            req.setEndpoint('https://klm.secure.force.com/services/apexrest/Ratings');
            req.setMethod('GET');
            req.setHeader('Authorization', 'OAuth ' + session);
            req.setTimeout(60000);
        */     
        httpresponse res = h.send(req);
 
        result = res.getBody();
        System.debug('**********Rest call performed');
        return result;
    }
 
}