/********************************************************************** 
 Name:  AFKLM_CaseCloseWaitOnCustomerBatch
 Task:    N/A
 Runs on: N/A
====================================================== 
Purpose: 
    This batch class will query for all cases that exceed 48 hours over the case_stage_datetime and 
    have the status 'Waiting on customer'. These cases will then be set to status 'Closed'
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      21/01/2014      Initial Development (based on Jelle Terpstra - jelle.terpstra@accenture.com)
    1.1     Stevano Cheung      13/02/2014      Only do it for recordtype 'Servicing'
***********************************************************************/
global class AFKLM_CaseCloseWaitOnCustomerBatch implements Database.Batchable<Case> {
    global Iterable<Case> start(Database.BatchableContext bc){
       
        AFKLM_SCS_CaseAutoClose__c cac = AFKLM_SCS_CaseAutoClose__c.getOrgDefaults();
		
        List <Case> cl = new List <Case>(); 
        DateTime dt = Datetime.now().addDays(cac.Number_of_Days__c.intValue());

		RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'Servicing' AND SobjectType = 'Case' LIMIT 1];
		// System.debug('*** Retrieved record type: ' + rt.Name);
        cl = [SELECT id, Reference__c FROM Case WHERE RecordTypeId = :rt.Id AND Status = 'Waiting for Client Answer' AND Case_stage_datetime__c < :dt AND Case_stage_datetime__c != null LIMIT 5000];        
        
        return cl;
    }
    global void execute(Database.BatchableContext bc, LIST<Case> cl) {
        for(Case c:cl) {
            c.Status = 'Closed - No Response';
        }      
        update cl;
    }
    global void finish(Database.BatchableContext bc) {
    }
}