/********************************************************************** 
 Name:  AFKLM_SCS_SocialPostWrapperCls
 Task:    N/A
 Runs on: AFKLM_SCS_SocialPostController
====================================================== 
Purpose: 
    Social Post Wrapper to order the Social Post by date and verify if the Social Post has been selected.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      19/01/2014      Initial Development
    1.1     Stevano Cheung      06/02/2014      Clean up wrapper class
                                                Reference: http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_list_sorting_sobject.htm 
***********************************************************************/
global class AFKLM_SCS_SocialPostWrapperCls implements Comparable {
    public transient Boolean isSelected {get;set;}
    public SocialPost cSocialPost {get;set;}
    public ApexPages.StandardSetController socialPostController {get;set;}
    public AFKLM_SCS_SocialPostController scsSocialPostController {get;set;}
	public String shortenContent {get;set;}

    /**
    * Standard Constructor. 
    */
    public AFKLM_SCS_SocialPostWrapperCls(AFKLM_SCS_SocialPostController scsSocialPostController) {}
    
    /**
    * Overloading Constructor. 
    * To wrap the SocialPost with selected checkbox value.
    */
//Comment here!!!
    public AFKLM_SCS_SocialPostWrapperCls(SocialPost cSocialPost, ApexPages.StandardSetController socialPostController){
          this.cSocialPost = cSocialPost;
          isSelected = false;
          
          if(cSocialPost != null && cSocialPost.Content != null){
          // Shorten the FB content where messageType is Post, Comment and Reply
            if('Post'.equals(cSocialPost.MessageType) && cSocialPost.Content != null && cSocialPost.Content.length() > 400) {
            	shortenContent = cSocialPost.Content.substring(0,400) + '...';
            } else if( ('Comment'.equals(cSocialPost.MessageType) || 'Reply'.equals(cSocialPost.MessageType) || 'WeChat'.equals(cSocialPost.Provider)) 
            		&& cSocialPost.Content.length() > 150) {
            	
            	shortenContent = cSocialPost.Content.substring(0,150) + '...';
            } else {
            	shortenContent = cSocialPost.Content;
            }
          }
          this.socialPostController = socialPostController;
    }

    /**
    * Order the Social Post using Comparable AFKLM_SCS_SocialPostWrapperCls.sort() this method will be used 
    *
    * @param    compareTo Object and field which needed to be sorted
    * @return   Integer return positive or negative value to verify if the posted date is higher than the previous record
    */
    global Integer compareTo(Object compareTo) {
        // Cast argument to Social Posts Wrapper
        AFKLM_SCS_SocialPostWrapperCls compareToSocialPost = (AFKLM_SCS_SocialPostWrapperCls)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (cSocialPost.Posted > compareToSocialPost.cSocialPost.Posted) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (cSocialPost.Posted < compareToSocialPost.cSocialPost.Posted) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        
        /*Integer returnValue = 0;
        if (cSocialPost.Handle > compareToSocialPost.cSocialPost.Handle) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (cSocialPost.Handle == compareToSocialPost.cSocialPost.Handle) {
            // Set return value to a zero.
            returnValue = 0;
        } else if (cSocialPost.Handle < compareToSocialPost.cSocialPost.Handle) {
            // Set return value to a negative value.
            returnValue = -1;
        }*/
        return returnValue;       
    }          
}