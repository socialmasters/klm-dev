/**
 * @author (s)      : David van 't Hooft
 * @requirement id  : 
 * @description     : Batch to delete all the SocialPost records that are marked to be deleted with the Delete Social Post field. 
 * @log:            : 20JUL2015 v1.0 
 */
global class SocialPostDeleteBatch implements Database.Batchable<SocialPost> {
	
    global Iterable<SocialPost> start(Database.BatchableContext bc){
        return getDataSet();
    }

	global LIST<SocialPost> getDataSet() {
        List <SocialPost> spList = [Select s.id, s.Delete_Social_Post__c, s.CreatedDate From SocialPost s where s.Delete_Social_Post__c = true order by s.CreatedDate asc limit 50000];
        return spList;         
	}
    
    global void execute(Database.BatchableContext bc, LIST<SocialPost> spList) {
   		delete spList;
   		system.debug('**** Deleted SocialPosts ****');
    }
    
    global void finish(Database.BatchableContext bc) {
    }
    
}