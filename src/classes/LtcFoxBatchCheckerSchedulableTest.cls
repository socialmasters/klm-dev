/**
 * Tests for the LtcFoxBatchCheckerSchedulable class.
 */
@isTest
private class LtcFoxBatchCheckerSchedulableTest {

    static testMethod void execute() {
        LtcFoxBatchCheckerSchedulable sched = new LtcFoxBatchCheckerSchedulable();
        integer result = sched.execute();
        System.assertEquals(5, result);
        
    }
}