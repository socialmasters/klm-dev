/**
 * Represents Json payload:
 * {"flightNumber": "KL1234",
 *  "startDate": "2014-05-05",
 *  "endDate": "2014-05-05",
 *  "limit": 15,
 *  "offset": 0,
 *  "ratingIds": [
 *       "kFzWWY1a3B2TEsycHdOU2hyJTJCRXozbU5uMVNodXJMJTJGN0gzcUdtellPbkdibGZ3RHlaMXpPalF2NUh4OWliaGo",
 *      "bladiebladeeieurSSKDSJDS(SIDSJIJUUIsusdjsisjsosbladiebladeeieurSSKDSJDS(SIDSJIJUUIsusdjsisjsos8s87si",
 *      "VNodXJMJTJGN0gzcUdtellPbkdibGZ3RHlaMXpPalF2NUh4OWliaGoVNodXJMJTJGN0gzcUdtellPbkdibGZ3RHlaMXpPalF2NUs"
 *   ]
 * }
 * 
 **/
global class LtcRatingSearchPostModel {
	public String flightNumber { get; set; }
	public String startDate { get; set; }
	public String endDate { get; set; }
	public List<String> ratingIds { get; set; }
	public Integer limitTo { get; set; }
	public Integer offset { get; set; }
	
	public LtcRatingSearchPostModel(String flightNumber, String startDate, String endDate, List<String> ratingIds, Integer limitTo, Integer offset) {
		this.flightNumber = flightNumber;
		this.startDate = startDate;
		this.endDate = endDate;
		this.ratingIds = ratingIds;
		this.limitTo = limitTo;
		this.offset = offset;
	}
}