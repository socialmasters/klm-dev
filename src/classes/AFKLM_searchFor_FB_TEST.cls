@IsTest
/**********************************************************************
 Name:  AFKLM_searchFor_FB_TEST.cls
======================================================
Purpose: Test class for the AFKLM_searchFor_FB class
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     A.J. van Kesteren	18/03/2012		Initial development
***********************************************************************/
public with sharing class AFKLM_searchFor_FB_TEST {

	static testMethod void testFbNumber() {
		AFKLM_searchFor_FB o = new AFKLM_searchFor_FB();
		o.fbNumber = 'test';
		System.assertEquals(o.fbNumber, 'test');	
	}

}