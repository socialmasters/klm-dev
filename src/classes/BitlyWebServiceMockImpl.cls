/********************************************************************** 
 Runs on: AFKLM_SCS_BitlyControllerTest, AFKLM_SCS_AFBitlyControllerTest
====================================================== 
Purpose:
    For code coverage. 
    This class contains Mock Callout XML data request/response for Bitly. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      16/03/2015      Initial Development
***********************************************************************/
@isTest    
global class BitlyWebServiceMockImpl implements HttpCalloutMock {
    // For XML response via HTTP
    global HTTPResponse respond(HTTPRequest req) {
        if('GET'.equals(req.getMethod())) {
        	System.assertEquals('GET', req.getMethod());
        }

        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml');

        if(req.getEndpoint().contains('airfrance')) {
	        res.setBody(
	        	'<?xml version="1.0" encoding="utf-8"?>'+
					'<bitly>'+
						'<errorCode>0</errorCode>'+
						'<errorMessage/>'+
						'<results>'+
							'<nodeKeyVal>'+
								'<shortKeywordUrl/>'+
								'<hash>16SJC6</hash>'+
								'<userHash>18wbhQT</userHash>'+
								'<nodeKey>http://www.airfrance.com</nodeKey>'+
								'<shortUrl>http://bit.ly/18wbhQT</shortUrl>'+
								'<shortCNAMEUrl>http://bit.ly/18wbhQT</shortCNAMEUrl>'+
							'</nodeKeyVal>'+
						'</results>'+
						'<statusCode>OK</statusCode>'+
					'</bitly>');
        } else if(req.getEndpoint().contains('klm')) {
	        res.setBody(
		        '<?xml version="1.0" encoding="utf-8"?>'+
						'<bitly>'+
							'<errorCode>0</errorCode>'+
							'<errorMessage/>'+
							'<results>'+
								'<nodeKeyVal>'+
									'<shortKeywordUrl/>'+
									'<hash>124ADS</hash>'+
									'<userHash>1234HGT</userHash>'+
									'<nodeKey>http://www.klm.com</nodeKey>'+
									'<shortUrl>http://klmf.ly/1234HGT</shortUrl>'+
									'<shortCNAMEUrl>http://klmf.ly/1234HGT</shortCNAMEUrl>'+
								'</nodeKeyVal>'+
							'</results>'+
							'<statusCode>OK</statusCode>'+
						'</bitly>');
        } else if(req.getEndpoint().contains('longUrl=http')) {
        	res.setBody(
	        	'<?xml version="1.0" encoding="utf-8"?>'+
					'<bitly>'+
						'<errorCode>400</errorCode>'+
						'<errorMessage>MISSING_ARG_LONGURL</errorMessage>'+
						'<results/>'+
						'<statusCode/>'+
					'</bitly>');
        }

        res.setStatusCode(200);
        return res;
    }
}