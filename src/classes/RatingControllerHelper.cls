/********************************************************************** 
 Name:  RatingControllerHelper
 Task:    N/A
 Runs on: RatingController
====================================================== 
Purpose: 
    Helper class for Social Post Controller.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     David van 't Hooft  01/19/2015      Initial Development
***********************************************************************/
public with sharing class RatingControllerHelper {
	public List<RatingWrapper> ratingList {get;set;}
	
	/** 
    * Helper class method to mark the Ratings as reviewed for wrapper controller
    *
    * @param RatingList - List of RatingWrapper object to be selected for mark as reviewed
    */
	public void markReviewed(List<RatingWrapper> ratingList) {
        List<Rating__c> updateRatingObjList = new List<Rating__c>();

        for(RatingWrapper wrapperRating : ratingList){
            if(wrapperRating.isSelected == true){
                wrapperRating.wRating.Moderation_Done__c  = true;
	            wrapperRating.wRating.OwnerId = userInfo.getUserId();
	            updateRatingObjList.add(wrapperRating.wRating);
            }
        }

        if(updateRatingObjList != null && updateRatingObjList.size() > 0){
            update updateRatingObjList;
            ratingList = null;
        }
	}
	

  
}