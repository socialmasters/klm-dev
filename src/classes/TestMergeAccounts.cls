/*This class is used for de-duplication of the accounts created with wrong FB_id, because of Facebook.com changes.
Script will query all the accounts with wrong FB_Ids, Original accounts and link cases/social posts/personas from 
duplicate accounts to original accounts. Then Delete all duplicates and fix ids with new accounts.
*/
global class TestMergeAccounts implements Database.Batchable<sObject> {
      	List<String> accIds=new List<String>();
	    List<String> fbIdList = new List<String>();
	    List<String> accountIds = new List<String>();
	    List<String> originalAccountIds = new List<String>();
	    
	    List<Case> casesToUpdate = new List<Case>();
	    List<SocialPost> postsToUpdate = new List<SocialPost>();
	    List<SocialPersona> personasToUpdate = new List<SocialPersona>();
	    List<Account> accountsToUpdate = new List<Account>();
	    Map<Id,Account> accountsToDelete = new Map<Id,Account>();
	    List<Account> accToDel = new List<Account>();
	    Map<Id,Account> accountsToFix = new Map<Id,Account>();
	    
	    String tmpid;
	    String acId;
	    
	    global Database.QueryLocator start(Database.BatchableContext bc) {
	    	String duplicateAccounts = 'SELECT id,sf4twitter__Fcbk_User_Id__pc,PersonEmail,Flying_Blue_Number__c,Phone,Official_First_Name__c,Official_Last_Name__c,Additional_Description__c,Description,Miscellaneous_information__c FROM Account WHERE sf4twitter__Fcbk_User_Id__pc LIKE \'%app_scoped_user_id%\'';
                return Database.getQueryLocator(duplicateAccounts);
	    }
	    
	    global void execute(Database.BatchableContext BC, list<Account> duplicateAccounts){
	    	//System.debug('^^^^Count '+duplicateAccounts.size());
        //System.debug('^^^^Account list '+duplicateAccounts);
        //System.debug('^^^Before for');
        
        for(Integer i=0;i<duplicateAccounts.size();i++){
            if(duplicateAccounts[i].sf4twitter__Fcbk_User_Id__pc != null){
                fbIdList.add(duplicateAccounts[i].sf4twitter__Fcbk_User_Id__pc);
            }
            accountIds.add(duplicateAccounts[i].id);
        } 
        
        SYstem.debug('^^^FB id list'+fbIdList);
        
        for(Integer i=0;i<duplicateAccounts.size();i++){
            tmpid = duplicateAccounts[i].sf4twitter__Fcbk_User_Id__pc.left(duplicateAccounts[i].sf4twitter__Fcbk_User_Id__pc.length()-1);
            acid = tmpid.substring(tmpid.lastindexof('/'));
            acid = 'FB_'+acid.replace('/','');
            
            accIds.add(acid);
        }
        
        System.debug('^^^AccIds '+accIds.size());
        System.debug('^^^Account Ids '+accountIds.size());
        
        System.debug('^^^FB_ID '+accIds);
        
        List<Account> originalAccounts = [SELECT id,sf4twitter__Fcbk_User_Id__pc,PersonEmail,Flying_Blue_Number__c,Phone,Official_First_Name__c,Official_Last_Name__c,Additional_Description__c,Description,Miscellaneous_information__c 
                FROM Account WHERE sf4twitter__Fcbk_User_Id__pc IN: accIds];
        //System.debug('^^^Names: '+originalAccounts);
        //System.debug('^^^Duplicates '+originalAccounts.size());
        
        for(Account origAcc : originalAccounts){
            originalAccountIds.add(origAcc.id);
        }
        
        //TO DO: 
        //update fields (email, fb number, etc)
        List<SocialPost> socialPosts = [SELECT Id, WhoId FROM SocialPost WHERE whoID IN: accountIds];
        List<SocialPersona> personas = [SELECT id, parentId FROM SocialPersona WHERE FB_User_id__c IN: accIds];
        List<Case> cases = [SELECT id, accountId, contactId FROM Case WHERE accountId IN: accountIds];
        List<Contact> contacts = [SELECT id, AccountId FROM Contact WHERE AccountId IN: originalAccountIds];
        
        for(Account acc : duplicateAccounts){
            tmpid = acc.sf4twitter__Fcbk_User_Id__pc.left(acc.sf4twitter__Fcbk_User_Id__pc.length()-1);
            acid = tmpid.substring(tmpid.lastindexof('/'));
            acid = 'FB_'+acid.replace('/','');
            
            for(Account acc2 : originalAccounts){
                if(acid.equals(acc2.sf4twitter__Fcbk_User_Id__pc)){
                    if(acc2.PersonEmail == null  && acc.PersonEmail != null){
                        acc2.PersonEmail = acc.PersonEmail;
                    } 
                    if (acc2.Flying_Blue_Number__c == null && acc.Flying_Blue_Number__c != null){
                        acc2.Flying_Blue_Number__c = acc.Flying_Blue_Number__c;
                    }
                    if (acc2.Phone == null && acc.Phone != null){
                        acc2.Phone = acc.Phone;
                    }
                    if (acc2.Official_First_Name__c == null && acc.Official_First_Name__c != null){
                        acc2.Official_First_Name__c = acc.Official_First_Name__c;
                    }
                    if (acc2.Official_Last_Name__c == null && acc.Official_Last_Name__c != null){
                        acc2.Official_Last_Name__c = acc.Official_Last_Name__c;
                    }
                    if (acc2.Description == null && acc.Description != null){
                        acc2.Description = acc.Description;
                    }
                    if (acc2.Additional_Description__c == null && acc.Additional_Description__c != null){
                        acc2.Additional_Description__c = acc.Additional_Description__c;
                    }
                    if (acc2.Miscellaneous_information__c == null && acc.Miscellaneous_information__c != null){
                        acc2.Miscellaneous_information__c = acc.Miscellaneous_information__c;
                    }
                    
                    accountsToUpdate.add(acc2);
                    
                    if(!accountsToDelete.containsKey(acc.id)){
                        accountsToDelete.put(acc.id,acc);
                    }
                    
                    
                    for(SocialPost sp : socialPosts){
                        if(sp.whoId.equals(acc.id)){
                            sp.whoId = acc2.id;
                            postsToUpdate.add(sp);
                        }
                    }
                    
                    //check/update cases
                    for(Case cs : cases){
                        if(cs.AccountId.equals(acc.id)){
                            cs.AccountId = acc2.id;
                            for(Contact ct : contacts){
                                if(ct.AccountId.equals(acc2.id)){
                                    cs.ContactId = ct.id;
                                }
                            }
                            casesToUpdate.add(cs);
                        }
                    }
                    
                    for(SocialPersona sPersona : personas){
                        if(sPersona.ParentId.equals(acc.id)){
                            sPersona.ParentId = acc2.id;
                            personasToUpdate.add(sPersona);
                        }
                    }
                }
            }
        }
        
        System.debug('^^^OriginalAccounts to Update count: '+accountsToUpdate.size()+' Values: '+accountsToUpdate);
        System.debug('^^^Duplicate Accounts to Delete: '+accountsToDelete.size()+' Values: '+accountsToDelete);
        System.debug('^^^Posts to Update: '+postsToUpdate.size());
        System.debug('^^^Personas to Update: '+personasToUpdate.size());
        System.debug('^^^Cases to Update: '+casesToUpdate.size());
        update accountsToUpdate;
        update postsToUpdate;
        update personasToUpdate;
        update casesToUpdate;
        
        accToDel.addAll(accountsToDelete.values());
        delete accToDel; 
        System.debug('^^^Duplicate accounts to delete after putting into list '+accToDel.size()+', Values '+accToDel);
        
        System.debug('^^^^Account to delete size: '+accToDel.size()+' accounts '+accToDel);
        //Attach cases, socialPosts, Social Personas to correct Accounts
        //Check that there is no case/sp/persona in account and then delete duplicates
        
        //handleNewAccounts();
	    }
	    
	    global void finish(Database.BatchableContext BC){}
	    
    
    //Duplicate Personas
    public void handleNewAccounts(){
        List<Account> accToFix = [SELECT id,sf4twitter__Fcbk_User_Id__pc FROM Account WHERE sf4twitter__Fcbk_User_Id__pc LIKE '%app_scoped_user_id%'];
        //System.debug('^^^^Count '+accToFix.size());
        //System.debug('^^^^Account list '+accToFix);
        
        List<Account> newAccountsToUpdate = new List<Account>();
        String tempId, accId;
        
        for(Account acc : accToFix){
            if(acc.sf4twitter__Fcbk_User_Id__pc.contains('app_scoped_user_id')){
                tempId = acc.sf4twitter__Fcbk_User_Id__pc.left(acc.sf4twitter__Fcbk_User_Id__pc.length()-1);
                accId = tempId.substring(tempId.lastindexof('/'));
                accId = 'FB_'+accId.replace('/','');
                
                acc.sf4twitter__Fcbk_User_Id__pc = accId;
                
                newAccountsToUpdate.add(acc);
            }
        }
        System.debug('^^^New Accounts to Update size: '+newAccountsToUpdate.size());
        System.debug('^^^New Accounts to Update '+newAccountsToUpdate);
        update newAccountsToUpdate;
        
    }
}