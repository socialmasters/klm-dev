/********************************************************************** 
 Name:  AFKLM_SCS_CaseSummaryExtensionTest
 Task:    N/A
 Runs on: AFKLM_SCS_CaseSummaryExtension
====================================================== 
Purpose: 
    This class contains unit tests for validating the behavior of Apex classes and triggers. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta      	13/05/2014      Initial Development
***********************************************************************/
@isTest
private class AFKLM_SCS_CaseSummaryExtensionTest {

    static testMethod void getProfileURLTest() {
    	Account acc = new Account(FirstName='Test', Lastname='Account', sf4twitter__Fcbk_User_Id__pc='FB_123456789');
     	insert acc;
    	Case cs = new Case(Status='New', AccountId=acc.id);
    	insert cs;
    	
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(cs);
    	
     	SocialPersona sp = new SocialPersona(name='Test Persona', provider = 'Facebook', parentId = acc.id, ProfileURL='http://www.testingurlfortestuser.com/id=123456789');
     	insert sp;
     	SocialPersona sp2 = new SocialPersona(name='Test Persona2', provider = 'Facebook', parentId = acc.id, ProfileURL='http://www.testingurlfortestuser.com/id=12345678');
     	
     	AFKLM_SCS_CaseSummaryExtension cse = new AFKLM_SCS_CaseSummaryExtension(stdController); 
     	String profurl=cse.getProfileURL();
		System.assertEquals(sp.profileURL, profurl);
     	
 		insert sp2;
 		String profurll = cse.getProfileURL();
 		List<Apexpages.Message> msgs = ApexPages.getMessages();
		boolean b = false;
		for(Apexpages.Message msg : msgs){
		    if (msg.getDetail().contains('No Social Persona attached to the account or incorrect ID.')) b = true;
		}
		system.assert(b);
 		//System.assertEquals(message,'No Social Persona attached to the account or incorrect ID.');
    }
}