/********************************************************************** 
 Name:  AFKLM_SCS_SocialPostTwitterController
 Task:    N/A
 Runs on: AFKLM_SCS_SocialPosController
====================================================== 
Purpose: 
    Social Post Controller Class to utilize existing List View in Apex with Pagination Support with logic used for Twitter.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta          22/07/2014      Initial Development
    
***********************************************************************/
public without sharing class AFKLM_SCS_SocialPostTwitterController {
        
    public String createCasesTwitter(List<AFKLM_SCS_SocialPostWrapperCls> socPostList, Map<String, SocialPost> postMap, Boolean error, ApexPages.StandardSetController SocPostSetController, String newCaseId, String newCaseNumber, String existingTwitterCaseNumber, List<SocialPost> listSocialPosts, SocialPost post, String tempSocialHandle) {
        AFKLM_SCS_ControllerHelper controllerHelper = new AFKLM_SCS_ControllerHelper(); 
        List<String> returnedCaseId = new List<String>();
        //Case personAccountId;

         while(SocPostSetController.getHasNext()) {
            SocPostSetController.next();
            for(SocialPost c : (List<SocialPost>)SocPostSetController.getRecords()) {
                socPostList.add(new AFKLM_SCS_SocialPostWrapperCls(c, SocPostSetController));
            }
        }
        
        if(existingTwitterCaseNumber.equals('')) { 
            if((('Actioned').equals(post.SCS_Status__c) || ('Mark as reviewed').equals(post.SCS_Status__c) || post.SCS_MarkAsReviewed__c == true)) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The Social Post is already "Mark As Reviewed" OR "Actioned" and being handled by another agent. ' ) );
                return null;
            }
        
            post.SCS_Status__c = 'Mark as reviewed';
            post.SCS_MarkAsReviewed__c = true;
            post.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
            post.Ignore_for_SLA__c = true;
            post.OwnerId = userInfo.getUserId();
            listSocialPosts.add(post);
            postMap.put( post.Id, post );
            
            
            
            if( !postMap.IsEmpty() ) {
                returnedCaseId = controllerHelper.createNewCases(postMap, newCaseId, newCaseNumber, error);
                Case personAccountId = [SELECT accountId FROM Case WHERE id =: returnedCaseId[0] LIMIT 1];
                if(returnedCaseId.size() > 0) {
                    newCaseId = returnedCaseId[0];
                    List<SocialPost> listWrapperSocialPost = new List<SocialPost>();
                                    
                    // The non selected to merge by Handle   
                    for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : socPostList) {
                         if(tempSocialHandle.equals(wrapperSocialPost.cSocialPost.Handle) && !('Actioned').equals(wrapperSocialPost.cSocialPost.SCS_Status__c)) {
                            if(wrapperSocialPost.cSocialPost.ParentId == null) {
                                wrapperSocialPost.cSocialPost.ParentId = newCaseId;
                            } 
                            
                            wrapperSocialPost.cSocialPost.SCS_Status__c = 'Mark as reviewed';
                            wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                            wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                            wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                            // SC: This should not be done after test. The TemporaryPersonAccount will be on the SocialPost instead the user Account.
                            wrapperSocialPost.cSocialPost.WhoId = personAccountId.accountId;
                            wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();
                            listWrapperSocialPost.add(wrapperSocialPost.cSocialPost);
                        }
                        
                    }
                    update listWrapperSocialPost;
                }
            
                if( !error ){
                    return newCaseId;
                }
            }
                        
       } else {
            newCaseId = existingTwitterCaseNumber;
            Case personAccountId = [SELECT accountId FROM Case WHERE id =: newCaseId LIMIT 1];
            if(('Actioned').equals(post.SCS_Status__c) || ('Mark as reviewed').equals(post.SCS_Status__c) || post.SCS_MarkAsReviewed__c == true) {
                ApexPages.addmessage( new ApexPages.message( ApexPages.severity.WARNING, 'The case number  "' + post.SCS_CaseNumber__c + '" already exists OR is actioned and being handled by another agent. ' ) );
                return null;
            }
            List<SocialPost> listWrapperSocialPost = new List<SocialPost>();
            
            for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : socPostList) {
                if(tempSocialHandle.equals(wrapperSocialPost.cSocialPost.Handle) && !('Actioned').equals(wrapperSocialPost.cSocialPost.SCS_Status__c)) {
                    wrapperSocialPost.cSocialPost.ParentId = newCaseId;
                    wrapperSocialPost.cSocialPost.SCS_Status__c = 'Mark as Reviewed';
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
                    wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
                    wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
                    // SC: This should not be done after test. The TemporaryPersonAccount will be on the SocialPost instead the user Account.
                    // IBO: Fix done... Needs to be properly tested.
                    wrapperSocialPost.cSocialPost.WhoId = personAccountId.accountId;
                    wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();
                }
                listWrapperSocialPost.add(wrapperSocialPost.cSocialPost);
            }
            update listWrapperSocialPost;
            
            return newCaseId;
            
        }
    return null;
    }   
}