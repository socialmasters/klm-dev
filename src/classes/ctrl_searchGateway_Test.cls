@isTest
private class ctrl_searchGateway_Test{

	// runs for every test method
	static {

		// Load custom settings (mapping data) for each test method
		loadCustomSettings();

	}

	// method to load custom settings from static resources
	private static void loadCustomSettings(){

		List<AFKLM_AIRLINES__c> listOf1 		= Test.loadData(AFKLM_AIRLINES__c.SobjectType, 'testAFKLMAIRLINES');
		List<AFKLM_BookingClass__c> listOf2 	= Test.loadData(AFKLM_BookingClass__c.SobjectType, 'testAFKLMBookingClass');
		List<AFKLM_CONTRACT_STATUS__c> listOf3 	= Test.loadData(AFKLM_CONTRACT_STATUS__c.SobjectType, 'testAFKLMCONTRACTSTATUS');
		List<AFKLM_CONTRACT_TYPE__c> listOf4 	= Test.loadData(AFKLM_CONTRACT_TYPE__c.SobjectType, 'testAFKLMCONTRACTTYPE');
		List<AFKLM_CORPORATE__c> listOf5 		= Test.loadData(AFKLM_CORPORATE__c.SobjectType, 'testAFKLMCORPORATE');
		List<AFKLM_Countries__c> listOf6 		= Test.loadData(AFKLM_Countries__c.SobjectType, 'testAFKLMCountries');
		List<AFKLM_INDIVIDU_STATUS__c> listOf7 	= Test.loadData(AFKLM_INDIVIDU_STATUS__c.SobjectType, 'testAFKLMINDIVIDUSTATUS');
		List<AFKLM_LEVELS__c> listOf8 			= Test.loadData(AFKLM_LEVELS__c.SobjectType, 'testAFKLMLEVELS');
		List<AFKLM_MediumCode__c> listOf9 		= Test.loadData(AFKLM_MediumCode__c.SobjectType, 'testAFKLMMediumCode');
		List<AFKLM_MediumStatus__c> listOf10 	= Test.loadData(AFKLM_MediumStatus__c.SobjectType, 'testAFKLMMediumStatus');
		List<AFKLM_MemberType__c> listOf11 		= Test.loadData(AFKLM_MemberType__c.SobjectType, 'testAFKLMMemberType');
		List<AFKLM_TIERSLEVELS__c> listOf12 	= Test.loadData(AFKLM_TIERSLEVELS__c.SobjectType, 'testAFKLMTierLevels');
		
	}

	static testMethod void testSearchGatewayPageMethods(){

		Account tstAccount = new Account(FirstName = 'FirstName', LastName = 'LastName', RecordTypeId = getPersonAccountRecordTypeId());
		insert tstAccount;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(tstAccount);
		
		ctrl_searchGateway sG = new ctrl_searchGateway(sc);

		Test.startTest();

		sG.initPage();
		sG.getAirlines();

		AFKLM_WS_Manager.flyingBlueMember fbMember = sG.fbMember;

		Test.stopTest();

	}

	// Test search on Flight and creating a contact list
	static testMethod void testCreateCustomer(){
		
		Account tstAccount = new Account();
		
		ApexPages.StandardController sc = new ApexPages.StandardController(tstAccount);
		
		ctrl_searchGateway sG = new ctrl_searchGateway(sc);

		sG.newAccount.FirstName = 'Test First Name';
		sG.newAccount.LastName = 'Test Last Name';
		sG.newAccount.PersonEmail = 'test@testing.nl';
		sG.newAccount.PersonHomePhone = '+3106123456789';
		sG.newAccount.Country_of_Residence__c = 'NL';

		Test.startTest();

		PageReference thisPR = sG.createCustomer();

		Test.stopTest();

		tstAccount = [select Id from Account limit 1];

		System.assertEquals(tstAccount.Id, thisPR.getParameters().get('Id'));

	}


	// Test search on Flight and creating a contact list
	static testMethod void testFlightSearchAndCreateContactList(){
		
		createWSSetting('BookedPassengersListParam-v1');
		createWSSetting('ProvidePaxListInThePassengerHandlingWindow-v3');
		
		Account tstAccount = new Account();
		
		ApexPages.StandardController sc = new ApexPages.StandardController(tstAccount);
		
		ctrl_searchGateway sG = new ctrl_searchGateway(sc);

		Test.setMock(WebServiceMock.class, new AFKLM_WS_MockTest());

		Test.startTest();

		sG.searchInput.flightNumber = '1111a';
		sG.searchInput.flightDateString = '2013-12-12';
		Date flightDate = sG.searchInput.flightDate;
		
		sG.searchFlight();
		
		sG.paxCountry = 'NL';
		
		for (String key : sG.mapOfKeyToPax.keySet()){
			sG.mapOfKeyToPax.get(key).selected = true;	
		}	
		
		sG.createContactList();

		tstAccount = [select Id from Account limit 1];
		sG = new ctrl_searchGateway(sc);
		List<String> listOfString = sG.feedbackRelatedContactListAccountIds;
		sG.getContactListAccountsForFeedback();
				
		Test.stopTest();
		
		System.assertEquals(10, [select Id from Account].size(), 'Number of Accounts created should be 10!');
		System.assertEquals(1, [select Id from Contact_List__c].size(), 'Number of Contact Lists created should be 1!');
		System.assertEquals(10, [select Id from Contact_List_Account__c].size(), 'Number of Contact List Account created should be 10!');
		
	}

	// Test Flight Search via URL
	static testMethod void testFlightSearchByURL(){
		
		createWSSetting('BookedPassengersListParam-v1');
		createWSSetting('ProvidePaxListInThePassengerHandlingWindow-v3');
		
		PageReference pageRef = Page.searchGateway;
		pageRef.getParameters().put('theFlightDate', '2013-12-12');
		pageRef.getParameters().put('theAirlineCode', 'AF');
		pageRef.getParameters().put('theFlightNumber', '1111');
		Test.setCurrentPage(pageRef);
		
		Account tstAccount = new Account();
		
		ApexPages.StandardController sc = new ApexPages.StandardController(tstAccount);
		
		ctrl_searchGateway sG = new ctrl_searchGateway(sc);

		tstAccount = new Account(RecordTypeId = getPersonAccountRecordTypeId(),
										 FirstName = 'FirstName',
										 LastName = 'LastName',
										 Flying_Blue_Number__c = '0123456780'
										);
		insert tstAccount;

		Test.setMock(WebServiceMock.class, new AFKLM_WS_MockTest());
		
		Test.startTest();
		
		sG.searchFlight();

		Integer entries = sg.nrOfPaxEntries;

		sG.uniqueId = '001C5505AE17EAF0';
		sG.createCustomerWithUniqueId();
				
		Test.stopTest();
		
		System.assertEquals(10, sG.mapOfKeyToPax.size(), 'Number of Pax returned should be 10!');
		System.assertNotEquals(sG.airportOri, null, 'Origin Airport code should not be empty');
		System.assertNotEquals(sG.airportDes, null, 'Origin Airport code should not be empty');
	}
	
	// Search on FB Member and create a new account
	static testMethod void testFBSearchNewAccount(){
		
		createWSSetting('passenger_FBRecognition-v4');
		
		Account tstAccount = new Account();
		
		ApexPages.StandardController sc = new ApexPages.StandardController(tstAccount);
		
		ctrl_searchGateway sG = new ctrl_searchGateway(sc);

		Test.setMock(WebServiceMock.class, new AFKLM_WS_MockTest());

		Test.startTest();

		sG.searchInput.flyingBlueNumber = '1234567890';
		
		sG.searchFB();

		// Now create a new Feedback record
		sG.newFeedback.Account__c = sG.thisAccount.Id;
		sG.newFeedback.Contact_Reason__c = 'Customer care';
		sG.newFeedback.Contact_Medium__c = 'Phone';
		sG.newFeedback.Flight_Date__c = System.today();
		sG.newFeedback.Feedback__c = 'Feedback';
		sG.newFeedback.Category__c = 'In-Flight';
		sG.newFeedback.Subcategory__c = 'Seat';
		sG.newFeedback.Flight_Number__c = 'KL777';

		sG.saveNewFeedback();

		KLM_Feedback__c feedback = sG.newFeedback;

		ctrl_searchGateway.getFBMemberDetails('1234567890', sG.sfdcIP);

		sG.getContactListAccountsForFeedback();
				
		Test.stopTest();
		
		System.assertEquals(1, [select Flying_Blue_Number__c from Account].size(), 'Number of Accounts created should be 1!');
		System.assertEquals('1234567890', [select Flying_Blue_Number__c from Account][0].Flying_Blue_Number__c, 'Flying Blue Number of created Account should be 1234567890!');
		
	}
	
	// Test search on FB member which already exists
	static testMethod void testFBSearchExistingAccount(){
		
		createWSSetting('passenger_FBRecognition-v4');
		
		Account tstAccount = new Account(
									RecordTypeId = getPersonAccountRecordTypeId(),
									FirstName = 'PATRICK',
									LastName = 'BRINKSMA',
									Flying_Blue_Number__c = '1234567890');
		insert tstAccount;

		List<Case> listOfCase = new List<Case>();
		for (Integer x=0; x<5; x++){
			Case tstRecord = new Case(
										Origin = 'Other', 
										Reason = 'Seat', 
										Subject = 'Test Subject' + x, 
										ContactId = tstAccount.PersonContactId,
										AccountId = tstAccount.Id
									);
			listOfCase.add(tstRecord);
		}
		insert listOfCase;

		List<KLM_Feedback__c> listOfFB = new List<KLM_Feedback__c>();
		for (Integer x=0; x<1; x++){
			KLM_Feedback__c tstRecord = new KLM_Feedback__c(
													Account__c = tstAccount.Id,
													Contact_Reason__c = 'Customer care',
													Contact_Medium__c = 'Phone',
													Flight_Date__c = System.today(),
													Feedback__c = 'Feedback',
													Category__c = 'In-Flight',
													Subcategory__c = 'Seat',
													Flight_Number__c = 'KL777'
			);
			listOfFB.add(tstRecord);
		}
		insert listOfFB;

		ApexPages.StandardController sc = new ApexPages.StandardController(tstAccount);
		
		ctrl_searchGateway sG = new ctrl_searchGateway(sc);

		Test.setMock(WebServiceMock.class, new AFKLM_WS_MockTest());

		Test.startTest();

		sG.searchInput.flyingBlueNumber = '1234567890';
		
		sG.searchFB();
		
		List<ctrl_searchGateway.recentContact> recentList = sG.recentList;

		Test.stopTest();
		
		List<Account> listOfAcc = [select Flying_Blue_Number__c, Official_Last_Name__c, Official_First_Name__c from Account];
		System.assertEquals(1, listOfAcc.size(), 'Number of Accounts created should be 1!');
		System.assertEquals('1234567890', listOfAcc[0].Flying_Blue_Number__c, 'Flying Blue Number of Account should be 1234567890!');
		System.assertEquals('BRINKSMA', listOfAcc[0].Official_Last_Name__c, 'Official Last Name of Account should be BRINKSMA!');
		System.assertEquals('PATRICK', listOfAcc[0].Official_First_Name__c, 'Official First Name of Account should be PATRICK!');
		
	}	
	
	// Test search on FB Member for which more accounts exist
	static testMethod void testFBSearchExistingAccounts(){
		
		createWSSetting('passenger_FBRecognition-v4');
		
		Account tstAccount = new Account();
		
		Account tstAccount1 = new Account(
									RecordTypeId = [select Id from RecordType where SobjectType = 'Account' and Name = 'Person Account'].Id,
									FirstName = 'PATRICK',
									LastName = 'BRINKSMA',
									Flying_Blue_Number__c = '1234567890');
		insert tstAccount1;
		Account tstAccount2 = new Account(
									RecordTypeId = [select Id from RecordType where SobjectType = 'Account' and Name = 'Person Account'].Id,
									FirstName = 'JAN',
									LastName = 'HOOP',
									Flying_Blue_Number__c = '1234567890');
		insert tstAccount2;

		
		ApexPages.StandardController sc = new ApexPages.StandardController(tstAccount);
		
		ctrl_searchGateway sG = new ctrl_searchGateway(sc);

		Test.setMock(WebServiceMock.class, new AFKLM_WS_MockTest());

		Test.startTest();

		sG.searchInput.flyingBlueNumber = '1234567890';
		
		sG.searchFB();
		
		// Select one of the accounts
		for (ctrl_searchGateway.accntSelect thisAS : sG.accntSelectList){
			if (thisAS.accnt.Id == tstAccount1.Id){
				sG.accntSelectList[0].isSelected = true;	
			}
		}
		
		sG.updateSelectedAccount();
				
		Test.stopTest();
		
		
		Map<Id, Account> mapOfIdToAcc = new Map<Id, Account>([select Id, Flying_Blue_Number__c, Official_Last_Name__c, Official_First_Name__c from Account]);
		System.assertEquals(2, mapOfIdToAcc.size(), 'Number of Account should be 2!');
		System.assertEquals('BRINKSMA', mapOfIdToAcc.get(tstAccount1.Id).Official_Last_Name__c, 'Official Last Name of Account should be BRINKSMA!');
		System.assertEquals('PATRICK', mapOfIdToAcc.get(tstAccount1.Id).Official_First_Name__c, 'Official First Name of Account should be PATRICK!');
		
	}	

	// Search on First and Last Name
	static testMethod void testSearchOnName(){

		createWSSetting('SearchHomonymsIndividualType-v1');

		Account tstAccount = new Account();

		ApexPages.StandardController sc = new ApexPages.StandardController(tstAccount);
		
		ctrl_searchGateway sG = new ctrl_searchGateway(sc);

		Test.setMock(WebServiceMock.class, new AFKLM_WS_MockTest());

		Test.startTest();

		List<AFKLM_WS_Manager.sResult> listOfSResult = sG.listOfSResult;

		sG.searchInput.firstName = 'First Name';
		sG.searchInput.lastName = 'Last Name';

		sG.searchCustomer();

		sG.fbMember.clientNumber = '1234567890';
		sG.createCustomerWithGIN();

		listOfSResult = sG.listOfSResult;

		Test.stopTest();

	}

	// Search on PNR for FB number
	static testMethod void testSearchPNR(){

		createWSSetting('ProvidePNRListForACustomerParam-v3');

		Test.setMock(WebServiceMock.class, new AFKLM_WS_MockTest());

		Test.startTest();

		ctrl_searchGateway.getPRNListForFBMember('123456789');

		Test.stopTest();

	}
	
	// Test method for code coverage of non-used WSDL2APEX code
	static testMethod void testWSDL2APEXNonUsedCode(){
		
		AFKLM_WS_BPL_BookedPassengersListParam.PaxCriteria bpl_PC = new AFKLM_WS_BPL_BookedPassengersListParam.PaxCriteria();
		AFKLM_WS_BPL_BookedPassengersListParam.BookedPassengersListFaultBusinessFault bpl_Fault = new AFKLM_WS_BPL_BookedPassengersListParam.BookedPassengersListFaultBusinessFault();
		AFKLM_WS_BPL_BookedPassengersListParam.FlightCriteria bpl_FC = new AFKLM_WS_BPL_BookedPassengersListParam.FlightCriteria();
		AFKLM_WS_BPL_Common.InformationLevel pbl_IL = new AFKLM_WS_BPL_Common.InformationLevel();
		AFKLM_WS_BPL_PassengerIdentifier.PaxIdentifier bpl_PI = new AFKLM_WS_BPL_PassengerIdentifier.PaxIdentifier();
		
		AFKLM_WS_FBR_PassengerFBRecognitionX.FBRecognitionBusinessFault fbr_Fault = new AFKLM_WS_FBR_PassengerFBRecognitionX.FBRecognitionBusinessFault();
		
		AFKLM_WS_PLP_DeparturePassengerListParam.Comment plp_C = new AFKLM_WS_PLP_DeparturePassengerListParam.Comment();
		AFKLM_WS_PLP_DeparturePassengerListParam.IndividualBaggage plp_IB = new AFKLM_WS_PLP_DeparturePassengerListParam.IndividualBaggage();
		AFKLM_WS_PLP_DeparturePassengerListParam.Property plp_P = new AFKLM_WS_PLP_DeparturePassengerListParam.Property();
		AFKLM_WS_PLP_DeparturePassengerListParam.PaxCriteria plp_PC = new AFKLM_WS_PLP_DeparturePassengerListParam.PaxCriteria();
		AFKLM_WS_PLP_DeparturePassengerListParam.Ticket plp_TI = new AFKLM_WS_PLP_DeparturePassengerListParam.Ticket();
		AFKLM_WS_PLP_DeparturePassengerListParam.PassengerHandling plp_PH = new AFKLM_WS_PLP_DeparturePassengerListParam.PassengerHandling();
		AFKLM_WS_PLP_DeparturePassengerListParam.TravelCriteria plp_TC = new AFKLM_WS_PLP_DeparturePassengerListParam.TravelCriteria();
		AFKLM_WS_PLP_Identifier.PaxIdentifier plp_PI = new AFKLM_WS_PLP_Identifier.PaxIdentifier();
		AFKLM_WS_PLP_Common.PointOfSale plp_CO = new AFKLM_WS_PLP_Common.PointOfSale();
		AFKLM_WS_PLP_Common.Property plp_PR = new AFKLM_WS_PLP_Common.Property();
		AFKLM_WS_PLP_Common.InformationLevel plp_IL = new AFKLM_WS_PLP_Common.InformationLevel();

		AFKLM_WS_PNR_PNRListForACustomerParam.Service pnr_1 = new AFKLM_WS_PNR_PNRListForACustomerParam.Service();
		AFKLM_WS_PNR_PNRListForACustomerParam.Specificity pnr_2 = new AFKLM_WS_PNR_PNRListForACustomerParam.Specificity();
		AFKLM_WS_PNR_PNRListForACustomerParam.Product pnr_3 = new AFKLM_WS_PNR_PNRListForACustomerParam.Product();
		AFKLM_WS_PNR_PNRListForACustomerParam.Contact pnr_4 = new AFKLM_WS_PNR_PNRListForACustomerParam.Contact();
		AFKLM_WS_PNR_PNRListForACustomerParam.Passenger pnr_5 = new AFKLM_WS_PNR_PNRListForACustomerParam.Passenger();
		AFKLM_WS_PNR_PNRListForACustomerParam.TypePnr pnr_6 = new AFKLM_WS_PNR_PNRListForACustomerParam.TypePnr();
		AFKLM_WS_PNR_PNRListForACustomerParam.FlightIdentifier pnr_7 = new AFKLM_WS_PNR_PNRListForACustomerParam.FlightIdentifier();
		AFKLM_WS_PNR_PNRListForACustomerParam.AssociatedPNR pnr_8 = new AFKLM_WS_PNR_PNRListForACustomerParam.AssociatedPNR();
		AFKLM_WS_PNR_PNRListForACustomerParam.Upi pnr_9 = new AFKLM_WS_PNR_PNRListForACustomerParam.Upi();
		AFKLM_WS_PNR_PNRListForACustomerParam.Ticket pnr_10 = new AFKLM_WS_PNR_PNRListForACustomerParam.Ticket();
		AFKLM_WS_PNR_PNRListForACustomerParam.Classification pnr_11 = new AFKLM_WS_PNR_PNRListForACustomerParam.Classification();
		AFKLM_WS_PNR_PNRListForACustomerParam.ProvidePNRListForACustomerFaultBusinessFault pnr_12 = new AFKLM_WS_PNR_PNRListForACustomerParam.ProvidePNRListForACustomerFaultBusinessFault();
		AFKLM_WS_PNR_PNRListForACustomerParam.PnrIdentifier pnr_13 = new AFKLM_WS_PNR_PNRListForACustomerParam.PnrIdentifier();
		AFKLM_WS_PNR_PNRListForACustomerParam.ServiceIdentifier pnr_14 = new AFKLM_WS_PNR_PNRListForACustomerParam.ServiceIdentifier();

		AFKLM_WS_PNR_Common.InformationLevel pnr_IL = new AFKLM_WS_PNR_Common.InformationLevel();

		AFKLM_WS_SHI_Search.SearchHomonymsRequest shi_SHR = new AFKLM_WS_SHI_Search.SearchHomonymsRequest();
		AFKLM_WS_SHI_Search.SearchHomonymsRequestEmail shi_SHRE = new AFKLM_WS_SHI_Search.SearchHomonymsRequestEmail();
		AFKLM_WS_SHI_Search.BusinessError shi_BE = new AFKLM_WS_SHI_Search.BusinessError();
		AFKLM_WS_SHI_SicIndividuType.SignatureLight shi_SL = new AFKLM_WS_SHI_SicIndividuType.SignatureLight();
		AFKLM_WS_SHI_SicIndividuType.PostalAddressSIC shi_PASIC = new AFKLM_WS_SHI_SicIndividuType.PostalAddressSIC();
		AFKLM_WS_SHI_SicIndividuType.ClientUses shi_CU = new AFKLM_WS_SHI_SicIndividuType.ClientUses();
		AFKLM_WS_SHI_SicIndividuType.Individual shi_IND = new AFKLM_WS_SHI_SicIndividuType.Individual();
		AFKLM_WS_SHI_SicIndividuType.Signature  shi_SIG = new AFKLM_WS_SHI_SicIndividuType.Signature();
		AFKLM_WS_SHI_SicIndividuType.Habilitation  shi_HB = new AFKLM_WS_SHI_SicIndividuType.Habilitation();
		AFKLM_WS_SHI_SicIndividuType.Alias shi_AL = new AFKLM_WS_SHI_SicIndividuType.Alias();
		AFKLM_WS_SHI_SicIndividuType.IndividualProfil shi_IP = new AFKLM_WS_SHI_SicIndividuType.IndividualProfil();
		AFKLM_WS_SHI_SicIndividuType.AirFranceProfil shi_AP = new AFKLM_WS_SHI_SicIndividuType.AirFranceProfil();
		AFKLM_WS_SHI_SicIndividuType.IndividualInformationError shi_IE = new AFKLM_WS_SHI_SicIndividuType.IndividualInformationError();
		AFKLM_WS_SHI_SicIndividuType.MediumUsage shi_MU = new AFKLM_WS_SHI_SicIndividuType.MediumUsage();
		AFKLM_WS_SHI_SicIndividuType.Civilian shi_CI = new AFKLM_WS_SHI_SicIndividuType.Civilian();
		AFKLM_WS_SHI_SicIndividuType.AddressRole shi_AR = new AFKLM_WS_SHI_SicIndividuType.AddressRole();
		AFKLM_WS_SHI_SicIndividuType.PostalAddressSICResponse shi_PARES = new AFKLM_WS_SHI_SicIndividuType.PostalAddressSICResponse();
		AFKLM_WS_SHI_SoftcomputingType.SoftComputingResponse shiSCR = new AFKLM_WS_SHI_SoftcomputingType.SoftComputingResponse();

	}
	
	// Create Web Service Setting record in custom setting
	private static void createWSSetting(String wsName){
		
		AFKLM_WS_Settings__c wsSetting = new AFKLM_WS_Settings__c(
													Name = wsName.left(20),
													Endpoint__c = 'https://url.to.ws',
													Org_Id__c = Userinfo.getOrganizationId(),
													Password__c = 'pwd',
													Timeout__c = 10000,
													Username__c = 'user',
													Web_Service__c = wsName
													);
		insert wsSetting;
		
	}

    private static Id getPersonAccountRecordTypeId(){
        return [select Id from RecordType where SobjectType = 'Account' and Name = 'Person Account'].Id;
    }	

}