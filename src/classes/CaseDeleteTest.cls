/**
 * @author (s)    	: David van 't Hooft
 * @requirement id  : 
 * @description   	: Test class to test the CaseDeletebatch
 * @log:            : 21JUL2015 v1.0 
 */
@isTest
private class CaseDeleteTest {
    static testMethod void caseDeleteTestOk() {
        Case c = new Case(Description = 'Test case to delete', Delete_Case__c = true, Status = 'Closed - No Response', Case_Detail__c = 'Delay', Sentiment_at_start_of_case__c = 'Positive', Sentiment_at_close_of_case__c = 'Positive');
        insert c;
		
        List <Case> cList = [Select c.id From Case c limit 10];		     
        system.assertEquals(1, cList.size());
        
        test.starttest();
        CaseDeleteScheduler cds = new CaseDeleteScheduler();
        cds.execute(null);
        test.stoptest();
        
        cList = [Select c.id From Case c limit 10];		     
        system.assertEquals(0, cList.size(), 'Case should be deleted');        
    }
    
    //Test case field combination under which it should not be deleted
    static testMethod void caseDeleteTestNotOk() {
        Case c1 = new Case(Description = 'Test case post to not delete1', Delete_Case__c = false, Status = 'Closed - No Response', Case_Detail__c = 'Delay', Sentiment_at_start_of_case__c = 'Positive', Sentiment_at_close_of_case__c = 'Positive');
        Case c3 = new Case(Description = 'Test case post to delete3', Delete_Case__c = false, Status = 'Closed', Case_Detail__c = 'Delay', Sentiment_at_start_of_case__c = 'Positive', Sentiment_at_close_of_case__c = 'Positive');
        List<Case> cList = new List<Case>();
        cList.add(c1);
        cList.add(c3);
        insert cList;

        //Case c2 = new Case(Description = 'Test case post to not delete2', Delete_Case__c = true, Status = 'Closed - No Response', Case_Detail__c = 'Delay', Sentiment_at_start_of_case__c = 'Positive', Sentiment_at_close_of_case__c = 'Positive');
        //insert c2;
		
		//c2.Status = 'Closed';
		//update c2;
		
        cList = [Select c.id From Case c limit 10];		     
        system.assertEquals(2, cList.size());
        
        test.starttest();
        CaseDeleteScheduler cds = new CaseDeleteScheduler();
        cds.execute(null);
        test.stoptest();
        
        cList = [Select c.id From case c limit 10];		     
        system.assertEquals(2, cList.size(), 'Case should *not* be deleted');        
    }
    
    //Test if the case is not deleted if it has SocialPosts connected to it
    static testMethod void caseDeleteTestNotOkSocialPost() {
        Case c = new Case(Description = 'Test case to delete', Delete_Case__c = true, Status = 'Closed - No Response', Case_Detail__c = 'Delay', Sentiment_at_start_of_case__c = 'Positive', Sentiment_at_close_of_case__c = 'Positive');
        insert c;

        Datetime postedDate = Datetime.now();
        SocialPost tstSocialPost = new SocialPost(
                                    Name = 'SocialTest',
                                    Posted = postedDate,
                                    Company__c='AirFrance',
                                    TopicProfileName='SCS Air France',
                                    ParentId = c.Id
                                );
		insert tstSocialPost;
		
        List <Case> cList = [Select c.id From Case c limit 10];		     
        system.assertEquals(1, cList.size());
        
        test.starttest();
        CaseDeleteScheduler cds = new CaseDeleteScheduler();
        cds.execute(null);
        test.stoptest();
        
        cList = [Select c.id From Case c limit 10];		     
        system.assertEquals(1, cList.size(), 'Case should *not* be deleted');        
    }

    //Test if the case is not deleted if it has SocialPosts connected to it
    static testMethod void caseDeleteTestOkSocialPost() {
        Case c = new Case(Description = 'Test case to delete', Delete_Case__c = true, Status = 'Closed - No Response', Case_Detail__c = 'Delay', Sentiment_at_start_of_case__c = 'Positive', Sentiment_at_close_of_case__c = 'Positive');
        insert c;

        Datetime postedDate = Datetime.now();
        SocialPost tstSocialPost = new SocialPost(
                                    Name = 'SocialTest',
                                    Posted = postedDate,
                                    Company__c='AirFrance',
                                    TopicProfileName='SCS Air France',
                                    ParentId = c.Id
                                );
		insert tstSocialPost;
		
        List <Case> cList = [Select c.id From Case c limit 10];		     
        system.assertEquals(1, cList.size());
		
		//remove attached social post        
        delete tstSocialPost;
        
        test.starttest();
        CaseDeleteScheduler cds = new CaseDeleteScheduler();
        cds.execute(null);
        test.stoptest();
        
        cList = [Select c.id From case c limit 10];		     
        system.assertEquals(0, cList.size(), 'Case should now be deleted');                
    }    
}