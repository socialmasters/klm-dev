/********************************************************************** 
 Name:  AFKLM_SCS_ControllerHelper
 Task:    N/A
 Runs on: AFKLM_SCS_SocialPostController
====================================================== 
Purpose: 
    Helper class for Social Post Controller.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      06/02/2014      Initial Development
    1.1     Ivan Botta			07/02/2014      Added Javadoc comments		
***********************************************************************/
public with sharing class AFKLM_SCS_ControllerHelper {
    private static final Integer DEFAULT_CASE_CLOSED = 5;
	public List<AFKLM_SCS_SocialPostWrapperCls> socPostList {get;set;} 
	
	/** 
	* Helper class method to mark the SocialPosts as reviewed for wrapper controller
	*
	* @param socPostList - List of AFKLM_SCS_SocialPostWrapperCls object to be selected for mark as reviewed
	* @param isSpam - Boolean field to set if the Social Post is Spam.
	*/
  	public void markReviewed(List<AFKLM_SCS_SocialPostWrapperCls> socPostList, List<SocialPost> listSocialPosts, Boolean isSpam) {
		List<SocialPost> updateSocPostObjList = new List<SocialPost>();

		if(listSocialPosts != null && listSocialPosts.size() > 0){
			for(SocialPost sp : listSocialPosts){
				sp.SCS_MarkAsReviewed__c = true;
				sp.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
				sp.SCS_Status__c = 'Mark as reviewed';
				sp.Ignore_for_SLA__c = true;
				sp.OwnerId = userInfo.getUserId();
  
				if(isSpam) {
					sp.SCS_Spam__c  = isSpam;
				}
              
				updateSocPostObjList.add(sp);
			}
		} else {

			for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : socPostList){
				if(wrapperSocialPost.isSelected == true){
					wrapperSocialPost.cSocialPost.SCS_MarkAsReviewed__c = true;
					wrapperSocialPost.cSocialPost.SCS_MarkAsReviewedBy__c = userInfo.getFirstName() + ' '+userInfo.getLastName();
					wrapperSocialPost.cSocialPost.SCS_Status__c = 'Mark as reviewed';
					wrapperSocialPost.cSocialPost.Ignore_for_SLA__c = true;
					wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();
	  
					if(isSpam) {
						wrapperSocialPost.cSocialPost.SCS_Spam__c  = isSpam;
					}
	              
					updateSocPostObjList.add(wrapperSocialPost.cSocialPost);
	            }
	        }
	    }

        if(updateSocPostObjList != null && updateSocPostObjList.size() > 0) {
			try {
				// SocialCustomerServiceUtils.userSetSkipValidation(true);			
	            update updateSocPostObjList;
				// SocialCustomerServiceUtils.userSetSkipValidation(false);
	            socPostList = null;
			} catch(Exception ex) {
				System.debug('AFKLM_SCS_ControllerHelper - markReviewed: Exception in updating Social Post: '+ex.getMessage());
			} finally {
				//SocialCustomerServiceUtils.userSetSkipValidation(false);
			}		
		}
	}

	public void changeToFlyingBlue(List<AFKLM_SCS_SocialPostWrapperCls> socPostList, ApexPages.StandardSetController SocPostSetController, Map<String,String> tmpHandle, Set<String> caseIds){
	
            List<SocialPost> updateSocPostObjList = new List<SocialPost>();

            if(caseIds != null){
            	List<SocialPost> lsp = [SELECT id, OwnerId, isFlyingBlue__c FROM SocialPost WHERE ParentId IN: caseIds AND (SCS_Status__c != 'Mark as reviewed' OR SCS_Status__c != 'Actioned')];

            	for(SocialPost sp : lsp){
            		sp.OwnerId = userInfo.getUserId();
            		sp.isFlyingBlue__c = true;

            		updateSocPostObjList.add(sp);
            	}
        	} else {

	            // check if there is more posts from this users then selected
	            while(SocPostSetController.getHasNext()) {

	                SocPostSetController.next();
	                for(SocialPost c : (List<SocialPost>)SocPostSetController.getRecords()) {

	                	socPostList.add(new AFKLM_SCS_SocialPostWrapperCls(c, SocPostSetController));	                
	                }
	            }

	            // update selected + more
	            for(AFKLM_SCS_SocialPostWrapperCls wrapperSocialPost : socPostList){

	                if(tmpHandle.containsKey(wrapperSocialPost.cSocialPost.handle) && 
	                	tmpHandle.get(wrapperSocialPost.cSocialPost.handle) == wrapperSocialPost.cSocialPost.Provider){
	                    
	                    	wrapperSocialPost.cSocialPost.OwnerId = userInfo.getUserId();
	                        wrapperSocialPost.cSocialPost.isFlyingBlue__c = true;
	                                   
	                        updateSocPostObjList.add(wrapperSocialPost.cSocialPost);
	                }
	            }
	        }

            if(updateSocPostObjList != null && updateSocPostObjList.size() > 0) {

                try {

                    update updateSocPostObjList;

                    socPostList = null;
                } catch(Exception ex) {
                    System.debug('AFKLM_SCS_ControllerHelper - change To flying blue: Exception in updating Social Post: '+ex.getMessage());
                } 
            }

    }

	/** 
    * Helper class method to create new cases for the SocialPost in the Controller
    * Method is called by agent clicking on "Engage" button, or when new Wall Post or Private Message is created.
    * 
    *@param postMap - map consists of SocialPost ID and SocialPost
    *@param newCaseId - Id of the new Case
    *@param newCaseNumber - Number of new Case
    *@param error - error message
    *
    *@return returnCaseId as a List value
    */
	public List<String> createNewCases(Map<String, SocialPost> postMap, String newCaseId, String newCaseNumber, Boolean error) {
        String errorMessage = '';
        Map<String, Case> case2insert = new Map<String, Case>();
		List<String> returnCaseId = new List<String>();
		
        for( SocialPost post: postMap.values() ){
            if( post.ParentId != null ){
            	newCaseId = post.ParentId;
            	error = true;
            } else {
            	if( post.ParentId == null ){
				    Case newCase = checkCreatePersonAccount(post);
                    case2insert.put(post.Id, newCase);
                }
            }
        }

	    // system.debug('************ case2insert.values **********: '+case2insert.keySet()); 
	    insert case2insert.values();
    	// system.debug('  **********: '+case2insert.keySet());

        for( String x : case2insert.keySet() ){
            postMap.get( x ).ParentId = case2insert.get(x).Id;
            //postMap.get( x ).SCS_Status__c = 'Actioned';
            
            newCaseId = case2insert.get(x).Id;
            newCaseNumber = case2insert.get(x).CaseNumber;
            returnCaseId.add(newCaseId);
            System.debug('Return Case Id in controller Helper class '+returncaseId);
        }

        update postMap.values();
        return returnCaseId;
    }
    
    /**
    * The "Person Account" should exist for the "Social Persona" (Master Detail).
    * If "Social Persona" parent name is "TemporaryPersonAccount": 
    *  - It will re-allocate and create a new "Person Account" when "Engage" button is selected by agents manually.
    *  - The SocialPost ownerid is changed to the user which press 'Engage' button or via automatic Radian6/SocialHub Inboundhandler 
    *
    * @param    SocialPost to check the handle name and create the PersonAccount if the handle name doesn't exist
    * @return   Case to be inserted
    */
    private Case checkCreatePersonAccount(SocialPost post) {
        Case newCase = new Case();
        // We need to query for validation of the fields between SocialPersona and PersonAccount.
        List<SocialPost> socialPost = [SELECT Id, Name, Language__c, OwnerId, Provider, ExternalPostId, Company__c, Entity__c, MessageType, TopicProfileName FROM SocialPost WHERE Id = :post.Id LIMIT 1];
        
        if('Twitter'.equals(socialPost[0].Provider)) {
			List<SocialPersona> socPersonaHandleNameList = [SELECT Id, ExternalId,  Name, ParentId, RealName, FB_User_ID__c FROM SocialPersona WHERE Id = :post.PersonaId LIMIT 1]; // SCH: used post.PersonaId for now instead Persona.Id
			List<Account> accountHandleNameList = [SELECT Id, sf4twitter__Fcbk_User_Id__pc, Company__c, WeChat_User_ID__c,WeChat_Username__c,SinaWeibo_User_ID__c, SinaWeibo_Username__c,sf4twitter__Twitter_User_Id__pc, sf4twitter__Fcbk_Username__pc, sf4twitter__Twitter_Username__pc FROM Account WHERE Id = :socPersonaHandleNameList[0].ParentId LIMIT 1];
			newCase = createSocialCase(socialPost, accountHandleNameList[0], socPersonaHandleNameList);
			socialPost[0].OwnerId = userInfo.getUserId();
			update socialPost;
        } else if('Facebook'.equals(socialPost[0].Provider)) {
        	List<SocialPersona> socPersonaHandleNameList = [SELECT Id, ExternalId, Name, ParentId, RealName, FB_User_ID__c FROM SocialPersona WHERE Id = :post.PersonaId LIMIT 1]; // SCH: used post.PersonaId for now instead Persona.Id

			if(socPersonaHandleNameList.size() > 0) {
			
				List<Account> accountHandleNameList = [SELECT Id, sf4twitter__Fcbk_User_Id__pc, Company__c, WeChat_User_ID__c,WeChat_Username__c,SinaWeibo_User_ID__c, SinaWeibo_Username__c,sf4twitter__Twitter_User_Id__pc, sf4twitter__Fcbk_Username__pc, sf4twitter__Twitter_Username__pc FROM Account WHERE Id = :socPersonaHandleNameList[0].ParentId LIMIT 1];
   	     		newCase = createSocialCase(socialPost, accountHandleNameList[0], socPersonaHandleNameList);
        	}

        	if('Comment'.equals(socialPost[0].MessageType)){
        		socialPost[0].OwnerId = userInfo.getUserId();
	        	update socialPost;
        	}
	    } else if('WeChat'.equals(socialPost[0].Provider)) {
			List<SocialPersona> socPersonaHandleNameList = [SELECT Id, ExternalId,  Name, ParentId, RealName,FB_User_ID__c FROM SocialPersona WHERE Id = :post.PersonaId LIMIT 1]; // SCH: used post.PersonaId for now instead Persona.Id
			List<Account> accountHandleNameList = [SELECT Id, sf4twitter__Fcbk_User_Id__pc, Company__c, WeChat_User_ID__c,WeChat_Username__c,SinaWeibo_User_ID__c, SinaWeibo_Username__c,sf4twitter__Twitter_User_Id__pc, sf4twitter__Fcbk_Username__pc, sf4twitter__Twitter_Username__pc FROM Account WHERE Id = :socPersonaHandleNameList[0].ParentId LIMIT 1];
			newCase = createSocialCase(socialPost, accountHandleNameList[0], socPersonaHandleNameList);
			socialPost[0].OwnerId = userInfo.getUserId();
			update socialPost;
        } /*else if('SinaWeibo'.equals(socialPost[0].Provider)) {
			List<SocialPersona> socPersonaHandleNameList = [SELECT Id, ExternalId, Name, ParentId, RealName, FB_User_ID__c FROM SocialPersona WHERE Id = :post.PersonaId LIMIT 1]; // SCH: used post.PersonaId for now instead Persona.Id
			List<Account> accountHandleNameList = [SELECT Id, sf4twitter__Fcbk_User_Id__pc, Company__c, WeChat_User_ID__c,WeChat_Username__c,Messenger_User_ID__c, Messenger_Username__c,sf4twitter__Twitter_User_Id__pc, sf4twitter__Fcbk_Username__pc, sf4twitter__Twitter_Username__pc FROM Account WHERE Id = :socPersonaHandleNameList[0].ParentId LIMIT 1];
			newCase = createSocialCase(socialPost, accountHandleNameList[0], socPersonaHandleNameList);
		}*/

        return newCase;
    }

    /**
    * The helper method will create a new Case object. 
    * Also a new PersonAccount object is created if the TW, FB external Id from SocialPersona and PersonAccount object are not the same.  
    * Otherwise it will create a new Case object and attach the existing PersonAccount to the SocialPost.
    *
    * @param    socialPost to create a new case for and attach the newly created person account
    * @param    socialHandleFieldName the social handle fieldname from Account object to verify, e.g. TW, FB
    * @param	socPersonaHandleNameList the social handle of the persona to check with the appropriate person account handle name 
    * @return   Case the newly created case
    */
	private Case createSocialCase(List<SocialPost> socialPost, Account account, List<SocialPersona> socPersonaHandleNameList) {
    	Case newCase = new Case();
    	String companyName;
		String personAccountRecTypeId = [SELECT Id FROM RecordType WHERE (Name='Person Account') AND (SobjectType='Account')].Id;
		Account newPersonAccount = new Account();

		if(socialPost[0].TopicProfileName != null){
		    if(socialPost[0].TopicProfileName.contains('France')){
		      companyName = 'AirFrance';
		    } else {
		      companyName = 'KLM';
		    }
		}

		if(!socPersonaHandleNameList[0].FB_User_ID__c.equals(account.sf4twitter__Fcbk_User_Id__pc) && 'Facebook'.equals(socialPost[0].Provider)) {
			List<Account> persAcc = [SELECT id FROM Account WHERE sf4twitter__Fcbk_User_Id__pc =: socPersonaHandleNameList[0].FB_User_ID__c LIMIT 1];
			
			if(!persAcc.isEmpty()){
				socPersonaHandleNameList[0].ParentId=persAcc[0].id;
				update socPersonaHandleNameList[0];
				
				newPersonAccount = persAcc[0];
			} else {
				newPersonAccount = new Account(
					LastName = socPersonaHandleNameList[0].RealName,
					RecordTypeId = personAccountRecTypeId,
					sf4twitter__Fcbk_Username__pc = socPersonaHandleNameList[0].Name,
					sf4twitter__Fcbk_User_Id__pc = socPersonaHandleNameList[0].FB_User_ID__c,
					Company__c = companyName
				);
				
				insert newPersonAccount;
			}
			
			socPersonaHandleNameList[0].ParentId = newPersonAccount.Id;
			update socPersonaHandleNameList[0];
			
			String newPersonAccountId = [SELECT Id FROM Contact WHERE accountId = :newPersonAccount.Id].Id;
			newCase = createNewCase(socPersonaHandleNameList[0].ParentId, newPersonAccountId, socialPost[0]);
			socialPost[0].WhoId = newPersonAccount.Id;
			update socialPost[0];
			

    } else if(!socPersonaHandleNameList[0].Name.equals(account.sf4twitter__Twitter_Username__pc) && 'Twitter'.equals(socialPost[0].Provider) && !socPersonaHandleNamelist[0].ExternalId.equals(account.sf4twitter__Twitter_User_Id__pc)) {
			newPersonAccount = new Account(
				LastName = socPersonaHandleNameList[0].RealName,
				RecordTypeId = personAccountRecTypeId,
				sf4twitter__Twitter_Username__pc = socPersonaHandleNameList[0].Name,
				sf4twitter__Twitter_User_Id__pc = socPersonaHandleNameList[0].Externalid,
	        	Company__c = companyName
			);
			
			insert newPersonAccount;

			
			socPersonaHandleNameList[0].ParentId = newPersonAccount.Id;
			update socPersonaHandleNameList[0];
			
			String newPersonAccountId = [SELECT Id FROM Contact WHERE accountId = :newPersonAccount.Id].Id;
			newCase = createNewCase(socPersonaHandleNameList[0].ParentId, newPersonAccountId, socialPost[0]);
			
			socialPost[0].WhoId = newPersonAccount.Id;
			update socialPost[0];

	} else if(!socPersonaHandleNameList[0].ExternalId.equals(account.WeChat_User_ID__c) && 'WeChat'.equals(socialPost[0].Provider)) {
			newPersonAccount = new Account(
				LastName = socPersonaHandleNameList[0].RealName,
				RecordTypeId = personAccountRecTypeId,
				WeChat_Username__c = socPersonaHandleNameList[0].Name,
				WeChat_User_ID__c = socPersonaHandleNameList[0].Externalid,
	        	Company__c = companyName
			);
			
			insert newPersonAccount;

			
			socPersonaHandleNameList[0].ParentId = newPersonAccount.Id;
			update socPersonaHandleNameList[0];
			
			String newPersonAccountId = [SELECT Id FROM Contact WHERE accountId = :newPersonAccount.Id].Id;
			newCase = createNewCase(socPersonaHandleNameList[0].ParentId, newPersonAccountId, socialPost[0]);
			
			socialPost[0].WhoId = newPersonAccount.Id;
			update socialPost[0];

	} /*else if(!socPersonaHandleNameList[0].ExternalId.equals(account.SinaWeibo_User_ID__c) && 'SinaWeibo'.equals(socialPost[0].Provider)) {
	        newPersonAccount = new Account(
	          LastName = socPersonaHandleNameList[0].RealName,
	          RecordTypeId = personAccountRecTypeId,
	          Messenger_Username__c = socPersonaHandleNameList[0].Name,
	          Messenger_User_ID__c = socPersonaHandleNameList[0].ExternalId,
	          Company__c = companyName
	        );
	        
	        insert newPersonAccount;
	      
	      
	      socPersonaHandleNameList[0].ParentId = newPersonAccount.Id;
	      update socPersonaHandleNameList[0];
	      
	      String newPersonAccountId = [SELECT Id FROM Contact WHERE accountId = :newPersonAccount.Id].Id;
	      newCase = createNewCase(socPersonaHandleNameList[0].ParentId, newPersonAccountId, socialPost[0]);
	      socialPost[0].WhoId = newPersonAccount.Id;
	      update socialPost[0]; 
    }*/ else {
			if(account.Company__c == null){
	          account.Company__c = companyName;
		      }
		      
		      if('Twitter'.equals(socialPost[0].Provider) && socPersonaHandleNamelist[0].ExternalId.equals(account.sf4twitter__Twitter_User_Id__pc)){
		        if(account.Company__c == null){
		          account.Company__c = companyName;
		        }
		        account.sf4twitter__Twitter_Username__pc = socPersonaHandleNameList[0].Name;
		        
		      }
		    update account;
			String personAccountId = [SELECT Id FROM Contact WHERE accountId = :socPersonaHandleNameList[0].ParentId].Id;
			newCase = createNewCase(socPersonaHandleNameList[0].ParentId, personAccountId, socialPost[0]);
			socialPost[0].WhoId = socPersonaHandleNameList[0].ParentId;
			update socialPost[0];
			}

			return newCase;
	}


	/**
    * Helper method for "checkCreatePersonAccount" method
    * Creates a new Case for SocialPost
    *
    * @param    personAccountId - ID of "Person Account"
    * @param    contactPersonAccountId - ID of Contact related to Account
    * @param    socialPost - to provide additional informations for new case
    *
    * @return   new case
    */
  private Case createNewCase(String personAccountId, String contactPersonAccountId, SocialPost socialPost) {
    Case cs = new Case();
    
    if('AirFrance'.equals(socialPost.Company__c)) {
      RecordType rtAF = [SELECT Id, Name FROM RecordType WHERE Name = 'AF Servicing' AND SobjectType = 'Case' LIMIT 1];
      
      cs = new Case(
              accountId = personAccountId,
              contactId = contactPersonAccountId,
              RecordTypeId = rtAf.Id,
              subject = socialPost.Name,
              ownerId = socialPost.OwnerId,
              Entity__c = socialPost.Entity__c,
              Language__c = socialPost.Language__c,
              Origin = socialPost.Provider,
              suppliedName = userInfo.getUserName()                           
          );
    } else {
      RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'Servicing' AND SobjectType = 'Case' LIMIT 1];
      
      if('WeChat'.equals(socialPost.Provider)){
          cs = new Case(
              accountId = personAccountId,
              contactId = contactPersonAccountId,
              RecordTypeId = rt.Id,
              subject = socialPost.Name,
              ownerId = socialPost.OwnerId,
              Language__c = socialPost.Language__c,
              Origin = socialPost.Provider,
              suppliedName = userInfo.getUserName(),
              chatHash__c = socialPost.ExternalPostId                           
          );
       } else {
  		cs = new Case(
              accountId = personAccountId,
              contactId = contactPersonAccountId,
              RecordTypeId = rt.Id,
              subject = socialPost.Name,
              ownerId = socialPost.OwnerId,
              Language__c = socialPost.Language__c,
              Origin = socialPost.Provider,
              suppliedName = userInfo.getUserName()                          
          );
      }
    }
    
    return cs;
  }
}