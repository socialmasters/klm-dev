/**                     
 * @author (s)      : David van 't Hooft, Mees Witteman
 * @description     : Model class to contain the Flight domain objects
 * @log:   6JUN2014: version 1.0
 */
global class LtcFlightResponseModel {

    public Flight flight;   
    public String errorResponse;
    public String mobileAppPromotionOn = 'false';
    
    public override String toString() { 
        return '{' +
            '"flight": ' + (flight == null ? null : flight.toString()) + ', ' +
            '"miscellaneousFeatures" : {' +
                   '"mobileAppPromotionOn" : ' + mobileAppPromotionOn + 
            '}' +
        '}';
    }

    /**
     * Construct a model, used for indicating the error f.e. noMonitorFlight
     **/
    public LtcFlightResponseModel(String errorResponse) {
    	this.errorResponse = errorResponse;
        this.initialize();
    }

    /**
     * Construct the model
     **/
    public LtcFlightResponseModel(Flight__c flight, LtcAircraft__c ltcAircraft, LtcAircraftType__c ltcAircraftType, 
    		String ratingStatus, 
            String crewLanguages,
            String acceptLanguage) {
                
        this.flight = new Flight(flight.Flight_Number__c, 
	        flight.Scheduled_Departure_Date__c, 
	        (Integer) flight.currentleg__c, 
	        ltcAircraft, 
	        ltcAircraftType, 
	        ratingStatus, 
            crewLanguages,
            acceptLanguage); 
        this.initialize();
    }
    
    private void initialize() {
        if (LtcSettings__c.getInstance('Mobile App Promotion Toggle') != null) {
            this.mobileAppPromotionOn =  LtcSettings__c.getInstance('Mobile App Promotion Toggle').value__c;
        }
    }

    public boolean isEmpty() {
        return flight == null;
    }
            
    global class Flight { 
        public String flightNumber;
        public Date travelDate { get; set; }
        public Integer currentLeg  { get; set; }
        public String ratingStatus { get; set; }    //Inactive / Active / Closed   
        public String acceptLanguage { get; set; }    
        public Aircraft aircraft { get; set; }
        public Crew crew { get; set; }
        public List<Leg> legs = new List<Leg>();
    
        public Flight(String flightNumber, 
        		Date travelDate,
        		Integer currentLeg,
        		LtcAircraft__c ltcAircraft, 
        		LtcAircraftType__c acType, 
        		String ratingStatus, 
                String crewLanguages,
                String acceptLanguage) {
                    
            this.flightNumber = flightNumber;
            this.travelDate = travelDate;
            this.currentLeg = currentLeg;
            this.ratingStatus = ratingStatus;
            this.acceptLanguage = acceptLanguage;
            this.aircraft = ltcAircraft == null ? null : new Aircraft(ltcAircraft.name, ltcAircraft.aircraft_Name__c, acType);
            CrewAmounts ca = acType == null ? null : new CrewAmounts(acType.Senior_Pursers__c, acType.Pursers__c, acType.businessclass_CA_s__c, acType.Economy_CA_s__c);
            this.crew = new Crew(ca, crewLanguages);
        }
        
        public void addLeg(Leg leg) {
        	this.legs.add(leg);
        }
        
        public override String toString() {
            LtcDateTimeFormatter dateFormatter = new LtcDateTimeFormatter();
            String legsString = null;
            if (this.legs != null && !this.legs.isEmpty()) {
            	legsString = '[';
	            for (Leg leg : this.legs) {
	            	legsString = legsString + leg.toString() + ',';
	            }            
	            legsString = legsString.substring(0, legsString.length() - 1);
		        legsString = legsString + ']';
            }
            return '{' +
                '"flightNumber": "' + flightNumber +  '", ' +
                '"travelDate": "' + String.ValueOf(travelDate) +  '", ' +
                '"travelDateToLocale": "' + dateFormatter.formatDate(travelDate, acceptLanguage) +  '", ' +
                '"currentLeg": ' + currentLeg +  ', ' +
                '"aircraft": ' + (aircraft == null ? null : aircraft.toString()) +  ', ' +
                '"ratingStatus": "' + ratingStatus +  '", ' +
                '"crew": ' + crew.toString() +  ', ' +
                '"legs": ' + legsString +
            '}';
        }
    }
    global class Leg {
    	public Airport origin { get; set; }
		public String status { get; set; }
		public Airport destination { get; set; }
		public Integer legNumber { get; set; }
		public Integer timeToArrival { get; set; }
		public String scheduledArrivalDate { get; set; }
		public String scheduledArrivalTime { get; set; }
		public String actualArrivalTime { get; set; }
		public String estimatedArrivalTime { get; set; }
		public Integer arrivalDelay { get; set; }
		public String scheduledDepartureDate { get; set; }
		public String scheduledDepartureTime { get; set; }
		public String actualDepartureTime { get; set; }
		public String estimatedDepartureTime { get; set; }
		public Integer timeToDeparture { get; set; }
		public Integer departureDelay { get; set; }
		public String actualArrivalDate { get; set; }
		public String actualDepartureDate { get; set; }
		public String arrivalDate { get; set; }
		public String departureDate { get; set; }
		public String estimatedArrivalDate { get; set; }
		public String estimatedDepartureDate { get; set; }
		public List<CabinClass> cabinClasses = new List<CabinClass>();
		
		public Leg(Leg__c leg, Airport origin, Airport destination) {
			this.origin = origin;
			this.status = leg.status__c;
			this.destination = destination;
			this.legNumber = (Integer) leg.legNumber__c;
			this.timeToArrival = (Integer) leg.timeToArrival__c;
			this.scheduledArrivalDate = String.ValueOf(leg.scheduledArrivalDate__c);
			this.scheduledArrivalTime = leg.scheduledArrivalTime__c;
			this.actualArrivalTime = leg.actualArrivalTime__c;
			this.estimatedArrivalTime = leg.estimatedArrivalTime__c;
			this.arrivalDelay = (Integer) leg.arrivalDelay__c;
			this.scheduledDepartureDate = String.ValueOf(leg.scheduledDepartureDate__c);
			this.scheduledDepartureTime = leg.scheduledDepartureTime__c;
			this.actualDepartureTime = leg.actualDepartureTime__c;
			this.estimatedDepartureTime = leg.estimatedDepartureTime__c;
			this.timeToDeparture = (Integer) leg.timeToDeparture__c;
			this.departureDelay = (Integer)leg.departureDelay__c;
			this.actualArrivalDate = String.ValueOf(leg.actualArrivalDate__c);
			this.actualDepartureDate = String.ValueOf(leg.actualDepartureDate__c);
			this.arrivalDate = String.ValueOf(leg.arrivalDate__c);
			this.departureDate = String.ValueOf(leg.departureDate__c);
			this.estimatedArrivalDate = String.ValueOf(leg.estimatedArrivalDate__c);
			this.estimatedDepartureDate = String.ValueOf(leg.estimatedDepartureDate__c);
		}
		
		public void addClass(CabinClass cabinClass) {
			this.cabinClasses.add(cabinClass);
		}
		
		public override String toString() {
			String classesString;
        	if (cabinClasses.size() != 0) {
        		classesString = '[';
	        	for (CabinClass cabinClass : cabinClasses) {
	        		classesString = classesString +  cabinClass.toString() + ',';
	        	}
	        	// remove the last comma
	        	classesString = classesString.substring(0, classesString.length() - 1);
	        	classesString = classesString + ']';
        	}
			
		 	return '{' +
                '"status": "' + status + '",' + 
                '"origin": ' + origin.toString() + ',' + 
                '"destination": ' + destination.toString() + ',' + 
                '"legNumber": ' + legNumber + ', ' +
                '"departureDate": "' + departureDate + '", ' +
                '"timeToDeparture": ' + timeToDeparture + ', ' +
                '"departureDelay": ' + departureDelay + ', ' +
                '"arrivalDate": "' + arrivalDate +  '", ' +
                '"timeToArrival": ' + timeToArrival +  ', ' +
                '"arrivalDelay": ' + arrivalDelay +  ', ' +
                '"scheduledDepartureDate": "' + scheduledDepartureDate +  '", ' +
                '"scheduledDepartureTime": "' + scheduledDepartureTime +  '", ' +
                '"scheduledArrivalDate": "' + scheduledArrivalDate +  '", ' +
                '"scheduledArrivalTime": "' + scheduledArrivalTime +  '", ' +
                '"estimatedDepartureDate": "' + estimatedDepartureDate +  '",' +
                '"estimatedDepartureTime": "' + estimatedDepartureTime +  '", ' +
                '"estimatedArrivalDate": "' + estimatedArrivalDate +  '", ' +
                '"estimatedArrivalTime": "' + estimatedArrivalTime +  '", ' +
                '"actualDepartureDate": "' + actualDepartureDate +  '", ' +
                '"actualDepartureTime": "' + actualDepartureTime +  '", ' +
                '"actualArrivalDate": "' + actualArrivalDate +  '", ' +
                '"actualArrivalTime": "' + actualArrivalTime +  '",' +
            	'"classes" : ' + classesString +
            '}';
		}
    }

    
    global class CabinClass {
        public String name;
        public String description;
        public List<ServiceItem> serviceItems;
        
        public CabinClass(String name, String description, List<ServiceItem> serviceItems) {
            this.name = name;
            this.description = description;
            this.serviceItems = serviceItems;
        }
        
        public override String toString() { 
        	String itemsString;
        	if (serviceItems.size() != 0) {
        		itemsString = '[';
	        	for (ServiceItem item : serviceItems) {
	        		itemsString = itemsString +  item.toString() + ',';
	        	}
	        	// remove the last comma
	        	itemsString = itemsString.substring(0, itemsString.length() - 1);
	        	itemsString = itemsString + ']';
        	}
            return '{' +
                '"name": "' + name +  '", ' +
                '"description": "' + description  + '",' +
                '"serviceItems": ' + itemsString  +
            '}';
        }
    }
    
    global class ServiceItem {
        public String name;
        public String description;
		public Integer order;
		public String type;

        public ServiceItem(String name, String description, Integer order, String type) {
            this.name = name;
            this.description = description;
            this.order = order;
            this.type = type;
        }
        
        public override String toString() { 
            return '{' +
                '"name": "' + name +  '", ' +
                '"description": "' + description +  '", ' +
                '"order": ' + order +  ', ' +
                '"type": "' + type  +  '"' + 
            '}';
        }
    }
        
    global class Crew {
        public CrewAmounts crewAmounts;
        public String language;
        
        public Crew(CrewAmounts crewAmounts, String language) {
            this.crewAmounts = crewAmounts;
            this.language = language;
        }
        
        public override String toString() { 
            return '{' +
                '"crewAmounts": ' + crewAmounts +  ', ' +
                '"language": ' + language  + 
                '}';
        }
    }
    
    global class CrewAmounts {
        public String seniorPursers;
        public String pursers;
        public String businessCAs;
        public String economyCAs;
        
        public CrewAmounts(String seniorPursers, String pursers, String businessCAs, String economyCAs) {
            this.seniorPursers = seniorPursers;
            this.pursers = pursers;
            this.businessCAs = businessCAs;
            this.economyCAs = economyCAs;
        }
        
        public override String toString() { 
            String result;
            if (seniorPursers == null && pursers == null && businessCAs == null && economyCAs == null) {
                result = null;
            } else {
                result = '{' +
                    '"seniorPursers": "' + seniorPursers +  '", ' +
                    '"pursers": "' + pursers +  '", ' +
                    '"businessCAs": "' + businessCAs +  '", ' +
                    '"economyCAs": "' + economyCAs + '"' +
                '}';
            }
            return result;
        }
    }
    
    global class Aircraft {
        public String registrationCode;
        public String name;
        public AircraftType aircraftType;
        
        public Aircraft(String registrationCode, String name, LtcAircraftType__c acType) {
            this.registrationCode = registrationCode;
            this.name = name;
            this.aircraftType = new AircraftType(acType);
        }
        
        public override String toString() { 
            return '{' +
                '"registrationCode": "' + registrationCode +  '", ' +
                '"name": ' + (name == null ? null : + '"' + name  + '"') + ', ' +
                '"aircraftType": ' + aircraftType +
                '}';
        } 
    }
    global class AircraftTypeFact {
    	public String name;
    	public String content;
    	public String description;
    	
    	public AircraftTypeFact(LtcAircraftType_Fact__c fact) {
    		this.name = fact.name;
    		this.content = fact.content__c;
    		this.description = fact.description__c;
    	}
    	
    	public override String toString() { 
            return '{' +
                '"name": "' + name +  '", ' +
                '"content": "' + content +  '", ' +
                '"description": "' + description + '"' +
            '}';
        } 
    }
    
    global class AircraftType {
        public String name;
        public Integer amountOwnedByKLM;
        public String imageUrl;
        public Decimal length;
        public String lengthUnit;
        public Decimal width;
        public String widthUnit;
        public Decimal maxWeight;
        public String maxWeightUnit;
        public Decimal maxDistance;
        public String maxDistanceUnit;
        public Integer cruisingSpeed;
        public String cruisingSpeedUnit;
        public Integer nrOfFlightsPerWeek;
        public Integer nrOfSeats;
        public List<AircraftTypeFact> facts = new List<AircraftTypeFact>();
        
        public AircraftType(LtcAircraftType__c acType) {
            this.name = acType.name;
            this.amountOwnedByKLM = (Integer) acType.amount_owned_by_KLM__c;
            this.length = acType.length__c;
            this.lengthUnit = acType.length_unit__c;
            this.width = acType.width__c;
            this.widthUnit = acType.width_unit__c;
            this.maxWeight = acType.max_weight__c;
            this.maxWeightUnit = acType.max_weight_unit__c;
            this.maxDistance = acType.max_distance__c;
            this.maxDistanceUnit = acType.max_distance_unit__c;
            this.cruisingSpeed = (Integer) acType.cruising_speed__c;
            this.cruisingSpeedUnit = acType.cruising_speed_unit__c;
            this.nrOfFlightsPerWeek = (Integer)acType.nr_of_flights_per_week__c;
            this.nrOfSeats = (Integer) acType.nr_of_seats__c;
            
            // strip http protocol
            String imgUrl = acType.Image_URL__c;
            if (imgUrl != null && imgUrl.length() > 6 && imgUrl.startsWith('http:')) {
                imgUrl = imgUrl.substring(5);
            }
           	this.imageUrl = imgUrl;
            
            for (LtcAircraftType_Fact__c ltcFact : acType.Aircraft_type_facts__r) {
            	AircraftTypeFact fact = new AircraftTypeFact(ltcFact);
            	facts.add(fact);
            }
        }
        
        public override String toString() { 
        	String factsString;
        	if (facts.size() != 0) {
        		factsString = '[';
	        	for (AircraftTypeFact fact : facts) {
	        		factsString = factsString +  fact.toString() + ',';
	        	}
	        	// remove the last comma
	        	factsString = factsString.substring(0, factsString.length() - 1);
	        	factsString = factsString + ']';
        	}
        	
            return '{' +
                '"name": "' + name +  '", ' +
                '"amountOwnedByKLM": ' + amountOwnedByKLM +  ', ' +
                '"imageUrl": "' + imageUrl +  '", ' +
                '"length": ' + length +  ', ' +
                '"lengthUnit": "' + lengthUnit +  '", ' +
                '"width": ' + width +  ', ' +
                '"widthUnit": "' + widthUnit +  '", ' +
                '"maxWeight": ' + maxWeight +  ', ' +
                '"maxWeightUnit": "' + maxWeightUnit +  '", ' +
                '"maxDistance": ' + maxDistance +  ', ' +
                '"maxDistanceUnit": "' + maxDistanceUnit +  '", ' +
                '"cruisingSpeed": ' + cruisingSpeed +  ', ' +
                '"cruisingSpeedUnit": "' + cruisingSpeedUnit +  '", ' +
                '"nrOfFlightsPerWeek": ' + nrOfFlightsPerWeek +  ', ' +
                '"nrOfSeats": ' + nrOfSeats +  ', ' +
                '"funFacts": ' + factsString +
            '}';
        } 
    }
     
    global class Airport {
        public String iataCode;
        public String airportName;
        public String locationName;
        public String country;
        public Weather weather;
        public String cityPageLink;
        public String cityCode;
        
        public Airport(String iataCode, String airportName, String locationName, String country, Weather weather, String cityPageLink, String cityCode) {
            this.iataCode = iataCode;
            this.airportName = airportName;
            this.locationName = locationName;
            this.country = country;
            this.weather = weather;
            this.cityPageLink = cityPageLink;
            this.cityCode = cityCode;
        }
        
        public override String toString() { 
            return '{' +
                '"IATACode": "' + iataCode +  '", ' +
                '"airportName": "' + airportName +  '", ' +
                '"country": "' + country +  '", ' +
                '"locationName": "' + locationName + '", ' +
                '"cityPageLink": "' + cityPageLink + '", ' +
                '"cityCode": "' + cityCode + '", ' +
                '"weather": ' + weather  + 
            '}';
        } 
    }
     
    global class Weather {
        public String temp;
        public WeatherDescription description;
        
        public Weather(String temp,  WeatherDescription description) {
            this.temp = temp;
            this.description = description;
        }
        
        public override String toString() { 
            return '{' +
                '"temp": ' + temp +  ', ' +
                '"description": ' + description.toString() +
            '}';
        } 
    }
     
    global class WeatherDescription {
        public String id;
        public String icon;
        public String description;
        
        public WeatherDescription(String id, String icon, String description) {
            this.id = id;
            this.icon = icon;
            this.description = description;
        }
        
        public override String toString() { 
            return '{' +
                '"id": "' + id +  '", ' +
                '"icon": "' + icon +  '", ' +
                '"description": "' + description + '"' +
            '}';
        } 
    }
}