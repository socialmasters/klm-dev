/**********************************************************************
 Name:  AFKLM_SCS_SearchCustomer.cls
======================================================
Purpose: 
	1. Controller for SCS_SearchCustomer VF Page
	2. Remote action to search records for Type Ahead
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Patrick Brinksma    14 Jul 2014     Initial development
    1.1     Stevano Cheung	    23 Sep 2014     Initial added 'Log a call' and removed 'Save remarks'    
***********************************************************************/  
global with sharing class AFKLM_SCS_SearchCustomer {

	// pattern compilation is expensive
	private static Pattern PATTERN_FLYING_BLUE_NUMBER;
	private static Pattern PATTERN_QUERY_TOKENS;

	// note these are the developer names!
	private final static String RT_CASE_PA_DESK = 'PA_Desk';
	private final static String RT_CASE_PLATINUM_SERVICE_LINE = 'Platinum_Service_Line';
	private final static String RT_ACCT_PERSON_ACCOUNT = 'PersonAccount';

	private static final String SEARCH_CUSTOMER_URL = 'https://api.klm.com/customerapi/customer/';
    private static final String SEARCH_CUSTOMER_URL_BY_ID = 'https://api.klm.com/customerapi/customers/';

	public Boolean isPADeskUser {
		get {
			List<User> users = [ SELECT Id FROM User WHERE Id = :UserInfo.getUserId() AND Profile.Name LIKE '%PA_DESK%' ];
			return ( users.size() > 0 );
		}
	}

	public Id getPADeskCaseRecordTypeId() {
		Map<String, Id> caseRecordTypeMap = AFKLM_Utility.getRecordTypeIdsByDeveloperName( Case.SObjectType );
		return caseRecordTypeMap.get( RT_CASE_PA_DESK );
	}

	public Id getPSLCaseRecordTypeId() {
		Map<String, Id> caseRecordTypeMap = AFKLM_Utility.getRecordTypeIdsByDeveloperName( Case.SObjectType );
		return caseRecordTypeMap.get( RT_CASE_PLATINUM_SERVICE_LINE );
	}

	/*
	 * Search for records for Type Ahead
	 * Returns list of records found
	 */
	@RemoteAction
	global static list<sObject> searchForRecords( String queryString, String sObjectName, 
	list<String> fieldNames, String fieldsToSearch, Object filterClauseObj, String orderBy, Integer recordLimit ) {

	if (queryString == null) return null;

	If (sObjectName == null) return null;

	String sQuery = String.escapeSingleQuotes( queryString );
	if (sQuery.length() == 0) return null;

	String sInFields = 
		(fieldsToSearch == null || fieldsToSearch == '' || fieldsToSearch.toUpperCase() == 'ALL') ? '' : 
			( ' IN ' + String.escapeSingleQuotes(fieldsToSearch) + ' FIELDS' );

	String sFields = (fieldNames == null || fieldNames.isEmpty()) ? 'Id, Name' : 
		String.escapeSingleQuotes( String.join( fieldNames, ', ' ) );  

	String sOrder = ' ORDER BY ' + ( (orderBy == null || orderBy == '') ? 'Name' : String.escapeSingleQuotes(orderBy) ); 

	String sLimit = (recordLimit == null || recordLimit == 0 || recordLimit >= 2000) ? '' : 
		( ' LIMIT ' + String.valueOf(recordLimit));

	// can't escape the filter clause
	String filterClause = (String)filterClauseObj;
	String sWhere = (filterClause == null || filterClause == '') ? '' : 
		( ' WHERE ' + filterClause );

		String soslQuery = 'FIND {' + sQuery + '*}' + sInFields + 
			' RETURNING ' + sObjectName + '( ' + sFields + sWhere + sOrder + sLimit + ' )';
		System.debug( soslQuery );
		List<List<sObject>> results = Search.query( soslQuery );

		return results[0];
	}

	@RemoteAction
	global static List<SearchResultItem> searchExternalFlyingBlueMembers( String flyingBlueNumber ) {
		List<SearchResultItem> results = new List<SearchResultItem>();
		String sb = '?flying-blue-number=' + flyingBlueNumber;
		AFKLM_SCS_CustomerAPIClient customerAPIClient = new AFKLM_SCS_CustomerAPIClient();
		try {
			// retrieve customer id by fb number
			AFKLM_SCS_RestResponse response = customerAPIClient.request(SEARCH_CUSTOMER_URL + sb, 'GET', new Map<String,String>());
			if ( ! response.error ) {
				Map<String, Object> map1 = response.toMap();
				// retrieve customer details by id
				AFKLM_SCS_RestResponse response2 = customerAPIClient.request(SEARCH_CUSTOMER_URL_BY_ID + map1.get('id'), 'GET', new Map<String,String>());
				if ( ! response2.error ) {
					// manually parse response, this really needs to be fixed properly by the customer api client
					Map<String, Object> map2 = response2.toMap();
					Map<String, Object> map3 = (Map<String, Object>) map2.get('individual');
					String firstName = (String)map3.get('firstName');
					String lastName = (String)map3.get('familyName');
					SearchResultItem item = new SearchResultItem();
					item.name = firstName + ' ' + lastName;
					item.firstName = firstName;
					item.lastName = lastName;
					item.flyingBlueNumber = flyingBlueNumber;
					item.isExternal = true;
					item.isPADesk = false;
					item.isRestricted = false;
					results.add( item );
				}
			}
		} catch ( Exception e ) {
			throw new SearchException( 'Unable to search Customer API.', e );
		}
		return results;
	}

	@RemoteAction
	global static List<SearchResultItem> searchInternalFlyingBlueMembers( String flyingBlueNumber, Boolean limitToPaDesk ) {
		List<SearchResultItem> results = new List<SearchResultItem>();
		String soql = '';
		soql += 'SELECT';
		soql += ' FirstName,';
		soql += ' LastName,';
		soql += ' Initials__pc,';
		soql += ' Name,';
		soql += ' Flying_Blue_Number__c,';
		soql += ' Personal_Assistance__pc,';
		soql += ' Restricted_Account__c';
		soql += ' FROM';
		soql += ' Account';
		soql += ' WHERE';
		soql += ' Flying_Blue_Number__c = :flyingBlueNumber';
		if ( limitToPaDesk ) {
			soql += ' AND Personal_Assistance__pc = :limitToPaDesk';
		}
		List<Account> accounts = Database.query( soql );
		for ( Account a : accounts ) {
			results.add( new SearchResultItem( a ) );
		}
		return results;
	}

	private static Boolean isFlyingBlueNumber( String query ) {
		Boolean result = false;
		if ( ! String.isBlank( query ) ) {
			if ( PATTERN_FLYING_BLUE_NUMBER == null ) {
				PATTERN_FLYING_BLUE_NUMBER = Pattern.compile( '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' );
			}
			return PATTERN_FLYING_BLUE_NUMBER.matcher( query ).matches();
		}
		return result;
	}

	private static List<SearchResultItem> searchAccountWithWildcards( String query, Boolean limitToPaDesk, Integer resultLimit ) {
		List<SearchResultItem> results = new List<SearchResultItem>();

		String selectStmt = 'SELECT Id, FirstName, LastName, Initials__pc, Name, Flying_Blue_Number__c, Personal_Assistance__pc, Restricted_Account__c FROM Account';
		String filter = ' WHERE ( Flying_Blue_Number__c != null OR Restricted_Account__c = true )';
		if ( limitToPaDesk ) {
			filter += ' AND Personal_Assistance__pc = true';
		}

		// tokenize query string
		if ( PATTERN_QUERY_TOKENS == null ) {
			PATTERN_QUERY_TOKENS = Pattern.compile( '"([^"]+)"|([^\\s]+)\\s*' );
		}
		Matcher m = PATTERN_QUERY_TOKENS.matcher( query );
		final String AND_START = ' AND (';
		final String AND_END = ')';

		String clause = AND_START;
		while ( m.find() ) {
			if ( clause.length() > AND_START.length() ) {
				clause += ' OR';
			}
			String token;
			if ( ! String.isBlank( m.group( 1 ) ) ) {
				token = m.group( 1 );
			} else {
				token = m.group( 2 );
			}
			if ( token.contains( '*' ) ) {
				clause += ' Name LIKE \'' + String.escapeSingleQuotes( token.replaceAll( '\\*', '%' ) ) + '\'';
			} else {
				clause += ' Name = \'' + String.escapeSingleQuotes( token ) + '\'';
			}
		}
		if ( clause.length() > AND_START.length() ) {
			clause += AND_END;
		}
		String stmt = selectStmt + filter;
		if ( clause.length() > AND_START.length() ) {
			stmt += clause;
		}
		if ( resultLimit != null ) {
			stmt += ' LIMIT :resultLimit';
		}
		try {
			List<Account> accounts = Database.query( stmt );
			for ( Account a : accounts ) {
				results.add( new SearchResultItem( a ) );
			}
		} catch ( QueryException qe ) {
			System.debug( stmt );
			throw new SearchException( qe.getMessage() );
		}
		return results;
	}

	@RemoteAction
	global static List<SearchResultItem> searchAccounts( String query, Boolean limitToPaDesk, Integer resultLimit ) {

		if ( String.isBlank( query ) ) {
			throw new SearchException( 'Query string cannot be empty!' );
		}

		if ( query.contains( '*' ) ) {
			// wild card search
			return searchAccountWithWildcards( query, limitToPaDesk, resultLimit );
		} else if ( isFlyingBlueNumber( query ) ) {
			List<SearchResultItem> result = new List<SearchResultItem>();
			result.addAll( searchInternalFlyingBlueMembers( query, limitToPaDesk ) );
			if ( result.size() == 0 && ( ! limitToPaDesk ) ) {
				result.addAll( searchExternalFlyingBlueMembers( query ) );
			}
			return result;
		} else {
			// sosl search
			String filter = '( Flying_Blue_Number__c != null OR Restricted_Account__c = true )';
			if ( limitToPaDesk ) {
				filter += ' AND Personal_Assistance__pc = true';
			}
			List<sObject> results = searchForRecords(
				query,
				'Account',
				new List<String>{ 'FirstName', 'LastName', 'Name', 'Initials__pc', 'Flying_Blue_Number__c', 'Personal_Assistance__pc', 'Restricted_Account__c' },
				'NAME',
				filter,
				'Personal_Assistance__pc DESC, LastName, FirstName',
				resultLimit);
			List<SearchResultItem> searchResultItems = new List<SearchResultItem>();
			for ( sObject result : results ) {
				Account a = ( Account ) result;
				searchResultItems.add( new SearchResultItem( a ) );
			}
			return searchResultItems;
		}
	}

	@RemoteAction
	global static Account createAccount( String firstName, String lastName, String flyingBlueNumber ) {
		Map<String,Id> accountTypes = AFKLM_Utility.getRecordTypeIdsByDeveloperName( Account.SObjectType );
		Account a = new Account();
		a.FirstName = firstName;
		a.LastName = lastName;
		a.Flying_Blue_Number__c = flyingBlueNumber;
		a.RecordTypeId = accountTypes.get( RT_ACCT_PERSON_ACCOUNT );
		insert a;
		return a;
	}

	/*
	 * Method to create a Servicing Case for specific Account
	 * returns newly created Case
	 */
	@RemoteAction
	global static Case createCase( Id accountId ){
		Case result = new Case();
		Account a = [ SELECT PersonContactId, Personal_Assistance__pc FROM Account WHERE Id = :accountId ];
		Map<String,Id> caseTypes = AFKLM_Utility.getRecordTypeIdsByDeveloperName( Case.SObjectType );
		String recordTypeName;
		if ( a.Personal_Assistance__pc ) {
			recordTypeName = RT_CASE_PA_DESK;
		} else {
			recordTypeName = RT_CASE_PLATINUM_SERVICE_LINE;
		}
		if ( ! caseTypes.containsKey( recordTypeName ) ) {
			throw new SearchException('User does not have access to record type: ' + recordTypeName );
		}
		Case c = new Case();
		c.RecordTypeId = caseTypes.get( recordTypeName );
		c.ContactId = a.PersonContactId;
		c.AccountId = a.Id;
		insert c;
		return c;
	}

	global class SearchResultItem {

		public SearchResultItem() {}

		public SearchResultItem( Account a ) {
			id = a.Id;
			name = a.Name;
			firstName = a.FirstName;
			lastName = a.LastName;
			initials = a.Initials__pc;
			flyingBlueNumber = a.Flying_Blue_Number__c;
			isExternal = false;
			isPADesk = a.Personal_Assistance__pc;
			isRestricted = a.Restricted_Account__c;
		}

		public Id id { get; set; }
		public String name { get; set; }
		public String firstName { get; set; }
		public String lastName { get; set; }
		public String initials { get; set; }
		public String flyingBlueNumber { get; set; }
		public Boolean isExternal { get; set; }
		public Boolean isPADesk { get; set; }
		public Boolean isRestricted { get; set; }

	}

	global class SearchException extends Exception {}

}