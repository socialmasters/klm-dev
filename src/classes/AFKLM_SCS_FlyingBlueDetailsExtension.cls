/********************************************************************** 
 Name:  AFKLM_SCS_FlyingBlueDetailsExtension
 Task:    N/A
 Runs on: SCS_FlyingBlueDetails
====================================================== 
Purpose: 
    Getting the information from the 'Customer API' based on FB#, E-mail, Social Handle/Social Network
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      30/04/2014      Initial Development
    1.1     Patrick Brinksma    12 Jun 2014     Added support for Account standardcontroller
    1.2		Stevano Cheung		9 Dec 2014		Customer API 2.0 modification 'CustomerId' is 'id'. Sub class PostalAddress removed from PostalAddresses. Mobile phone changed.
***********************************************************************/
public with sharing class AFKLM_SCS_FlyingBlueDetailsExtension {
    private static final String SEARCH_CUSTOMER_URL = 'https://api.klm.com/customerapi/customer';
    private static final String SEARCH_CUSTOMER_URL_BY_ID = 'https://api.klm.com/customerapi/customers/';
    
    private Case cs;
    private Account acc;
    private String customerId; 
    private String socialHandle;
    private String socialId = '';
    private String socialNetwork;
    public String eMail = ''; 
    
    public String fullName {get; set;} // Combined of firstName and familyName 
    public String title {get; set;}
    private String flyingBlueNumber = ''; 
    public String tierLevel {get; set;}
    public String levelMiles {get; set;} 
    public String levelStartDate {get; set;} 
    public String awardMiles {get; set;}
    public String awardMilesExp {get; set;}
    public String language {get; set;} 
    public String nationality {get; set;}
    public String birthDate {get; set;}
    public String seatPreference {get; set;}
    public String mealPreference {get; set;} 
    public String flyingBlueEMail {get; set;}       
    public String mobilePhone {get; set;}
    public String fullAddressHouseNumber {get; set;}
    public String fullAddressPostalCode {get; set;}
    public String fullAddressCity {get; set;}
    public String fullAddressCountry {get; set;}
    
    /** 
     * Constructor to get the fields from Case and set some parameters FB#, e-mail
     */
    public AFKLM_SCS_FlyingBlueDetailsExtension(ApexPages.StandardController controller) {
        Id thisId = (Id) controller.getId();
        
        if(thisId != null) {
            if (thisId.getSObjectType() == Case.SObjectType){
                if(!Test.isRunningTest()){
                    controller.addFields(new List<String>{'AccountId', 'Origin'});
                }
                this.cs = (Case) controller.getRecord();
                this.socialNetwork = cs.Origin;
                thisId = this.cs.AccountId;
            } else if (thisId.getSObjectType() == Account.SObjectType){
                this.acc = (Account) controller.getRecord();
                thisId = this.acc.Id;
            }
            
            try {
                setAccountParameters(thisId);
                getIdCustomerAPI();
                getCustomerAPIAccountDetails(customerId);
            } catch (DmlException e) {
                // Try to reconnect instead of the Websrv callout, it's not possible to do a DML during Websrv callout
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Connection refused');
                ApexPages.addMessage(myMsg);
            }
        }
    }
    
    /** 
     * Set the Flying Blue number, E-mail and SocialHandle, SocialNetwork parameters
     */
    private void setAccountParameters(String accountId) {
        List<Account> accnt = [SELECT Flying_Blue_Number__c, PersonEmail, sf4twitter__Fcbk_Username__pc, sf4twitter__Fcbk_User_Id__pc, sf4twitter__Twitter_Username__pc, sf4twitter__Twitter_User_Id__pc FROM Account WHERE Id =: accountId LIMIT 1];
        
        if(accnt.size() > 0 ) {
            if(accnt[0].Flying_Blue_Number__c != null) {
                this.flyingBlueNumber = accnt[0].Flying_Blue_Number__c;
            }
            
            if(accnt[0].PersonEmail != null) {
                this.eMail = accnt[0].PersonEmail;
            }
            
            // Not needed now but can be used in the future
            if(accnt[0].sf4twitter__Fcbk_User_Id__pc != null) {
                this.socialId = accnt[0].sf4twitter__Fcbk_User_Id__pc.replace('FB_','');
                this.socialNetwork = 'Facebook';
            } else {
                this.socialId = accnt[0].sf4twitter__Twitter_User_Id__pc;
                this.socialNetwork = 'Twitter';
            }
        }
    }

    /** 
     * Search the Customer API Id based on Flying Blue number, E-mail and SocialHandle, SocialNetwork parameters
     */
    private void getIdCustomerAPI() {
        
        try {
            String sb = '';
            
            if( !''.equals(flyingBlueNumber) ) {
                sb = '/?flying-blue-number=' + flyingBlueNumber;
            } else if(!''.equals(eMail)) {
                sb = '/?email-address=' + eMail;
            } 
            else if(!''.equals(socialId) && !''.equals(socialNetwork)) {
                sb = '/?social-network=' +socialNetwork+'&social-identity-identifier=' +socialId;
            }
            
            AFKLM_SCS_CustomerAPIClient customerAPIClient = new AFKLM_SCS_CustomerAPIClient();
            AFKLM_SCS_RestResponse response = customerAPIClient.safeRequest(SEARCH_CUSTOMER_URL + sb, 'GET', new Map<String,String>());
            setFlyingBlueFieldValues(response.toMap());
        } catch(Exception e) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Unable to display information <br> - Please check Flying blue number, e-mail');
            ApexPages.addMessage(myMsg);
        } 
    }

    /** 
     * Get the Customer API account details.
     */
    private void getCustomerAPIAccountDetails(String customerId) {
        try {
            if(customerId != null) {
                AFKLM_SCS_CustomerAPIClient customerAPIClient = new AFKLM_SCS_CustomerAPIClient();
                AFKLM_SCS_RestResponse response = customerAPIClient.safeRequest(SEARCH_CUSTOMER_URL_BY_ID + customerId, 'GET', new Map<String,String>());
                setFlyingBlueFieldValues(response.toMap());
            }
        } catch(Exception e) {
            // ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Unable to display Customer Id: ' + customerId+ ' Exception: '+e);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Unable to display all customer information for Customer Id: ' + customerId+ '. Please contact your Administrator.');
            ApexPages.addMessage(myMsg);
        } 
    }
    
    /** 
     * Find the values from the Customer API and set the values
     */
    private void setFlyingBlueFieldValues(Map<String, Object> response) {
        for (String customerApiFields : response.keySet()) {
            // system.debug('*******'+ customerApiFields + '*** values: ' +response.get(customerApiFields));
            if('id'.equals(customerApiFields)) {
                this.customerId = (String) response.get(customerApiFields);
            }
            
            if('preference'.equals(customerApiFields)) {
                String serializedPreference = JSON.serialize(response.get(customerApiFields));
                JSONParser parserPreference = JSON.createParser(serializedPreference);
                
                while (parserPreference.nextToken() != null) {
                    Preference pref = (Preference) parserPreference.readValueAs(Preference.class);
                    
                    // Will give nullpointer if not checked
                    if(pref.flightPreference != null) {
                        if(pref.flightPreference.meal != null) {
                            this.mealPreference = pref.flightPreference.meal.code;
                        }
                    }

                    if(pref.communicationPreference != null) {
                        if(pref.communicationPreference.language != null) {
                            this.language = pref.communicationPreference.language; 
                        }
                    }
                    
                    if(pref.flightPreference != null) {
                        if(pref.flightPreference.seat != null) {
                            this.seatPreference = pref.flightPreference.seat;
                        }
                    }   
                }
            } 
            
            if('membership'.equals(customerApiFields)) {
                String serializedMembership = JSON.serialize(response.get(customerApiFields));
                serializedMembership = serializedMembership.replace('"number":', '"number_x":');
                JSONParser parserMembership = JSON.createParser(serializedMembership);
                
                while (parserMembership.nextToken() != null) {
                    Membership mem = (Membership) parserMembership.readValueAs(Membership.class);
                    
                    if(mem.flyingBlueMembership != null) {
                        this.flyingBlueNumber = mem.flyingBlueMembership.number_x;
                        this.tierLevel = mem.flyingBlueMembership.level;
                        this.levelMiles = mem.flyingBlueMembership.levelMilesBalance;
                        this.awardMiles = mem.flyingBlueMembership.awardMilesBalance;
                        
                        if(mem.flyingBlueMembership.levelStartDate != null) {
                            this.levelStartDate = mem.flyingBlueMembership.levelStartDate.format();
                        }
                        
                        if(mem.flyingBlueMembership.awardMilesExpiryDate != null) {
                            this.awardMilesExp = mem.flyingBlueMembership.awardMilesExpiryDate.format();
                        }
                    }
                }
            }
            
            if('individual'.equals(customerApiFields)) {
                String serialized = JSON.serialize(response.get(customerApiFields));
                serialized = serialized.replace('"number":', '"number_x":');
                JSONParser parser = JSON.createParser(serialized);
                
                while (parser.nextToken() != null) {
                    Individual indiv = (Individual) parser.readValueAs(Individual.class);
                    this.title = indiv.title;
                    this.fullName = indiv.firstName + ' ' + indiv.familyName;
                    this.flyingBlueEMail = indiv.emailAccount.address;
                    
                    if(indiv.dateOfBirth != null) {
                        this.birthDate = indiv.dateOfBirth.format();
                    }
                    
                    if(indiv.phoneNumbers != null) {
                        for(PhoneNumbers pn: indiv.phoneNumbers) {
                            if(pn.number_x != null) {
                                this.mobilePhone = pn.countryPrefix + pn.number_x;
                            }
                        }
                    }
                    
                    if(indiv.postalAddresses != null) {
                        for(PostalAddresses indivPostalAddresses: indiv.postalAddresses) {
                            if('Private'.equals(indivPostalAddresses.usageType)) {
                                if(indivPostalAddresses != null || indivPostalAddresses.streetHousenumber != null) {
                                    this.fullAddressHouseNumber = indivPostalAddresses.streetHousenumber;
                                }
                                if(indivPostalAddresses != null || indivPostalAddresses.postalCode != null) {   
                                    this.fullAddressPostalCode = indivPostalAddresses.postalCode;
                                }   
                                if(indivPostalAddresses != null || indivPostalAddresses.city != null) {
                                    this.fullAddressCity = indivPostalAddresses.city;
                                }
                                if(indivPostalAddresses != null || indivPostalAddresses.country != null) {
                                    this.fullAddressCountry = indivPostalAddresses.country;
                                }
                            }
                        }
                    }
                    
                    if(indiv.travelDocuments != null) {
                        if(indiv.travelDocuments.get(0).travelDocument != null || indiv.travelDocuments.get(0).travelDocument.nationality != null) {
                            this.nationality = indiv.travelDocuments.get(0).travelDocument.nationality;
                        }
                    }
                }
            } 
        }
    }

    // Get the Customer API Id
    public String getCustomerId() {
        return customerId;
    }

    // Get the Customer API Social Handle   
    public String getSocialHandle() {
        return socialHandle;
    }

    // Get the Customer API Social Network
    public String getSocialNetwork() {
        return socialNetwork;
    }

    public String getFlyingBlueNumber() {
        return flyingBlueNumber;
    }
    
    // Individual
    public class Individual {
        public String title;
        public String firstName;
        public String familyName;
        public EmailAccount emailAccount;
        public Date dateOfBirth;
        public PhoneNumbers[] phoneNumbers;
        public PostalAddresses[] postalAddresses;
        public TravelDocuments[] travelDocuments;
        
        public Individual(String title, String firstName, String familyName) {
            this.title = title;
            this.firstName = firstName;
            this.familyName = familyName;
            this.phoneNumbers = new List<PhoneNumbers>();
            this.postalAddresses = new List<PostalAddresses>();
            this.travelDocuments = new List<TravelDocuments>();
        }
    }
    
    public class EmailAccount {
        public String address;
        public EmailAccount(String address) {
            this.address = address;         
        }
    }
    
    public class PhoneNumbers {
        public String number_x;
        public String countryPrefix;
        public String phoneType;
        
        public PhoneNumbers(String number_x, String countryPrefix, String phoneType) {
            this.number_x = number_x;
            this.countryPrefix = countryPrefix;
            this.phoneType = phoneType; 
        }
    }
    
    public class TravelDocuments {
        public TravelDocument travelDocument;
        
        public TravelDocuments() {}
    }
    
    public class TravelDocument {
        public String nationality;
        public TravelDocument(String nationality) {
            this.nationality = nationality;
        }
    }
    
    public class PostalAddresses {
    	// Removed in Customer API 2.0
        // public PostalAddress postalAddress;
        public String usageType;
        public String streetHousenumber;
        public String postalCode;
        public String city;
        public String country;
        
        public PostalAddresses(String usageType, String streetHousenumber, String postalCode, String city, String country) {
        	this.usageType = usageType;
        	this.streetHousenumber = streetHousenumber;
        	this.postalCode = postalCode;
        	this.city = city;
        	this.country = country;
        }
    }
    
    // Preference
    public class Preference {
        public CommunicationPreference communicationPreference;
        public FlightPreference flightPreference;
        public LoyaltyProgramPreference loyaltyProgramPreference;
        
        public Preference() {}
    }
    
    public class CommunicationPreference {
        public String language;
        
        public CommunicationPreference(String language) {
            this.language = language;
        }
    }
    
    public class FlightPreference {
        public String cabinClass;
        public String seat;
        public Meal meal;
        
        public FlightPreference(String cabinClass, String seat) {
            this.cabinClass = cabinClass;
            this.seat = seat;
        }
    }
    
    public class Meal {
        public String code;
        public Meal(String code) {
            this.code = code;
        }
    }
    
    public class LoyaltyProgramPreference {
        public String frequentFlyerProgramNumber;
        public LoyaltyProgramPreference(String frequentFlyerProgramNumber) {
            this.frequentFlyerProgramNumber = frequentFlyerProgramNumber;
        }
    }
    
    // Membership
    public class Membership {
        public FlyingBlueMembership flyingBlueMembership;
        
        public Membership(){}
    }
    
    public class FlyingBlueMembership {
        public String levelMilesBalance;
        public String awardMilesBalance;
        public Date awardMilesExpiryDate;
        public Date levelStartDate;
        public String level;
        public String number_x;
        /* Patrick Brinksma - 13 Jun 2014: No need for an explicit constructor
        public FlyingBlueMembership(String levelMilesBalance, String level, String number_x) {
            this.levelMilesBalance = levelMilesBalance;
            this.level = level;
            this.number_x = number_x; 
        }*/
        public FlyingBlueMembership(){}
    }
}