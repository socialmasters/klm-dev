@isTest
public with sharing class ComplaintsResponseInfoTest {

	@isTest
	static void testParse() {
		String json = '{'+
		'    \"id\": 1007315474,'+
		'    \"individual\": {'+
		'        \"firstName\": \"GERBRAND\",'+
		'        \"familyName\": \"VANDERLINDE\",'+
		'        \"postalAddresses\": ['+
		'            {'+
		'                \"city\": \"AMERSFOORT\",'+
		'                \"country\": \"NL\"'+
		'            }'+
		'        ]'+
		'    },'+
		'    \"complaints\": ['+
		'        {'+
		'            \"fileID\": \"7354172000\",'+
		'            \"status\": {'+
		'                \"code\": \"C\",'+
		'                \"name\": \"Closed\"'+
		'            },'+
		'            \"trafficLight\": {'+
		'                \"code\": \"R\",'+
		'                \"name\": \"Red – closed\"'+
		'            },'+
		'            \"creationDate\": \"2014-08-24\",'+
		'            \"closureDate\": \"2014-08-26\",'+
		'            \"incidentType\": {'+
		'                \"code\": \"CTECTEC00\",'+
		'                \"name\": \"No compensation :Technical Pb\"'+
		'            },'+
		'            \"domain\": {'+
		'                \"code\": \"C\",'+
		'                \"name\": \"Cancellation\"'+
		'            },'+
		'            \"subDomain\": {'+
		'                \"code\": \"CTEC\",'+
		'                \"name\": \"Technical Problem\"'+
		'            },'+
		'            \"compensation\": {'+
		'                \"paid\": false'+
		'            }'+
		'        },'+
		'        {'+
		'            \"fileID\": \"7354172001\",'+
		'            \"status\": {'+
		'                \"code\": \"C\",'+
		'                \"name\": \"Closed\"'+
		'            },'+
		'            \"trafficLight\": {'+
		'                \"code\": \"R\",'+
		'                \"name\": \"Red – closed\"'+
		'            },'+
		'            \"creationDate\": \"2014-08-24\",'+
		'            \"closureDate\": \"2014-08-26\",'+
		'            \"incidentType\": {'+
		'                \"code\": \"CTECTEC00\",'+
		'                \"name\": \"No compensation :Technical Pb\"'+
		'            },'+
		'            \"domain\": {'+
		'                \"code\": \"C\",'+
		'                \"name\": \"Cancellation\"'+
		'            },'+
		'            \"subDomain\": {'+
		'                \"code\": \"CTEC\",'+
		'                \"name\": \"Technical Problem\"'+
		'            },'+
		'            \"compensation\": {'+
		'                \"paid\": true,'+
		'                \"types\": ['+
		'                    {'+
		'                        \"code\": \"M\",'+
		'                        \"name\": \"Miles\"'+
		'                    },'+
		'                    {'+
		'                        \"code\": \"F\",'+
		'                        \"name\": \"Money\"'+
		'                    }'+
		'                ]'+
		'            },'+
		'            \"marketingFlight\": {'+
		'                \"number_x\": \"KL639\",'+
		'                \"carrier\": {'+
		'                    \"code\": \"KL\",'+
		'                    \"name\": \"KL\"'+
		'                },'+
		'                \"operatingFlight\": {'+
		'                    \"scheduledLocalDeparture\": \"2014-08-20\"'+
		'                }'+
		'            }'+
		'        },'+
		'        {'+
		'            \"fileID\": \"4543620000\",'+
		'            \"status\": {'+
		'                \"code\": \"C\",'+
		'                \"name\": \"Closed\"'+
		'            },'+
		'            \"trafficLight\": {'+
		'                \"code\": \"R\",'+
		'                \"name\": \"Red – closed\"'+
		'            },'+
		'            \"creationDate\": \"2010-08-12\",'+
		'            \"closureDate\": \"2010-08-22\",'+
		'            \"incidentType\": {'+
		'                \"code\": \"DCOMGEN00\",'+
		'                \"name\": \"Delayed flight\"'+
		'            },'+
		'            \"domain\": {'+
		'                \"code\": \"D\",'+
		'                \"name\": \"Delay\"'+
		'            },'+
		'            \"subDomain\": {'+
		'                \"code\": \"DCOM\",'+
		'                \"name\": \"Others Cial gest\"'+
		'            },'+
		'            \"compensation\": {'+
		'                \"paid\": false'+
		'            }'+
		'        },'+
		'        {'+
		'            \"fileID\": \"4543620001\",'+
		'            \"status\": {'+
		'                \"code\": \"C\",'+
		'                \"name\": \"Closed\"'+
		'            },'+
		'            \"trafficLight\": {'+
		'                \"code\": \"R\",'+
		'                \"name\": \"Red – closed\"'+
		'            },'+
		'            \"creationDate\": \"2010-08-12\",'+
		'            \"closureDate\": \"2010-08-15\",'+
		'            \"incidentType\": {'+
		'                \"code\": \"CWEATNU00\",'+
		'                \"name\": \"Volcanic cloud\"'+
		'            },'+
		'            \"domain\": {'+
		'                \"code\": \"C\",'+
		'                \"name\": \"Cancellation\"'+
		'            },'+
		'            \"subDomain\": {'+
		'                \"code\": \"CWEA\",'+
		'                \"name\": \"Weather\"'+
		'            },'+
		'            \"compensation\": {'+
		'                \"paid\": true,'+
		'                \"types\": ['+
		'                    {'+
		'                        \"code\": \"M\",'+
		'                        \"name\": \"Miles\"'+
		'                    },'+
		'                    {'+
		'                        \"code\": \"F\",'+
		'                        \"name\": \"Money\"'+
		'                    }'+
		'                ]'+
		'            },'+
		'            \"marketingFlight\": {'+
		'                \"number_x\": \"KL878\",'+
		'                \"carrier\": {'+
		'                    \"code\": \"KL\",'+
		'                    \"name\": \"KL\"'+
		'                },'+
		'                \"operatingFlight\": {'+
		'                    \"scheduledLocalDeparture\": \"2010-04-15\"'+
		'                }'+
		'            }'+
		'        },'+
		'        {'+
		'            \"fileID\": \"4527254000\",'+
		'            \"status\": {'+
		'                \"code\": \"C\",'+
		'                \"name\": \"Closed\"'+
		'            },'+
		'            \"trafficLight\": {'+
		'                \"code\": \"R\",'+
		'                \"name\": \"Red – closed\"'+
		'            },'+
		'            \"creationDate\": \"2010-08-03\",'+
		'            \"closureDate\": \"2010-08-05\",'+
		'            \"incidentType\": {'+
		'                \"code\": \"CWEATNU00\",'+
		'                \"name\": \"Volcanic cloud\"'+
		'            },'+
		'            \"domain\": {'+
		'                \"code\": \"C\",'+
		'                \"name\": \"Cancellation\"'+
		'            },'+
		'            \"subDomain\": {'+
		'                \"code\": \"CWEA\",'+
		'                \"name\": \"Weather\"'+
		'            },'+
		'            \"compensation\": {'+
		'                \"paid\": false'+
		'            }'+
		'        },'+
		'        {'+
		'            \"fileID\": \"4527254001\",'+
		'            \"status\": {'+
		'                \"code\": \"C\",'+
		'                \"name\": \"Closed\"'+
		'            },'+
		'            \"trafficLight\": {'+
		'                \"code\": \"R\",'+
		'                \"name\": \"Red – closed\"'+
		'            },'+
		'            \"creationDate\": \"2010-08-03\",'+
		'            \"closureDate\": \"2010-08-05\",'+
		'            \"incidentType\": {'+
		'                \"code\": \"CWEATNU00\",'+
		'                \"name\": \"Volcanic cloud\"'+
		'            },'+
		'            \"domain\": {'+
		'                \"code\": \"C\",'+
		'                \"name\": \"Cancellation\"'+
		'            },'+
		'            \"subDomain\": {'+
		'                \"code\": \"CWEA\",'+
		'                \"name\": \"Weather\"'+
		'            },'+
		'            \"compensation\": {'+
		'                \"paid\": true,'+
		'                \"types\": ['+
		'                    {'+
		'                        \"code\": \"M\",'+
		'                        \"name\": \"Miles\"'+
		'                    },'+
		'                    {'+
		'                        \"code\": \"F\",'+
		'                        \"name\": \"Money\"'+
		'                    }'+
		'                ]'+
		'            },'+
		'            \"marketingFlight\": {'+
		'                \"number_x\": \"KL878\",'+
		'                \"carrier\": {'+
		'                    \"code\": \"KL\",'+
		'                    \"name\": \"KL\"'+
		'                },'+
		'                \"operatingFlight\": {'+
		'                    \"scheduledLocalDeparture\": \"2010-04-14\"'+
		'                }'+
		'            }'+
		'        }'+
		'    ]'+
		'}';

		ComplaintsResponseInfo obj = ComplaintsResponseInfo.parse(json);
		System.assert(obj != null);
	}

}