/**
 * @author (s)    : Archana Murthy Wuntakal Laxman, David van 't Hooft
 * @requirement id:     
 * @description   : Test class to test the Class to delete transscripts 
 */ 	
@isTest 
private class DeleteWebChatTranscriptsTest {

	static testmethod void testDelete() {
       	// Create some test LiveChatTranscript items to be deleted
       	//   by the batch job.       	       	
		testLoadData2();

        //Create custom settings
		Integer lastNDays = 90;
		WebChatBatchDeleteTranscripts__c batchDeleteTranscripts = new WebChatBatchDeleteTranscripts__c();
		batchDeleteTranscripts.Name = 'Delete Transcripts Older Than';
		batchDeleteTranscripts.LastNDays__c = lastNDays;
		insert batchDeleteTranscripts;
		
		//check query
    	String query = 'select id from LiveChatTranscript where CreatedDate != LAST_N_DAYS:' + lastNDays;
        List<LiveChatTranscript> lctList = Database.query('select id from LiveChatTranscript where CreatedDate != LAST_N_DAYS:' + lastNDays +' LIMIT 100');
        System.assert(lctList.size()==10);
		
       	Test.startTest();
       	//DeleteWebChatTranscripts c = new DeleteWebChatTranscripts();
       	//Database.executeBatch(c);
       	DeleteWebChatTranscriptsScheduler m = new DeleteWebChatTranscriptsScheduler();
		m.execute(null);       
       	Test.stopTest();

        List<Schema.FieldSetMember> fields = Schema.SObjectType.LiveChatTranscript.fieldSets.getMap().get('LastNDaysFields').getFields();
        Map<String, Schema.SObjectField> fieldsMap = Schema.SObjectType.LiveChatTranscript.fields.getMap();
        String queryString = 'select id';
        String fieldName;
        for(Schema.FieldSetMember fld :fields) {
         queryString += ', ' + fld.getFieldPath();
        }
        queryString += ' FROM LiveChatTranscript  limit 100';
        for(SObject sobj: Database.query(queryString))
        {
            for(Schema.FieldSetMember fld :Schema.SObjectType.LiveChatTranscript.fieldSets.getMap().get('LastNDaysFields').getFields()) 
            {
            	fieldName = fld.getFieldPath();            	
               	if(fieldsMap.get(fieldName).getDescribe().isUpdateable()) { 
                   System.assert(sobj.get(fieldName)==null);
               	}
            }
        }
        //------end of test only relevant if transcripts are updated and not hard deleted-----  
    }

	static testmethod void testNonDelete() {
       	// Create some test LiveChatTranscript items to be deleted
       	//   by the batch job.       	       	
		testLoadData();

        //Create custom settings
		Integer lastNDays = 90;
		WebChatBatchDeleteTranscripts__c batchDeleteTranscripts = new WebChatBatchDeleteTranscripts__c();
		batchDeleteTranscripts.Name = 'Delete Transcripts Older Than';
		batchDeleteTranscripts.LastNDays__c = lastNDays;
		insert batchDeleteTranscripts;
		
		//check query
    	String query = 'select id from LiveChatTranscript where CreatedDate != LAST_N_DAYS:' + lastNDays;
        List<LiveChatTranscript> lctList = Database.query('select id from LiveChatTranscript where CreatedDate != LAST_N_DAYS:' + lastNDays +' LIMIT 100');
        System.assert(lctList.size()==0);
		
       	Test.startTest();
       	//DeleteWebChatTranscripts c = new DeleteWebChatTranscripts();
       	//Database.executeBatch(c);
       	DeleteWebChatTranscriptsScheduler m = new DeleteWebChatTranscriptsScheduler();
		m.execute(null);    
       	Test.stopTest();

        List<Schema.FieldSetMember> fields = Schema.SObjectType.LiveChatTranscript.fieldSets.getMap().get('LastNDaysFields').getFields();
        Map<String, Schema.SObjectField> fieldsMap = Schema.SObjectType.LiveChatTranscript.fields.getMap();
        String queryString = 'select id';
        String fieldName;
        for(Schema.FieldSetMember fld :fields) {
         queryString += ', ' + fld.getFieldPath();
        }
        queryString += ' FROM LiveChatTranscript  limit 100';
        for(SObject sobj: Database.query(queryString))
        {
            for(Schema.FieldSetMember fld :Schema.SObjectType.LiveChatTranscript.fieldSets.getMap().get('LastNDaysFields').getFields()) 
            {
            	fieldName = fld.getFieldPath();            	
               	if((fieldName=='Body' || fieldName=='SupervisorTranscriptBody') && fieldsMap.get(fieldName).getDescribe().isUpdateable()) { 
                   System.assert(sobj.get(fieldName)!=null);
               	}
            }
        }
        //------end of test only relevant if transcripts are updated and not hard deleted-----  
    }

   	//Create transcripts in the past! Can only be done by Test.loaddata or json trick: https://help.salesforce.com/apex/HTViewSolution?id=000181873&language=en_US 
	static void testLoadData(){ 
       	LiveChatVisitor visitor = new LiveChatVisitor();
       	insert visitor;
       	LiveChatTranscript [] webChatTranscriptList= new List<LiveChatTranscript>();
       	LiveChatTranscript webChatTranscript;
       	for (Integer i=0;i<10;i++) {
           	webChatTranscript = new LiveChatTranscript (
            //------start of test only relevant if tranacripts are updated and not hard deleted-----
            Body = 'body ' + i,
            SupervisorTranscriptBody  = 'supervisor transcipt body' + i,     
            //------end of test only relevant if tranacripts are updated and not hard deelted-----  
            LiveChatVisitorId= visitor.Id        
              );
           	webChatTranscriptList.add(webChatTranscript );
       	}
       	insert webChatTranscriptList;
		//Check existance data
        List<LiveChatTranscript> lctList = [SELECT Id, CreatedDate FROM LiveChatTranscript LIMIT 100];
	    System.assert(lctList.size()==10);
	}
	
	
   	//Create transcripts in the past! Can only be done by Test.loaddata or json trick: https://help.salesforce.com/apex/HTViewSolution?id=000181873&language=en_US 
	static void testLoadData2(){ 
		List<sObject> lcv = Test.loadData(LiveChatVisitor.sObjectType,'testLiveChatVisitors');
		List<sObject> ls = Test.loadData(LiveChatTranscript.sObjectType,'testLiveTranscripts');
				
		LiveChatTranscript lct;
		LiveChatTranscript [] webChatTranscriptListUpdate= new List<LiveChatTranscript>();
		Integer cnt = 0;
		for (sObject lctTmp: ls) {
			lct = (LiveChatTranscript)lctTmp;
			//lct.LiveChatVisitorId = visitorId;
           	webChatTranscriptListUpdate.add((LiveChatTranscript)lctTmp);
		}
		update webChatTranscriptListUpdate;
		system.debug('*****'+webChatTranscriptListUpdate[0].CreatedDate);
	}
}