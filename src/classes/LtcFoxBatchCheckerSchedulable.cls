/**
 * @author (s)  :	Mees Witteman
 * @description   : Schedulable implementation that will check the results of the FoxService batch.
 *                  Assumumption: the night batch runs before this check program
 */
public class LtcFoxBatchCheckerSchedulable implements Schedulable {
	
	public LtcFoxBatchCheckerSchedulable() {
	
	}
 	
 	public void execute(SchedulableContext scMain) {
        execute();
    }
 	
 	 public integer execute() {
 	 	integer result = 0;
        result += checkFlightsDayPlusTwo();
        result += checkActualsForDay(-1);
        result += checkActualsForDay(-2);
        return result;
    }
    
    public integer checkFlightsDayPlusTwo() {
    	Date trDate = Date.toDay().addDays(2);
    	List<Flight__c> flights = [
			select id, Scheduled_Departure_date__c 
			from Flight__c where Scheduled_Departure_date__c =: trDate
		];
    	
    	if (flights.size() < 20) {
	   		sendMail('there are only ' + flights.size() + ' flights of day + 2 (' + trDate + ')');
    	}
    	return 1;
    }
    
    public integer checkActualsForDay(Integer dayFromNow) {
    	Date trDate = Date.toDay().addDays(dayFromNow);
 		List<Flight__c> flights = [
            select 
                f.Scheduled_Departure_Date__c,
        		f.flight_number__c,
                (select 
                    Leg__c.actualArrivalDate__c
                    from f.Legs__r
                )
            from Flight__c f
            where  f.Scheduled_Departure_Date__c =: trDate
        ];

        integer missingActuals = 0;
        
        String missingActualsFLightnrs = '';
        for (Flight__c flight : flights) {
			if (flight.Legs__r.get(0).actualArrivalDate__c == null) {
				missingActuals = missingActuals + 1;
				missingActualsFLightnrs += flight.flight_number__c + '\n';
			}
        }
    	
    	if (missingActuals > 0) {
	   		sendMail('there are ' + missingActuals + ' flights on day ' + trDate + ' with missing actual arrival date / time from fox \n\n' + missingActualsFLightnrs);
    	}
    	return 2;
    }
    

    
    public void sendMail(String tekst) {
    		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			mail.setSenderDisplayName('KL Fox Data Protector Service');
			mail.setUseSignature(false);
			mail.setBccSender(false);
			mail.setPlainTextBody(tekst);
			mail.setSubject('missing flight data: ' + (tekst.length() > 89 ? tekst.substring(0, 90) : tekst));
			mail.setToAddresses(getMailAddresses());
	
			try {
				Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
			} catch (Exception e) {
				System.debug(LoggingLevel.ERROR, 'send mail failed msg=' + e.getMessage());
			}
    }
    
	public List<String> getMailAddresses() {
		List<String> mailList = new List<String>();
		List<String> mailAddresses = new List<String>();
		 
		Group g = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE name = 'LTC_FAM'];
		for (GroupMember gm : g.groupMembers) {
			mailList.add(gm.userOrGroupId);
		}
		
		User[] usr = [SELECT email FROM user WHERE id IN :mailList];
		for (User u : usr) {
			mailAddresses.add(u.email);
		}
		return mailAddresses;
	}
}