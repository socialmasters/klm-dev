/********************************************************************** 
 Name:  AFKLM_SCS_FlyingBlueDetailsExtensionTest
 Task:    N/A
 Runs on: AFKLM_SCS_FlyingBlueDetailsExtension
====================================================== 
Purpose: 
    This class contains unit tests for validating the Flying Blue details based on Mock callout. 
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Ivan Botta      	22/05/2014      Initial Development
    1.1		Stevano Cheung		02/06/2014		Modification using Mock callout
***********************************************************************/
@isTest(SeeAllData=true)
private class AFKLM_SCS_FlyingBlueDetailsExtensionTest {

    static testMethod void flyingBlueDetailsExtensionAccountTest() {
        Account acc = new Account(LastName='Test Account',Flying_Blue_Number__c='7777777777', PersonEmail='test@email.com', sf4twitter__Fcbk_Username__pc='Test User', sf4twitter__Twitter_Username__pc='Test User');
        insert acc;
       
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(acc);
        AFKLM_SCS_FlyingBlueDetailsExtension flyBlueDetails = new AFKLM_SCS_FlyingBlueDetailsExtension(stdController);
        //System.assertEquals(flyBlueDetails.eMail, acc.PersonEmail);
        //System.assertEquals(flyBlueDetails.getSocialNetwork(), cs.Origin);
        System.assertEquals(flyBlueDetails.getFlyingBlueNumber(), acc.Flying_Blue_Number__c);
        
        // Return the AFKLM_SCS_HttpCalloutMock CustomerId
        System.assertEquals(flyBlueDetails.getCustomerId(), '1002149355');
    }

    static testMethod void flyingBlueDetailsExtensionTest() {
        Account acc = new Account(LastName='Test Account',Flying_Blue_Number__c='7777777777', PersonEmail='test@email.com', sf4twitter__Fcbk_Username__pc='Test User', sf4twitter__Twitter_Username__pc='Test User');
        insert acc;
        Case cs = new Case(Status='New', AccountId=acc.id, Origin='Facebook');
        insert cs;
        
        ApexPages.Standardcontroller stdController = new ApexPages.Standardcontroller(cs);
        AFKLM_SCS_FlyingBlueDetailsExtension flyBlueDetails = new AFKLM_SCS_FlyingBlueDetailsExtension(stdController);
        //System.assertEquals(flyBlueDetails.eMail, acc.PersonEmail);
        //System.assertEquals(flyBlueDetails.getSocialNetwork(), cs.Origin);
        System.assertEquals(flyBlueDetails.getFlyingBlueNumber(), acc.Flying_Blue_Number__c);
        
        // Return the AFKLM_SCS_HttpCalloutMock CustomerId
        System.assertEquals(flyBlueDetails.getCustomerId(), '1002149355');
        
        // Compare the AFKLM_SCS_HttpCalloutMock with the AFKLM_SCS_FlyingBlueDetailsExtension Inner class data 
		AFKLM_SCS_FlyingBlueDetailsExtension.Individual indiv = new AFKLM_SCS_FlyingBlueDetailsExtension.Individual('MR', 'JOHN', 'DOE');
        System.assertEquals(flyBlueDetails.title, indiv.title);
        System.assertEquals(flyBlueDetails.fullName, indiv.firstName + ' ' + indiv.familyName);

        AFKLM_SCS_FlyingBlueDetailsExtension.EmailAccount eMail = new AFKLM_SCS_FlyingBlueDetailsExtension.EmailAccount('john.doe@gmail.com');
        System.assertEquals(flyBlueDetails.flyingBlueEMail, eMail.address);
        
        AFKLM_SCS_FlyingBlueDetailsExtension.PhoneNumbers phone = new AFKLM_SCS_FlyingBlueDetailsExtension.PhoneNumbers('0612345678', '+42', 'Mobile');
        System.assertEquals(flyBlueDetails.mobilePhone, phone.countryPrefix + phone.number_x);
        System.assertEquals('Mobile', phone.phoneType);
        
        AFKLM_SCS_FlyingBlueDetailsExtension.PostalAddresses postalAddresses = 
        	new AFKLM_SCS_FlyingBlueDetailsExtension.PostalAddresses('Private', 'JOHNDOESTREET 43', '1058 ZZ', 'AMSTERDAM', 'NL');
        System.assertEquals('Private', postalAddresses.usageType);
        System.assertEquals(flyBlueDetails.fullAddressHouseNumber, postalAddresses.streetHousenumber);
        System.assertEquals(flyBlueDetails.fullAddressPostalCode, postalAddresses.postalCode);
        System.assertEquals(flyBlueDetails.fullAddressCity, postalAddresses.city);
        System.assertEquals(flyBlueDetails.fullAddressCountry, postalAddresses.country);
        
        AFKLM_SCS_FlyingBlueDetailsExtension.TravelDocument travelDoc = new AFKLM_SCS_FlyingBlueDetailsExtension.TravelDocument('GR');
        System.assertEquals(flyBlueDetails.nationality, travelDoc.nationality);
        
        AFKLM_SCS_FlyingBlueDetailsExtension.CommunicationPreference commPref = new AFKLM_SCS_FlyingBlueDetailsExtension.CommunicationPreference('GR');
        System.assertEquals(flyBlueDetails.language, commPref.language);
        
        AFKLM_SCS_FlyingBlueDetailsExtension.FlightPreference flightPref = new AFKLM_SCS_FlyingBlueDetailsExtension.FlightPreference('1', 'Window');
        System.assertEquals('1', flightPref.cabinClass);
        System.assertEquals('Window', flightPref.seat);
        
        AFKLM_SCS_FlyingBlueDetailsExtension.Meal meal = new AFKLM_SCS_FlyingBlueDetailsExtension.Meal('MOML');
        System.assertEquals('MOML', meal.code);
        
        AFKLM_SCS_FlyingBlueDetailsExtension.FlyingBlueMembership membership = new AFKLM_SCS_FlyingBlueDetailsExtension.FlyingBlueMembership();
        //System.assertEquals(flyBlueDetails.tierLevel, membership.level);
        //System.assertEquals(flyBlueDetails.levelMiles, membership.levelMilesBalance);
        //System.assertEquals('7777777777', membership.number_x);
    }
}