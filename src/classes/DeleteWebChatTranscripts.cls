/**
 * @author (s)    : Archana Murthy Wuntakal Laxman, David van 't Hooft
 * @requirement id:     
 * @description   : Batch Class to delete transscripts. For this fieldSets 'LastNDaysFields' needs to be availlable! 
 */ 	
global class DeleteWebChatTranscripts implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
        WebChatBatchDeleteTranscripts__c batchDeleteTranscripts= WebChatBatchDeleteTranscripts__c.getValues('Delete Transcripts Older Than');
        Integer lastNDays = Integer.valueOf(batchDeleteTranscripts.LastNDays__c);
    	String query = 'select id from LiveChatTranscript where CreatedDate != LAST_N_DAYS:' + lastNDays;
        return Database.getQueryLocator(query);
    }
 
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        /*delete scope; Database.emptyRecycleBin(scope);*/
        String fieldName;
        List<Schema.FieldSetMember> fields = Schema.SObjectType.LiveChatTranscript.fieldSets.getMap().get('LastNDaysFields').getFields();
        Map<String, Schema.SObjectField> fieldsMap = Schema.SObjectType.LiveChatTranscript.fields.getMap();
        List<Sobject> chatTranscriptsToUpdate = new List<Sobject>();
        for(sObject sobj : scope){
        	//LiveChatTranscript transcript = (LiveChatTranscript)sobj;
           	for(Schema.FieldSetMember field: fields) {
            	//verify that the field is not read only and is editable
               	fieldName = field.getFieldPath();
               	if(fieldsMap.get(fieldName).getDescribe().isUpdateable()) { 
               		sobj.put(fieldName, null);// TODO verify if emptying the field value works for fields of all data types
           		} else {
                   System.debug('Could not overwrite the field: '+ fieldName + '. Please verify if it is editable');
               	}
           	}
           	chatTranscriptsToUpdate.add(sobj);
		} 
        if(!chatTranscriptsToUpdate.isEmpty()){
            update chatTranscriptsToUpdate;
        }
    }

    global void finish(Database.BatchableContext BC) {
    }
}