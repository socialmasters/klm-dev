/**                     
 *  Model class to contain FlightStatistics Post objects
 */ 
global class LtcRatingFlightStatisticsPostModel {
	
	global class Flight {
        public String flightNumber;
        public String travelDate { get; set; }
    
        public Flight(String flightNumber, String travelDate) {
            this.flightNumber = flightNumber;
            this.travelDate = travelDate;
        }
        
        public override String toString() { 
            return '{' +
            	'flightNumber: ' + flightNumber + ', ' +
            	'travelDate: ' + travelDate + 
        	'}';
        }
    }
}