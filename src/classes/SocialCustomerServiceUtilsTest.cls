/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SocialCustomerServiceUtilsTest {

    static testMethod void userSetSkipValidationTest() {
    	Profile p = [select id from profile where name='Standard User'];
        User u = new User(alias = 'standt', email='userSetSkipValidation@klm.com', 
            emailencodingkey='UTF-8', lastname='userSetSkipValidation', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey='America/Los_Angeles', username='userSetSkipValidation@klm.com',
            SkipValidation__c = false);
        insert u;
        
        test.startTest();
        System.runAs(u) {
           // The following code runs as user 'u' 
           System.debug('Current User: ' + UserInfo.getUserName());
           System.debug('Current Profile: ' + UserInfo.getProfileId());
           
           // Run some code that checks record sharing
           SocialCustomerServiceUtils.userSetSkipValidation(true);
        }
        test.stopTest();
        
        System.assert(true, u.SkipValidation__c);
    }
}