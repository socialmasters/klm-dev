/**
 * @author (s)    :	m.lapere@gen25.com
 * @description   : Test class for LtcFoxServiceBatch
 *
 * @log: 	2015/04/13: version 1.0
 * Simple test class to expand test coverage of LtcFoxServiceBatch. No asserts
 */
@isTest(SeeAllData = true)
public with sharing class LtcFoxServiceBatchTest {
	public static testMethod void testLtcFoxServiceBatch() {
		LtcFoxServiceBatch fsb = new LtcFoxServiceBatch();
		System.assertEquals(0, fsb.preDays);
		fsb.initParams('1');
		System.assertEquals(1, fsb.preDays);
		fsb.actualStart(null);
		SObject[] scope = Database.query('SELECT Flight_Number__c FROM Monitor_Flight__c LIMIT 5');
		fsb.actualExecute(null, scope);
		fsb.actualFinish(null);
	}
}