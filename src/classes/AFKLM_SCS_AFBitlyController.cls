/********************************************************************** 
 Name:  AFKLM_SCS_AFBitlyController
 Task:    N/A
 Runs on: N/A
====================================================== 
Purpose: 
    The class is used for AirFrance copied URL information by agents.
    Shortened by integrating with Bitly.com.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung      10/03/2015      Initial Development
***********************************************************************/
global class AFKLM_SCS_AFBitlyController {
    public String mode;
    public String sUrl { get;set; }
    public String SelectedCheckboxTag { get;set; }
    private AFKLM_SCS_Bitly__c afBitly = AFKLM_SCS_Bitly__c.getValues('AFBitly');
    
    private Case cs;
    public AFKLM_SCS_AFBitlyController(){}

    public AFKLM_SCS_AFBitlyController(ApexPages.StandardController stdController) {
        // Works fine on old way of working on Case     
        try {
            this.cs = (Case)stdController.getRecord();
            this.myCaseId = [SELECT Id, Origin FROM Case WHERE Id =: cs.Id LIMIT 1].Origin; // TODO: Will fail on social console 'List has no rows for assignment to SObject' try / catch needed
        } catch(Exception e) {
            System.debug('**** Exception: '+e);
        }
    }
    
    // Returns a null as String for the tagging      
    public String myCaseId {
        get {
            if(myCaseId == null || myCaseId.trim().equals('')) {
                return 'null';
            } 
            return myCaseId;
        } 
        set;
    }

    /**
    * Get bitly custom settings which mode to execute. Build the Webservice request.
    * Invoke the webservice call.
    *
    * @return the readXMLResponse
    */
    public String getbitly() {
        // Set is as parameter in the Custom Settings
        mode = afBitly.SCS_Bitly_Mode__c;
        String shorten;
        XmlStreamReader reader;
        HttpResponse res;
    
        //First, build the http request
        Http h = new Http();
        HttpRequest req = buildWebServiceRequest(sURL);
       
        //Second, invoke web service call 
        if (mode=='live') {
            res = invokeWebService(h, req);    
            reader = res.getXmlStreamReader();     
        }
        else {
            String str = '<bitly><results shortUrl="http://bit.ly/QqHEm">Setup basic string message</results></bitly>';
            reader = new XmlStreamReader(str);
        }
        return readXMLResponse(reader,'shortUrl');
    }
    
    /**
    * Build the Webservice request.
    *
    * @return HTTPRequest
    */
    public HttpRequest buildWebServiceRequest(String purl) {
        String endpoint;
        HttpRequest req = new HttpRequest();
        endpoint = buildTagBitlyInfo(purl);
        req.setEndpoint(endpoint); 
        req.setMethod('GET');
        return req;
    }

    /**
    * Invoke the webservice call.
    *
    * @return HttpResponse
    */
    public static HttpResponse invokeWebService(Http h, HttpRequest req) {
         //Invoke Web Service
         HttpResponse res = h.send(req);
         return res;
    }

    /**
    * Read through the XML
    *
    * @return String
    */
    public static String readXMLResponse(XmlStreamReader reader, String sxmltag) {
        string retValue;
        // Read through the XML
        // system.debug('**** Reader: '+reader.toString());

        while(reader.hasNext()) {
            if (reader.getEventType() == XmlTag.START_ELEMENT) {
               // System.debug('**** reader.getLocalName(): '+reader.getLocalName());
                              
               if (reader.getLocalName() == sxmltag) {
                  reader.next();
                  // System.debug('**** reader.getEventType(): '+reader.getEventType());
                      if (reader.getEventType() == XmlTag.characters) {
                           retValue = reader.getText();    
                      }
                } else {
                  reader.next();
                  if (reader.getEventType() == XmlTag.characters) {
                      System.debug('**** Info about the error message or other: '+reader.getText());    
                  }
                }
            }
            reader.next();
        }
        return retValue;
    }

    public Pagereference getTagSelected() {
        return null;
    }

    /**
    * Build the bitly endpoint information for integration.
    *
    * @return String endpoint for the bitly
    */
    private String buildTagBitlyInfo(String urlInfo) {
        String newEndpoint = '';
        
        if(urlInfo == null || urlInfo.trim().equals('')) {
            newEndpoint = afBitly.SCS_Bitly_Endpoint__c + '?' 
                            + 'version=' + afBitly.SCS_Bitly_Version__c + '&'
                            + 'format=' + afBitly.SCS_Bitly_Format__c + '&'
                            + 'history='+ afBitly.SCS_Bitly_History__c + '&'
                            + 'longUrl=' + urlInfo + '&'
                            + 'login=' + afBitly.SCS_Bitly_Login__c + '&'
                            + 'apiKey=' + afBitly.SCS_Bitly_ApiKey__c;
        } else {
                newEndpoint = afBitly.SCS_Bitly_Endpoint__c + '?' 
                + 'version=' + afBitly.SCS_Bitly_Version__c + '&'
                + 'format=' + afBitly.SCS_Bitly_Format__c + '&'
                + 'history='+ afBitly.SCS_Bitly_History__c + '&'
                + 'longUrl=' + EncodingUtil.URLENCODE(urlInfo,'UTF-8') + '&'
                + 'login=' + afBitly.SCS_Bitly_Login__c + '&'
                + 'apiKey=' + afBitly.SCS_Bitly_ApiKey__c;
        }
        return newEndpoint;
    }
    
    /**
    * Gets the caseId from the Console when Case is openend on Event.
    * Uses the Origin, e.g. Twitter, Facebook to set the 4th tag parameter
    *
    * @return PageReference
    */ 
    public PageReference caseIdConsole(){
        try {
            if(!myCaseId.equals('null')) {
                this.myCaseId = [SELECT Origin FROM Case WHERE Id = :myCaseId].Origin;
            }
        } catch(Exception e) {
            System.debug('**** Exception: '+e);
        }
        
        return null;
    }
    
    public PageReference ClearBitly(){
    	sURL = '';
    	
    	return null;
    }
}