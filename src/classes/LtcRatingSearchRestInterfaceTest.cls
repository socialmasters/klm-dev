/**
 * Tests for LtcRatingSearchRestInterface
 */
@isTest
private class LtcRatingSearchRestInterfaceTest {

    static testMethod void ratingSearchRestInterfaceNoRatings() {
    	LtcRatingSearchPostModel searchModel = new LtcRatingSearchPostModel(
    		'KL1234', '2014-01-01', '2014-12-31', new List<String>(), 15, 0);
    	String response = LtcRatingSearchRestInterface.getRatings(searchModel);
    	String expected = '{ "flight": {"flightNumber": "KL1234", "startDate": "2014-01-01", "endDate": "2014-12-31", "total": 0, "ratingStatistics": {"numberOfRatings": "0", "averageRating": "", "ratingCategory": [{"level": "1", "count": "0"}, {"level": "2", "count": "0"}, {"level": "3", "count": "0"}, {"level": "4", "count": "0"}, {"level": "5", "count": "0"}]}, "ratings": []}}';
    	System.assertEquals(expected, response);
    }
    
    static testMethod void ratingSearchRestInterfaceFailure() {
    	LtcRatingSearchPostModel searchModel = new LtcRatingSearchPostModel(
    		null, null, null, new List<String>(), 15, 0);
    	String response = LtcRatingSearchRestInterface.getRatings(searchModel);
    	System.assert(response.startsWith('Argument cannot be null'));
    }
}