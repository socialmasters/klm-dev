/**
 * @author (s)    :	David van 't Hooft
 * @description   : MetaDdata parser for one object
 * @log: 	29July2014: version 1.0
 */
public with sharing class MetaDataParser {
	private String sObjectname;
	
    /**
     * Hold and retrieve the field types
     **/
    private Map<String, Schema.SObjectField> fieldMap {
    	get {
        	if (fieldMap == null) {    			
	            Schema.DescribeSObjectResult[] descResult = Schema.describeSObjects(new String[]{this.sObjectname});
	            fieldMap = descResult[0].fields.getMap();
	        }
	        return fieldMap;
    	}
    	set;
    }

    /**
     * Hold and retrieve the field types
     **/
    private Map<String,Schema.DisplayType> fieldTypeMap {
    	get {
        	if (fieldTypeMap == null) {    			
	            fieldTypeMap = new Map<String,Schema.DisplayType>();	            
	            for (Schema.SObjectField ft : this.fieldMap.values()){ // loop through all field tokens (ft)
	                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
	                fieldTypeMap.put(fd.getName().toUpperCase(),fd.getType());
	            }
	        }
	        return fieldTypeMap;
    	}
    	set;
    }
    
    /**
     * Hold and retrieve the listviews
     **/
    private List<MetadataService.ListView> listviews {
    	get {
    		if (listviews == null) {
				listviews = ((MetadataService.CustomObject) createService().readMetadata('CustomObject', 
	                	new String[] { sObjectName }).getRecords()[0]).listViews;
    		}
    		return listviews;
    	}
    	set;
    }
    
	/**
	 * @param sObjectname String name of the sobject to use the parser on
	 **/
	public MetaDataParser(String sObjectname) {
		this.sObjectname = sObjectname;
	}

    /**
     * Read the the selected listview filter parameters and create a where clause of it
     * @param listViewFullName String
     * @return String query filter (where clause)
     **/    
    public String getQueryFilter(String listViewFullName) {
        String timeStamp='';
        String queryFilter = '';
        String operator = '';
        Integer cnt=1;
        Schema.DisplayType fType;
        String value = '';
        String fieldName;
        String filterLogic = '';
        Integer filterCnt;
        String[] valuesTmp;
        Integer valCnt;
        Boolean multi;
        String[] fieldsplit;
        String ownerFilter = '';
        String tmpId = '';
        //if(Test.isRunningTest()) {
        //	System.debug('****************MetadataService.ListView: '+this.listViews);
        //}
        for(MetadataService.ListView listView : this.listViews) {
        	System.debug('***@#@*#*$@*#*#@*listView.filters: '+listview.filters); 
            if (listView.fullName==listViewFullName) {
                if (listView.filters!=null) {
                    System.debug('ListView type:'+listView.type);

                    filterLogic = listView.booleanFilter;   //1 OR 2
                    if (filterLogic==null) {
                        filterLogic = '1';
                    }
                    filterCnt = listView.filters.size();

                    for(Integer x=filterCnt; x>0; x--) {
                        filterLogic = filterLogic.replace(''+x, 'ZZZxxQ' + String.fromCharArray( new List<integer> { 64+x } ) + 'QxxZZZ');
                    }

                    for (MetadataService.ListViewFilter lvf: listView.filters) {  
                        System.debug('ListView type:'+lvf.operation);
                        System.debug('ListView field:'+lvf.field);
                        System.debug('ListView value:'+lvf.value);
                        if(lvf.value==null){
                        	lvf.value = '';
                        }
                                                
                        fieldsplit = lvf.field.split('\\.');
                        System.debug('ListView fieldsplit:'+fieldsplit);
                        if (fieldsplit.size()>1) {
                        	lvf.field = fieldsplit[fieldsplit.size()-1];
                        }
                        System.debug('ListView field2:'+lvf.field);
                        //prepare value depending on the 
                        fType = getFieldType(lvf.field);
                        System.debug('ListView fType:'+fType);
                        /** only needed for cases-> need to test in conflict wth referenced fields */
                        if (fType==null) {
                            fType = getFieldType(lvf.field+'Id');
                            //System.debug('Ftype2:'+fType);
                        }
                        if (fType==null) {
                            fType = getFieldType('is'+lvf.field);
	                        if (fType!=null) {
	                        	lvf.field = 'is'+lvf.field;
		                        System.debug('ListView value > converted to is{fieldname}:'+lvf.value);
	                        }
                        }
                        System.debug('ListView field:'+lvf.field);
											
                        //Parse multiple values                         
                        queryFilter = '(';
                        valCnt = 0;
                        valuesTmp = lvf.value.split(',');
                        System.debug('ValuesTmp '+valuesTmp);
                        for (String val : valuesTmp) {
                            if (valCnt>0) {
                            	if(lvf.operation=='notEqual'){
                            		queryFilter += ' AND ';	
                            	}else{
                                	queryFilter += ' OR ';
                            	}
                            }
                            //Parse Operator
                            if (lvf.operation=='equals') {
                                operator = ' = ';
                                if (fType == Schema.DisplayType.Boolean) {                          
                                    value = String.valueOf('1'==val);   
                                } else if (fType == Schema.DisplayType.Integer) {
                                    value = val;    
                                } else if (fType == Schema.DisplayType.Double) {
                                    value = val;
                                } else if (fType == Schema.DisplayType.Picklist) {
                                	if(val.length()<=2){
                                		value = isPicklist(val, lvf.field);
                                	} else {
                                		value = '\''+val+'\'';
                                	}
                                } else if (fType == Schema.DisplayType.Datetime) {
                                	//if(val.contains(' ')){
                                	//	queryFilter += '(';
                                	//	operator = ' > ';
                                	//	fieldName = lvf.field;
                                	//}
                                    value = dateTimeValuetoString(val,timeStamp, lvf.operation, fieldName);
                                } else {
                                    value = '\''+val+'\'';
                                }
                            } else if(lvf.operation=='notEqual') {
                                operator = ' != ';
                                if (fType == Schema.DisplayType.Boolean) {                          
                                    value = String.valueOf('1'==val);   
                                } else if (fType == Schema.DisplayType.Integer) {
                                    value = val;    
                                } else if (fType == Schema.DisplayType.Double) {
                                    value = val;
                                } else if (fType == Schema.DisplayType.Picklist) {
                                	if(val.length()<=2){
                                		value = isPicklist(val, lvf.field);
                                	} else {
                                		value = '\''+val+'\'';
                                	}
                                } else if (fType == Schema.DisplayType.Datetime) {
                                    //if(val.contains(' ')){
                                    //	queryFilter += '(';
	                                //	operator = ' > ';
	                                //	fieldName = lvf.field;
                                	//}
                                    value = dateTimeValuetoString(val,timeStamp, lvf.operation, fieldName);
                                } else {
                                	value = '\''+val+'\'';
                                }
                            } else if(lvf.operation=='startsWith') {
                                operator = ' LIKE ';
                                value = '\''+val+'%\'';
                            } else if(lvf.operation=='contains') {
                                operator = ' LIKE ';
                                value = '\'%'+val+'%\'';
                            } else if(lvf.operation=='notContain') {
                                //IBO: fix for notContain (NOT Field LIKE 'value')
                                if (fType == Schema.DisplayType.Reference) {
                                    queryFilter += ' NOT ' +lvf.field+'.Name';
                                    operator = ' LIKE ';
                                    value = '\'%'+val+'%\'';
                                    }else{
                                    	queryFilter += ' NOT ' +lvf.field;
                                    	operator = ' LIKE ';
                                    	value = '\'%'+val+'%\'';
                                    }
                                //operator = ' NOT LIKE ';
                                //value = '\'%'+val+'%\'';
                            } else if(lvf.operation=='lessThan') {
                                operator = ' < ';
                                if (fType == Schema.DisplayType.Integer) {
                                    value = val;    
                                } else if (fType == Schema.DisplayType.Double) {
                                    value = val;
                                } else if (fType == Schema.DisplayType.Datetime) {
                                    value = dateTimeValuetoString(val,timeStamp, lvf.operation,fieldName);
                                } else {
                                    value = '\''+val+'\'';
                                }
                            } else if(lvf.operation=='greaterThan') {
                                operator = ' > ';
                                if (fType == Schema.DisplayType.Integer) {
                                    value = val;    
                                } else if (fType == Schema.DisplayType.Double) {
                                    value = val;
                                } else if (fType == Schema.DisplayType.Datetime) {
                                    timeStamp = 'T23:59:59.000Z';
                                	fieldName = lvf.field;
                                    value = dateTimeValuetoString(val,timeStamp, lvf.operation,fieldName);
                                } else {
                                    value = '\''+val+'\'';
                                }
                            } else if(lvf.operation=='lessOrEqual') {
                                operator = ' <= ';
                                if (fType == Schema.DisplayType.Integer) {
                                    value = val;    
                                } else if (fType == Schema.DisplayType.Double) {
                                    value = val;
                                } else if (fType == Schema.DisplayType.Datetime) {
                                	fieldName = lvf.field;
                                    value = dateTimeValuetoString(val,timeStamp, lvf.operation,fieldName);
                                } else {
                                    value = '\''+val+'\'';
                                }
                            } else if(lvf.operation=='greaterOrEqual') {
                                operator = ' >= ';
                                if (fType == Schema.DisplayType.Integer) {
                                    value = val;    
                                } else if (fType == Schema.DisplayType.Double) {
                                    value = val;
                                } else if (fType == Schema.DisplayType.Datetime) {
                                	fieldName = lvf.field;
                                    value = dateTimeValuetoString(val,timeStamp, lvf.operation,fieldName);
                                } else {
                                    value = '\''+val+'\'';
                                }
                            } else if(lvf.operation=='includes') {
                                //only multipicklist
                                operator = ' IN ';
                                value = '(';
                                multi = false;
                                for (String valmp : valuesTmp) {
                                    if (multi) {
                                        value += ' ,';
                                    }
                                    multi = true;
                                    value += '\''+valmp+'\'';
                                }
                                value += ')';
                            } else if(lvf.operation=='excludes') {
                                //only multipicklist
                                operator = ' NOT IN ';
                                value = '(';
                                multi = false;
                                for (String valmp : valuesTmp) {
                                    if (multi) {
                                        value += ' ,';
                                    }
                                    multi = true;
                                    value += '\''+valmp+'\'';
                                }
                                value += ')';
                            } else {
                                operator = '';
                                value = '';
                            }
                            System.debug('ListView type:'+lvf.operation+'  cnt = '+cnt);
                            
                            if (operator!='') {
                                if (fType == Schema.DisplayType.Reference) {
                                	//IBO: fix for notContain (NOT Field LIKE 'value')
                                	if(lvf.operation=='notContain') {
                                		System.debug('************ notContain');
                                	} else if(lvf.field.indexOf('__c', lvf.field.length() - 3)==-1){
                                    	queryFilter += lvf.field+'.Name';
                                	} else {
                                    	queryFilter += lvf.field;
                                	}
                                } else {
                                	//IBO: fix for notContain (NOT Field LIKE 'value')
                                	if(lvf.operation=='notContain'){
                                		System.debug('************** notContain');
                                	}else{
                                    	queryFilter += lvf.field;
                                	}
                                }
                                queryFilter += operator;
                                queryFilter += value;
                            } else {
                                queryFilter += 'Id <> \'\'';
                            }
                            if(lvf.operation=='includes' || lvf.operation=='excludes') {
                                //all split values are in the collecton for includes and excludes!
                                break;
                            }
                            valCnt++;
                        }
                        queryFilter += ')';

                        filterLogic = filterLogic.replace('ZZZxxQ'+String.fromCharArray( new List<integer> { 64+cnt } )+'QxxZZZ', queryFilter);            
                        if(cnt==1) {
                            filterLogic = 'WHERE ' +filterLogic;
                        }
                        cnt++;
                    }
                }
            	if (listView.filterScope!=null && listView.filterScope.equals('Queue')) { 
            		tmpId = getQueueIdByName(listView.queue);
            		if( !(tmpId==null || tmpId=='') ) {           		
	            		ownerFilter = 'OwnerId=\'' + tmpId + '\' ';
		                if (filterLogic=='') {
		                	filterLogic += 'WHERE ';
		                } else {
		                	filterLogic += 'AND ';	                	
		                }
		                filterLogic += ownerFilter;
            		}
            	}
                break;
            }
        }
       
        return filterLogic;
    }
    
    /**
     * Get the id from the queue owner
     **/
    private String getQueueIdByName(String queueName) {
    	String retId = '';
    	List<Group> queueList = [Select g.RelatedId, g.Name, g.Id From Group g where g.Type='Queue' and g.DeveloperName =:queueName];
    	if (queueList == null || queueList.size() == 0) {
        	System.debug('Queue not found!');
    	} else if (queueList.size() > 1) {
        	System.debug('More than one Queue found!');
    	} else {
    		retId = queueList[0].id;
    	}
    	return retId;
    } 
                
    /**
     *
     **/           
    private String dateTimeValuetoString(String val, String timeStamp, String operation, String fieldName){
        String correctDate='';
        String dd,mm,dd2;
        Integer tempInt;
        List<String> yyyy;
        if(!val.contains(' ')){
            correctDate=val;
        }else{
        	if(timeStamp.equals('')){
        		timeStamp = 'T00:00:00.000Z';
       		}
        	if(operation == 'equals' || operation == 'notEqual'){
        		List<String> dateList=val.split('/');
		        mm=dateList[0];
		        dd=dateList[1];
		        tempInt=Integer.valueOf(dd);
		        tempInt=tempInt+1;
		        dd2=String.valueOf(tempInt);
		        
		        if(dd.length()<2){dd='0'+dd;}
		        if(dd2.length()<2){dd2='0'+dd2;}
		        if(mm.length()<2){mm='0'+mm;}
		        
		        yyyy=dateList[2].split(' ');
		        correctDate = yyyy[0]+'-'+mm+'-'+dd+timeStamp+ ' AND '+fieldName+' < '+yyyy[0]+'-'+mm+'-'+dd2+timeStamp+')';
        	}else{
		        List<String> dateList=val.split('/');
		        mm=dateList[0];
		        dd=dateList[1];
		        if(dd.length()<2){dd='0'+dd;}
		        if(mm.length()<2){mm='0'+mm;}
		        yyyy=dateList[2].split(' ');
		        correctDate=yyyy[0]+'-'+mm+'-'+dd+timeStamp;
        	}
        }
        return correctDate;        
    }
    
    /**
     * Get the field type by field name
     * 
     * @param fieldName String field Name to retrieve the type from
     * @return String field Type
     */
    private Schema.DisplayType getFieldType(String fieldName) {
        return fieldTypeMap.get(fieldName.toUpperCase());
    }
    
    /**
     * Retrieve picklist value label
     * @param val string value of the field
     * @param field String name of the field
     */
    private String isPicklist(String val, String field){
    	Integer i = Integer.valueOf(val);    	
		Schema.DescribeFieldResult F = fieldMap.get(field).getDescribe();
		List<Schema.PicklistEntry> P = F.getPicklistValues();
    	return '\''+p[i].getLabel()+'\'';    	
    }

    /**
     * Retrieve picklist value label
     * @param field String name of the field
     */
    public List<Schema.PicklistEntry> getPicklistValues(String field) {
		Schema.DescribeFieldResult F = fieldMap.get(field).getDescribe();
		return F.getPicklistValues();
    }

    /**
     * read the Case custom object to get the listview metadata
     * @param sObjectName String with the object name like 'Case'
     * @return List with the retrieved listviews 
     **/
    public List<MetadataService.ListView> readListViews() {
		return this.listviews;
    }   

    /**
     * Create a metadata service to get access to the metadata
     * @return MetadataService.MetadataPort
     **/    
    private MetadataService.MetadataPort createService() {
    	AFKLM_SCS_IntegrationUser__c custSetting = AFKLM_SCS_IntegrationUser__c.getOrgDefaults(); // Get integration user custom settings
		partnerSoapSforceCom.Soap myPartnerSoap = new partnerSoapSforceCom.Soap(); // Class generated by Partner's API
    	
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        if (Test.isRunningTest()) {        	
        	service.SessionHeader.sessionId = UserInfo.getSessionId();
    	} else {
			partnerSoapSforceCom.LoginResult LoginResult = myPartnerSoap.login(custSetting.Username__c, EncodingUtil.base64Decode(custSetting.Password__c).toString());
        	service.SessionHeader.sessionId = LoginResult.sessionId;
        }
                
        return service;     
    }
}