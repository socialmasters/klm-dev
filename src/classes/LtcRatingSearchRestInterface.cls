/**                     
 * @author (s)      : Mees Witteman
 * @description     : Rest API interface for the rating feedback of a flight
 **/
@RestResource(urlMapping='/Ratings/Search/*')
global class LtcRatingSearchRestInterface {
    
    /**
     * Retrieve the stored ratings for a specific flight and date range
     * 
     * Expected Json payload:
     * "searchModel" : 
     * {"flightNumber": "KL1234",
     *  "startDate": "2014-05-05",
     *  "endDate": "2014-05-05",
     *  "limit": 15,
     *  "offset": 0,
     *  "ratingIds": ["kFzWWY1a3B2TEsycHdOU2hyJTJCRXozbU5uMVNodXJMJTJGN0gzcUdtellPbkdibGZ3RHlaMXpPalF2NUh4OWliaGo",
     *      "bladiebladeeieurSSKDSJDS(SIDSJIJUUIsusdjsisjsosbladiebladeeieurSSKDSJDS(SIDSJIJUUIsusdjsisjsos8s87si",
     *      "VNodXJMJTJGN0gzcUdtellPbkdibGZ3RHlaMXpPalF2NUh4OWliaGoVNodXJMJTJGN0gzcUdtellPbkdibGZ3RHlaMXpPalF2NUs"
     *   ]
     * }
     * ratingId should be an encoded String
     **/
	@HttpPost
    global static String getRatings(LtcRatingSearchPostModel searchModel) {
        String response = '';
        try {
            LtcRating ltcRating = new LtcRating();
            
            LtcRatingGetModel.Flight flight = ltcRating.getRatings(
            		searchModel.flightNumber, searchModel.startDate, searchModel.endDate, 
            		searchModel.ratingIds, searchModel.limitTo, searchModel.offset); 
            response = '{ "flight": ' + flight.toString() + '}';
        } catch (Exception ex) {
            if (LtcUtil.isDevOrg() || Test.isRunningTest()) {
            	response = ex.getMessage() + ' ' + ex.getStackTraceString();
            }
            System.debug(LoggingLevel.ERROR, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
//        RestResponse res = RestContext.response;
//        res.addHeader('Content-Type', 'application/json');
//        res.responseBody = Blob.valueOf(response);
        return response;
    }   
}