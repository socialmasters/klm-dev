/********************************************************************** 
 Name:  AFKLM_SCS_HttpCalloutMock
 Task:    N/A
 Runs on: AFKLM_SCS_CustomerAPIClient, AFKLM_SCS_SocialPostController
====================================================== 
Purpose:
    For code coverage. 
    This class contains Mock Callout JSON data for Flying Blue details and Customer flights. 
    It will not trigger the real Rest API Callout but instead uses this Mock Callout class.
======================================================
History                                                            
-------                                                            
VERSION     AUTHOR              DATE            DETAIL                                 
    1.0     Stevano Cheung     	02/06/2014      Initial Development
***********************************************************************/
@isTest
global class AFKLM_SCS_HttpCalloutMock implements HttpCalloutMock {

	// For Rest API
    global HTTPResponse respond(HTTPRequest req) {
        if('POST'.equals(req.getMethod())) {
        	System.assertEquals('POST', req.getMethod());
        } else if('GET'.equals(req.getMethod())) {
        	System.assertEquals('GET', req.getMethod());
        }
		
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        
        if(req.getEndpoint().contains('flying-blue-number')) {
        	res.setBody('{"id" : 1002149355,"membertype" : "flyingblue"}');
        } else if(req.getEndpoint().contains('/flight-reservations')) {
        	res.setBody('{'+
			  '"reservations" : [ {'+
			    '"identifier" : "7S4NSF",'+
			    '"awardBooking" : false,'+
			    '"dayReturn" : false,'+
			    '"group" : false,'+
			    '"direct" : false,'+
			    '"historical" : true,'+
			    '"flightReservation" : {'+
			      '"itinerary" : {'+
			        '"destinationsCities" : [ {'+
			          '"code" : "MAD",'+
			          '"country" : {'+
			            '"code" : "ES"'+
			          '}'+
			        '} ],'+
			        '"connections" : [ {'+
			          '"identifier" : 1,'+
			          '"originAirport" : {'+
			            '"code" : "AMS",'+
			            '"city" : {'+
			              '"code" : "AMS",'+
			              '"country" : {'+
			                '"code" : "NL"'+
			              '}'+
			            '}'+
			          '},'+
			          '"destinationAirport" : {'+
			            '"code" : "MAD",'+
			            '"city" : {'+
			              '"code" : "MAD",'+
			              '"country" : {'+
			                '"code" : "ES"'+
			              '}'+
			            '}'+
			          '},'+
			          '"segments" : [ {'+
			            '"identifier" : "1",'+
			            '"localDepartureTime" : "2014-05-18T11:40:00",'+
			            '"localArrivalTime" : "2014-05-18T14:15:00",'+
			            '"cabinClass" : "1",'+
			            '"haulType" : "SHORT",'+
			            '"numberOfStops" : 0,'+
			            '"flown" : false,'+
			            '"informational" : false,'+
			            '"open" : false,'+
			            '"award" : false,'+
			            '"arrivalAirport" : {'+
			              '"code" : "MAD",'+
			              '"city" : {'+
			                '"code" : "MAD",'+
			                '"country" : {'+
			                  '"code" : "ES"'+
			                '}'+
			              '}'+
			            '},'+
			            '"departureAirport" : {'+
			              '"code" : "AMS",'+
			              '"city" : {'+
			                '"code" : "AMS",'+
			                '"country" : {'+
			                  '"code" : "NL"'+
			                '}'+
			              '}'+
			            '},'+
			            '"marketingFlight" : {'+
			              '"flightNumber" : "1703",'+
			              '"marketingCarrier" : {'+
			                '"code" : "KL"'+
			              '}'+
			            '},'+
			            '"operatingFlight" : {'+
			              '"operatingCarrier" : {'+
			                '"code" : "KL"'+
			              '}'+
			            '}'+
			          '} ]'+
			        '}, {'+
			          '"identifier" : 2,'+
			          '"originAirport" : {'+
			            '"code" : "MAD",'+
			            '"city" : {'+
			              '"code" : "MAD",'+
			              '"country" : {'+
			                '"code" : "ES"'+
			              '}'+
			            '}'+
			          '},'+
			          '"destinationAirport" : {'+
			            '"code" : "AMS",'+
			            '"city" : {'+
			              '"code" : "AMS",'+
			              '"country" : {'+
			                '"code" : "NL"'+
			              '}'+
			            '}'+
			          '},'+
			          '"segments" : [ {'+
			            '"identifier" : "2",'+
			            '"localDepartureTime" : "2014-05-25T15:10:00",'+
			            '"localArrivalTime" : "2014-05-25T17:50:00",'+
			            '"cabinClass" : "1",'+
			            '"haulType" : "SHORT",'+
			            '"numberOfStops" : 0,'+
			            '"flown" : false,'+
			            '"informational" : false,'+
			            '"open" : false,'+
			            '"award" : false,'+
			            '"arrivalAirport" : {'+
			              '"code" : "AMS",'+
			              '"city" : {'+
			                '"code" : "AMS",'+
			                '"country" : {'+
			                  '"code" : "NL"'+
			                '}'+
			              '}'+
			            '},'+
			            '"departureAirport" : {'+
			              '"code" : "MAD",'+
			              '"city" : {'+
			                '"code" : "MAD",'+
			                '"country" : {'+
			                  '"code" : "ES"'+
			                '}'+
			              '}'+
			            '},'+
			            '"marketingFlight" : {'+
			              '"flightNumber" : "1704",'+
			              '"marketingCarrier" : {'+
			                '"code" : "KL"'+
			              '}'+
			            '},'+
			            '"operatingFlight" : {'+
			              '"operatingCarrier" : {'+
			                '"code" : "KL"'+
			              '}'+
			            '}'+
			          '} ]'+
			        '} ]'+
			      '},'+
			      '"passengers" : [ {'+
			        '"identifier" : 1,'+
			        '"type" : "ADT",'+
			        '"individual" : {'+
			          '"title" : "MR",'+
			          '"firstName" : "OMAR",'+
			          '"familyName" : "OUALIF"'+
			        '},'+
			        '"tickets" : [ {'+
			          '"number" : "0744837444179"'+
			        '} ],'+
			        '"passengerOnSegments" : [ { } ]'+
			      '} ]'+
			    '}'+
			  '}, {'+
			    '"identifier" : "8LV8XD",'+
			    '"awardBooking" : false,'+
			    '"dayReturn" : false,'+
			    '"group" : false,'+
			    '"direct" : false,'+
			    '"historical" : true,'+
			    '"flightReservation" : {'+
			      '"itinerary" : {'+
			        '"destinationsCities" : [ {'+
			          '"code" : "DXB",'+
			          '"country" : {'+
			            '"code" : "AE"'+
			          '}'+
			        '} ],'+
			        '"connections" : [ {'+
			          '"identifier" : 1,'+
			          '"originAirport" : {'+
			            '"code" : "AMS",'+
			            '"city" : {'+
			              '"code" : "AMS",'+
			              '"country" : {'+
			                '"code" : "NL"'+
			              '}'+
			            '}'+
			          '},'+
			          '"destinationAirport" : {'+
			            '"code" : "DXB",'+
			            '"city" : {'+
			              '"code" : "DXB",'+
			              '"country" : {'+
			                '"code" : "AE"'+
			              '}'+
			            '}'+
			          '},'+
			          '"segments" : [ {'+
			            '"identifier" : "1",'+
			            '"localDepartureTime" : "2014-02-23T13:30:00",'+
			            '"localArrivalTime" : "2014-02-23T22:59:00",'+
			            '"cabinClass" : "1",'+
			            '"haulType" : "LONG",'+
			            '"numberOfStops" : 0,'+
			            '"flown" : false,'+
			            '"informational" : false,'+
			            '"open" : false,'+
			            '"award" : false,'+
			            '"arrivalAirport" : {'+
			              '"code" : "DXB",'+
			              '"city" : {'+
			                '"code" : "DXB",'+
			                '"country" : {'+
			                  '"code" : "AE"'+
			                '}'+
			              '}'+
			            '},'+
			            '"departureAirport" : {'+
			              '"code" : "AMS",'+
			              '"city" : {'+
			                '"code" : "AMS",'+
			                '"country" : {'+
			                  '"code" : "NL"'+
			                '}'+
			              '}'+
			            '},'+
			            '"marketingFlight" : {'+
			              '"flightNumber" : "427",'+
			              '"marketingCarrier" : {'+
			                '"code" : "KL"'+
			              '}'+
			            '},'+
			            '"operatingFlight" : {'+
			              '"operatingCarrier" : {'+
			                '"code" : "KL"'+
			              '}'+
			            '}'+
			          '} ]'+
			        '}, {'+
			          '"identifier" : 2,'+
			          '"originAirport" : {'+
			            '"code" : "DXB",'+
			            '"city" : {'+
			              '"code" : "DXB",'+
			              '"country" : {'+
			                '"code" : "AE"'+
			              '}'+
			            '}'+
			          '},'+
			          '"destinationAirport" : {'+
			            '"code" : "AMS",'+
			            '"city" : {'+
			              '"code" : "AMS",'+
			              '"country" : {'+
			                '"code" : "NL"'+
			              '}'+
			            '}'+
			          '},'+
			          '"segments" : [ {'+
			            '"identifier" : "4",'+
			            '"localDepartureTime" : "2014-03-03T00:55:00",'+
			            '"localArrivalTime" : "2014-03-03T05:15:00",'+
			            '"cabinClass" : "1",'+
			            '"haulType" : "LONG",'+
			            '"numberOfStops" : 0,'+
			            '"flown" : false,'+
			            '"informational" : false,'+
			            '"open" : false,'+
			            '"award" : false,'+
			            '"arrivalAirport" : {'+
			              '"code" : "AMS",'+
			              '"city" : {'+
			                '"code" : "AMS",'+
			                '"country" : {'+
			                  '"code" : "NL"'+
			                '}'+
			              '}'+
			            '},'+
			            '"departureAirport" : {'+
			              '"code" : "DXB",'+
			              '"city" : {'+
			                '"code" : "DXB",'+
			                '"country" : {'+
			                  '"code" : "AE"'+
			                '}'+
			              '}'+
			            '},'+
			            '"marketingFlight" : {'+
			              '"flightNumber" : "428",'+
			              '"marketingCarrier" : {'+
			                '"code" : "KL"'+
			              '}'+
			            '},'+
			            '"operatingFlight" : {'+
			              '"operatingCarrier" : {'+
			                '"code" : "KL"'+
			              '}'+
			            '}'+
			          '} ]'+
			        '} ]'+
			      '},'+
			      '"passengers" : [ {'+
			        '"identifier" : 1,'+
			        '"type" : "ADT",'+
			        '"individual" : {'+
			          '"title" : "MR",'+
			          '"firstName" : "OMAR",'+
			          '"familyName" : "OUALIF"'+
			        '},'+
			        '"tickets" : [ {'+
			          '"number" : "0742467678436"'+
			        '} ],'+
			        '"passengerOnSegments" : [ { } ]'+
			      '} ]'+
			    '}'+
			  '}, {'+
			    '"identifier" : "4NWUWN",'+
			    '"awardBooking" : false,'+
			    '"dayReturn" : false,'+
			    '"group" : false,'+
			    '"direct" : false,'+
			    '"historical" : true,'+
			    '"flightReservation" : {'+
			      '"itinerary" : {'+
			        '"destinationsCities" : [ {'+
			          '"code" : "DXB",'+
			          '"country" : {'+
			            '"code" : "AE"'+
			          '}'+
			        '} ],'+
			        '"connections" : [ {'+
			          '"identifier" : 1,'+
			          '"originAirport" : {'+
			            '"code" : "AMS",'+
			            '"city" : {'+
			              '"code" : "AMS",'+
			              '"country" : {'+
			                '"code" : "NL"'+
			              '}'+
			            '}'+
			          '},'+
			          '"destinationAirport" : {'+
			            '"code" : "DXB",'+
			            '"city" : {'+
			              '"code" : "DXB",'+
			              '"country" : {'+
			                '"code" : "AE"'+
			              '}'+
			            '}'+
			          '},'+
			          '"segments" : [ {'+
			            '"identifier" : "4",'+
			            '"localDepartureTime" : "2014-05-03T12:30:00",'+
			            '"localArrivalTime" : "2014-05-03T20:55:00",'+
			            '"cabinClass" : "16",'+
			            '"haulType" : "LONG",'+
			            '"numberOfStops" : 0,'+
			            '"flown" : false,'+
			            '"informational" : false,'+
			            '"open" : false,'+
			            '"award" : false,'+
			            '"arrivalAirport" : {'+
			              '"code" : "DXB",'+
			              '"city" : {'+
			                '"code" : "DXB",'+
			                '"country" : {'+
			                  '"code" : "AE"'+
			                '}'+
			              '}'+
			            '},'+
			            '"departureAirport" : {'+
			              '"code" : "AMS",'+
			              '"city" : {'+
			                '"code" : "AMS",'+
			                '"country" : {'+
			                  '"code" : "NL"'+
			                '}'+
			              '}'+
			            '},'+
			            '"marketingFlight" : {'+
			              '"flightNumber" : "427",'+
			              '"marketingCarrier" : {'+
			                '"code" : "KL"'+
			              '}'+
			            '},'+
			            '"operatingFlight" : {'+
			              '"operatingCarrier" : {'+
			                '"code" : "KL"'+
			              '}'+
			            '}'+
			          '} ]'+
			        '}, {'+
			          '"identifier" : 2,'+
			          '"originAirport" : {'+
			            '"code" : "DXB",'+
			            '"city" : {'+
			              '"code" : "DXB",'+
			              '"country" : {'+
			                '"code" : "AE"'+
			              '}'+
			            '}'+
			          '},'+
			          '"destinationAirport" : {'+
			            '"code" : "AMS",'+
			            '"city" : {'+
			              '"code" : "AMS",'+
			              '"country" : {'+
			                '"code" : "NL"'+
			              '}'+
			            '}'+
			          '},'+
			          '"segments" : [ {'+
			            '"identifier" : "5",'+
			            '"localDepartureTime" : "2014-05-08T22:50:00",'+
			            '"localArrivalTime" : "2014-05-09T04:00:00",'+
			            '"cabinClass" : "16",'+
			            '"haulType" : "LONG",'+
			            '"numberOfStops" : 0,'+
			            '"flown" : false,'+
			            '"informational" : false,'+
			            '"open" : false,'+
			            '"award" : false,'+
			            '"arrivalAirport" : {'+
			              '"code" : "AMS",'+
			              '"city" : {'+
			                '"code" : "AMS",'+
			                '"country" : {'+
			                  '"code" : "NL"'+
			                '}'+
			              '}'+
			            '},'+
			            '"departureAirport" : {'+
			              '"code" : "DXB",'+
			              '"city" : {'+
			                '"code" : "DXB",'+
			                '"country" : {'+
			                  '"code" : "AE"'+
			                '}'+
			              '}'+
			            '},'+
			            '"marketingFlight" : {'+
			              '"flightNumber" : "428",'+
			              '"marketingCarrier" : {'+
			                '"code" : "KL"'+
			              '}'+
			            '},'+
			            '"operatingFlight" : {'+
			              '"operatingCarrier" : {'+
			                '"code" : "KL"'+
			              '}'+
			            '}'+
			          '} ]'+
			        '} ]'+
			      '},'+
			      '"passengers" : [ {'+
			        '"identifier" : 1,'+
			        '"type" : "ADT",'+
			        '"individual" : {'+
			          '"title" : "MR",'+
			          '"firstName" : "OMAR",'+
			          '"familyName" : "OUALIF"'+
			        '},'+
			        '"tickets" : [ {'+
			          '"number" : "0742469221883"'+
			        '} ],'+
			        '"passengerOnSegments" : [ { } ]'+
			      '} ]'+
			    '}'+
			  '} ]'+
			'}');
        } else if (req.getEndpoint().contains('/customers')){
        	res.setBody('{'+
			  '"id" : 1002149355,'+
			  '"individual" : {'+
			    '"title" : "MR",'+
			    '"firstName" : "JOHN",'+
			    '"familyName" : "DOE",'+
			    '"emailAccount" : {'+
			      '"address" : "john.doe@gmail.com"'+
			    '},'+
			    '"dateOfBirth" : "1980-05-25",'+
			    '"phoneNumbers" : [ {'+
			      '"number_x" : "0612345678",'+
			      '"countryPrefix" : "+42",'+
			      '"phoneType" : "Mobile"'+
			    '} ],'+
			    '"postalAddresses" : [ {'+
			      // '"postalAddress" : {'+
			        '"usageType" : "Private",'+
			        '"streetHousenumber" : "JOHNDOESTREET 43",'+
			        '"postalCode" : "1058 ZZ",'+
			        '"city" : "AMSTERDAM",'+
			        '"country" : "NL"'+
			      // '}'+
			    '}, {'+
			      // '"postalAddress" : {'+
			        '"usageType" : "Business",'+
			        '"streetHousenumber" : "POSTBUS 77777",'+
			        '"postalCode" : "1070 AT",'+
			        '"city" : "AMSTERDAM",'+
			        '"country" : "NL",'+
			        '"company" : {'+
			          '"name" : "JOHNDOE BV"'+
			        '}'+
			      // '}'+
			    '} ],'+
			    '"travelDocuments" : [ {'+
			      '"travelDocument" : {'+
			        '"identifier" : "777777777",'+
			        '"type" : "Passport",'+
			        '"countryIssued" : "GR",'+
			        '"expirationDate" : "2020-02-20",'+
			        '"nationality" : "GR"'+
			      '}'+
			    '} ]'+
			  '},'+
			  '"account" : {'+
			    '"flyingBlueAccount" : {'+
			      '"membershipNumberAsId" : 7777777777,'+
			      '"emailAddressAsId" : "john.doe@gmail.com",'+
			      '"securityQuestion" : "John Doe has a female version what is it called ?"'+
			    '}'+
			  '},'+
			  '"membership" : {'+
			    '"flyingBlueMembership" : {'+
			      '"number" : 7777777777,'+
			      '"level" : "Ivory",'+
			      '"type" : "Standard",'+
			      '"enrollPointOfSale" : "GB",'+
			      '"awardMilesBalance" : 777,'+
			      '"awardMilesExpiryDate" : "2016-01-31",'+
			      '"awardMilesExpiryProtection" : false,'+
			      '"levelMilesBalance" : 777,'+
			      '"levelFlightsBalance" : 2,'+
			      '"levelStartDate" : "2013-04-01",'+
			      '"numberOfYearsPlatinum" : 0,'+
			      '"lastTransactionDate" : "2014-05-26",'+
			      '"carriedOverLevelMiles" : 0,'+
			      '"carriedOverLevelFlights" : 0'+
			    '}'+
			  '},'+
			  '"profileFilledPercentage" : 55,'+
			  '"preference" : {'+
			    '"communicationPreference" : {'+
			      '"language" : "GR",'+
			      '"postalAddressPreference" : {'+
			        '"postalAddress" : {'+
			          '"usageType" : "Private"'+
			        '}'+
			      '},'+
			      '"subscriptions" : [ {'+
			        '"publication" : "A",'+
			        '"postalMail" : false'+
			      '}, {'+
			        '"publication" : "E",'+
			        '"postalMail" : false'+
			      '} ]'+
			    '},'+
			    '"flightPreference" : {'+
			      '"meal" : {'+
			        '"code" : "MOML"'+
			      '}'+
			    '},'+
			    '"locationPreference" : {'+
			      '"arrivalAirport" : {'+
			        '"locationCode" : "CMN"'+
			      '},'+
			      '"departureAirport" : {'+
			        '"locationCode" : "AMS"'+
			      '}'+
			    '},'+
			    '"loyaltyProgramPreference" : {'+
			      '"frequentFlyerProgram" : {'+
			        '"code" : "KL",'+
			        '"frequentFlyerprogram" : true'+
			      '},'+
			      '"frequentFlyerProgramNumber" : "7777777777"'+
			    '},'+
			    '"wheelchairPreference" : { }'+
			  '}'+
			'}');
        } else if(req.getEndpoint().contains('https') && !req.getEndpoint().contains('/customers')){
        	System.assertEquals('https://api.klm.com/customerapi/customer', req.getEndpoint());
        	res.setBody('{"foo":"bar"}');
        }
        
        res.setStatusCode(200);
        return res;
    }
}