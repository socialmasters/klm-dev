/**                     
 * @author (s)      : David van 't Hooft
 * @description     : Class to hold all static LTC general used Util functions
 * @log:    13MAR2014: version 1.0
 */ 
public with sharing class LtcUtil {
	
	/**
	 * Returns a list containg the elements from allRatings from index offset untill ofset + limitTo
	 */
	public static List<Rating__c> getLimitedList(List<Rating__c> allRatings, Integer limitTo, Integer offset) {
		List<Rating__c> result = new List<Rating__c>();
		Integer fromRecord = offset;
		Integer untillRecord = offset + limitTo;
		for (integer i = fromRecord; i < untillRecord && i < allRatings.size(); i++) {
			result.add(allRatings.get(i));
		}
		return result;
	}
	
	/**
	 * Clean flightNumber to a format AA0000
	 * Expected format AA?...? or aa?...? or Aa?...? or aA?...?
	 */
	static public String cleanFlightNumber(String flightNumber) {
        //Clean flightNumber
        if (flightNumber != null) {        	
	        flightNumber = flightNumber.toUpperCase();
	        String num = String.valueOf(Integer.valueOf(flightNumber.substring(2,flightNumber.length())));	        
	        flightNumber = flightNumber.substring(0, 2);
	        if (!flightNumber.isAlpha() || !num.isNumeric() || num.length() > 4) {
        		throw new LtcUtilException('Invalid flight number');
        	}
	        for (Integer x = num.length(); x < 4; x++) {
	        	flightNumber += '0';
	        }
	        flightNumber += num;
        }
        return flightNumber;
	}
	
	/**
	 * returns the language part of the acceptLanguage header attribute, default en
	 */
	public static String getLanguage(String acceptLanguage) {
		String language = 'en'; // default to en
		if (acceptLanguage != null && acceptLanguage.length() >= 2) {
			language = acceptLanguage.substring(0, 2);
		}
		return language.toLowerCase();
	}

	/**
	 * returns the country part of the acceptLanguage header attribute, default NL
	 */
	public static String getCountry(String acceptLanguage) {
		String country = 'NL'; // default to en
		if (acceptLanguage != null && acceptLanguage.length() >= 5) {
			country = acceptLanguage.substring(3,5);
		}
		return country.toUpperCase();
	}
	
	public static String translate(String translationKey, String acceptLanguage) {
		String language = getLanguage(acceptLanguage);
		Translation__c translation;
		String translatedText = '';
		List<Translation__c> translationList = [Select t.Translated_Text__c From Translation__c t where t.Translation_Key__c =:translationKey and t.Language__c =:language];
        //Result should only be one or none!
        if (translationList != null && !translationList.isEmpty()) {
        	translation = translationList[0];
        	translatedText = translation.Translated_Text__c;
        } else if (!'en'.equalsIgnoreCase(language)) {
        	// not found, now try english
        	translationList = [Select t.Translated_Text__c From Translation__c t where t.Translation_Key__c =:translationKey and t.Language__c =:'en'];
	        if (translationList!=null && !translationList.isEmpty()) {
	        	translation = translationList[0];
	        	translatedText = translation.Translated_Text__c;
	        } 
        }
        return translatedText;
	}
	
	public static String determineRatingStatus(Datetime arrivalDateTime, Integer timeToArrival) {
		// Default 
		String ratingStatus = 'Inactive';
		
		// Active if now after timeToArrival <= 0
		if (timeToArrival < 0) {
			ratingStatus = 'Active';
		}
		// Closed from 35 days after landing
		Date now = Date.today();
		Integer daysBetween = arrivalDateTime.date().daysBetween(now);
		if (daysBetween > 35) {
			ratingStatus = 'Closed';
		} else if (daysBetween > 1) {
			// fallback for missing FOX info
			ratingStatus = 'Active';
		}
		return ratingStatus;
	}

	public static Boolean showBaggageInformation(Datetime arrivalDateTime, Integer utcOffSet) {
		// Default 
		Boolean ratingStatus = false;
		
		// Active if now after arrivalDateTime
		arrivalDateTime = arrivalDateTime.addMinutes(utcOffSet * -1);
		// Allowed to show 15 min before landing
		arrivalDateTime = arrivalDateTime.addMinutes(15);
		if (System.now().getTime() > arrivalDateTime.getTime()) {
			ratingStatus = true;
		}
		return ratingStatus;
	}
	
    /**
     * Used for logging debuginfo to the client: 
     * http://baggdev-klm.cs18.force.com  ==>  true
     */
	public static Boolean isDevOrg () {
	    return URL.getSalesforceBaseUrl().toExternalForm().contains('dev');
	}

	public class LtcUtilException extends Exception {

	}
	
}