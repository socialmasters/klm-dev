public with sharing class ComplaintsResponseInfo {

//
// Generated by JSON2Apex http://json2apex.herokuapp.com/
//

	public class Complaint {
		public String fileID {get;set;}
		public Status status {get;set;}
		public Status trafficLight {get;set;}
		public String creationDate {get;set;}
		public String closureDate {get;set;}
		public Status incidentType {get;set;}
		public Status domain {get;set;}
		public Status subDomain {get;set;}
		public Compensation compensation {get;set;}
		public MarketingFlight marketingFlight {get;set;}
	}

	public class Status {
		public String code {get;set;}
		public String name {get;set;}
	}

	public class Compensation {
		public Boolean paid {get;set;}
		public List<Status> types {get;set;}
	}

	public class MarketingFlight {
		public String number_x {get;set;}
		public Status carrier {get;set;}
		public OperatingFlight operatingFlight {get;set;}
	}

	public Integer id {get;set;}
	public Individual individual {get;set;}
	public List<Complaint> complaints {get;set;}

	public class PostalAddresses {
		public String city {get;set;}
		public String country {get;set;}
	}

	public class Individual {
		public String firstName {get;set;}
		public String familyName {get;set;}
		public List<PostalAddresses> postalAddresses {get;set;}
	}

	public class OperatingFlight {
		public String scheduledLocalDeparture {get;set;}
	}

	public static ComplaintsResponseInfo parse(String json) {
		return (ComplaintsResponseInfo) System.JSON.deserialize(json, ComplaintsResponseInfo.class);
	}

}